#!/bin/bash

export CC="ccache clang"
export CXX="ccache clang++"
meson setup -Dwarning_level=2 -Dllvm-fuzz=true -Db_sanitize=address,undefined -Db_lundef=false . build_fuzz_llvm
