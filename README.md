# TigerMail

This project implements a distributed e-mail, with true asynchronous
communication. It has also built-in encryption with forward-secrecy.

It uses [Ethereum](https://ethereum.org/) and
[Swarm](https://swarm.ethereum.org/) as it's infrastructure
and [Signal protocol](https://signal.org/docs/) for encryption.

This program is released under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) (or later).

If you find any part of the code hard to understand (it may be due to lack of
comments or poorly named variables) please report is as a bug.


## How it works

Every participant is identified by their Ethereum address. A contract is used to
store simple and small events on blockchain. All data that is transferred
between clients is stored on Swarm.

Alice and Bob want to communicate. Bob generates his Signal pre keys and
stores them on Swarm Feed, under predefined topic.

Alice uses one of Bob's pre keys to initiate Signal session. She encrypts her
message and uploads it to Swarm. She takes Swarm hash of her message, encrypts
with the same key, that she encrypted her message and posts it on Ethereum
contract as event.

Bob watches for new events on Ethereum contract. He tries to decrypt every one
of them with all keys he has. He knows if decryption was successful by using
HMAC code (it's posted on Ethereum contract together with encrypted Swarm hash).
With decrypted Swarm hash, he downloads his message and decrypts it.

At any moment anyone can publish an event telling that their key is invalidated.
TigerMail will refuse to send to and receive messages from such address.


## Threat model

Use [OWASP Threat Dragon](https://owasp.org/www-project-threat-dragon/) to open
[docs/tigermail_security_model.json](docs/tigermail_security_model.json).


## Building

TigerMail is written in C++17. It uses [Meson](http://mesonbuild.com/) for
building.

```
meson setup . build
ninja -C bulid
```

Dependencies:

  - [boost](https://www.boost.org/) 1.71
  - [CryptoPP](https://www.cryptopp.com/) 8.2.0
  - [gsl](https://github.com/Microsoft/gsl) 2.0.0
  - [jsoncpp](https://github.com/open-source-parsers/jsoncpp) 1.9.1
  - [libethereum](https://github.com/ethereum/aleth/) 1.6.0
  - [libsignal-protocol-c](https://github.com/signalapp/libsignal-protocol-c) 2.3.2
  - [LibreSSL](https://www.libressl.org/) 3.0.2 (this could be dropped, when CryptoPP
    [learns how to Rijndael 256-bit](https://github.com/weidai11/cryptopp/issues/297))
  - [protobuf-lite](https://developers.google.com/protocol-buffers/) 3.9.1
  - [range-v3](https://ericniebler.github.io/range-v3/) 0.9.1
  - [SML](https://github.com/boost-experimental/sml/) 1.1.0
  - [sqlite_modern_cpp](https://github.com/SqliteModernCpp/sqlite_modern_cpp/) 3.2
  - [vmime](https://www.vmime.org/) 0.9.2

It's developed and tested on GNU/Linux with both gcc and clang compilers. Should
work on different platforms with minimal or no changes.


## Installing

Run `ninja -C build install` or just put `tigermail` executable wherever you
want.


## Running


### Requirements

  - ethereum key
  - geth unix domain socket
  - swarm unix domain and tcp sockets
  - [ip2unix](https://github.com/nixcloud/ip2unix/) 2.1.3


### Synopsis

```
tigermail contract_address user_address password_file [options]
```

There are contracts already deployed on test networks:
   - Görli: `0xD6B41d0888CAbDEe3E733B210553E2E361988383`
   - Rinkeby: `0xf36dd36130dd9adbd4b467426fc9970820563583`

You can find additional options using `tigermail --help`


### Usage

TigerMail has no user interface. Instead it exposes POP3 and SMTP server
sockets, that can be used with any mail client.

Setting up Thunderbird:

  - adjust socketPath in ip2unix_pop3_smtp.yaml
  - start Thunderbird with `ip2unix -f ip2unix_pop3_smtp.yaml thunderbird`
  - create new account with following parameters:
      - email address: your Ethereum address with `@tigermail.eth` appended (like `0123...cdef@tigermail.eth`, notice there's no `0x` at the beginning)
      - password: whatever. currently ignored
      - incoming mail server:
          - POP3
          - address: `localhost`
          - port: 9110
          - SSL: no
          - authorization: encrypted password
          - username: your full email address
      - outgoing mail server:
          - address: `localhost`
          - port: 9587
          - SSL: no
          - authorization: no
  - create account and edit settings
  - disable leaving copies of messages on server
  - copies of sent messages or drafts must be stored in local folders

## Testing

### Unit tests

To build and run unit tests, use Meson:

```
ninja -C build test
```

### Toys

Toy programs, that test selected parts of code are build, when enabled:

```
meson setup -Dtoys=true . build
```

The toys are:
  - `ethereum_rpc_client` executes a series of predefined actions with Ethereum.
  - `simple_echo_server` opens a socket, repeats everything written to it.
    Disconnects when input contains "quit".
  - `pop3_smtp_echo_server` a simple POP3/SMTP server, that returns every
    message sent to it. Sender address must be `me@localhost`.


### Integration test

Currently integration testing is done manually. An off-line setup consists of
[ganache](https://truffleframework.com/ganache) as Ethereum node and a single
Swarm node with custom `--bzznetworkid`.

Connecting with ganache is possible only through TCP, so you need to change
source code, to use it instead of unix-domain socket. It requires uncommenting
few lines of code.

An automated test framework could be created using command-line POP3 and SMTP
clients and [ganache-cli](https://github.com/trufflesuite/ganache-cli).


### Fuzzing

AFL and libFuzzer are supported. See [fuzz/README.md](fuzz/README.md).


## TODO

  - `grep -R TODO .`
  - Multithreading? TigerMail is built on boost::asio with coroutines, but runs
    everything on a single thread. It's not a big problem, because it runs
    smoothly and most of the time just waits for input.
  - Rewrite all this in [Rust](https://www.rust-lang.org/).
