# Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>
#
# This file is part of TigerMail.
#
# TigerMail is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TigerMail is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.

project(
    'tigermail', 'cpp',
    default_options: [
        'cpp_std=c++17',
        'b_ndebug=if-release',
    ],
    version: '0.2.9',
)

fuzz_link_args = []

want_libfuzzer = get_option('llvm-fuzz')
if want_libfuzzer
    add_project_arguments('-DTIGERMAIL_FUZZ_NO_MAIN', language: 'cpp')
    add_project_arguments('-DFUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION', language: 'cpp')
    add_project_arguments('-fsanitize=fuzzer-no-link', language: 'cpp')
    fuzz_link_args += ['-fsanitize=fuzzer']
endif

want_afl = get_option('afl-fuzz')
# if want_afl
#     this should be specified already by afl-gcc
#     add_project_arguments('-DFUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION', language: 'cpp')
# endif


want_fuzz = want_libfuzzer or want_afl


cpp = meson.get_compiler('cpp')

# boost::process fails to build with this
# add_project_arguments('-DBOOST_ASIO_NO_DEPRECATED', language: 'cpp')
# TODO: remove this when asio uses coroutines2
# https://github.com/boostorg/asio/issues/93
add_project_arguments('-DBOOST_COROUTINES_NO_DEPRECATION_WARNING', language: 'cpp')
add_project_arguments('-DBOOST_FILESYSTEM_NO_DEPRECATED', language: 'cpp')
add_project_arguments('-DBOOST_ALL_DYN_LINK', language: 'cpp')

# TODO: add conditionals and options for different implementations
# add_project_arguments('-DTIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_HMAC_SHA256_USE_CRYPTOPP', language: 'cpp')
# add_project_arguments('-DTIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_RANDOM_USE_CRYPTOPP', language: 'cpp')
# add_project_arguments('-DTIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_USE_CRYPTOPP', language: 'cpp')
add_project_arguments('-DTIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_HMAC_SHA256_USE_OPENSSL', language: 'cpp')
add_project_arguments('-DTIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_RANDOM_USE_OPENSSL', language: 'cpp')
add_project_arguments('-DTIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_USE_OPENSSL', language: 'cpp')

if get_option('buildtype').startswith('debug')
    add_project_arguments('-DGSL_THROW_ON_CONTRACT_VIOLATION', language: 'cpp')
    # add_project_arguments('-DBOOST_ASIO_ENABLE_HANDLER_TRACKING', language: 'cpp')
endif

cryptoppdep = cpp.find_library('cryptopp')
ethashdep = cpp.find_library('ethash')
jsoncppdep = dependency('jsoncpp')
libcryptodep = dependency('libcrypto')
libdevcoredep = cpp.find_library('libdevcore')
libdevcryptodep = cpp.find_library('libdevcrypto')
libethcoredep = cpp.find_library('libethcore')
libsignaldep = dependency('libsignal-protocol-c')
# openmpdep = dependency('openmp')
protobuflitedep = dependency('protobuf-lite')
sqlite3dep = dependency('sqlite3')
sqlite_modern_cppdep = dependency('sqlite_modern_cpp')
threadsdep = dependency('threads')
vmimedep = dependency('vmime')


libcommon = static_library(
    'common',
    'common/buffer.cpp',
    'common/buffer_view.cpp',
    'common/fs.cpp',
    'common/hex.cpp',
    'common/on_scope_exit.cpp',
    'common/string_view.cpp',
)

libjson_rpc = static_library(
    'json_rpc',
    'json_rpc/json.cpp',
    'json_rpc/json_rpc.cpp',
    dependencies: jsoncppdep,
    link_with: libcommon,
)

libethereum = static_library(
    'ethereum',
    'ethereum/abi.cpp',
    'ethereum/common.cpp',
    'ethereum/crypto.cpp',
    'ethereum/rpc_client.cpp',
    'ethereum/tigermail_contract.cpp',
    'ethereum/tigermail_contract_interface.cpp',
    dependencies: [
        cryptoppdep,
        ethashdep,
        libethcoredep,
        threadsdep,
        dependency('boost', modules: ['coroutine', 'log']),
    ],
    link_with: [
        libcommon,
        libjson_rpc,
    ],
)

libpop3_smtp_common = static_library(
    'pop3_smtp_common',
    'pop3_smtp_common/parser.cpp'
)

libpop3_smtp_server = static_library(
    'pop3_smtp_server',
    'pop3_smtp_common/server/connection_manager.cpp',
    'pop3_smtp_common/server/server.cpp',
    'pop3_smtp_common/server/session.cpp',
    'pop3_smtp_common/server/shared_const_buffer.cpp',
    dependencies: dependency('boost'),
)

libpop3 = static_library(
    'pop3',
    'pop3/format_response.cpp',
    'pop3/messages_store.cpp',
    'pop3/parser.cpp',
    'pop3/session_handler.cpp',
    'pop3/state_machine.cpp',
    dependencies: [
        dependency('boost', modules: ['filesystem', 'log']),
        threadsdep
    ],
    link_with: [
        libcommon,
        libpop3_smtp_common,
    ]
)

libsmtp = static_library(
    'smtp',
    'smtp/format_response.cpp',
    'smtp/parser.cpp',
    'smtp/session_handler.cpp',
    'smtp/state_machine.cpp',
    dependencies: [
        dependency('boost', modules: ['log']),
        threadsdep
    ],
    link_with: [
        libcommon,
        libpop3_smtp_common,
    ]
)

libsignal = static_library(
    'signal',
    'signal/address.cpp',
    'signal/context.cpp',
    'signal/crypto_provider.cpp',
    'signal/crypto_provider_impl/hmac_sha256.cpp',
    'signal/crypto_provider_impl/random.cpp',
    'signal/crypto_provider_impl/sha512.cpp',
    # 'signal/crypto_provider_impl/cryptopp/hmac_sha256.cpp',
    # 'signal/crypto_provider_impl/cryptopp/random.cpp',
    # 'signal/crypto_provider_impl/cryptopp/sha512.cpp',
    'signal/crypto_provider_impl/fake/random.cpp',
    'signal/crypto_provider_impl/openssl/aes.cpp',
    'signal/crypto_provider_impl/openssl/hmac_sha256.cpp',
    'signal/crypto_provider_impl/openssl/random.cpp',
    'signal/crypto_provider_impl/openssl/sha512.cpp',
    'signal/identity_key_store.cpp',
    'signal/identity_key_store_impl/memory.cpp',
    'signal/identity_key_store_impl/sqlite.cpp',
    'signal/key_bundle.cpp',
    'signal/key_bundle_impl/key_bundle.pb.cc',
    'signal/pre_key_store.cpp',
    'signal/pre_key_store_impl/memory.cpp',
    'signal/pre_key_store_impl/sqlite.cpp',
    'signal/session_store.cpp',
    'signal/session_store_impl/memory.cpp',
    'signal/session_store_impl/sqlite.cpp',
    'signal/signal_buffer.cpp',
    'signal/signalpp/curve.cpp',
    'signal/signalpp/hkdf.cpp',
    'signal/signalpp/key_helper.cpp',
    'signal/signalpp/protocol.cpp',
    'signal/signalpp/ratchet.cpp',
    'signal/signalpp/session_builder.cpp',
    'signal/signalpp/session_cipher.cpp',
    'signal/signalpp/session_pre_key.cpp',
    'signal/signalpp/signal_protocol.cpp',
    dependencies: [
        dependency('boost', modules: ['log']),
        # cryptoppdep,
        libcryptodep,
        libsignaldep,
        protobuflitedep,
        sqlite3dep,
        sqlite_modern_cppdep,
        threadsdep,
    ],
    link_with: [
        libcommon,
    ],
)

libswarm = static_library(
    'swarm',
    'swarm/ipc_client.cpp',
    'swarm/tcp_client.cpp',
    dependencies: [
        dependency('boost', modules: ['coroutine', 'filesystem', 'log']),
        jsoncppdep,
        threadsdep,
    ],
    link_with: [
        libcommon,
        libjson_rpc,
    ],
)

libtigermail = static_library(
    'tigermail',
    'tigermail/client.cpp',
    'tigermail/client_state/sqlite.cpp',
    'tigermail/mime.cpp',
    'tigermail/options.cpp',
    dependencies: [
        dependency('boost', modules: ['log', 'program_options']),
        libsignaldep,
        sqlite3dep,
        sqlite_modern_cppdep,
        vmimedep,
    ],
)

executable(
    'tigermail',
    'tigermail/main.cpp',
    dependencies: [
        dependency('boost', modules: ['log_setup', 'log', 'thread']),
        libdevcryptodep,
        libsignaldep,
    ],
    link_with: [
        libcommon,
        libethereum,
        libpop3,
        libpop3_smtp_server,
        libsignal,
        libsmtp,
        libswarm,
        libtigermail,
    ],
    install: true,
)

if get_option('toys')
    executable(
        'ethereum_rpc_client',
        'ethereum/rpc_client_main.cpp',
        dependencies: [
            dependency('boost', modules: ['coroutine', 'program_options']),
            libdevcryptodep,
            threadsdep,
        ],
        link_with: [
            libethereum,
        ]
    )

    executable(
        'simple_echo_server',
        'pop3_smtp_common/server/simple_echo_server.cpp',
        link_with: libpop3_smtp_server,
        dependencies: [
            # openmpdep,
            threadsdep,
        ],
    )

    executable(
        'pop3_smtp_echo_server',
        'pop3_smtp_echo_server/main.cpp',
        link_with: [
            libpop3,
            libpop3_smtp_server,
            libsmtp,
        ],
        dependencies: [
            dependency('boost', modules: ['log_setup', 'thread']),
            # openmpdep,
            threadsdep,
        ],
    )
endif


if get_option('buildtype').startswith('debug')
    boost_test_dep = dependency('boost', modules: ['unit_test_framework'])
    test_deps = [
        boost_test_dep
    ]

    common_test = executable(
        'common_test',
        'common/test/buffer.cpp',
        'common/test/buffer_view.cpp',
        'common/test/fixed_size_buffer.cpp',
        'common/test/fixed_size_buffer_view.cpp',
        'common/test/hex.cpp',
        'common/test/main.cpp',
        'common/test/on_scope_exit.cpp',
        'common/test/retrying.cpp',
        'common/test/string_view.cpp',
        'common/test/zpad.cpp',
        dependencies: test_deps,
        link_with: libcommon,
    )
    test('common', common_test)

    ethereum_test = executable(
        'ethereum_test',
        'ethereum/test/abi.cpp',
        'ethereum/test/crypto.cpp',
        'ethereum/test/main.cpp',
        'ethereum/test/rpc_client.cpp',
        'ethereum/test/tigermail_contract.cpp',
        dependencies: [
            test_deps,
            libdevcoredep,
        ],
        link_with: libethereum,
    )
    test('ethereum', ethereum_test)

    json_rpc_test = executable(
        'json_rpc_test',
        'json_rpc/test/json.cpp',
        'json_rpc/test/json_rpc.cpp',
        'json_rpc/test/main.cpp',
        dependencies: test_deps,
        link_with: libjson_rpc,
    )
    test('json_rpc', json_rpc_test)

    pop3_test = executable(
        'pop3_test',
        'pop3/test/format_response/err.cpp',
        'pop3/test/format_response/multiline.cpp',
        'pop3/test/format_response/ok.cpp',
        'pop3/test/format_response/ok_with_banner.cpp',
        'pop3/test/format_response/response_to_LIST_multi.cpp',
        'pop3/test/format_response/response_to_LIST_single.cpp',
        'pop3/test/format_response/response_to_RETR.cpp',
        'pop3/test/format_response/response_to_STAT.cpp',
        'pop3/test/integration.cpp',
        'pop3/test/main.cpp',
        'pop3/test/messages_store.cpp',
        'pop3/test/msg_id.cpp',
        'pop3/test/parser/parse_command.cpp',
        'pop3/test/session_handler.cpp',
        'pop3/test/state_machine/Authorization.cpp',
        'pop3/test/state_machine/Start.cpp',
        'pop3/test/state_machine/Transaction.cpp',
        'pop3/test/state_machine/common.cpp',
        dependencies: test_deps,
        link_with: libpop3,
    )
    test('pop3', pop3_test)

    pop3_smtp_common_test = executable(
        'pop3_smtp_common_test',
        'pop3_smtp_common/test/main.cpp',
        'pop3_smtp_common/test/parser/process_command.cpp',
        dependencies: test_deps,
        link_with: libpop3_smtp_common,
    )
    test('pop3_smtp_common', pop3_smtp_common_test)

    signal_test = executable(
        'signal_test',
        'signal/test/crypto_provider_impl/aes.cpp',
        'signal/test/crypto_provider_impl/hmac_sha256.cpp',
        'signal/test/crypto_provider_impl/random.cpp',
        'signal/test/crypto_provider_impl/sha512.cpp',
        'signal/test/identity_key_store.cpp',
        'signal/test/identity_key_store_impl/memory.cpp',
        'signal/test/identity_key_store_impl/sqlite.cpp',
        'signal/test/key_bundle.cpp',
        'signal/test/main.cpp',
        'signal/test/session_store.cpp',
        'signal/test/session_store_impl/memory.cpp',
        'signal/test/session_store_impl/sqlite.cpp',
        'signal/test/pre_key_store.cpp',
        'signal/test/pre_key_store_impl/memory.cpp',
        'signal/test/pre_key_store_impl/sqlite.cpp',
        dependencies: [
            test_deps,
            libsignaldep,
        ],
        link_with: [
            libsignal,
        ],
    )
    test('signal', signal_test)

    smtp_test = executable(
        'smtp_test',
        'smtp/test/session_handler.cpp',
        'smtp/test/format_response.cpp',
        'smtp/test/integration.cpp',
        'smtp/test/main.cpp',
        'smtp/test/parser/parse_MAIL.cpp',
        'smtp/test/parser/parse_RCPT.cpp',
        'smtp/test/parser/parse_VRFY.cpp',
        'smtp/test/parser/parse_command.cpp',
        'smtp/test/parser/process_data.cpp',
        'smtp/test/state_machine/Idle.cpp',
        'smtp/test/state_machine/ReceivingData.cpp',
        'smtp/test/state_machine/ReceivingRecipients.cpp',
        'smtp/test/state_machine/Start.cpp',
        'smtp/test/state_machine/WaitForHelo.cpp',
        'smtp/test/state_machine/common.cpp',
        dependencies: test_deps,
        link_with: libsmtp,
    )
    test('smtp', smtp_test)

    swarm_test = executable(
        'swarm_test',
        'swarm/test/ipc_client.cpp',
        'swarm/test/main.cpp',
        'swarm/test/tcp_client.cpp',
        dependencies: [
            test_deps,
            dependency('boost', modules: ['log_setup', 'thread']),
        ],
        link_with: libswarm,
    )
    test('swarm', swarm_test)

    tigermail_test = executable(
        'tigermail_test',
        'tigermail/test/client/client.cpp',
        'tigermail/test/client/fake_ethereum.cpp',
        'tigermail/test/client/fake_swarm.cpp',
        'tigermail/test/client_state/memory.cpp',
        'tigermail/test/client_state/sqlite.cpp',
        'tigermail/test/client_state/test_sqlite_client_state.cpp',
        'tigermail/test/main.cpp',
        'tigermail/test/mime.cpp',
        dependencies: [
            test_deps,
            dependency('boost', modules: ['log_setup', 'log', 'thread']),
            libsignaldep,
        ],
        link_with: [
            libethereum,
            libsignal,
            libswarm,
            libtigermail,
        ],
    )
    test('tigermail', tigermail_test)

    if want_fuzz
        ethereum_transport_protocol_fuzz = executable(
            'ethereum_transport_protocol_fuzz',
            'fuzz/ethereum_transport_protocol/fuzz.cpp',
            link_args: fuzz_link_args,
            link_with: [
                libethereum,
                libpop3_smtp_server,
            ],
        )
        if want_afl
            test(
                'ethereum_transport_protocol_fuzz',
                ethereum_transport_protocol_fuzz,
                args: meson.current_source_dir() / 'fuzz/ethereum_transport_protocol/testcases_generated/',
            )
        endif

        ethereum_jsonrpc_fuzz = executable(
            'ethereum_jsonrpc_fuzz',
            'fuzz/ethereum_jsonrpc/fuzz.cpp',
            dependencies: libdevcoredep,
            link_args: fuzz_link_args,
            link_with: libethereum,
        )
        if want_afl
            test(
                'ethereum_jsonrpc_fuzz', ethereum_jsonrpc_fuzz,
                args: meson.current_source_dir() / 'fuzz/ethereum_jsonrpc/testcases_generated/',
            )
        endif

        pop3_fuzz = executable(
            'pop3_fuzz',
            'fuzz/pop3/fuzz.cpp',
            link_args: fuzz_link_args,
            link_with: libpop3,
        )
        if want_afl
            test(
                'pop3_fuzz', pop3_fuzz,
                args: meson.current_source_dir() / 'fuzz/pop3/testcases_generated/',
            )
        endif

        smtp_fuzz = executable(
            'smtp_fuzz',
            'fuzz/smtp/fuzz.cpp',
            link_args: fuzz_link_args,
            link_with: libsmtp,
        )
        if want_afl
            test(
                'smtp_fuzz', smtp_fuzz,
                args: meson.current_source_dir() / 'fuzz/smtp/testcases_generated/',
            )
        endif

        swarm_ipc_client_fuzz = executable(
            'swarm_ipc_client_fuzz',
            'fuzz/swarm_ipc_client/fuzz.cpp',
            link_args: fuzz_link_args,
            link_with: [
                libswarm,
                libpop3_smtp_server,
            ],
        )
        if want_afl
            test(
                'swarm_ipc_client_fuzz', swarm_ipc_client_fuzz,
                args: meson.current_source_dir() / 'fuzz/swarm_ipc_client/testcases_generated/',
            )
        endif

        swarm_tcp_client_fuzz = executable(
            'swarm_tcp_client_fuzz',
            'fuzz/swarm_tcp_client/fuzz.cpp',
            link_args: fuzz_link_args,
            link_with: [
                libswarm,
                libpop3_smtp_server,
            ],
        )
        if want_afl
            test(
                'swarm_tcp_client_fuzz', swarm_tcp_client_fuzz,
                args: meson.current_source_dir() / 'fuzz/swarm_tcp_client/testcases_generated/',
            )
        endif

        tigermail_fuzz = executable(
            'tigermail_fuzz',
            'fuzz/tigermail/fuzz.cpp',
            dependencies: libsignaldep,
            link_args: fuzz_link_args,
            link_with: [
                libsignal,
                libtigermail,
            ],
        )
        # if want_afl
        #     test(
        #         'tigermail_fuzz', tigermail_fuzz,
        #         args: meson.current_source_dir() / 'fuzz/tigermail/testcases_generated/',
        #     )
        # endif
    endif
endif
