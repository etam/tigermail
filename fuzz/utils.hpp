/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_FUZZ_UTILS_HPP
#define TIGERMAIL_FUZZ_UTILS_HPP

#include <vector>
#include <string_view>

#include <gsl/gsl_util>

#include "../common/buffer_view.hpp"


std::vector<std::string_view> split_input(std::string_view data)
{
    const auto delimiter = std::string_view{"\n==\n"};
    auto chunks = std::vector<std::string_view>{};
    auto start = std::size_t{0};
    for (auto delim_start = data.find(delimiter, start);
         delim_start != std::string_view::npos;
         delim_start = data.find(delimiter, start)) {
        chunks.push_back(data.substr(start, delim_start - start));
        start = delim_start + delimiter.size();
    }
    chunks.push_back(data.substr(start));
    return chunks;
}


TigerMail::BufferView to_buffer_view(std::string_view view)
{
    return {reinterpret_cast<const std::uint8_t*>(view.data()),
            gsl::narrow<TigerMail::BufferView::index_type>(view.size())};
}


#endif // TIGERMAIL_FUZZ_UTILS_HPP
