#!/bin/bash

name="$1"

if [[ ! -d "$name" ]]; then
    echo "\"${name}\" does not exists" >&2
    exit 1
fi

ninja -C ../build_fuzz_afl || exit 1

cd "$name"

dict_arg=""
[[ -f dict.txt ]] && dict_arg="-x dict.txt"

#export AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1
#export AFL_SKIP_CPUFREQ=y
afl-fuzz \
    -E 1000000 \
    -i testcases_generated \
    -o findings \
    $dict_arg \
    "../../build_fuzz_afl/${name}_fuzz" || exit 1

mkdir -p testcases_new
cp findings/queue/id:*src:* testcases_new/

exec ../merge_new_cases.sh "$name"
