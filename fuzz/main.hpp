/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
  Include this file at the end of fuzz target implementation.
  It requires one function
  void test_one_input(std::string_view data);
 */

#ifndef TIGERMAIL_FUZZ_MAIN_HPP
#define TIGERMAIL_FUZZ_MAIN_HPP

#include <filesystem>
#include <iostream>
#include <iterator>
#include <string>
#include <string_view>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>

#include "../common/fs.hpp"


namespace {


void test_one_input_and_catch(std::string_view data)
try {
    test_one_input(data);
}
catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(debug) << "caught exception: " << e.what();
}


} // namespace


extern "C" int LLVMFuzzerTestOneInput(const std::uint8_t *data, std::size_t size) {
    boost::log::core::get()->set_logging_enabled(false);
    test_one_input_and_catch(std::string_view{reinterpret_cast<const char*>(data), size});
    return 0;
}


#ifndef TIGERMAIL_FUZZ_NO_MAIN
namespace {


void test_one_file(const std::filesystem::path& path)
{
    BOOST_LOG_TRIVIAL(info) << "testing file " << path;
    const auto input = TigerMail::read_file_contents(path);
    test_one_input_and_catch(input);
}


} // namespace


int main(int argc, char* argv[])
{
    // boost::log::core::get()->set_logging_enabled(false);

    if (argc == 1) {
        BOOST_LOG_TRIVIAL(info) << "reading stdin";
        auto input = std::string{
            std::istreambuf_iterator<char>{std::cin},
            std::istreambuf_iterator<char>{},
        };
        test_one_input_and_catch(input);
    }
    else {
        for (char** arg = argv+1; arg < argv+argc; ++arg) {
            BOOST_LOG_TRIVIAL(info) << "arg \"" << *arg << '"';
            const auto path = std::filesystem::path{*arg};
            if (std::filesystem::is_directory(path)) {
                for (const auto sub_path : std::filesystem::directory_iterator{path}) {
                    test_one_file(sub_path);
                }
            }
            else {
                test_one_file(path);
            }
        }
    }
}
#endif // TIGERMAIL_FUZZ_NO_MAIN


#endif // TIGERMAIL_FUZZ_MAIN_HPP
