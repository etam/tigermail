#!/bin/bash

# removes redundant testcases

if [[ "$#" != 2 ]]; then
    echo "usage: $0 dir fuzzing_binary" >&2
    exit 1
fi

dir="$1"
bin="$2"

# afl-cmin discourages using /tmp
tmp="$(mktemp -d -p $PWD)"
trap "rm -rf \"$tmp\"" EXIT

afl-cmin -i "$dir" -o "$tmp" -- "$bin"
rm "$dir"/*
mv "$tmp"/* "$dir"/
