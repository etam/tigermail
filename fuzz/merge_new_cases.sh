#!/bin/bash

# This script is supposed to be run inside test directory.
# It's used by other scripts.

name="$1"

if [[ "$(ls -1 testcases_new/ | wc -l)" == "0" ]]; then
    echo "no new testcases"
    rmdir testcases_new
    exit 0
fi

# merge new cases with old ones, but keep them distinguishable by "_new" suffix
for i in $(ls testcases_new/); do
    mv "testcases_new/$i" "testcases_generated/${i}_new"
done
rmdir testcases_new

# run afl-cmin to remove redundant cases
# for some reason doing it once is not enough
while true; do
    cases_num_before="$(ls -1 testcases_generated/ | wc -l)"
    ../afl_cmin.sh testcases_generated "../../build_fuzz_afl/${name}_fuzz"
    cases_num_after="$(ls -1 testcases_generated/ | wc -l)"
    [[ "$cases_num_before" == "$cases_num_after" ]] && break
done

# minimize new cases with afl-tmin
for i in testcases_generated/*_new; do
    ../afl_tmin.sh $i testcases_generated "../../build_fuzz_afl/${name}_fuzz"
done

rm -f testcases_generated/*_new
