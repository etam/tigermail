/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

// #include <boost/log/trivial.hpp>

#include <any>
#include <optional>
#include <vector>

#include <gsl/pointers>

#include "../../common/buffer.hpp"
#include "../../common/buffer_view.hpp"
#include "../../common/fixed_size_buffer.hpp"
#include "../../common/hex.hpp"
#include "../../common/utils.hpp"
#include "../../common/test/fake_yield.hpp"

#include "../../ethereum/tigermail_contract_interface.hpp"

#include "../../swarm/swarm_client_interface.hpp"

#include "../../signal/context.hpp"

#include "../../tigermail/client.hpp"
#include "../../tigermail/client_internal.hpp"
#include "../../tigermail/client_state/memory.hpp"

#include "../utils.hpp"

namespace Ethereum = TigerMail::Ethereum;
namespace Signal = TigerMail::Signal;
namespace Swarm = TigerMail::Swarm;
using TigerMail::Test::FakeYield;
using TigerMail::Buffer;
using TigerMail::BufferView;
using TigerMail::FixedSizeBuffer;
using TigerMail::MemoryClientState;
using TigerMail::copy;


namespace {


enum class Method {
    send_message,
    receive_messages,
};

enum class Scenario {
    new_session,
    existing_session,
};


class FakeContract
    : public Ethereum::TigerMailContractInterface
{
private:
    Ethereum::MessageIdLog m_message_id_log;
    bool m_invalidated = false;

public:
    using Ethereum::TigerMailContractInterface::TigerMailContractInterface;

    FixedSizeBuffer<32> postMessageId(BufferView /*id*/, std::any yield) override
    {
        std::any_cast<FakeYield>(yield);
        return {};
    }

    void set_message_id_log(Ethereum::MessageIdLog&& message_id_log)
    {
        m_message_id_log = std::move(message_id_log);
    }

    std::vector<Ethereum::MessageIdLog> get_message_id_logs(
        unsigned /*from_block*/, unsigned /*to_block*/, std::any yield) override
    {
        std::any_cast<FakeYield>(yield);
        return {m_message_id_log};
    }

    FixedSizeBuffer<32> invalidate(std::any yield) override
    {
        std::any_cast<FakeYield>(yield);
        return {};
    }

    void set_invalidated(bool new_value)
    {
        m_invalidated = new_value;
    }

    bool is_invalidated(
        const Ethereum::Address& /*address*/, Ethereum::BlockNumberOrTag /*to_block*/, std::any yield) override
    {
        std::any_cast<FakeYield>(yield);
        return m_invalidated;
    }

    unsigned get_block_number(std::any yield) override
    {
        std::any_cast<FakeYield>(yield);
        return 0;
    }
};


class FakeSwarm
    : public Swarm::SwarmClientInterface
{
private:
    std::optional<Buffer> m_bzz_data = std::nullopt;
    std::optional<Buffer> m_feed_data = std::nullopt;

public:
    void set_bzz_data(Buffer&& bzz_data)
    {
        m_bzz_data = std::move(bzz_data);
    }

    std::optional<Buffer> bzz_get(const Swarm::SwarmHash& /*id*/, std::any yield) override
    {
        std::any_cast<FakeYield>(yield);
        return m_bzz_data;
    }

    Swarm::SwarmHash bzz_post(BufferView /*data*/, std::any yield) override
    {
        std::any_cast<FakeYield>(yield);
        return {};
    }


    void set_feed_data(Buffer&& feed_data)
    {
        m_feed_data = std::move(feed_data);
    }

    std::optional<Buffer> feed_get(const Ethereum::Address& /*address*/, std::string_view /*name*/,
                                   std::any yield) override
    {
        std::any_cast<FakeYield>(yield);
        return m_feed_data;
    }

    void feed_update(const Ethereum::Address& /*address*/, std::string_view /*password*/, std::string_view /*name*/,
                     BufferView /*data*/, std::any yield) override
    {
        std::any_cast<FakeYield>(yield);
    }
};


void test_one_input(std::string_view data)
{
    const auto action =
        [&]() -> Method {
            const auto default_method = Method::send_message;
            if (data.empty()) {
                return default_method;
            }
            const auto c = data[0];
            data.remove_prefix(1);
            switch (c) {
            case 's': return Method::send_message;
            case 'r': return Method::receive_messages;
            default: return default_method;
            }
        }();

    const auto scenario =
        [&]() -> Scenario {
            const auto default_sub_action = Scenario::new_session;
            if (data.empty()) {
                return default_sub_action;
            }
            const auto c = data[0];
            data.remove_prefix(1);
            switch (c) {
            case 'n': return Scenario::new_session;
            case 'e': return Scenario::existing_session;
            default: return default_sub_action;
            }
        }();

    auto fake_contract = FakeContract{{}};
    auto fake_swarm = FakeSwarm{};
    auto signal_context = Signal::Context{};
    auto signal_store_context = Signal::MemoryStoreContext{signal_context.get()};
    auto client_state = MemoryClientState{};
    
    auto client = TigerMail::Client{
        gsl::not_null{&fake_contract},
        gsl::not_null{&fake_swarm},
        {},
        {},
        gsl::not_null{&signal_context},
        gsl::not_null{&signal_store_context},
        gsl::not_null{&client_state}};

    auto yield = FakeYield(FakeYield::explicit_construction);

    const auto peer_address = Ethereum::Address{1};

    switch (action) {

    case Method::send_message:
        {
            switch (scenario) {

            case Scenario::new_session:
                {
                    // this allows to make distinction between no data and empty data
                    const auto has_feed_data =
                        [&]() -> bool {
                            if (data.empty()) {
                                return false;
                            }
                            const auto c = data[0];
                            data.remove_prefix(1);
                            return c == 'f';
                        }();
                    if (has_feed_data) {
                        fake_swarm.set_feed_data(Buffer(data.begin(), data.end()));
                    }

                }
                break;

            case Scenario::existing_session:
                {
                    auto chunks = split_input(data);
                    chunks.resize(3, std::string_view{""});
                    signal_store_context.get_session_store()->store(
                        ethereum_address_to_signal(peer_address),
                        to_buffer_view(chunks[0]));
                    signal_store_context.get_session_store()->store_keys_for_next_message_id(
                        ethereum_address_to_signal(peer_address),
                        to_buffer_view(chunks[1]),
                        to_buffer_view(chunks[2]));
                }
                break;
            }

            client.send_message(peer_address, {}, yield);
        }
        break;

    case Method::receive_messages:
        {
            auto chunks = split_input(data);
            switch (scenario) {
            case Scenario::new_session:
                chunks.resize(3);
                break;
            case Scenario::existing_session:
                chunks.resize(5);
                break;
            }

            fake_contract.set_message_id_log(
                Ethereum::MessageIdLog{
                    .block_number = 0,
                    .sender_address = copy(peer_address),
                    .data = Buffer(chunks[0].begin(), chunks[0].end()),
                });

            {
                auto chunk = chunks[1];
                const auto has_swarm_data =
                    [&]() -> bool {
                        if (chunk.empty()) {
                            return false;
                        }
                        const auto c = chunk[0];
                        chunk.remove_prefix(1);
                        return c == 's';
                    }();
                if (has_swarm_data) {
                    fake_swarm.set_bzz_data(Buffer(chunk.begin(), chunk.end()));
                }
            }

            switch (scenario) {

            case Scenario::new_session:
                {
                    auto chunk = chunks[2];
                    const auto has_signed_pre_key =
                        [&]() -> bool {
                            if (chunk.empty()) {
                                return false;
                            }
                            const auto c = chunk[0];
                            chunk.remove_prefix(1);
                            return c == 'p';
                        }();
                    if (has_signed_pre_key) {
                        signal_store_context.get_signed_pre_key_store()->store(0, to_buffer_view(chunk));
                    }
                }
                break;

            case Scenario::existing_session:
                {

                }
                break;
            }

            client.receive_messages(yield);
            /*
            // make sure get_message_id_logs returns one log
            sender_address = static
            ethereum data
            optional bzz_get data
            signal session store

            receive_messages
              tigermail_contract.get_message_id_logs
              try_decrypt_message
                try_decrypt_message_id
                  signal_protocol_session_contains_session
                  signal_protocol_session_load_session
                  signal_store_context->get_signed_pre_key_store()->key_ids()
                  signal_protocol_signed_pre_key_load_key
                tigermail_contract.is_invalidated
                swarm.bzz_get
            */
        }
        break;
    }
}


} // namespace


#include "../main.hpp"
