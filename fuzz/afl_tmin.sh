#!/bin/bash

# reduces size of a testcase

if [[ "$#" != 3 ]]; then
    echo "usage: $0 input output_dir fuzzing_binary" >&2
    exit 1
fi

input="$1"
output_dir="$2"
bin="$3"

tmp="$(mktemp -p $PWD)"
trap "rm -rf \"$tmp\"" EXIT

afl-tmin -i "$input" -o "$tmp" -- "$bin"
set -- $(sha1sum "$tmp")
mv "$tmp" "$output_dir/$1"
