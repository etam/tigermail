/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <sstream>
#include <string>
#include <string_view>

#include <gsl/pointers>

#include "../../pop3/limits.hpp"
#include "../../pop3/messages_store.hpp"
#include "../../pop3/parser.hpp"
#include "../../pop3/session_handler.hpp"

using TigerMail::POP3::Impl;
using TigerMail::POP3::Limits;
using TigerMail::POP3::MessagesStore;
using TigerMail::POP3::Parser;


namespace {


std::string test_banner_generator()
{
    return "<foo@bar>";
}


class TestImpl
    : public Impl
{
private:
    std::ostringstream m_output;
    bool m_disconnected;

public:
    TestImpl(gsl::not_null<MessagesStore*> messages_store)
        : Impl{
            [](std::string_view user, std::string_view) {
                return user == "A";
            },
            messages_store,
            m_output,
            m_disconnected,
            test_banner_generator
        }
        , m_output{}
        , m_disconnected{false}
    {}
};


void test_one_input(std::string_view data)
{
    const auto limits = Limits{};
    auto messages_store = MessagesStore{};
    messages_store.push_message("foo\r\nbar");
    messages_store.push_message("abcdefgh\r\nijklmno\r\npqrstuvwxyz");

    auto pop3 = TestImpl{gsl::not_null{&messages_store}};
    auto parser = Parser{pop3, limits};

    parser.process_input(data);
}


} // namespace


#include "../main.hpp"
