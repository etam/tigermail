#!/bin/bash

name="$1"

if [[ ! -d "$name" ]]; then
    echo "\"${name}\" does not exists" >&2
    exit 1
fi

ninja -C ../build_fuzz_afl || exit 1
ninja -C ../build_fuzz_llvm || exit 1

cd "$name"

dict_arg=""
[[ -f dict.txt ]] && dict_arg="-dict=dict.txt"

mkdir -p testcases_new
"../../build_fuzz_llvm/${name}_fuzz" \
    -runs=1000000 \
    $dict_arg \
    testcases_new testcases_generated testcases || exit 1

if ! command -v afl-cmin >/dev/null 2>&1; then
    echo "AFL not found. Will not merge new testcases"
    exit 0
fi

exec ../merge_new_cases.sh "$name"
