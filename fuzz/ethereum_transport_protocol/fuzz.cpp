/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <exception>
#include <functional>
#include <memory>
#include <random>
#include <sstream>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

#include <boost/asio/io_context.hpp>
#include <boost/asio/local/stream_protocol.hpp>
#include <boost/asio/spawn.hpp>

#include <boost/log/trivial.hpp>

#include <gsl/gsl_assert>
#include <gsl/pointers>

#include "../../common/string_view.hpp"

#include "../../ethereum/rpc_client.hpp"

#include "../../pop3_smtp_common/server/server.hpp"
#include "../../pop3_smtp_common/server/session_factory.hpp"
#include "../../pop3_smtp_common/server/session_handler_interface.hpp"

#include "../utils.hpp"

namespace asio = boost::asio;
namespace Ethereum = TigerMail::Ethereum;
using TigerMail::starts_with;
using TigerMail::POP3_SMTP_common::server::Server;
using TigerMail::POP3_SMTP_common::server::SessionHandlerInterface;
using TigerMail::POP3_SMTP_common::server::make_session_factory;


namespace {


class SessionHandler
    : public SessionHandlerInterface
{
private:
    const std::vector<std::string_view>& m_responses;
    std::vector<std::string_view>::const_iterator m_it;

public:
    explicit
    SessionHandler(const std::vector<std::string_view>& responses)
        : m_responses{responses}
        , m_it{m_responses.cbegin()}
    {}

    std::string handle_hello() override
    {
        return "";
    }

    std::tuple<std::string, bool> handle_input(std::string_view) override
    {
        Expects(m_it != m_responses.cend());
        auto response = std::string{*m_it} + '\n';
        ++m_it;
        return {std::move(response), m_it == m_responses.cend()};
    }

    std::string handle_stop() override
    {
        return "";
    }
};


void test_one_input(std::string_view data)
{
    auto io_context = asio::io_context{};

    const auto use_http = starts_with(data, "HTTP");

    const auto responses = split_input(data);

    const auto session_factory = make_session_factory<SessionHandler>(std::cref(responses));
    const auto socket_name =
        [&] {
            auto o = std::ostringstream{};
            o << "_tigermail_ethereum_fuzz_" << std::random_device{}();
            auto s = o.str();
            s[0] = '\0';
            return s;
        }();

    auto server = Server{
        io_context,
        asio::local::stream_protocol::acceptor{io_context, socket_name},
        gsl::not_null{&session_factory}};


    auto transport_protocol_client =
        [&]() -> std::unique_ptr<Ethereum::TransportProtocolClientInterface> {
            if (use_http) {
                return std::make_unique<Ethereum::HTTPTransportProtocolClient>(
                    io_context,
                    asio::local::stream_protocol::endpoint{socket_name});
            }
            return std::make_unique<Ethereum::RawTransportProtocolClient>(
                io_context,
                asio::local::stream_protocol::endpoint{socket_name});
        }();

    asio::spawn(
        io_context,
        [&](asio::yield_context yield) {
            transport_protocol_client->connect(yield);
            for (auto i = std::size_t{0}; i < responses.size(); ++i) {
                const auto result = transport_protocol_client->call("\n", yield);
                BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
            }
            server.stop();
        });

    io_context.run();
}


} // namespace


#include "../main.hpp"
