/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <any>
#include <string>
#include <string_view>
#include <vector>

#include <boost/log/trivial.hpp>

#include <gsl/gsl_assert>
#include <gsl/pointers>

#include <json/value.h>

#include "../../common/hex.hpp"
#include "../../common/test/fake_yield.hpp"
#include "../../common/test/out/fixed_size_buffer.hpp"
#include "../../common/test/out/std_optional.hpp"
#include "../../common/test/out/std_vector.hpp"

#include "../../ethereum/rpc_client.hpp"
#include "../../ethereum/tigermail_contract.hpp"
#include "../../ethereum/test/out/rpc_client.hpp"
#include "../../ethereum/test/out/tigermail_contract_interface.hpp"

#include "../../json_rpc/json.hpp"

#include "../utils.hpp"

namespace Ethereum = TigerMail::Ethereum;
using TigerMail::Test::FakeYield;


namespace {


class FakeTransportProtocolClient
    : public Ethereum::TransportProtocolClientInterface
{
private:
    const std::vector<std::string_view>& m_responses;
    std::vector<std::string_view>::const_iterator m_it;

public:
    explicit
    FakeTransportProtocolClient(const std::vector<std::string_view>& responses)
        : m_responses{responses}
        , m_it{m_responses.cbegin()}
    {}

    void connect(std::any) override {}

    std::string call(std::string_view request, std::any yield) override
    {
        std::any_cast<FakeYield>(yield);
        BOOST_LOG_TRIVIAL(debug) << "rpc request: " << request;

        if (m_it == m_responses.cend()) {
            // just repeat the last one
            auto r = std::string{m_responses.back()};
            BOOST_LOG_TRIVIAL(debug) << "rpc response: " << r;
            return r;
        }

        auto response = std::string{*m_it};
        ++m_it;
        BOOST_LOG_TRIVIAL(debug) << "rpc response: " << response;
        return response;
    }
};


enum class Method
{
    client_eth_blockNumber,
    client_eth_gasPrice,
    client_eth_getTransactionByHash,
    client_eth_getTransactionCount,
    // client_eth_newFilter,
    // client_eth_uninstallFilter,
    // client_eth_getFilterChanges,
    client_eth_getLogs,
    client_eth_sendRawTransaction,
    contract_setup_nonce,
    contract_postMessageId,
    contract_get_message_id_logs,
    contract_invalidate,
    contract_is_invalidated,
};


void test_one_input(std::string_view data)
{
    const auto client_action =
        [&]() -> Method {
            const auto default_method = Method::client_eth_blockNumber;
            if (data.empty()) {
                return default_method;
            }
            const auto c = data[0];
            data.remove_prefix(1);
            switch (c) {
            case '0': return Method::client_eth_blockNumber;
            case '1': return Method::client_eth_gasPrice;
            case '2': return Method::client_eth_getTransactionByHash;
            case '3': return Method::client_eth_getTransactionCount;
            // case '4': return Method::client_eth_newFilter;
            // case '5': return Method::client_eth_uninstallFilter;
            // case '6': return Method::client_eth_getFilterChanges;
            case '7': return Method::client_eth_getLogs;
            case '8': return Method::client_eth_sendRawTransaction;
            case '9': return Method::contract_setup_nonce;
            case 'a': return Method::contract_postMessageId;
            case 'b': return Method::contract_get_message_id_logs;
            case 'c': return Method::contract_invalidate;
            case 'd': return Method::contract_is_invalidated;
            default: return default_method;
            }
        }();

    const auto responses = split_input(data);

    auto transport_protocol_client = FakeTransportProtocolClient{responses};
    auto rpc_client = Ethereum::RPCClient{gsl::not_null{&transport_protocol_client}};
    auto contract = Ethereum::TigerMailContract{
        gsl::not_null{&rpc_client},
        {},
        {}, // 8a40bfaa73256b60764c1bf40675a99083efb075
        dev::Secret{"0x3ecb44df2159c26e0f995712d4f39b6f6e499b40749b1cf1246c37f9516cb6a4"}};

    auto yield = FakeYield{FakeYield::explicit_construction};

    switch (client_action) {

    case Method::client_eth_blockNumber:
        {
            const auto result = rpc_client.eth_blockNumber(yield);
            BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
        }
        break;

    case Method::client_eth_gasPrice:
        {
            const auto result = rpc_client.eth_gasPrice(yield);
            BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
        }
        break;

    case Method::client_eth_getTransactionByHash:
        {
            const auto result = rpc_client.eth_getTransactionByHash({}, yield);
            BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
        }
        break;

    case Method::client_eth_getTransactionCount:
        {
            const auto result = rpc_client.eth_getTransactionCount({}, yield);
            BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
        }
        break;

    // case Method::client_eth_newFilter:
    // case Method::client_eth_uninstallFilter:
    // case Method::client_eth_getFilterChanges:

    case Method::client_eth_getLogs:
        {
            const auto result = rpc_client.eth_getLogs({}, {}, {}, {}, yield);
            BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
        }
        break;

    case Method::client_eth_sendRawTransaction:
        {
            const auto result = rpc_client.eth_sendRawTransaction({}, yield);
            BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
        }
        break;

    case Method::contract_setup_nonce:
        contract.setup_nonce(yield);
        break;

    case Method::contract_postMessageId:
        {
            contract.setup_nonce(yield);
            const auto result = contract.postMessageId({}, yield);
            BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
        }
        break;

    case Method::contract_get_message_id_logs:
        {
            const auto result = contract.get_message_id_logs(0, 0, yield);
            BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
        }
        break;

    case Method::contract_invalidate:
        {
            contract.setup_nonce(yield);
            const auto result = contract.invalidate(yield);
            BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
        }
        break;

    case Method::contract_is_invalidated:
        {
            const auto result = contract.is_invalidated({}, 0u, yield);
            BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
        }
        break;

    }
}


} // namespace


#include "../main.hpp"
