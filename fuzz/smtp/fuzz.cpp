/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <sstream>
#include <string>
#include <string_view>

#include "../../smtp/limits.hpp"
#include "../../smtp/parser.hpp"
#include "../../smtp/session_handler.hpp"
#include "../../smtp/state_machine.hpp"

using TigerMail::SMTP::Impl;
using TigerMail::SMTP::Limits;
using TigerMail::SMTP::Message;
using TigerMail::SMTP::Parser;


namespace {


class TestImpl
    : public Impl
{
private:
    std::ostringstream m_output;
    bool m_disconnected;

public:
    explicit
    TestImpl(const Limits& limits)
        : Impl{
            [](std::string_view reverse_path) {
                return reverse_path == "A@A.com";
            },
            [](Message&&) {},
            limits,
            m_output,
            m_disconnected
        }
        , m_output{}
        , m_disconnected{false}
    {}
};


void test_one_input(std::string_view data)
{
    const auto limits = Limits{};
    auto smtp = TestImpl{limits};
    auto parser = Parser{smtp, limits};

    parser.process_input(data);
}


} // namespace


#include "../main.hpp"
