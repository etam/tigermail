/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <exception>
#include <random>
#include <sstream>
#include <string>
#include <string_view>
#include <thread>
#include <tuple>

#include <boost/asio/executor_work_guard.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/local/stream_protocol.hpp>
#include <boost/asio/post.hpp>

#include <boost/log/trivial.hpp>

#include "../../common/on_scope_exit.hpp"

#include "../../pop3_smtp_common/server/server.hpp"
#include "../../pop3_smtp_common/server/session_factory.hpp"
#include "../../pop3_smtp_common/server/session_handler_interface.hpp"

#include "../../swarm/ipc_client.hpp"

using TigerMail::POP3_SMTP_common::server::Server;
using TigerMail::POP3_SMTP_common::server::SessionHandlerInterface;
using TigerMail::POP3_SMTP_common::server::make_session_factory;

namespace asio = boost::asio;
namespace Swarm = TigerMail::Swarm;


namespace {


class SessionHandler
    : public SessionHandlerInterface
{
private:
    std::string m_response;

public:
    explicit
    SessionHandler(std::string_view response)
        : m_response{response}
    {}

    std::string handle_hello() override
    {
        return "";
    }

    std::tuple<std::string, bool> handle_input(std::string_view request) override
    {
        BOOST_LOG_TRIVIAL(debug) << "Got request \"" << request << '"';
        return {m_response, true};
    }

    std::string handle_stop() override
    {
        return "";
    }
};


void test_one_input(std::string_view data)
{
    auto io_context = asio::io_context{};

    const auto session_factory = make_session_factory<SessionHandler>(data);
    const auto socket_name =
        [&] {
            auto o = std::ostringstream{};
            o << "_tigermail_swarm_fuzz_" << std::random_device{}();
            auto s = o.str();
            s[0] = '\0';
            return s;
        }();

    auto server = Server{
        io_context,
        asio::local::stream_protocol::acceptor{io_context, socket_name},
        gsl::not_null{&session_factory}};

    auto work_guard = boost::asio::make_work_guard(io_context);
    
    auto io_thread = std::thread{
        [&] {
            io_context.run();
        }};

    auto on_scope_exit = TigerMail::OnScopeExit{};
    on_scope_exit.push(
        [&] {
            boost::asio::post(
                io_context,
                [&] {
                    server.stop();
                });
            work_guard.reset();
            io_thread.join();
        });

    Swarm::get_swarm_tcp_endpoint(
        Swarm::get_bzz_info(socket_name));
}


} // namespace


#include "../main.hpp"
