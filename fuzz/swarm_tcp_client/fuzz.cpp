/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <exception>
#include <random>
#include <sstream>
#include <string>
#include <string_view>
#include <tuple>

#include <boost/asio/io_context.hpp>
#include <boost/asio/local/stream_protocol.hpp>
#include <boost/asio/spawn.hpp>

#include <boost/log/trivial.hpp>

#include "../../common/test/out/buffer.hpp"
#include "../../common/test/out/fixed_size_buffer.hpp"
#include "../../common/test/out/std_optional.hpp"

#include "../../pop3_smtp_common/server/server.hpp"
#include "../../pop3_smtp_common/server/session_factory.hpp"
#include "../../pop3_smtp_common/server/session_handler_interface.hpp"

#include "../../swarm/tcp_client.hpp"


using TigerMail::POP3_SMTP_common::server::Server;
using TigerMail::POP3_SMTP_common::server::SessionHandlerInterface;
using TigerMail::POP3_SMTP_common::server::make_session_factory;

namespace asio = boost::asio;
namespace Swarm = TigerMail::Swarm;


namespace {


class SessionHandler
    : public SessionHandlerInterface
{
private:
    std::string m_response;

public:
    explicit
    SessionHandler(std::string_view response)
        : m_response{response}
    {}

    std::string handle_hello() override
    {
        return "";
    }

    std::tuple<std::string, bool> handle_input(std::string_view request) override
    {
        BOOST_LOG_TRIVIAL(debug) << "Got request \"" << request << '"';
        return {m_response, true};
    }

    std::string handle_stop() override
    {
        return "";
    }
};


enum class Method
{
    bzz_get,
    bzz_post,
    feed_get,
    feed_update,
};


void test_one_input(std::string_view data)
{
    const auto client_action =
        [&]() -> Method {
            const auto default_method = Method::bzz_get;
            if (data.empty()) {
                return default_method;
            }
            const auto c = data[0];
            data.remove_prefix(1);
            switch (c) {
            case '0': return Method::bzz_get;
            case '1': return Method::bzz_post;
            case '2': return Method::feed_get;
            case '3': return Method::feed_update;
            default: return default_method;
            }
        }();

    auto io_context = asio::io_context{};

    const auto session_factory = make_session_factory<SessionHandler>(data);
    const auto socket_name =
        [&] {
            auto o = std::ostringstream{};
            o << "_tigermail_swarm_fuzz_" << std::random_device{}();
            auto s = o.str();
            s[0] = '\0';
            return s;
        }();

    auto server = Server{
        io_context,
        asio::local::stream_protocol::acceptor{io_context, socket_name},
        gsl::not_null{&session_factory}};

    auto client = Swarm::TCPClient{io_context};

    asio::spawn(
        io_context,
        [&](asio::yield_context yield) {
            client.connect(asio::local::stream_protocol::endpoint{socket_name}, yield);

            switch (client_action) {
            case Method::bzz_get:
                {
                    const auto result = client.bzz_get({}, yield);
                    BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
                }
                break;

            case Method::bzz_post:
                {
                    const auto result = client.bzz_post({}, yield);
                    BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
                }
                break;

            case Method::feed_get:
                {
                    const auto result = client.feed_get({}, {}, yield);
                    BOOST_LOG_TRIVIAL(debug) << "call result: " << result;
                }
                break;

            case Method::feed_update:
                {
                    // name is 1 char and if equal to 'x', then subcommand fails
                    const auto name =
                        [&]() -> std::string_view {
                            if (data.empty()) {
                                return {};
                            }
                            const auto name = data.substr(0, 1);
                            data.remove_prefix(1);
                            return name;
                        }();
                    
                    client.feed_update({}, data, name, {}, yield);
                    BOOST_LOG_TRIVIAL(debug) << "call finished";
                }
                break;
            }

            server.stop();
        });

    io_context.run();
}


} // namespace


#include "../main.hpp"
