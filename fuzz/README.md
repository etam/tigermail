# Fuzzing

## Requirements

- AFL (https://lcamtuf.coredump.cx/afl/, but probably it's in your distro repository)
- Clang >= 6

Both are optional. The only caveat is that AFL is needed for merging and minimizing newly generated testcases.


## Setup

In project's root directory execute `meson_fuzz_afl.sh` and `meson_fuzz_llvm.sh` scripts.


## Run

In this directory run `run_afl.sh` or `run_llvm.sh` with subdirectory name as argument.

For example: `./run_llvm.sh pop3`.


## Debugging a single testcase

Use testing binary compiled under "afl" setup, with testcase path as argument.

For example: `../build_fuzz_afl/pop3_fuzz pop3/testcases/0.txt`.

If you want to have debugging output, comment out `set_logging_enabled(false)` in `main.hpp`.


## Known false positives

- [ethereum_transport_protocol/llvm_false_positives.txt](ethereum_transport_protocol/llvm_false_positives.txt)
