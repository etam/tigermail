/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "signal_buffer.hpp"

#include <gsl/gsl_util>

#include "../common/buffer.hpp"


namespace TigerMail::Signal {


SignalBufferRef::operator BufferView() const
{
    if (m_ptr == nullptr) {
        return {};
    }
    return {data(), gsl::narrow<BufferView::index_type>(size())};
}


MallocBuffer::operator BufferView() const
{
    return {data, gsl::narrow<BufferView::index_type>(size)};
}


} // namespace TigerMail::Signal
