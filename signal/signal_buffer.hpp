/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_COMMON_BUFFERS_HPP
#define TIGERMAIL_SIGNAL_COMMON_BUFFERS_HPP

#include <gsl/gsl_assert>

#include <signal_protocol.h>

namespace TigerMail {
class BufferView;
} // namespace TigerMail


namespace TigerMail::Signal {


class SignalBufferRef
{
private:
    signal_buffer* m_ptr = nullptr;

public:
    SignalBufferRef() = default;

    explicit
    SignalBufferRef(signal_buffer* ptr)
        : m_ptr(ptr)
    {}

    ~SignalBufferRef() noexcept
    {
        signal_buffer_free(m_ptr);
    }

    SignalBufferRef(const SignalBufferRef&) = delete;
    SignalBufferRef& operator=(const SignalBufferRef&) = delete;

    SignalBufferRef(SignalBufferRef&& other)
        : m_ptr{other.m_ptr}
    {
        other.m_ptr = nullptr;
    }

    SignalBufferRef& operator=(SignalBufferRef&& other)
    {
        std::swap(m_ptr, other.m_ptr);
        return *this;
    }

    operator BufferView() const;

    signal_buffer* get() const
    {
        return m_ptr;
    }

    signal_buffer** get2()
    {
        return &m_ptr;
    }

    std::uint8_t* data()
    {
        Expects(m_ptr != nullptr);
        return signal_buffer_data(m_ptr);
    }

    const std::uint8_t* data() const
    {
        Expects(m_ptr != nullptr);
        return signal_buffer_const_data(m_ptr);
    }

    std::size_t size() const
    {
        Expects(m_ptr != nullptr);
        return signal_buffer_len(m_ptr);
    }

    std::uint8_t* begin()
    {
        return data();
    }

    const std::uint8_t* begin() const
    {
        return data();
    }

    std::uint8_t* end()
    {
        return data() + size();
    }

    const std::uint8_t* end() const
    {
        return data() + size();
    }
};


struct MallocBuffer
{
    std::uint8_t* data;
    std::size_t size;

    MallocBuffer() = default;

    ~MallocBuffer()
    {
        std::free(data);
    }

    MallocBuffer(const MallocBuffer&) = delete;
    MallocBuffer& operator=(const MallocBuffer&) = delete;

    MallocBuffer(MallocBuffer&& other)
        : data{other.data}
        , size{other.size}
    {
        other.data = nullptr;
        other.size = 0;
    }

    MallocBuffer& operator=(MallocBuffer&& other)
    {
        std::swap(data, other.data);
        std::swap(size, other.size);
        return *this;
    }

    operator BufferView() const;
};


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_COMMON_BUFFERS_HPP
