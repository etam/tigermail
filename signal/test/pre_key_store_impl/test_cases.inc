/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

BOOST_AUTO_TEST_CASE(load_missing)
{
    BOOST_CHECK(!store.contains(0));
    BOOST_CHECK_EQUAL(store.load(0), std::nullopt);
}

BOOST_AUTO_TEST_CASE(load)
{
    // given
    const auto buffer = Buffer{'d', 'a', 't', 'a'};
    const auto id = PreKeyIdT{5};
    store.store(id, buffer);

    // when
    const auto received_opt_buffer = store.load(id);

    // then
    BOOST_CHECK(store.contains(id));
    BOOST_CHECK(!store.contains(id+1));
    BOOST_REQUIRE_NE(received_opt_buffer, std::nullopt);
    BOOST_CHECK_EQUAL_COLLECTIONS(
        buffer.begin(),
        buffer.end(),
        received_opt_buffer->begin(),
        received_opt_buffer->end()
    );
}

BOOST_AUTO_TEST_CASE(overwrite)
{
    // given
    const auto buffer1 = Buffer{'d', 'a', 't', 'a'};
    const auto buffer2 = Buffer{'f', 'o', 'o', 'b', 'a', 'r'};
    const auto id = PreKeyIdT{5};
    store.store(id, buffer1);
    store.store(id, buffer2);

    // when
    const auto received_opt_buffer = store.load(id);

    // then
    BOOST_REQUIRE_NE(received_opt_buffer, std::nullopt);
    BOOST_CHECK_EQUAL_COLLECTIONS(
        buffer2.begin(),
        buffer2.end(),
        received_opt_buffer->begin(),
        received_opt_buffer->end()
    );
}

BOOST_AUTO_TEST_CASE(remove)
{
    // given
    const auto buffer1 = Buffer{'f', 'o', 'o'};
    const auto buffer2 = Buffer{'b', 'a', 'r'};

    const auto id1 = PreKeyIdT{3};
    const auto id2 = PreKeyIdT{5};
    const auto id3 = PreKeyIdT{7};

    store.store(id1, buffer1);
    store.store(id2, buffer2);

    // when
    store.remove(id1);
    store.remove(id3);
    store.remove(id3); // should not throw

    // then
    BOOST_CHECK(!store.contains(id1));
    BOOST_CHECK(store.contains(id2));
}

BOOST_AUTO_TEST_CASE(key_ids)
{
    // given
    const auto buffer = Buffer{'f', 'o', 'o'};

    const auto id1 = PreKeyIdT{3};
    const auto id2 = PreKeyIdT{5};

    store.store(id1, buffer);
    store.store(id2, buffer);

    // when
    const auto ids = store.key_ids();

    // then
    const auto expected_ids = std::set<PreKeyIdT>{3, 5};
    BOOST_CHECK_EQUAL_COLLECTIONS(ids.begin(), ids.end(), expected_ids.begin(), expected_ids.end());
}
