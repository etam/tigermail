/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstring>
#include <set>

#include <boost/test/unit_test.hpp>

#include <session_record.h>
#include <session_state.h>

#include "../address.hpp"
#include "../common.hpp"
#include "../session_store.hpp"
#include "../signalpp/signal_protocol.hpp"

using TigerMail::Signal::DeviceIdT;
using TigerMail::Signal::Ref;
using TigerMail::Signal::makeMemorySessionStore;
namespace Signal = TigerMail::Signal;


namespace {


struct Fixture
{
    signal_context *context;
    signal_protocol_store_context* store_context;

    Fixture()
        : context{nullptr}
        , store_context{nullptr}
    {
        signal_context_create(&context, nullptr);
        signal_protocol_store_context_create(&store_context, context);
        const auto memory_session_store = makeMemorySessionStore();
        signal_protocol_store_context_set_session_store(store_context, &memory_session_store);
    }

    ~Fixture()
    {
        signal_protocol_store_context_destroy(store_context);
        signal_context_destroy(context);
    }

    void create_and_store_session(const signal_protocol_address* address)
    {
        Ref<session_record> record;
        BOOST_CHECK_EQUAL(session_record_create(record.get2(), nullptr, context), 0);
        Signal::signal_protocol_session_store_session(store_context, address, record.get());
    }

    void create_and_store_session_with_version(
        const signal_protocol_address* address,
        uint32_t version)
    {
        Ref<session_state> state;
        BOOST_CHECK_EQUAL(session_state_create(state.get2(), context), 0);
        session_state_set_session_version(state.get(), version);
        Ref<session_record> record;
        BOOST_CHECK_EQUAL(session_record_create(record.get2(), state.get(), context), 0);
        Signal::signal_protocol_session_store_session(store_context, address, record.get());
    }
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(session_store, Fixture)


BOOST_AUTO_TEST_CASE(load_missing)
{
    const char* name = "bla";
    const auto address = signal_protocol_address{name, std::strlen(name), 1};

    BOOST_CHECK_EQUAL(signal_protocol_session_contains_session(store_context, &address), 0);

    const auto record = Signal::signal_protocol_session_load_session(store_context, &address);
    BOOST_CHECK_EQUAL(session_record_is_fresh(record.get()), 1);
}

BOOST_AUTO_TEST_CASE(load)
{
    // given
    const char* name1 = "name1";
    const char* name2 = "name2";

    const auto address1 = signal_protocol_address{name1, std::strlen(name1), 1};
    const auto address2 = signal_protocol_address{name2, std::strlen(name2), 1};
    create_and_store_session(&address1);

    // when
    const auto record = Signal::signal_protocol_session_load_session(store_context, &address1);

    // then
    BOOST_CHECK_EQUAL(session_record_is_fresh(record.get()), 0);
    BOOST_CHECK_EQUAL(signal_protocol_session_contains_session(store_context, &address1), 1);
    BOOST_CHECK_EQUAL(signal_protocol_session_contains_session(store_context, &address2), 0);
}

BOOST_AUTO_TEST_CASE(overwrite)
{
    // given
    const char* name = "name";
    const auto address = signal_protocol_address{name, std::strlen(name), 1};
    create_and_store_session_with_version(&address, 5);
    create_and_store_session_with_version(&address, 7);

    // when
    const auto record = Signal::signal_protocol_session_load_session(store_context, &address);

    // then
    auto* state = session_record_get_state(record.get());
    BOOST_CHECK_EQUAL(session_state_get_session_version(state), 7);
}

BOOST_AUTO_TEST_CASE(get_sub_devices)
{
    // given
    const char* name1 = "name1";
    const char* name2 = "name2";

    const auto address11 = signal_protocol_address{name1, std::strlen(name1), 1};
    const auto address15 = signal_protocol_address{name1, std::strlen(name1), 5};
    const auto address17 = signal_protocol_address{name1, std::strlen(name1), 7};
    const auto address25 = signal_protocol_address{name2, std::strlen(name2), 5};

    create_and_store_session(&address11);
    create_and_store_session(&address15);
    create_and_store_session(&address17);
    create_and_store_session(&address25);

    // when
    // const auto sessions = store.get_sub_device_sessions(name1);
    signal_int_list* sessions = nullptr;
    BOOST_CHECK_EQUAL(signal_protocol_session_get_sub_device_sessions(store_context, &sessions, name1, std::strlen(name1)), 3);

    // then
    const auto sessions_size = signal_int_list_size(sessions);
    BOOST_CHECK_EQUAL(sessions_size, 3);

    // sessions may come in any order
    const auto expected_sessions = std::set<DeviceIdT>{1, 5, 7};
    auto sessions_set = std::set<DeviceIdT>{};
    for (unsigned int i = 0; i < sessions_size; ++i) {
        sessions_set.insert(signal_int_list_at(sessions, i));
    }
    BOOST_CHECK_EQUAL_COLLECTIONS(
        sessions_set.begin(),
        sessions_set.end(),
        expected_sessions.begin(),
        expected_sessions.end()
    );

    signal_int_list_free(sessions);
}

BOOST_AUTO_TEST_CASE(delete_)
{
    // given
    const char* name1 = "name1";
    const char* name2 = "name2";
    const char* name3 = "name3";

    const auto address1 = signal_protocol_address{name1, std::strlen(name1), 1};
    const auto address2 = signal_protocol_address{name2, std::strlen(name2), 2};
    const auto address3 = signal_protocol_address{name3, std::strlen(name3), 3};

    create_and_store_session(&address1);
    create_and_store_session(&address2);

    // when
    BOOST_CHECK_EQUAL(signal_protocol_session_delete_session(store_context, &address1), 1);
    BOOST_CHECK_EQUAL(signal_protocol_session_delete_session(store_context, &address3), 0);

    // then
    BOOST_CHECK_EQUAL(signal_protocol_session_contains_session(store_context, &address1), 0);
    BOOST_CHECK_EQUAL(signal_protocol_session_contains_session(store_context, &address2), 1);
}

BOOST_AUTO_TEST_CASE(delete_all)
{
    // given
    const char* name1 = "name1";
    const char* name2 = "name2";
    const char* name3 = "name3";

    const auto address11 = signal_protocol_address{name1, std::strlen(name1), 1};
    const auto address15 = signal_protocol_address{name1, std::strlen(name1), 5};
    const auto address17 = signal_protocol_address{name1, std::strlen(name1), 7};
    const auto address25 = signal_protocol_address{name2, std::strlen(name2), 5};

    create_and_store_session(&address11);
    create_and_store_session(&address15);
    create_and_store_session(&address17);
    create_and_store_session(&address25);

    // when
    BOOST_CHECK_EQUAL(signal_protocol_session_delete_all_sessions(store_context, name1, std::strlen(name1)), 3);
    BOOST_CHECK_EQUAL(signal_protocol_session_delete_all_sessions(store_context, name3, std::strlen(name3)), 0);

    // then
    BOOST_CHECK_EQUAL(signal_protocol_session_contains_session(store_context, &address11), 0);
    BOOST_CHECK_EQUAL(signal_protocol_session_contains_session(store_context, &address15), 0);
    BOOST_CHECK_EQUAL(signal_protocol_session_contains_session(store_context, &address17), 0);
    BOOST_CHECK_EQUAL(signal_protocol_session_contains_session(store_context, &address25), 1);
}


BOOST_AUTO_TEST_SUITE_END() // session_store
