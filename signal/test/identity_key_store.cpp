/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include <ratchet.h>

#include "../common.hpp"
#include "../context.hpp"
#include "../identity_key_store.hpp"
#include "../signalpp/curve.hpp"
#include "../signalpp/signal_protocol.hpp"

using TigerMail::Signal::Context;
using TigerMail::Signal::Ref;
using TigerMail::Signal::curve_generate_key_pair;
using TigerMail::Signal::makeMemoryIdentityKeyStore;
namespace Signal = TigerMail::Signal;


namespace {


struct Fixture
{
    Context context;
    signal_protocol_store_context* store_context;

    Fixture()
        : context{}
        , store_context{nullptr}
    {
        BOOST_REQUIRE_EQUAL(signal_protocol_store_context_create(&store_context, context.get()), 0);
        const auto identity_key_store = makeMemoryIdentityKeyStore(context.get());
        BOOST_REQUIRE_EQUAL(signal_protocol_store_context_set_identity_key_store(store_context, &identity_key_store), 0);        
    }

    ~Fixture()
    {
        signal_protocol_store_context_destroy(store_context);
    }

    Ref<ec_public_key> create_key()
    {
        const auto key_pair = curve_generate_key_pair(context.get());
        Ref pub_key{ec_key_pair_get_public(key_pair.get())};
        return pub_key;
    }
    
    Ref<ec_public_key> create_and_store_key(const signal_protocol_address *address)
    {
        auto pub_key = create_key();
        BOOST_CHECK_EQUAL(signal_protocol_identity_save_identity(store_context, address, pub_key.get()), 0);
        return pub_key;
    }
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(identity_key_store, Fixture)


BOOST_AUTO_TEST_CASE(key_pair)
{
    const auto key_pair = Signal::signal_protocol_identity_get_key_pair(store_context);

    auto* pub_key = ratchet_identity_key_pair_get_public(key_pair.get());
    auto* priv_key = ratchet_identity_key_pair_get_private(key_pair.get());

    auto pub_key2 = Ref<ec_public_key>{};
    BOOST_CHECK_EQUAL(curve_generate_public_key(pub_key2.get2(), priv_key), 0);
    BOOST_CHECK_EQUAL(ec_public_key_compare(pub_key2.get(), pub_key), 0);
}

BOOST_AUTO_TEST_CASE(reg_id)
{
    const auto reg_id = Signal::signal_protocol_identity_get_local_registration_id(store_context);

    BOOST_CHECK_GT(reg_id, 0);
    BOOST_CHECK_LE(reg_id, 16380);
}

BOOST_AUTO_TEST_CASE(is_unknown_trusted)
{
    const char* name = "name";
    const auto address = signal_protocol_address{name, std::strlen(name), 1};
    auto pub_key = create_key();

    BOOST_CHECK_EQUAL(signal_protocol_identity_is_trusted_identity(store_context, &address, pub_key.get()), 1);
}

BOOST_AUTO_TEST_CASE(is_known_trusted)
{
    const char* name = "name";
    const auto address = signal_protocol_address{name, std::strlen(name), 1};
    auto pub_key = create_and_store_key(&address);

    BOOST_CHECK_EQUAL(signal_protocol_identity_is_trusted_identity(store_context, &address, pub_key.get()), 1);
}

BOOST_AUTO_TEST_CASE(is_untrusted)
{
    const char* name = "name";
    const auto address = signal_protocol_address{name, std::strlen(name), 1};
    auto pub_key1 = create_and_store_key(&address);
    auto pub_key2 = create_key();

    BOOST_CHECK_EQUAL(signal_protocol_identity_is_trusted_identity(store_context, &address, pub_key2.get()), 0);
}

BOOST_AUTO_TEST_CASE(overwrite)
{
    const char* name = "name";
    const auto address = signal_protocol_address{name, std::strlen(name), 1};
    auto pub_key1 = create_and_store_key(&address);
    auto pub_key2 = create_and_store_key(&address);

    BOOST_CHECK_EQUAL(signal_protocol_identity_is_trusted_identity(store_context, &address, pub_key1.get()), 0);
    BOOST_CHECK_EQUAL(signal_protocol_identity_is_trusted_identity(store_context, &address, pub_key2.get()), 1);
}

BOOST_AUTO_TEST_CASE(remove)
{
    const char* name = "name";
    const auto address = signal_protocol_address{name, std::strlen(name), 1};
    auto pub_key1 = create_and_store_key(&address);
    auto pub_key2 = create_key();

    BOOST_CHECK_EQUAL(signal_protocol_identity_save_identity(store_context, &address, nullptr), 0);

    BOOST_CHECK_EQUAL(signal_protocol_identity_is_trusted_identity(store_context, &address, pub_key1.get()), 1);
    BOOST_CHECK_EQUAL(signal_protocol_identity_is_trusted_identity(store_context, &address, pub_key2.get()), 1);
}


BOOST_AUTO_TEST_SUITE_END() // identity_key_store
