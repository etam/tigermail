/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

BOOST_AUTO_TEST_CASE(load_missing)
{
    const auto address = Address{"bla", 1};

    BOOST_CHECK(!store.contains(address));
    BOOST_CHECK_EQUAL(store.load(address), std::nullopt);
}

BOOST_AUTO_TEST_CASE(load)
{
    // given
    const auto buffer = Buffer{'d', 'a', 't', 'a'};
    const auto address1 = Address{"name1", 1};
    const auto address2 = Address{"name2", 1};
    store.store(address1, buffer);

    // when
    const auto received_opt_buffer = store.load(address1);

    // then
    BOOST_CHECK(store.contains(address1));
    BOOST_CHECK(!store.contains(address2));
    BOOST_REQUIRE_NE(received_opt_buffer, std::nullopt);
    BOOST_CHECK_EQUAL_COLLECTIONS(
        buffer.begin(),
        buffer.end(),
        received_opt_buffer->begin(),
        received_opt_buffer->end()
    );
}

BOOST_AUTO_TEST_CASE(overwrite)
{
    // given
    const auto address = Address{"name", 1};
    const auto buffer1 = Buffer{'d', 'a', 't', 'a'};
    const auto buffer2 = Buffer{'f', 'o', 'o', 'b', 'a', 'r'};
    store.store(address, buffer1);
    store.store(address, buffer2);

    // when
    const auto received_opt_buffer = store.load(address);

    // then
    BOOST_REQUIRE_NE(received_opt_buffer, std::nullopt);
    BOOST_CHECK_EQUAL_COLLECTIONS(
        buffer2.begin(),
        buffer2.end(),
        received_opt_buffer->begin(),
        received_opt_buffer->end()
    );
}

BOOST_AUTO_TEST_CASE(get_sub_devices)
{
    // given
    const auto buffer1 = Buffer{'f', 'o', 'o'};
    const auto buffer2 = Buffer{'b', 'a', 'r'};
    const auto buffer3 = Buffer{'b', 'a', 'z'};
    const auto buffer4 = Buffer{'c', 'u', 'x'};

    const auto name1 = std::string{"name1"};
    const auto name2 = std::string{"name2"};

    store.store({name1, 1}, buffer1);
    store.store({name1, 5}, buffer2);
    store.store({name1, 7}, buffer3);
    store.store({name2, 5}, buffer4);

    // when
    const auto sessions = store.get_sub_devices(name1);

    // then
    const auto expected_sessions = std::set<DeviceIdT>{1, 5, 7};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        sessions.begin(),
        sessions.end(),
        expected_sessions.begin(),
        expected_sessions.end()
    );
}

BOOST_AUTO_TEST_CASE(delete_)
{
    // given
    const auto buffer1 = Buffer{'f', 'o', 'o'};
    const auto buffer2 = Buffer{'b', 'a', 'r'};

    const auto address1 = Address{"name1", 1};
    const auto address2 = Address{"name2", 2};
    const auto address3 = Address{"name3", 3};

    store.store(address1, buffer1);
    store.store(address2, buffer2);

    // when
    const auto result1 = store.delete_(address1);
    const auto result3 = store.delete_(address3);

    // then
    BOOST_CHECK(result1);
    BOOST_CHECK(!result3);
    BOOST_CHECK(!store.contains(address1));
    BOOST_CHECK(store.contains(address2));
}

BOOST_AUTO_TEST_CASE(delete_all)
{
    // given
    const auto buffer1 = Buffer{'f', 'o', 'o'};
    const auto buffer2 = Buffer{'b', 'a', 'r'};
    const auto buffer3 = Buffer{'b', 'a', 'z'};
    const auto buffer4 = Buffer{'c', 'u', 'x'};

    const auto name1 = std::string{"name1"};
    const auto name2 = std::string{"name2"};
    const auto name3 = std::string{"name3"};

    store.store({name1, 1}, buffer1);
    store.store({name1, 5}, buffer2);
    store.store({name1, 7}, buffer3);
    store.store({name2, 5}, buffer4);

    // when
    const auto result1 = store.delete_all(name1);
    const auto result3 = store.delete_all(name3);

    // then
    BOOST_CHECK_EQUAL(result1, 3);
    BOOST_CHECK_EQUAL(result3, 0);
    BOOST_CHECK(!store.contains({name1, 1}));
    BOOST_CHECK(!store.contains({name1, 5}));
    BOOST_CHECK(!store.contains({name1, 7}));
    BOOST_CHECK(store.contains({name2, 5}));
}

BOOST_AUTO_TEST_CASE(pub_key_no_session)
{
    const auto buffer1 = Buffer{'f', 'o', 'o'};
    const auto buffer2 = Buffer{'b', 'a', 'r'};
    const auto address = Address{"name", 1};

    BOOST_CHECK_THROW(store.store_keys_for_next_message_id(address, buffer1, buffer2), std::out_of_range);
    BOOST_CHECK_THROW(store.get_keys_for_message_id(address), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(pub_key)
{
    // given
    const auto buffer1 = Buffer{'f', 'o', 'o'};
    const auto buffer2 = Buffer{'b', 'a', 'r'};
    const auto buffer3 = Buffer{'b', 'a', 'z'};
    const auto address1 = Address{"name1", 1};
    const auto address2 = Address{"name2", 1};
    store.store(address1, buffer1);

    // when
    store.store_keys_for_next_message_id(address1, buffer2, buffer3);
    const auto received_buffers = store.get_keys_for_message_id(address1);

    // then
    BOOST_CHECK_THROW(store.get_keys_for_message_id(address2), std::out_of_range);
    BOOST_CHECK_EQUAL_COLLECTIONS(
        buffer2.begin(), buffer2.end(),
        received_buffers.serialized_ratchet_pub_key.begin(), received_buffers.serialized_ratchet_pub_key.end());
    BOOST_CHECK_EQUAL_COLLECTIONS(
        buffer3.begin(), buffer3.end(),
        received_buffers.message_keys.begin(), received_buffers.message_keys.end());
}

BOOST_AUTO_TEST_CASE(pub_key_delete)
{
    // given
    const auto buffer1 = Buffer{'f', 'o', 'o'};
    const auto buffer2 = Buffer{'b', 'a', 'r'};
    const auto buffer3 = Buffer{'b', 'a', 'z'};
    const auto address = Address{"name1", 1};
    store.store(address, buffer1);
    store.store_keys_for_next_message_id(address, buffer2, buffer3);

    // when
    store.delete_(address);

    // then
    BOOST_CHECK_THROW(store.get_keys_for_message_id(address), std::out_of_range);
}
