/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../common.hpp"
#include "../context.hpp"
#include "../pre_key_store.hpp"
#include "../signalpp/curve.hpp"
#include "../signalpp/session_pre_key.hpp"
#include "../signalpp/signal_protocol.hpp"

using TigerMail::Signal::Context;
using TigerMail::Signal::PreKeyIdT;
using TigerMail::Signal::Ref;
using TigerMail::Signal::makeMemoryPreKeyStore;
namespace Signal = TigerMail::Signal;


namespace {


struct Fixture
{
    Context context;
    signal_protocol_store_context* store_context;

    Fixture()
        : context{}
        , store_context{nullptr}
    {
        BOOST_REQUIRE_EQUAL(signal_protocol_store_context_create(&store_context, context.get()), 0);
        const auto pre_key_store = makeMemoryPreKeyStore();
        BOOST_REQUIRE_EQUAL(signal_protocol_store_context_set_pre_key_store(store_context, &pre_key_store), 0);
    }

    ~Fixture()
    {
        signal_protocol_store_context_destroy(store_context);
    }

    Ref<ec_private_key> create_and_store_key_with_id(PreKeyIdT pre_key_id)
    {
        const auto key_pair = Signal::curve_generate_key_pair(context.get());
        Ref private_key{ec_key_pair_get_private(key_pair.get())};

        const auto pre_key = Signal::session_pre_key_create(pre_key_id, key_pair.get());
        Signal::signal_protocol_pre_key_store_key(store_context, pre_key.get());

        return private_key;
    }
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(pre_key_store, Fixture)


BOOST_AUTO_TEST_CASE(load_missing)
{
    BOOST_CHECK_EQUAL(signal_protocol_pre_key_contains_key(store_context, 0), 0);
    session_pre_key* pre_key = nullptr;
    BOOST_CHECK_EQUAL(signal_protocol_pre_key_load_key(store_context, &pre_key, 0), SG_ERR_INVALID_KEY_ID);
    BOOST_CHECK_EQUAL(pre_key, nullptr);
}

BOOST_AUTO_TEST_CASE(load)
{
    // given
    const auto id = PreKeyIdT{5};
    auto given_private_key = create_and_store_key_with_id(id);

    // when
    const auto received_pre_key = Signal::signal_protocol_pre_key_load_key(store_context, id);

    // then
    BOOST_REQUIRE_NE(received_pre_key.get(), nullptr);
    BOOST_CHECK_EQUAL(signal_protocol_pre_key_contains_key(store_context, id), 1);
    BOOST_CHECK_EQUAL(signal_protocol_pre_key_contains_key(store_context, id+1), 0);
    auto* received_key_pair = session_pre_key_get_key_pair(received_pre_key.get());
    auto* received_private_key = ec_key_pair_get_private(received_key_pair);
    BOOST_CHECK_EQUAL(ec_private_key_compare(given_private_key.get(), received_private_key), 0);
}

BOOST_AUTO_TEST_CASE(overwrite)
{
    // given
    const auto id = PreKeyIdT{5};
    create_and_store_key_with_id(id);
    auto given_private_key = create_and_store_key_with_id(id);

    // when
    const auto received_pre_key = Signal::signal_protocol_pre_key_load_key(store_context, id);

    // then
    BOOST_REQUIRE_NE(received_pre_key.get(), nullptr);
    auto* received_key_pair = session_pre_key_get_key_pair(received_pre_key.get());
    auto* received_private_key = ec_key_pair_get_private(received_key_pair);
    BOOST_CHECK_EQUAL(ec_private_key_compare(given_private_key.get(), received_private_key), 0);
}

BOOST_AUTO_TEST_CASE(remove)
{
    // given
    const auto id1 = PreKeyIdT{3};
    const auto id2 = PreKeyIdT{5};
    const auto id3 = PreKeyIdT{7};

    create_and_store_key_with_id(id1);
    create_and_store_key_with_id(id2);

    // when
    Signal::signal_protocol_pre_key_remove_key(store_context, id1);
    Signal::signal_protocol_pre_key_remove_key(store_context, id3);
    Signal::signal_protocol_pre_key_remove_key(store_context, id3); // should not throw

    // then
    BOOST_CHECK_EQUAL(signal_protocol_pre_key_contains_key(store_context, id1), 0);
    BOOST_CHECK_EQUAL(signal_protocol_pre_key_contains_key(store_context, id2), 1);
}


BOOST_AUTO_TEST_SUITE_END() // pre_key_store
