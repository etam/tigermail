/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <memory>

#include <boost/test/unit_test.hpp>

#include "../../../common/buffer.hpp"
#include "../../../common/fixed_size_buffer.hpp"

#include "../../crypto_provider_impl/sha512.hpp"

using TigerMail::Buffer;
using TigerMail::FixedSizeBuffer;
using TigerMail::Signal::CryptoProviderImpl::Sha512Interface;


BOOST_AUTO_TEST_SUITE(crypto_provider_impl)
BOOST_AUTO_TEST_SUITE(sha512)

// data from https://csrc.nist.gov/projects/cryptographic-standards-and-guidelines/example-values
// https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/SHA512.pdf


BOOST_AUTO_TEST_CASE(known_answer1)
{
    const auto data = Buffer{'a','b','c'};

    auto result =
        [&] {
            auto impl = std::unique_ptr<Sha512Interface>{Sha512Interface::create()};
            impl->update(data);
            return impl->final();
        }();

    const auto expected_result = FixedSizeBuffer<64>{
        0xDD,0xAF,0x35,0xA1,0x93,0x61,0x7A,0xBA,0xCC,0x41,0x73,0x49,0xAE,0x20,0x41,0x31,
        0x12,0xE6,0xFA,0x4E,0x89,0xA9,0x7E,0xA2,0x0A,0x9E,0xEE,0xE6,0x4B,0x55,0xD3,0x9A,
        0x21,0x92,0x99,0x2A,0x27,0x4F,0xC1,0xA8,0x36,0xBA,0x3C,0x23,0xA3,0xFE,0xEB,0xBD,
        0x45,0x4D,0x44,0x23,0x64,0x3C,0xE8,0x0E,0x2A,0x9A,0xC9,0x4F,0xA5,0x4C,0xA4,0x9F};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        result.begin(), result.end(),
        expected_result.begin(), expected_result.end());
}

BOOST_AUTO_TEST_CASE(known_answer2)
{
    const auto data = Buffer{
        'a','b','c','d','e','f','g','h',
        'b','c','d','e','f','g','h','i',
        'c','d','e','f','g','h','i','j',
        'd','e','f','g','h','i','j','k',
        'e','f','g','h','i','j','k','l',
        'f','g','h','i','j','k','l','m',
        'g','h','i','j','k','l','m','n',
        'h','i','j','k','l','m','n','o',
        'i','j','k','l','m','n','o','p',
        'j','k','l','m','n','o','p','q',
        'k','l','m','n','o','p','q','r',
        'l','m','n','o','p','q','r','s',
        'm','n','o','p','q','r','s','t',
        'n','o','p','q','r','s','t','u'};

    auto result =
        [&] {
            auto impl = std::unique_ptr<Sha512Interface>{Sha512Interface::create()};
            impl->update(data);
            return impl->final();
        }();

    const auto expected_result = FixedSizeBuffer<64>{
        0x8E,0x95,0x9B,0x75,0xDA,0xE3,0x13,0xDA,0x8C,0xF4,0xF7,0x28,0x14,0xFC,0x14,0x3F,
        0x8F,0x77,0x79,0xC6,0xEB,0x9F,0x7F,0xA1,0x72,0x99,0xAE,0xAD,0xB6,0x88,0x90,0x18,
        0x50,0x1D,0x28,0x9E,0x49,0x00,0xF7,0xE4,0x33,0x1B,0x99,0xDE,0xC4,0xB5,0x43,0x3A,
        0xC7,0xD3,0x29,0xEE,0xB6,0xDD,0x26,0x54,0x5E,0x96,0xE5,0x5B,0x87,0x4B,0xE9,0x09};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        result.begin(), result.end(),
        expected_result.begin(), expected_result.end());
}


BOOST_AUTO_TEST_SUITE_END() // sha512
BOOST_AUTO_TEST_SUITE_END() // crypto_provider_impl
