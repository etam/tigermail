/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <memory>

#include <boost/test/unit_test.hpp>

#include "../../../common/buffer.hpp"
#include "../../../common/fixed_size_buffer.hpp"

#include "../../crypto_provider_impl/hmac_sha256.hpp"

using TigerMail::Buffer;
using TigerMail::FixedSizeBuffer;
using TigerMail::Signal::CryptoProviderImpl::HmacSha256Interface;


BOOST_AUTO_TEST_SUITE(crypto_provider_impl)
BOOST_AUTO_TEST_SUITE(hmac_sha256)


BOOST_AUTO_TEST_CASE(known_answer)
{
    // data from https://csrc.nist.gov/projects/cryptographic-standards-and-guidelines/example-values
    // https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/HMAC_SHA256.pdf
    const auto key = FixedSizeBuffer<64>{
        0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
        0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,
        0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,
        0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F};
    const auto data = Buffer{
        0x53,0x61,0x6D,0x70,0x6C,0x65,0x20,0x6D,0x65,0x73,0x73,0x61,0x67,0x65,0x20,0x66,
        0x6F,0x72,0x20,0x6B,0x65,0x79,0x6C,0x65,0x6E,0x3D,0x62,0x6C,0x6F,0x63,0x6B,0x6C,
        0x65,0x6E};

    const auto result =
        [&] {
            auto impl = std::unique_ptr<HmacSha256Interface>{HmacSha256Interface::create(key)};
            impl->update(data);
            return impl->final();
        }();

    const auto expected_result = FixedSizeBuffer<32>{
        0x8B,0xB9,0xA1,0xDB,0x98,0x06,0xF2,0x0D,0xF7,0xF7,0x7B,0x82,0x13,0x8C,0x79,0x14,
        0xD1,0x74,0xD5,0x9E,0x13,0xDC,0x4D,0x01,0x69,0xC9,0x05,0x7B,0x13,0x3E,0x1D,0x62};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        result.begin(), result.end(),
        expected_result.begin(), expected_result.end());
}


BOOST_AUTO_TEST_SUITE_END() // hmac_sha256
BOOST_AUTO_TEST_SUITE_END() // crypto_provider_impl
