/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <memory>

#include <boost/test/unit_test.hpp>

#include "../../../common/fixed_size_buffer.hpp"

#include "../../crypto_provider_impl/random.hpp"

using TigerMail::FixedSizeBuffer;
using Random = TigerMail::Signal::CryptoProviderImpl::RandomInterface;


BOOST_AUTO_TEST_SUITE(crypto_provider_impl)
BOOST_AUTO_TEST_SUITE(random)


BOOST_AUTO_TEST_CASE(fake_known_answer)
{
    auto random = std::unique_ptr<Random>(Random::create_fake());

    auto buffer1 = FixedSizeBuffer<8>{};
    auto buffer2 = FixedSizeBuffer<8>{};
    random->generate(buffer1);
    random->generate(buffer2);

    const auto expected1 = FixedSizeBuffer<8>{
        0x13,0x3a,0xc7,0x51,0x70,0xfa,0xb9,0x74};
    const auto expected2 = FixedSizeBuffer<8>{
        0xfa,0x4e,0x89,0x43,0x80,0x16,0x12,0x6b};

    BOOST_CHECK_EQUAL_COLLECTIONS(
        buffer1.begin(), buffer1.end(),
        expected1.begin(), expected1.end());
    BOOST_CHECK_EQUAL_COLLECTIONS(
        buffer2.begin(), buffer2.end(),
        expected2.begin(), expected2.end());
}


BOOST_AUTO_TEST_SUITE_END() // random
BOOST_AUTO_TEST_SUITE_END() // crypto_provider_impl
