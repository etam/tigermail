/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../../common/buffer.hpp"

#include "../common.hpp"
#include "../context.hpp"
#include "../key_bundle.hpp"
#include "../signalpp/curve.hpp"

using TigerMail::Buffer;
using TigerMail::Signal::Context;
using TigerMail::Signal::KeyBundle;
using TigerMail::Signal::Ref;
using TigerMail::Signal::curve_generate_key_pair;
using TigerMail::Signal::deserializeKeyBundle;
using TigerMail::Signal::serializeKeyBundle;


namespace {


struct Fixture
{
    Context context;

    Ref<ec_public_key> create_key()
    {
        const auto key_pair = curve_generate_key_pair(context.get());
        Ref pub_key{ec_key_pair_get_public(key_pair.get())};
        return pub_key;
    }
};


} // namespace


BOOST_FIXTURE_TEST_CASE(key_bundle, Fixture)
{
    const auto key_bundle = KeyBundle{
        .registration_id = 5,
        .device_id = 18,
        .identity_key = create_key(),
        .signed_pre_key = {
            .id = 1,
            .key = create_key(),
        },
        .signed_pre_key_signature = Buffer{'s', 'i', 'g', 'n'},
        .pre_keys = {
            {
                .id = 3,
                .key = create_key(),
            },
            {
                .id = 4,
                .key = create_key(),
            }
        }
    };

    const auto serialized_key_bundle = serializeKeyBundle(key_bundle);
    const auto key_bundle2 = deserializeKeyBundle(serialized_key_bundle, context.get());

    BOOST_CHECK_EQUAL(key_bundle.registration_id, key_bundle2.registration_id);
    BOOST_CHECK_EQUAL(key_bundle.device_id, key_bundle2.device_id);
    BOOST_CHECK_EQUAL(ec_public_key_compare(key_bundle.identity_key.get(), key_bundle2.identity_key.get()), 0);
    BOOST_CHECK_EQUAL(key_bundle.signed_pre_key.id, key_bundle2.signed_pre_key.id);
    BOOST_CHECK_EQUAL(ec_public_key_compare(key_bundle.signed_pre_key.key.get(), key_bundle2.signed_pre_key.key.get()), 0);
    BOOST_CHECK_EQUAL_COLLECTIONS(
        key_bundle.signed_pre_key_signature.begin(),
        key_bundle.signed_pre_key_signature.end(),
        key_bundle2.signed_pre_key_signature.begin(),
        key_bundle2.signed_pre_key_signature.end());
    BOOST_REQUIRE_EQUAL(key_bundle.pre_keys.size(), key_bundle2.pre_keys.size());
    for (std::size_t i = 0, end = key_bundle.pre_keys.size(); i < end; ++i) {
        BOOST_CHECK_EQUAL(key_bundle.pre_keys[i].id, key_bundle2.pre_keys[i].id);
        BOOST_CHECK_EQUAL(ec_public_key_compare(key_bundle.pre_keys[i].key.get(), key_bundle2.pre_keys[i].key.get()), 0);
    }
}
