/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

BOOST_AUTO_TEST_CASE(key_pair)
{
    const auto serialized_key_pair = store.get_identity_key_pair();

    const auto pub_key = Signal::curve_decode_point(*serialized_key_pair.pub_key, context.get());

    auto priv_key = Ref<ec_private_key>{};
    BOOST_CHECK_EQUAL(
        curve_decode_private_point(
            priv_key.get2(),
            signal_buffer_const_data(serialized_key_pair.priv_key->get()),
            signal_buffer_len(serialized_key_pair.priv_key->get()),
            context.get()),
        0);

    auto pub_key2 = Ref<ec_public_key>{};
    BOOST_CHECK_EQUAL(curve_generate_public_key(pub_key2.get2(), priv_key.get()), 0);
    BOOST_CHECK_EQUAL(ec_public_key_compare(pub_key2.get(), pub_key.get()), 0);
}

BOOST_AUTO_TEST_CASE(reg_id)
{
    const auto reg_id = store.get_local_registration_id();
    BOOST_CHECK_GT(reg_id, 0);
    BOOST_CHECK_LE(reg_id, 16380);
}

BOOST_AUTO_TEST_CASE(is_unknown_trusted)
{
    const auto address = Address{"name", 1};
    const auto buffer = Buffer{'d', 'a', 't', 'a'};

    BOOST_CHECK(store.is_trusted(address, buffer));
}

BOOST_AUTO_TEST_CASE(is_known_trusted)
{
    const auto address = Address{"name", 1};
    const auto buffer = Buffer{'d', 'a', 't', 'a'};
    store.save(address, buffer);

    BOOST_CHECK(store.is_trusted(address, buffer));
}

BOOST_AUTO_TEST_CASE(is_untrusted)
{
    const auto address = Address{"name", 1};
    const auto buffer1 = Buffer{'d', 'a', 't', 'a'};
    const auto buffer2 = Buffer{'f', 'o', 'o'};
    store.save(address, buffer1);

    BOOST_CHECK(!store.is_trusted(address, buffer2));
}

BOOST_AUTO_TEST_CASE(overwrite)
{
    const auto address = Address{"name", 1};
    const auto buffer1 = Buffer{'d', 'a', 't', 'a'};
    const auto buffer2 = Buffer{'f', 'o', 'o'};
    store.save(address, buffer1);
    store.save(address, buffer2);

    BOOST_CHECK(!store.is_trusted(address, buffer1));
    BOOST_CHECK(store.is_trusted(address, buffer2));
}

BOOST_AUTO_TEST_CASE(remove)
{
    const auto address = Address{"name", 1};
    const auto buffer1 = Buffer{'d', 'a', 't', 'a'};
    const auto buffer2 = Buffer{'f', 'o', 'o'};
    store.save(address, buffer1);
    store.save(address, {});

    BOOST_CHECK(store.is_trusted(address, buffer1));
    BOOST_CHECK(store.is_trusted(address, buffer2));
}
