/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include <gsl/pointers>

#include <signal_protocol.h>

#include <sqlite_modern_cpp.h>

#include "../../../common/buffer.hpp"

#include "../../address.hpp"
#include "../../common.hpp"
#include "../../context.hpp"
#include "../../signalpp/curve.hpp"
#include "../../identity_key_store_impl/interface.hpp"
#include "../../identity_key_store_impl/sqlite.hpp"

using TigerMail::Buffer;
using TigerMail::Signal::Address;
using TigerMail::Signal::Context;
using TigerMail::Signal::Ref;
using TigerMail::Signal::IdentityKeyStoreImpl::SqliteIdentityKeyStore;
namespace Signal = TigerMail::Signal;


namespace {


struct Fixture
{
    Context context;
    sqlite::database database{":memory:"};
    SqliteIdentityKeyStore store{gsl::not_null{&database}, context.get()};
};


} // namespace


BOOST_AUTO_TEST_SUITE(identity_key_store_impl)
BOOST_FIXTURE_TEST_SUITE(sqlite, Fixture)


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsubobject-linkage"
#include "test_cases.inc"
#pragma GCC diagnostic pop


BOOST_AUTO_TEST_SUITE_END() // sqlite
BOOST_AUTO_TEST_SUITE_END() // identity_key_store_impl
