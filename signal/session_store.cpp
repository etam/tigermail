/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "session_store.hpp"

#include <exception>

#include <boost/log/trivial.hpp>

#include <gsl/gsl_util>

#include "session_store_impl/interface.hpp"
#include "session_store_impl/memory.hpp"
#include "session_store_impl/sqlite.hpp"


namespace TigerMail::Signal {


namespace {

using SessionStoreImpl::SessionStoreInterface;



int load(
        signal_buffer** record,
        signal_buffer** /*user_record*/,
        const signal_protocol_address* address,
        void* user_data)
try {
    auto* store = static_cast<SessionStoreInterface*>(user_data);
    auto opt_record_vec = store->load(Address::from_signal(address));
    if (opt_record_vec == std::nullopt) {
        *record = nullptr;
        return 0;
    }
    *record = signal_buffer_create(opt_record_vec->data(), opt_record_vec->size());
    return 1;
}
catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(error) << "load_session error: " << e.what();
    return SG_ERR_UNKNOWN;
}

int get_sub_devices(
        signal_int_list** sessions,
        const char* name,
        std::size_t name_len,
        void* user_data)
try {
    const auto* store = static_cast<const SessionStoreInterface*>(user_data);
    const auto sessions_vec = store->get_sub_devices({name, name_len});

    *sessions = signal_int_list_alloc();
    for (int i : sessions_vec) {
        signal_int_list_push_back(*sessions, i);
    }

    return sessions_vec.size();
}
catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(error) << "get_sub_device_sessions error: " << e.what();
    return SG_ERR_UNKNOWN;
}

int store(
        const signal_protocol_address* address,
        std::uint8_t* record,
        std::size_t record_len,
        std::uint8_t* /*user_record*/,
        std::size_t /*user_record_len*/,
        void* user_data)
try {
    auto* store = static_cast<SessionStoreInterface*>(user_data);
    store->store(Address::from_signal(address), {record, gsl::narrow<BufferView::index_type>(record_len)});
    return 0;
}
catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(error) << "store_sessions error: " << e.what();
    return SG_ERR_UNKNOWN;
}

int contains(const signal_protocol_address* address, void* user_data)
{
    const auto* store = static_cast<const SessionStoreInterface*>(user_data);
    return store->contains(Address::from_signal(address));
}

int delete_(const signal_protocol_address* address, void* user_data)
try {
    auto* store = static_cast<SessionStoreInterface*>(user_data);
    return store->delete_(Address::from_signal(address));
}
catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(error) << "selete_session error: " << e.what();
    return SG_ERR_UNKNOWN;
}

int delete_all(const char* name, std::size_t name_len, void* user_data)
try {
    auto* store = static_cast<SessionStoreInterface*>(user_data);
    return store->delete_all({name, name_len});
}
catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(error) << "selete_all_sessions error: " << e.what();
    return SG_ERR_UNKNOWN;
}

void destroy(void* user_data)
{
    auto* store = static_cast<SessionStoreInterface*>(user_data);
    delete store;
}

signal_protocol_session_store makeSessionStore(void* user_data)
{
    return {
        .load_session_func = load,
        .get_sub_device_sessions_func = get_sub_devices,
        .store_session_func = store,
        .contains_session_func = contains,
        .delete_session_func = delete_,
        .delete_all_sessions_func = delete_all,
        .destroy_func = destroy,
        .user_data = user_data,
    };
}


} // namespace


signal_protocol_session_store makeMemorySessionStore()
{
    return makeSessionStore(new SessionStoreImpl::MemorySessionStore{});
}

signal_protocol_session_store makeSqliteSessionStore(gsl::not_null<sqlite::database*> database)
{
    return makeSessionStore(new SessionStoreImpl::SqliteSessionStore{database});
}


} // namespace TigerMail::Signal
