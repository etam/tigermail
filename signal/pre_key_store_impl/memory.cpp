/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "memory.hpp"


namespace TigerMail::Signal::PreKeyStoreImpl {


MemoryPreKeyStore::MemoryPreKeyStore()
    : m_keys{}
{}

std::optional<Buffer> MemoryPreKeyStore::load(PreKeyIdT pre_key_id) const
{
    const auto it = m_keys.find(pre_key_id);
    if (it == m_keys.end()) {
        return std::nullopt;
    }
    return it->second;
}

void MemoryPreKeyStore::store(PreKeyIdT pre_key_id, BufferView record)
{
    m_keys.insert_or_assign(pre_key_id, Buffer{record.begin(), record.end()});
}

bool MemoryPreKeyStore::contains(PreKeyIdT pre_key_id) const
{
    return m_keys.count(pre_key_id);
}

void MemoryPreKeyStore::remove(PreKeyIdT pre_key_id)
{
    m_keys.erase(pre_key_id);
}

std::set<PreKeyIdT> MemoryPreKeyStore::key_ids() const
{
    auto ids = std::set<PreKeyIdT>{};
    for (const auto& pair : m_keys) {
        ids.insert(pair.first);
    }
    return ids;
}


} // namespace TigerMail::Signal::PreKeyStoreImpl
