/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sqlite.hpp"

#include <sstream>
#include <string_view>

#include <sqlite_modern_cpp.h>


namespace TigerMail::Signal::PreKeyStoreImpl {


SqlitePreKeyStore::SqlitePreKeyStore(gsl::not_null<sqlite::database*> database, bool signed_)
    : m_database{database}
    , m_signed{signed_}
{
    *m_database <<
        "create table if not exists "
        "signal_pre_key ( "
        "  id int not null, "
        "  signed bool not null, "
        "  data blob not null, "
        "  primary key (id, signed));";
}

std::optional<Buffer> SqlitePreKeyStore::load(PreKeyIdT pre_key_id) const
{
    auto result = std::optional<Buffer>{};
    *m_database <<
        "select data "
        "from signal_pre_key "
        "where "
        "  id = ? "
        "  and signed = ?;"
                << pre_key_id
                << m_signed
                >> [&](Buffer data) {
                       result = std::move(data);
                   };
    return result;
}

void SqlitePreKeyStore::store(PreKeyIdT pre_key_id, BufferView record)
{
    *m_database <<
        "insert into signal_pre_key (id, signed, data) "
        "values (?, ?, ?) "
        "on conflict (id, signed) do update set data=excluded.data;"
                << pre_key_id
                << m_signed
                << Buffer(record.begin(), record.end()); // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
}

bool SqlitePreKeyStore::contains(PreKeyIdT pre_key_id) const
{
    auto exists = bool{};
    *m_database <<
        "select exists ( "
        "  select * "
        "  from signal_pre_key "
        "  where "
        "    id = ? "
        "    and signed = ?);"
                << pre_key_id
                << m_signed
                >> exists;
    return exists;
}

void SqlitePreKeyStore::remove(PreKeyIdT pre_key_id)
{
    *m_database <<
        "delete "
        "from signal_pre_key "
        "where "
        "  id = ? "
        "  and signed = ?;"
                << pre_key_id
                << m_signed;
}

std::set<PreKeyIdT> SqlitePreKeyStore::key_ids() const
{
    auto ids = std::set<PreKeyIdT>{};
    *m_database <<
        "select id "
        "from signal_pre_key "
        "where "
        "  signed = ?;"
                << m_signed
                >> [&](PreKeyIdT id) {
                       ids.insert(id);
                   };
    return ids;
}


} // namespace TigerMail::Signal::PreKeyStoreImpl
