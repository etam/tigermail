/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_PRE_KEY_STORE_IMPL_INTERFACE_HPP
#define TIGERMAIL_SIGNAL_PRE_KEY_STORE_IMPL_INTERFACE_HPP

#include <optional>
#include <set>

#include "../../common/buffer.hpp"
#include "../../common/buffer_view.hpp"

#include "../common.hpp"


namespace TigerMail::Signal::PreKeyStoreImpl {


class PreKeyStoreInterface
{
public:
    virtual ~PreKeyStoreInterface() noexcept = default;

    virtual std::optional<Buffer> load(PreKeyIdT pre_key_id) const = 0;
    virtual void store(PreKeyIdT pre_key_id, BufferView record) = 0;
    virtual bool contains(PreKeyIdT pre_key_id) const = 0;
    virtual void remove(PreKeyIdT pre_key_id) = 0;
    virtual std::set<PreKeyIdT> key_ids() const = 0;
};


} // namespace TigerMail::Signal::PreKeyStoreImpl

#endif // TIGERMAIL_SIGNAL_PRE_KEY_STORE_IMPL_INTERFACE_HPP
