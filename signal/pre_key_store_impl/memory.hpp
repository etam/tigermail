/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_PRE_KEY_STORE_IMPL_MEMORY_HPP
#define TIGERMAIL_SIGNAL_PRE_KEY_STORE_IMPL_MEMORY_HPP

#include <unordered_map>

#include "../../common/buffer.hpp"
#include "../../common/buffer_view.hpp"

#include "../common.hpp"

#include "interface.hpp"


namespace TigerMail::Signal::PreKeyStoreImpl {


class MemoryPreKeyStore
    : public PreKeyStoreInterface
{
private:
    std::unordered_map<PreKeyIdT, Buffer> m_keys;

public:
    MemoryPreKeyStore();

    std::optional<Buffer> load(PreKeyIdT pre_key_id) const override;
    void store(PreKeyIdT pre_key_id, BufferView record) override;
    bool contains(PreKeyIdT pre_key_id) const override;
    void remove(PreKeyIdT pre_key_id) override;
    std::set<PreKeyIdT> key_ids() const override;
};


} // namespace TigerMail::Signal::PreKeyStoreImpl

#endif // TIGERMAIL_SIGNAL_PRE_KEY_STORE_IMPL_MEMORY_HPP
