/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "hmac_sha256.hpp"

#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_HMAC_SHA256_USE_CRYPTOPP
#include "cryptopp/hmac_sha256.hpp"
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_HMAC_SHA256_USE_CRYPTOPP
#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_HMAC_SHA256_USE_OPENSSL
#include "openssl/hmac_sha256.hpp"
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_HMAC_SHA256_USE_OPENSSL


namespace TigerMail::Signal::CryptoProviderImpl {


#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_HMAC_SHA256_USE_CRYPTOPP
using HmacSha256 = CryptoPP::HmacSha256;
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_HMAC_SHA256_USE_CRYPTOPP
#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_HMAC_SHA256_USE_OPENSSL
using HmacSha256 = OpenSSL::HmacSha256;
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_HMAC_SHA256_USE_OPENSSL


HmacSha256Interface* HmacSha256Interface::create(BufferView key)
{
    return new HmacSha256{key};
}


} // namespace TigerMail::Signal::CryptoProviderImpl
