/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "random.hpp"

#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_RANDOM_USE_CRYPTOPP
#include "cryptopp/random.hpp"
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_RANDOM_USE_CRYPTOPP
#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_RANDOM_USE_OPENSSL
#include "openssl/random.hpp"
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_RANDOM_USE_OPENSSL

#include "fake/random.hpp"


namespace TigerMail::Signal::CryptoProviderImpl {


#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_RANDOM_USE_CRYPTOPP
using Random = CryptoPP::Random;
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_RANDOM_USE_CRYPTOPP
#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_RANDOM_USE_OPENSSL
using Random = OpenSSL::Random;
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_RANDOM_USE_OPENSSL


RandomInterface* RandomInterface::create()
{
    return new Random();
}

RandomInterface* RandomInterface::create_fake()
{
    return new Fake::Random();
}


} // namespace TigerMail::Signal::CryptoProviderImpl
