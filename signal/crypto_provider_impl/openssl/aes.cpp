/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../aes.hpp"

#include <limits>
#include <memory>
#include <stdexcept>
#include <string>

#include <openssl/evp.h>

#include "common.hpp"


namespace TigerMail::Signal::CryptoProviderImpl {

using OpenSSL::openssl_alloc_error;


namespace {


const EVP_CIPHER* aes_cipher(size_t key_len)
{
    if (key_len == 16) {
        // seems unused
        // return EVP_aes_128_cbc();
        return nullptr;
    }
    else if (key_len == 24) {
        // seems unused
        // return EVP_aes_192_cbc();
        return nullptr;
    }
    else if (key_len == 32) {
        return EVP_aes_256_cbc();
    }
    return nullptr;
}


} // namespace


Buffer encrypt(BufferView key, BufferView iv, BufferView plaintext)
{
    const auto* evp_cipher = aes_cipher(key.size());
    if (!evp_cipher) {
        throw std::invalid_argument("invalid key size: " + std::to_string(key.size()));
    }

    if (iv.size() != 16) {
        throw std::invalid_argument("invalid AES IV size: " + std::to_string(iv.size()));
    }

    const auto cipher_block_size = EVP_CIPHER_block_size(evp_cipher);

    if (plaintext.size() > static_cast<unsigned int>(std::numeric_limits<int>::max() - cipher_block_size)) {
        throw std::invalid_argument("invalid plaintext length: " + std::to_string(plaintext.size()));
    }

    auto ctx = std::unique_ptr<EVP_CIPHER_CTX, decltype(&EVP_CIPHER_CTX_free)>{
        EVP_CIPHER_CTX_new(), &EVP_CIPHER_CTX_free};
    if (!ctx) {
        throw openssl_alloc_error();
    }

    if (!EVP_EncryptInit_ex(ctx.get(), evp_cipher, 0, key.data(), iv.data())) {
        throw std::runtime_error("cannot initialize cipher");
    }

    auto out_buf = Buffer(plaintext.size() + cipher_block_size);
    auto out_len = int{0};

    if (!EVP_EncryptUpdate(ctx.get(),
                           out_buf.data(), &out_len,
                           plaintext.data(), plaintext.size())) {
        throw std::runtime_error("cannot encrypt plaintext");
    }

    auto final_len = int{0};
    if (!EVP_EncryptFinal_ex(ctx.get(), out_buf.data() + out_len, &final_len)) {
        throw std::runtime_error("cannot finish encrypting plaintext");
    }

    out_buf.resize(out_len + final_len);
    out_buf.shrink_to_fit();

    return out_buf;
}

Buffer decrypt(BufferView key, BufferView iv, BufferView ciphertext)
{
    const auto* evp_cipher = aes_cipher(key.size());
    if (!evp_cipher) {
        throw std::invalid_argument("invalid key size: " + std::to_string(key.size()));
    }

    if (iv.size() != 16) {
        throw std::invalid_argument("invalid AES IV size: " + std::to_string(iv.size()));
    }

    const auto cipher_block_size = EVP_CIPHER_block_size(evp_cipher);

    if (ciphertext.size() > static_cast<unsigned int>(std::numeric_limits<int>::max() - cipher_block_size)) {
        throw std::invalid_argument("invalid ciphertext length: " + std::to_string(ciphertext.size()));
    }

    auto ctx = std::unique_ptr<EVP_CIPHER_CTX, decltype(&EVP_CIPHER_CTX_free)>{
        EVP_CIPHER_CTX_new(), &EVP_CIPHER_CTX_free};
    if (!ctx) {
        throw openssl_alloc_error();
    }

    if (!EVP_DecryptInit_ex(ctx.get(), evp_cipher, 0, key.data(), iv.data())) {
        throw std::runtime_error("cannot initialize cipher");
    }

    auto out_buf = Buffer(ciphertext.size() + cipher_block_size);
    auto out_len = int{0};

    if (!EVP_DecryptUpdate(ctx.get(),
                           out_buf.data(), &out_len,
                           ciphertext.data(), ciphertext.size())) {
        throw std::runtime_error("cannot decrypt ciphertex");
    }

    auto final_len = int{0};
    if (!EVP_DecryptFinal_ex(ctx.get(), out_buf.data() + out_len, &final_len)) {
        throw std::runtime_error("cannot finish decrypting ciphertext");
    }

    out_buf.resize(out_len + final_len);
    out_buf.shrink_to_fit();

    return out_buf;
}


} // namespace TigerMail::Signal::CryptoProviderImpl
