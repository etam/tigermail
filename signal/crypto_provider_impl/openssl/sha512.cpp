/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sha512.hpp"

#include <gsl/gsl_assert>

#include "common.hpp"


namespace TigerMail::Signal::CryptoProviderImpl::OpenSSL {


Sha512::Sha512()
    : m_ctx{EVP_MD_CTX_create()}
{
    if (!m_ctx) {
        throw openssl_alloc_error();
    }

    if (EVP_DigestInit_ex(m_ctx, EVP_sha512(), 0) != 1) {
        EVP_MD_CTX_destroy(m_ctx);
        throw openssl_error();
    }
}

Sha512::~Sha512() noexcept
{
    EVP_MD_CTX_destroy(m_ctx);
}

void Sha512::update(BufferView data)
{
    if (EVP_DigestUpdate(m_ctx, data.data(), data.size()) != 1) {
        throw openssl_error();
    }
}

FixedSizeBuffer<64> Sha512::final()
{
    unsigned char md[EVP_MAX_MD_SIZE];
    unsigned int len = 0;

    if (EVP_DigestFinal_ex(m_ctx, md, &len) != 1) {
        throw openssl_error();
    }
    Ensures(len == 64);

    if (EVP_DigestInit_ex(m_ctx, EVP_sha512(), 0) != 1) {
        throw openssl_error();
    }

    return FixedSizeBuffer<64>(md, md+len);
}


} // namespace TigerMail::Signal::CryptoProviderImpl::OpenSSL
