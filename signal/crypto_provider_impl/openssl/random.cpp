/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

// https://wiki.openssl.org/index.php/Random_Numbers

#include "random.hpp"

#include <openssl/rand.h>

#include "common.hpp"


namespace TigerMail::Signal::CryptoProviderImpl::OpenSSL {


Random::Random()
{
    if (RAND_load_file("/dev/urandom", 32) != 32) {
        throw openssl_error();
    }
}

void Random::generate(BufferSpan data)
{
    if (RAND_bytes(data.data(), data.size()) != 1) {
        throw openssl_error();
    }
}


} // namespace TigerMail::Signal::CryptoProviderImpl::OpenSSL
