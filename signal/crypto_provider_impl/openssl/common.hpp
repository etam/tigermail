/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_OPENSSL_COMMON_HPP
#define TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_OPENSSL_COMMON_HPP

#include <new>
#include <exception>

#include <openssl/err.h>


namespace TigerMail::Signal::CryptoProviderImpl::OpenSSL {


class openssl_alloc_error
    : public std::bad_alloc
{
public:
    const char* what() const noexcept override
    {
        return "openssl failed to allocate object";
    }
};

// TODO: probably not the best solution.
// might leave some messages in the queue
class openssl_error
    : public std::exception
{
    unsigned long m_err_code;

public:
    openssl_error()
        : std::exception{}
        , m_err_code{ERR_get_error()}
    {}

    const char* what() const noexcept override
    {
        return ERR_reason_error_string(m_err_code);
    }
};


} // namespace TigerMail::Signal::CryptoProviderImpl::OpenSSL

#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_OPENSSL_COMMON_HPP
