/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "hmac_sha256.hpp"

#include <gsl/gsl_assert>

#include "common.hpp"


namespace TigerMail::Signal::CryptoProviderImpl::OpenSSL {


HmacSha256::HmacSha256(BufferView key)
    : m_ctx{nullptr}
{
    m_ctx = HMAC_CTX_new();
    if (!m_ctx) {
        throw openssl_alloc_error();
    }

    if (HMAC_Init_ex(m_ctx, key.data(), key.size(), EVP_sha256(), 0) != 1) {
        HMAC_CTX_free(m_ctx);
        throw openssl_error();
    }
}

HmacSha256::~HmacSha256() noexcept
{
    HMAC_CTX_free(m_ctx);
}

void HmacSha256::update(BufferView data)
{
    if (HMAC_Update(m_ctx, data.data(), data.size()) != 1) {
        throw openssl_error();
    }
}

FixedSizeBuffer<32> HmacSha256::final()
{
    unsigned char md[EVP_MAX_MD_SIZE];
    unsigned int len = 0;

    if (HMAC_Final(m_ctx, md, &len) != 1) {
        throw openssl_error();
    }
    Ensures(len == 32);

    return FixedSizeBuffer<32>(md, md+len);
}


} // namespace TigerMail::Signal::CryptoProviderImpl::OpenSSL
