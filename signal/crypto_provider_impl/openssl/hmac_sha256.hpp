/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_OPENSSL_HMAC_SHA256_HPP
#define TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_OPENSSL_HMAC_SHA256_HPP

#include <openssl/hmac.h>

#include "../hmac_sha256.hpp"


namespace TigerMail::Signal::CryptoProviderImpl::OpenSSL {


class HmacSha256
    : public HmacSha256Interface
{
private:
    HMAC_CTX* m_ctx;

public:
    HmacSha256(BufferView key);
    ~HmacSha256() noexcept override;
    void update(BufferView data) override;
    FixedSizeBuffer<32> final() override;
};


} // namespace TigerMail::Signal::CryptoProviderImpl::OpenSSL

#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_OPENSSL_HMAC_SHA256_HPP
