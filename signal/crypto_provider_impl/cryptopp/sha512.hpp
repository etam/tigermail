/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_CRYPTOPP_SHA512_HPP
#define TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_CRYPTOPP_SHA512_HPP

#include <cryptopp/sha.h>

#include "../sha512.hpp"


namespace TigerMail::Signal::CryptoProviderImpl::CryptoPP {


class Sha512
    : public Sha512Interface
{
private:
    ::CryptoPP::SHA512 m_digest;

public:
    Sha512();
    void update(BufferView data) override;
    FixedSizeBuffer<64> final() override;
};


} // namespace TigerMail::Signal::CryptoProviderImpl::CryptoPP

#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_CRYPTOPP_SHA512_HPP
