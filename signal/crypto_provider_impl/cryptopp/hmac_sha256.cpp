/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "hmac_sha256.hpp"


namespace TigerMail::Signal::CryptoProviderImpl::CryptoPP {


HmacSha256::HmacSha256(BufferView key)
    : m_hmac{key.data(), key.size()}
{}

void HmacSha256::update(BufferView data)
{
    m_hmac.Update(data.data(), data.size());
}

FixedSizeBuffer<32> HmacSha256::final()
{
    static_assert(decltype(m_hmac)::DIGESTSIZE == 32);
    auto output = FixedSizeBuffer<32>{};
    m_hmac.Final(output.data());
    return output;
}


} // namespace TigerMail::Signal::CryptoProviderImpl::CryptoPP
