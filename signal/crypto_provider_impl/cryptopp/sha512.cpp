/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sha512.hpp"


namespace TigerMail::Signal::CryptoProviderImpl::CryptoPP {


Sha512::Sha512() = default;

void Sha512::update(BufferView data)
{
    m_digest.Update(data.data(), data.size());
}

FixedSizeBuffer<64> Sha512::final()
{
    static_assert(decltype(m_digest)::DIGESTSIZE == 64);
    auto output = FixedSizeBuffer<64>{};
    m_digest.Final(output.data());
    return output;
}


} // namespace TigerMail::Signal::CryptoProviderImpl::CryptoPP
