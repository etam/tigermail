/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_CRYPTOPP_HMAC_SHA256_HPP
#define TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_CRYPTOPP_HMAC_SHA256_HPP

#include <cryptopp/hmac.h>
#include <cryptopp/sha.h>

#include "../hmac_sha256.hpp"


namespace TigerMail::Signal::CryptoProviderImpl::CryptoPP {


class HmacSha256
    : public HmacSha256Interface
{
private:
    ::CryptoPP::HMAC<::CryptoPP::SHA256> m_hmac;

public:
    HmacSha256(BufferView key);
    void update(BufferView data) override;
    FixedSizeBuffer<32> final() override;
};


} // namespace TigerMail::Signal::CryptoProviderImpl::CryptoPP

#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_CRYPTOPP_HMAC_SHA256_HPP
