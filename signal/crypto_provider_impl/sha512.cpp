/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sha512.hpp"

#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_USE_CRYPTOPP
#include "cryptopp/sha512.hpp"
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_USE_CRYPTOPP
#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_USE_OPENSSL
#include "openssl/sha512.hpp"
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_USE_OPENSSL


namespace TigerMail::Signal::CryptoProviderImpl {


#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_USE_CRYPTOPP
using Sha512 = CryptoPP::Sha512;
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_USE_CRYPTOPP
#ifdef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_USE_OPENSSL
using Sha512 = OpenSSL::Sha512;
#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_USE_OPENSSL


Sha512Interface* Sha512Interface::create()
{
    return new Sha512();
}


} // namespace TigerMail::Signal::CryptoProviderImpl
