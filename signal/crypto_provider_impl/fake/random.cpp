/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "random.hpp"

#include <random>


namespace TigerMail::Signal::CryptoProviderImpl::Fake {


static std::mt19937 s_engine{7};


void Random::reset()
{
    s_engine.seed(7);
}

void Random::generate(BufferSpan data)
{
    std::uniform_int_distribution<unsigned char> dist;

    for (auto& i : data) {
        i = dist(s_engine);
    }
}


} // namespace TigerMail::Signal::CryptoProviderImpl::Fake
