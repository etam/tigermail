/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_HPP
#define TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_HPP

#include "../../common/buffer_view.hpp"
#include "../../common/fixed_size_buffer.hpp"


namespace TigerMail::Signal::CryptoProviderImpl {


class Sha512Interface
{
public:
    static
    Sha512Interface* create();

    virtual ~Sha512Interface() noexcept = default;

    virtual void update(BufferView data) = 0;
    virtual FixedSizeBuffer<64> final() = 0;
};


} // namespace TigerMail::Signal::CryptoProviderImpl

#endif // TIGERMAIL_SIGNAL_CRYPTOPROVIDERIMPL_SHA512_HPP
