/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "pre_key_store.hpp"

#include <exception>
#include <optional>

#include <boost/log/trivial.hpp>

#include <gsl/gsl_util>

#include "pre_key_store_impl/interface.hpp"
#include "pre_key_store_impl/memory.hpp"
#include "pre_key_store_impl/sqlite.hpp"


namespace TigerMail::Signal {


namespace {

using PreKeyStoreImpl::PreKeyStoreInterface;


int load(signal_buffer** record, std::uint32_t pre_key_id, void* user_data)
{
    const auto* store = static_cast<const PreKeyStoreInterface*>(user_data);
    const auto opt_key = store->load(pre_key_id);
    if (opt_key == std::nullopt) {
        return SG_ERR_INVALID_KEY_ID;
    }
    *record = signal_buffer_create(opt_key->data(), opt_key->size());
    return SG_SUCCESS;
}

int store(std::uint32_t pre_key_id, std::uint8_t* record, size_t record_len, void* user_data)
try {
    auto* store = static_cast<PreKeyStoreInterface*>(user_data);
    store->store(pre_key_id, {record, gsl::narrow<BufferView::index_type>(record_len)});
    return 0;
}
catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(error) << "store_pre_key error: " << e.what();
    return SG_ERR_UNKNOWN;
}

int contains(std::uint32_t pre_key_id, void* user_data)
{
    const auto* store = static_cast<const PreKeyStoreInterface*>(user_data);
    return store->contains(pre_key_id);
}

int remove(std::uint32_t pre_key_id, void* user_data)
try {
    auto* store = static_cast<PreKeyStoreInterface*>(user_data);
    store->remove(pre_key_id);
    return 0;
}
catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(error) << "remove_pre_key error: " << e.what();
    return SG_ERR_UNKNOWN;
}

void destroy(void* user_data)
{
    auto* store = static_cast<PreKeyStoreInterface*>(user_data);
    delete store;
}

signal_protocol_pre_key_store makePreKeyStore(void* user_data)
{
    return {
        .load_pre_key = load,
        .store_pre_key = store,
        .contains_pre_key = contains,
        .remove_pre_key = remove,
        .destroy_func = destroy,
        .user_data = user_data,
    };
}

signal_protocol_signed_pre_key_store makeSignedPreKeyStore(void* user_data)
{
    return {
        .load_signed_pre_key = load,
        .store_signed_pre_key = store,
        .contains_signed_pre_key = contains,
        .remove_signed_pre_key = remove,
        .destroy_func = destroy,
        .user_data = user_data,
    };
}


} // namespace


signal_protocol_pre_key_store makeMemoryPreKeyStore()
{
    return makePreKeyStore(new PreKeyStoreImpl::MemoryPreKeyStore{});
}

signal_protocol_signed_pre_key_store makeMemorySignedPreKeyStore()
{
    return makeSignedPreKeyStore(new PreKeyStoreImpl::MemoryPreKeyStore{});
}


signal_protocol_pre_key_store makeSqlitePreKeyStore(gsl::not_null<sqlite::database*> database)
{
    return makePreKeyStore(new PreKeyStoreImpl::SqlitePreKeyStore{database, false});
}

signal_protocol_signed_pre_key_store makeSqliteSignedPreKeyStore(gsl::not_null<sqlite::database*> database)
{
    return makeSignedPreKeyStore(new PreKeyStoreImpl::SqlitePreKeyStore{database, true});
}


} // namespace TigerMail::Signal
