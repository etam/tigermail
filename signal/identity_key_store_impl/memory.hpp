/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_IDENTITY_KEY_STORE_IMPL_MEMORY_HPP
#define TIGERMAIL_SIGNAL_IDENTITY_KEY_STORE_IMPL_MEMORY_HPP

#include <string>
#include <unordered_map>

#include <boost/container_hash/hash.hpp>

struct signal_context;

#include "../../common/buffer.hpp"
#include "../../common/buffer_view.hpp"

#include "../common.hpp"
#include "../signal_buffer.hpp"

#include "interface.hpp"


namespace TigerMail::Signal::IdentityKeyStoreImpl {


class MemoryIdentityKeyStore
    : public IdentityKeyStoreInterface
{
public:
    using AddrKey = std::pair<std::string, DeviceIdT>;

private:
    SignalBufferRef m_priv_key;
    SignalBufferRef m_pub_key;
    RegistrationIdT m_reg_id;

    std::unordered_map<AddrKey, Buffer, boost::hash<AddrKey>> m_trusted;

public:
    explicit
    MemoryIdentityKeyStore(signal_context* context);
    ~MemoryIdentityKeyStore() noexcept override = default;

    KeyPair get_identity_key_pair() const override;
    RegistrationIdT get_local_registration_id() const override;
    void save(const Address& address, BufferView key) override;
    bool is_trusted(const Address& address, BufferView key) const override;
};


} // namespace TigerMail::Signal::IdentityKeyStoreImpl

#endif // TIGERMAIL_SIGNAL_IDENTITY_KEY_STORE_IMPL_MEMORY_HPP
