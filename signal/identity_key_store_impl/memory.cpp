/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "memory.hpp"

#include "../signalpp/curve.hpp"
#include "../signalpp/key_helper.hpp"


namespace TigerMail::Signal::IdentityKeyStoreImpl {


namespace {


MemoryIdentityKeyStore::AddrKey toAddrKey(const Address& address)
{
    return {std::string{address.name()}, address.device_id()};
}


} // namespace


MemoryIdentityKeyStore::MemoryIdentityKeyStore(signal_context* context)
    : m_priv_key{}
    , m_pub_key{}
    , m_reg_id{signal_protocol_key_helper_generate_registration_id(false, context)}
    , m_trusted{}
{
    const auto key_pair = curve_generate_key_pair(context);

    const auto* pub_key = ec_key_pair_get_public(key_pair.get());
    const auto* priv_key = ec_key_pair_get_private(key_pair.get());

    m_priv_key = ec_private_key_serialize(priv_key);
    m_pub_key = ec_public_key_serialize(pub_key);
}

auto MemoryIdentityKeyStore::get_identity_key_pair() const -> KeyPair
{
    return {&m_priv_key, &m_pub_key};
}

RegistrationIdT MemoryIdentityKeyStore::get_local_registration_id() const
{
    return m_reg_id;
}

void MemoryIdentityKeyStore::save(const Address& address, BufferView key)
{
    const auto addr_key = toAddrKey(address);
    if (key.data() == nullptr) {
        m_trusted.erase(addr_key);
        return;
    }
    m_trusted.insert_or_assign(addr_key, Buffer{key.begin(), key.end()});
}

bool MemoryIdentityKeyStore::is_trusted(const Address& address, BufferView key) const
{
    const auto addr_key = toAddrKey(address);
    const auto it = m_trusted.find(addr_key);
    if (it == m_trusted.end()) {
        return true;
    }
    return it->second == key;
}


} // namespace TigerMail::Signal::IdentityKeyStoreImpl
