/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sqlite.hpp"

#include <optional>
#include <stdexcept>
#include <string>
#include <tuple>

#include <boost/log/trivial.hpp>

#include <sqlite_modern_cpp.h>

#include "../../common/hex.hpp"

#include "../signalpp/curve.hpp"
#include "../signalpp/key_helper.hpp"


namespace TigerMail::Signal::IdentityKeyStoreImpl {


SqliteIdentityKeyStore::SqliteIdentityKeyStore(gsl::not_null<sqlite::database*> database, signal_context* context)
    : m_database{database}
{
    *m_database <<
        "create table if not exists "
        "signal_identity ( "
        "  priv_key blob not null, "
        "  reg_id int not null "
        ");";
    *m_database <<
        "create table if not exists "
        "signal_trusted ( "
        "  name text not null, "
        "  device_id int not null, "
        "  key blob not null, "
        "  primary key (name, device_id) "
        ");";

    auto opt_identity = std::optional<std::tuple<Buffer, RegistrationIdT>>{};
    *m_database << "select priv_key, reg_id from signal_identity;"
                >> [&](Buffer priv_key, RegistrationIdT reg_id) {
                       if (opt_identity) {
                           throw std::runtime_error{"got one than one identity from database"};
                       }
                       opt_identity = std::make_tuple(std::move(priv_key), reg_id);
                   };

    if (opt_identity) {
        auto& serialized_priv_key = std::get<0>(*opt_identity);
        m_priv_key = SignalBufferRef{::signal_buffer_create(serialized_priv_key.data(), serialized_priv_key.size())};
        const auto deserialized_priv_key = curve_decode_private_point(m_priv_key, context);
        const auto pub_key = curve_generate_public_key(deserialized_priv_key.get());
        m_pub_key = ec_public_key_serialize(pub_key.get());

        m_reg_id = std::get<1>(*opt_identity);

        BOOST_LOG_TRIVIAL(info) << "Read identity from database pub_key=" << bin2hex(BufferView(m_pub_key)) << ", reg_id=" << m_reg_id;
    }
    else {
        BOOST_LOG_TRIVIAL(info) << "Creating new identity";
        const auto key_pair = curve_generate_key_pair(context);

        const auto* pub_key = ec_key_pair_get_public(key_pair.get());
        const auto* priv_key = ec_key_pair_get_private(key_pair.get());

        m_priv_key = ec_private_key_serialize(priv_key);
        m_pub_key = ec_public_key_serialize(pub_key);

        m_reg_id = signal_protocol_key_helper_generate_registration_id(false, context);

        *m_database << "insert into signal_identity (priv_key, reg_id) values (?, ?);"
                   << Buffer(m_priv_key.begin(), m_priv_key.end())
                   << m_reg_id;

        BOOST_LOG_TRIVIAL(info) << "Created identity pub_key=" << bin2hex(BufferView(m_pub_key)) << ", reg_id=" << m_reg_id;
    }
}

auto SqliteIdentityKeyStore::get_identity_key_pair() const -> KeyPair
{
    return {&m_priv_key, &m_pub_key};
}

RegistrationIdT SqliteIdentityKeyStore::get_local_registration_id() const
{
    return m_reg_id;
}

void SqliteIdentityKeyStore::save(const Address& address, BufferView key)
{
    if (key.data() == nullptr) {
        BOOST_LOG_TRIVIAL(debug) << "Deleting trusted identity name=\"" << address.name() << "\", dev_id=" << address.device_id();
        *m_database <<
            "delete "
            "from signal_trusted "
            "where "
            "  name = ? "
            "  and device_id = ?;"
                    << std::string{address.name()} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                    << address.device_id();
        return;
    }
    BOOST_LOG_TRIVIAL(debug) << "Saving trusted identity name=\"" << address.name() << "\", dev_id=" << address.device_id() << ", "
                             << "key=" << bin2hex(key);
    *m_database <<
        "insert "
        "into signal_trusted (name, device_id, key) "
        "values (?, ?, ?) "
        "on conflict (name, device_id) do update set key=excluded.key;"
                << std::string{address.name()} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                << address.device_id()
                << Buffer(key.begin(), key.end()); // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
}

bool SqliteIdentityKeyStore::is_trusted(const Address& address, BufferView key) const
{
    BOOST_LOG_TRIVIAL(debug) << "Checking if identity is trusted name=\"" << address.name() << "\", dev_id=" << address.device_id() << ", "
                             << "key=" << bin2hex(key);
    auto saved_key = std::optional<Buffer>{};
    *m_database <<
        "select key "
        "from signal_trusted "
        "where "
        "  name = ? "
        "  and device_id = ?;"
                << std::string{address.name()} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                << address.device_id()
                >> [&](Buffer key) {
                       saved_key = std::move(key);
                   };
    if (!saved_key) {
        BOOST_LOG_TRIVIAL(debug) << "Not found in database, so trusted";
        return true;
    }

    const auto is_trusted = (*saved_key == key);
    BOOST_LOG_TRIVIAL(debug) << "It's " << (is_trusted ? "" : "not ") << "trusted";
    return is_trusted;
}


} // namespace TigerMail::Signal::IdentityKeyStoreImpl
