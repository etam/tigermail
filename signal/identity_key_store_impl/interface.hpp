/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_IDENTITY_KEY_STORE_IMPL_INTERFACE_HPP
#define TIGERMAIL_SIGNAL_IDENTITY_KEY_STORE_IMPL_INTERFACE_HPP

#include <optional>

#include "../../common/buffer_view.hpp"

#include "../address.hpp"
#include "../common.hpp"

namespace TigerMail::Signal {
class SignalBufferRef;
} // namespace TigerMail::Signal


namespace TigerMail::Signal::IdentityKeyStoreImpl {


class IdentityKeyStoreInterface
{
public:
    struct KeyPair
    {
        const SignalBufferRef* priv_key;
        const SignalBufferRef* pub_key;
    };

    virtual ~IdentityKeyStoreInterface() noexcept = default;

    virtual KeyPair get_identity_key_pair() const = 0;
    virtual RegistrationIdT get_local_registration_id() const = 0;
    virtual void save(const Address& address, BufferView key) = 0;
    virtual bool is_trusted(const Address& address, BufferView key) const = 0;
};


} // namespace TigerMail::Signal::IdentityKeyStoreImpl

#endif // TIGERMAIL_SIGNAL_IDENTITY_KEY_STORE_IMPL_INTERFACE_HPP
