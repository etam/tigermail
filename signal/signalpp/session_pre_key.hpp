/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_SIGNALPP_SESSION_PRE_KEY_HPP
#define TIGERMAIL_SIGNAL_SIGNALPP_SESSION_PRE_KEY_HPP

#include <session_pre_key.h>

#include "../../common/buffer_view.hpp"

#include "../common.hpp"


namespace TigerMail::Signal {


Ref<session_pre_key> session_pre_key_create(uint32_t id, ec_key_pair* key_pair);

Ref<session_pre_key_bundle> session_pre_key_bundle_create(
        uint32_t registration_id, int device_id, uint32_t pre_key_id,
        ec_public_key* pre_key_public,
        uint32_t signed_pre_key_id, ec_public_key* signed_pre_key_public,
        BufferView signed_pre_key_signature,
        ec_public_key* identity_key);


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_SIGNALPP_SESSION_PRE_KEY_HPP
