/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "hkdf.hpp"


namespace TigerMail::Signal {


Ref<hkdf_context> hkdf_create(int message_version, signal_context* context)
{
    auto hkdf = Ref<hkdf_context>{};
    if (const auto result = ::hkdf_create(hkdf.get2(), message_version, context); result < 0) {
        throw SignalError{"failed creating khdf", result};
    }
    return hkdf;
}


} // namespace TigerMail::Signal
