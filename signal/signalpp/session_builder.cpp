/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "session_builder.hpp"

#include "../common.hpp"


namespace TigerMail::Signal {


std::unique_ptr<session_builder, void(*)(session_builder*)>
session_builder_create(
        signal_protocol_store_context* store, const signal_protocol_address* remote_address,
        signal_context* context)
{
    session_builder* builder;
    if (const auto result = ::session_builder_create(&builder, store, remote_address, context); result < 0) {
        throw SignalError{"failed creating session builder", result};
    }
    return std::unique_ptr<session_builder, void(*)(session_builder*)>(builder, &::session_builder_free);
}

void session_builder_process_pre_key_bundle(session_builder* builder, session_pre_key_bundle* bundle)
{
    if (const auto result = ::session_builder_process_pre_key_bundle(builder, bundle); result < 0) {
        throw SignalError{"failed processing signal key bundle", result};
    }
}

std::optional<uint32_t> session_builder_process_pre_key_signal_message(
    session_builder* builder, session_record* record, pre_key_signal_message* message)
{
    auto unsigned_pre_key_id = uint32_t{0};
    const auto has_unsigned_pre_key_id = ::session_builder_process_pre_key_signal_message(
        builder, record, message, &unsigned_pre_key_id);
    if (has_unsigned_pre_key_id < 0) {
        throw SignalError{"failed processing pre key message", has_unsigned_pre_key_id};
    }
    if (!has_unsigned_pre_key_id) {
        return std::nullopt;
    }
    return unsigned_pre_key_id;
}


} // namespace TigerMail::Signal
