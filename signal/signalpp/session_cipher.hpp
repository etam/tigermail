/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_SIGNALPP_SESSION_CIPHER_HPP
#define TIGERMAIL_SIGNAL_SIGNALPP_SESSION_CIPHER_HPP

#include <memory>

#include <session_cipher.h>

#include "../../common/buffer_view.hpp"

#include "../common.hpp"
#include "../signal_buffer.hpp"


namespace TigerMail::Signal {


std::unique_ptr<session_cipher, void(*)(session_cipher*)>
session_cipher_create(
        signal_protocol_store_context* store, const signal_protocol_address* remote_address,
        signal_context* context);

Ref<ciphertext_message> session_cipher_encrypt(session_cipher* cipher, BufferView message);

SignalBufferRef session_cipher_decrypt_pre_key_signal_message(
    session_cipher *cipher, pre_key_signal_message *ciphertext);

SignalBufferRef session_cipher_decrypt_signal_message(
    session_cipher* cipher, signal_message* ciphertext);

session_builder* session_cipher_get_builder(session_cipher *cipher);


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_SIGNALPP_SESSION_CIPHER_HPP
