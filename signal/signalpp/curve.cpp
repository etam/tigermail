/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "curve.hpp"

#include <curve.h>


namespace TigerMail::Signal {


Ref<ec_public_key> curve_decode_point(BufferView key, signal_context* context)
{
    auto pub_key = Ref<ec_public_key>{};
    if (const auto result = ::curve_decode_point(pub_key.get2(), key.data(), key.size(), context); result < 0) {
        throw SignalError{"failed deserializing public key", result};
    }
    return pub_key;
}

SignalBufferRef ec_public_key_serialize(const ec_public_key* key)
{
    auto serialized_pub_key = SignalBufferRef{};
    if (const auto result = ::ec_public_key_serialize(serialized_pub_key.get2(), key); result < 0) {
        throw SignalError{"failed serializing public key", result};
    }
    return serialized_pub_key;
}

Ref<ec_private_key> curve_decode_private_point(BufferView key, signal_context* context)
{
    auto priv_key = Ref<ec_private_key>{};
    if (const auto result = ::curve_decode_private_point(priv_key.get2(), key.data(), key.size(), context); result < 0) {
        throw SignalError{"failed deserializing private key", result};
    }
    return priv_key;
}

SignalBufferRef ec_private_key_serialize(const ec_private_key *key)
{
    auto serialized_priv_key = SignalBufferRef{};
    if (const auto result = ::ec_private_key_serialize(serialized_priv_key.get2(), key); result < 0) {
        throw SignalError{"failed serializing private key", result};
    }
    return serialized_priv_key;
}

Ref<ec_public_key> curve_generate_public_key(const ec_private_key* private_key)
{
    auto pub_key = Ref<ec_public_key>{};
    if (const auto result = ::curve_generate_public_key(pub_key.get2(), private_key); result < 0) {
        throw SignalError{"failed generating public key", result};
    }
    return pub_key;
}

Ref<ec_key_pair> curve_generate_key_pair(signal_context* context)
{
    auto key_pair = Ref<ec_key_pair>{};
    if (const auto result = ::curve_generate_key_pair(context, key_pair.get2()); result < 0) {
        throw SignalError{"failed creating key pair", result};
    }
    return key_pair;
}

MallocBuffer curve_calculate_agreement(const ec_public_key* public_key, const ec_private_key* private_key)
{
    auto shared_key = MallocBuffer{};
    // shared_key.size is unsigned, so assigning it directly would break the check
    const auto shared_key_size = ::curve_calculate_agreement(&shared_key.data, public_key, private_key);
    if (shared_key_size < 0) {
        throw SignalError{"failed calculating agreement", shared_key_size};
    }
    shared_key.size = shared_key_size;
    return shared_key;
}


} // namespace TigerMail::Signal
