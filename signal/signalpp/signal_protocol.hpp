/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_SIGNALPP_SIGNAL_PROTOCOL_HPP
#define TIGERMAIL_SIGNAL_SIGNALPP_SIGNAL_PROTOCOL_HPP

#include <signal_protocol.h>

#include "../common.hpp"


namespace TigerMail::Signal {


Ref<session_record> signal_protocol_session_load_session(signal_protocol_store_context* context, const signal_protocol_address* address);
void signal_protocol_session_store_session(signal_protocol_store_context* context, const signal_protocol_address* address, session_record* record);
bool signal_protocol_session_contains_session(signal_protocol_store_context *context, const signal_protocol_address *address);

Ref<session_pre_key> signal_protocol_pre_key_load_key(signal_protocol_store_context* context, uint32_t pre_key_id);
void signal_protocol_pre_key_store_key(signal_protocol_store_context* context, session_pre_key* pre_key);
bool signal_protocol_pre_key_contains_key(signal_protocol_store_context* context, uint32_t pre_key_id);
void signal_protocol_pre_key_remove_key(signal_protocol_store_context* context, uint32_t pre_key_id);

Ref<session_signed_pre_key> signal_protocol_signed_pre_key_load_key(signal_protocol_store_context* context, uint32_t signed_pre_key_id);
void signal_protocol_signed_pre_key_store_key(signal_protocol_store_context* context, session_signed_pre_key* pre_key);
bool signal_protocol_signed_pre_key_contains_key(signal_protocol_store_context *context, uint32_t signed_pre_key_id);
void signal_protocol_signed_pre_key_remove_key(signal_protocol_store_context *context, uint32_t signed_pre_key_id);

Ref<ratchet_identity_key_pair> signal_protocol_identity_get_key_pair(signal_protocol_store_context* context);
uint32_t signal_protocol_identity_get_local_registration_id(signal_protocol_store_context* context);


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_SIGNALPP_SIGNAL_PROTOCOL_HPP
