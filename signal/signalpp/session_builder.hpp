/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_SIGNALPP_SESSION_BUILDER_HPP
#define TIGERMAIL_SIGNAL_SIGNALPP_SESSION_BUILDER_HPP

#include <memory>
#include <optional>

#include <session_builder.h>
// FIXME!!! THIS IS AN UGLY HACK
// defined in session_builder_internal.h
extern "C" {
int session_builder_process_pre_key_signal_message(session_builder *builder,
        session_record *record, pre_key_signal_message *message, uint32_t *unsigned_pre_key_id);
} // extern "C"


namespace TigerMail::Signal {


std::unique_ptr<session_builder, void(*)(session_builder*)>
session_builder_create(
        signal_protocol_store_context* store, const signal_protocol_address* remote_address,
        signal_context* context);

void session_builder_process_pre_key_bundle(session_builder* builder, session_pre_key_bundle* bundle);

std::optional<uint32_t> session_builder_process_pre_key_signal_message(
    session_builder* builder, session_record* record, pre_key_signal_message* message);


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_SIGNALPP_SESSION_BUILDER_HPP
