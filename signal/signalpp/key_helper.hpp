/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_SIGNALPP_KEY_HELPER_HPP
#define TIGERMAIL_SIGNAL_SIGNALPP_KEY_HELPER_HPP

#include <key_helper.h>

#include "../common.hpp"


namespace TigerMail::Signal {


uint32_t signal_protocol_key_helper_generate_registration_id(bool extended_range, signal_context* context);

Ref<session_signed_pre_key> signal_protocol_key_helper_generate_signed_pre_key(
    const ratchet_identity_key_pair* identity_key_pair,
    uint32_t signed_pre_key_id,
    uint64_t timestamp,
    signal_context* context);


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_SIGNALPP_KEY_HELPER_HPP
