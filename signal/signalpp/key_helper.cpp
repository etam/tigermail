/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "key_helper.hpp"


namespace TigerMail::Signal {


uint32_t signal_protocol_key_helper_generate_registration_id(bool extended_range, signal_context* context)
{
    auto registration_id = uint32_t{0};
    if (const auto result = ::signal_protocol_key_helper_generate_registration_id(
                   &registration_id, extended_range, context);
                   result < 0) {
        throw SignalError{"failed generating registration id", result};
    }
    return registration_id;
}

Ref<session_signed_pre_key> signal_protocol_key_helper_generate_signed_pre_key(
    const ratchet_identity_key_pair* identity_key_pair,
    uint32_t signed_pre_key_id,
    uint64_t timestamp,
    signal_context* context)
{
    auto signed_pre_key = Ref<session_signed_pre_key>{};
    if (const auto result = ::signal_protocol_key_helper_generate_signed_pre_key(
                   signed_pre_key.get2(), identity_key_pair, signed_pre_key_id, timestamp, context);
                   result < 0) {
        throw SignalError{"failed generating signed pre key", result};
    }
    return signed_pre_key;
}


} // namespace TigerMail::Signal
