/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_SIGNALPP_RATCHET_HPP
#define TIGERMAIL_SIGNAL_SIGNALPP_RATCHET_HPP

#include <utility>

#include <ratchet.h>

#include "../../common/buffer_view.hpp"

#include "../common.hpp"
#include "../signal_buffer.hpp"


namespace TigerMail::Signal {


Ref<ratchet_chain_key> ratchet_chain_key_create(
    hkdf_context* kdf, BufferView key, uint32_t index, signal_context* context);

SignalBufferRef ratchet_chain_key_get_key(const ratchet_chain_key *chain_key);

ratchet_message_keys ratchet_chain_key_get_message_keys(ratchet_chain_key* chain_key);

Ref<ratchet_chain_key> ratchet_chain_key_create_next(const ratchet_chain_key *chain_key);


std::pair<Ref<ratchet_root_key>, Ref<ratchet_chain_key>>
ratchet_root_key_create_chain(ratchet_root_key *root_key, ec_public_key *their_ratchet_key,
                              ec_private_key *our_ratchet_key_private);


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_SIGNALPP_RATCHET_HPP

