/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_SIGNALPP_PROTOCOL_HPP
#define TIGERMAIL_SIGNAL_SIGNALPP_PROTOCOL_HPP

#include <protocol.h>

#include "../../common/buffer_view.hpp"
#include "../common.hpp"


namespace TigerMail::Signal {


Ref<signal_message> signal_message_deserialize(
    BufferView data, signal_context* context);

Ref<pre_key_signal_message> pre_key_signal_message_deserialize(
    BufferView data, signal_context* context);


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_SIGNALPP_PROTOCOL_HPP
