/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "session_cipher.hpp"

#include "../common.hpp"


namespace TigerMail::Signal {


std::unique_ptr<session_cipher, void(*)(session_cipher*)>
session_cipher_create(
        signal_protocol_store_context* store, const signal_protocol_address* remote_address,
        signal_context* context)
{
    session_cipher* cipher;
    if (const auto result = ::session_cipher_create(&cipher, store, remote_address, context); result < 0) {
        throw SignalError{"failed creating session cipher", result};
    }
    return std::unique_ptr<session_cipher, void(*)(session_cipher*)>(cipher, &::session_cipher_free);
}

Ref<ciphertext_message> session_cipher_encrypt(session_cipher* cipher, BufferView message)
{
    auto encrypted_message = Ref<ciphertext_message>{};
    if (const auto result = ::session_cipher_encrypt(
                   cipher, message.data(), message.size(), encrypted_message.get2());
                   result < 0) {
        throw SignalError{"failed encrypting message", result};
    }
    return encrypted_message;
}

SignalBufferRef session_cipher_decrypt_pre_key_signal_message(
    session_cipher *cipher, pre_key_signal_message *ciphertext)
{
    auto plaintext = SignalBufferRef{};
    if (const auto result = ::session_cipher_decrypt_pre_key_signal_message(
                   cipher, ciphertext, nullptr, plaintext.get2());
                   result < 0) {
        throw SignalError{"failed decrypting pre key signal message", result};
    }
    return plaintext;
}

SignalBufferRef session_cipher_decrypt_signal_message(
    session_cipher* cipher, signal_message* ciphertext)
{
    auto plaintext = SignalBufferRef{};
    if (const auto result = ::session_cipher_decrypt_signal_message(
                   cipher, ciphertext, nullptr, plaintext.get2());
                   result < 0) {
        throw SignalError{"failed decrypting message", result};
    }
    return plaintext;
}

/*
  FIXME!!! THIS IS AN UGLY HACK
  but it works without patching signal library
 */
session_builder* session_cipher_get_builder(session_cipher *cipher)
{
    return reinterpret_cast<session_builder**>(cipher)[2];
}


} // namespace TigerMail::Signal
