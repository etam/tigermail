/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ratchet.hpp"


namespace TigerMail::Signal {


Ref<ratchet_chain_key> ratchet_chain_key_create(
    hkdf_context* kdf, BufferView key, uint32_t index, signal_context* context)
{
    auto chain_key = Ref<ratchet_chain_key>{};
    if (const auto result = ::ratchet_chain_key_create(
                   chain_key.get2(), kdf, key.data(), key.size(), index, context);
                   result < 0) {
        throw SignalError{"failed creating ratchet chain", result};
    }
    return chain_key;
}

SignalBufferRef ratchet_chain_key_get_key(const ratchet_chain_key *chain_key)
{
    auto buffer = SignalBufferRef{};
    if (const auto result = ::ratchet_chain_key_get_key(chain_key, buffer.get2()); result < 0) {
        throw SignalError{"failed getting chain key", result};
    }
    return buffer;
}

ratchet_message_keys ratchet_chain_key_get_message_keys(ratchet_chain_key* chain_key)
{
    auto keys = ratchet_message_keys{};
    if (const auto result = ::ratchet_chain_key_get_message_keys(chain_key, &keys); result < 0) {
        throw SignalError{"failed getting message keys", result};
    }
    return keys;
}

Ref<ratchet_chain_key> ratchet_chain_key_create_next(const ratchet_chain_key *chain_key)
{
    auto next_chain_key = Ref<ratchet_chain_key>{};
    if (const auto result = ::ratchet_chain_key_create_next(chain_key, next_chain_key.get2()); result < 0) {
        throw SignalError{"failed getting next ratchet chain key", result};
    }
    return next_chain_key;
}

std::pair<Ref<ratchet_root_key>, Ref<ratchet_chain_key>>
ratchet_root_key_create_chain(ratchet_root_key *root_key, ec_public_key *their_ratchet_key,
                              ec_private_key *our_ratchet_key_private)
{
    auto new_root_key = Signal::Ref<ratchet_root_key>{};
    auto new_chain_key = Signal::Ref<ratchet_chain_key>{};
    if (const int result = ::ratchet_root_key_create_chain(
            root_key, new_root_key.get2(), new_chain_key.get2(),
            their_ratchet_key, our_ratchet_key_private);
            result < 0) {
        throw SignalError{"failed creating ratchet root and chain keys", result};
    }
    return {new_root_key, new_chain_key};
}


} // namespace TigerMail::Signal
