/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "session_pre_key.hpp"


namespace TigerMail::Signal {


Ref<session_pre_key> session_pre_key_create(uint32_t id, ec_key_pair* key_pair)
{
    auto pre_key = Ref<session_pre_key>{};
    if (const auto result = ::session_pre_key_create(pre_key.get2(), id, key_pair); result < 0) {
        throw SignalError{"failed creating pre key", result};
    }
    return pre_key;
}

Ref<session_pre_key_bundle> session_pre_key_bundle_create(
        uint32_t registration_id, int device_id, uint32_t pre_key_id,
        ec_public_key* pre_key_public,
        uint32_t signed_pre_key_id, ec_public_key* signed_pre_key_public,
        BufferView signed_pre_key_signature,
        ec_public_key* identity_key)
{
    auto signal_bundle = Ref<session_pre_key_bundle>{};
    if (const auto result = ::session_pre_key_bundle_create(
            signal_bundle.get2(),
            registration_id,
            device_id,
            pre_key_id,
            pre_key_public,
            signed_pre_key_id,
            signed_pre_key_public,
            signed_pre_key_signature.data(),
            signed_pre_key_signature.size(),
            identity_key);
            result < 0) {
        throw SignalError{"failed creating signal key bundle", result};
    }
    return signal_bundle;
}


} // namespace TigerMail::Signal
