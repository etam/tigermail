/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_SIGNALPP_CURVE_HPP
#define TIGERMAIL_SIGNAL_SIGNALPP_CURVE_HPP

#include <curve.h>

#include "../../common/buffer_view.hpp"

#include "../common.hpp"
#include "../signal_buffer.hpp"


namespace TigerMail::Signal {


Ref<ec_public_key> curve_decode_point(BufferView key, signal_context* context);
SignalBufferRef ec_public_key_serialize(const ec_public_key* key);

Ref<ec_private_key> curve_decode_private_point(BufferView key, signal_context* context);
SignalBufferRef ec_private_key_serialize(const ec_private_key *key);

Ref<ec_public_key> curve_generate_public_key(const ec_private_key* private_key);

Ref<ec_key_pair> curve_generate_key_pair(signal_context* context);

MallocBuffer curve_calculate_agreement(const ec_public_key* public_key, const ec_private_key* private_key);


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_SIGNALPP_CURVE_HPP
