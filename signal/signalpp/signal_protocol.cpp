/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "signal_protocol.hpp"


namespace TigerMail::Signal {


Ref<session_record> signal_protocol_session_load_session(signal_protocol_store_context* context, const signal_protocol_address* address)
{
    auto record = Ref<session_record>{};
    if (const auto result = ::signal_protocol_session_load_session(context, record.get2(), address); result < 0) {
        throw SignalError{"failed getting session record", result};
    }
    return record;
}

void signal_protocol_session_store_session(signal_protocol_store_context* context, const signal_protocol_address* address, session_record* record)
{
    if (const auto result = ::signal_protocol_session_store_session(context, address, record); result < 0) {
        throw SignalError{"failed storing session", result};
    }
}

bool signal_protocol_session_contains_session(signal_protocol_store_context *context, const signal_protocol_address *address)
{
    return ::signal_protocol_session_contains_session(context, address);
}



Ref<session_pre_key> signal_protocol_pre_key_load_key(signal_protocol_store_context* context, uint32_t pre_key_id)
{
    auto pre_key = Ref<session_pre_key>{};
    if (const auto result = ::signal_protocol_pre_key_load_key(context, pre_key.get2(), pre_key_id); result < 0) {
        throw SignalError{"failed getting pre key", result};
    }
    return pre_key;
}

void signal_protocol_pre_key_store_key(signal_protocol_store_context* context, session_pre_key* pre_key)
{
    if (const auto result = ::signal_protocol_pre_key_store_key(context, pre_key); result < 0) {
        throw SignalError{"failed storing pre key", result};
    }
}

bool signal_protocol_pre_key_contains_key(signal_protocol_store_context* context, uint32_t pre_key_id)
{
    return !!::signal_protocol_pre_key_contains_key(context, pre_key_id);
}


void signal_protocol_pre_key_remove_key(signal_protocol_store_context* context, uint32_t pre_key_id)
{
    if (const auto result = ::signal_protocol_pre_key_remove_key(context, pre_key_id); result < 0) {
        throw SignalError{"failed removing pre key", result};
    }
}


Ref<session_signed_pre_key> signal_protocol_signed_pre_key_load_key(signal_protocol_store_context* context, uint32_t signed_pre_key_id)
{
    auto signed_pre_key = Ref<session_signed_pre_key>{};
    if (const auto result = ::signal_protocol_signed_pre_key_load_key(context, signed_pre_key.get2(), signed_pre_key_id); result < 0) {
        throw SignalError{"failed getting signed pre key", result};
    }
    return signed_pre_key;
}

void signal_protocol_signed_pre_key_store_key(signal_protocol_store_context *context, session_signed_pre_key *pre_key)
{
    if (const auto result = ::signal_protocol_signed_pre_key_store_key(context, pre_key); result < 0) {
        throw SignalError{"failed storing signed pre key", result};
    }
}

bool signal_protocol_signed_pre_key_contains_key(signal_protocol_store_context *context, uint32_t signed_pre_key_id)
{
    return !!::signal_protocol_signed_pre_key_contains_key(context, signed_pre_key_id);
}

void signal_protocol_signed_pre_key_remove_key(signal_protocol_store_context *context, uint32_t signed_pre_key_id)
{
    if (const auto result = ::signal_protocol_signed_pre_key_remove_key(context, signed_pre_key_id); result < 0) {
        throw SignalError{"failed removing signed pre key", result};
    }
}


Ref<ratchet_identity_key_pair> signal_protocol_identity_get_key_pair(signal_protocol_store_context* context)
{
    auto key_pair = Ref<ratchet_identity_key_pair>{};
    if (const auto result = ::signal_protocol_identity_get_key_pair(context, key_pair.get2()); result < 0) {
        throw SignalError{"failed getting identity key pair", result};
    }
    return key_pair;
}

uint32_t signal_protocol_identity_get_local_registration_id(signal_protocol_store_context* context)
{
    auto registration_id = uint32_t{0};
    if (const auto result = ::signal_protocol_identity_get_local_registration_id(context, &registration_id); result < 0) {
        throw SignalError{"failed getting registration id", result};
    }
    return registration_id;
}


} // namespace TigerMail::Signal
