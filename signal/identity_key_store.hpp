/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_IDENTITY_KEY_STORE_HPP
#define TIGERMAIL_SIGNAL_IDENTITY_KEY_STORE_HPP

#include <gsl/pointers>

#include <signal_protocol.h>

namespace sqlite {
class database;
} // namespace sqlite


namespace TigerMail::Signal {


signal_protocol_identity_key_store makeMemoryIdentityKeyStore(signal_context* context);
signal_protocol_identity_key_store makeSqliteIdentityKeyStore(gsl::not_null<sqlite::database*> database, signal_context* context);


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_IDENTITY_KEY_STORE_HPP
