/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "context.hpp"

#include <functional>
#include <iostream>
#include <stdexcept>

#include <boost/log/trivial.hpp>

#include <signal_protocol.h>

#include "../common/on_scope_exit.hpp"

#include "identity_key_store.hpp"
#include "pre_key_store.hpp"
#include "session_store.hpp"


namespace TigerMail::Signal {


namespace {


void lock(void* user_data_)
{
    auto* user_data = static_cast<Context::UserData*>(user_data_);
    user_data->mutex.lock();
}

void unlock(void* user_data_)
{
    auto* user_data = static_cast<Context::UserData*>(user_data_);
    user_data->mutex.unlock();
}


boost::log::trivial::severity_level signal_to_boost_log_level(int signal_log_level)
{
    switch (signal_log_level) {
    case SG_LOG_ERROR:   return boost::log::trivial::error;
    case SG_LOG_WARNING: return boost::log::trivial::warning;
    case SG_LOG_NOTICE:
    case SG_LOG_INFO:    return boost::log::trivial::info;
    case SG_LOG_DEBUG:   return boost::log::trivial::debug;
    default: std::abort();
    }
}

void log(int level, const char* message, size_t /*len*/, void* /*user_data_*/)
{
    // This is a copy-paste of BOOST_LOG_TRIVIAL, with different severity source
    BOOST_LOG_STREAM_WITH_PARAMS(
        ::boost::log::trivial::logger::get(),
        (::boost::log::keywords::severity = signal_to_boost_log_level(level)))
        << "Signal: " << message;
}


} // namespace



Context::Context_::Context_(UserData* user_data)
    : m_context{nullptr}
{
    signal_context_create(&m_context, user_data);
}

Context::Context_::~Context_() noexcept
{
    signal_context_destroy(m_context);
}


Context::Context()
    : m_user_data{}
    , m_context{&m_user_data}
    , m_crypto_provider{}
{
    if (signal_context_set_log_function(m_context.get(), log) < 0) {
        throw SignalError{"failed setting log functon"};
    }

    if (signal_context_set_locking_functions(m_context.get(), lock, unlock) < 0) {
        throw SignalError{"failed setting locking functions"};
    }

    if (signal_context_set_crypto_provider(m_context.get(), m_crypto_provider.get()) < 0) {
        throw SignalError{"failed setting crypto provider"};
    }
}


StoreContext::StoreContext(signal_context* context)
    : m_store_context{nullptr}
    , m_identity_key_store{nullptr}
    , m_pre_key_store{nullptr}
    , m_signed_pre_key_store{nullptr}
    , m_session_store{nullptr}
{
    if (signal_protocol_store_context_create(&m_store_context, context) < 0) {
        throw SignalError{"failed creating store context"};
    }
}

StoreContext::~StoreContext() noexcept
{
    signal_protocol_store_context_destroy(m_store_context);
}


namespace {


void createStores(
    signal_protocol_store_context* store_context,
    IdentityKeyStoreInterface** identity_key_store,
    std::function<signal_protocol_identity_key_store()> makeIdentityKeyStore,
    PreKeyStoreInterface** pre_key_store,
    std::function<signal_protocol_pre_key_store()> makePreKeyStore,
    PreKeyStoreInterface** signed_pre_key_store,
    std::function<signal_protocol_signed_pre_key_store()> makeSignedPreKeyStore,
    SessionStoreInterface** session_store,
    std::function<signal_protocol_session_store()> makeSessionStore)
{
    auto on_scope_exit = OnScopeExit{};
    {
        const auto store = makeIdentityKeyStore();
        if (signal_protocol_store_context_set_identity_key_store(store_context, &store) < 0) {
            on_scope_exit.push(
                [&] {
                    store.destroy_func(store.user_data);
                });
            throw SignalError{"failed setting identity key store"};
        }
        *identity_key_store = static_cast<IdentityKeyStoreInterface*>(store.user_data);
    }
    {
        const auto store = makePreKeyStore();
        if (signal_protocol_store_context_set_pre_key_store(store_context, &store) < 0) {
            on_scope_exit.push(
                [&] {
                    store.destroy_func(store.user_data);
                });
            throw SignalError{"failed setting pre key store"};
        }
        *pre_key_store = static_cast<PreKeyStoreInterface*>(store.user_data);
    }
    {
        const auto store = makeSignedPreKeyStore();
        if (signal_protocol_store_context_set_signed_pre_key_store(store_context, &store) < 0) {
            on_scope_exit.push(
                [&] {
                    store.destroy_func(store.user_data);
                });
            throw SignalError{"failed setting signed pre key store"};
        }
        *signed_pre_key_store = static_cast<PreKeyStoreInterface*>(store.user_data);
    }
    {
        const auto store = makeSessionStore();
        if (signal_protocol_store_context_set_session_store(store_context, &store) < 0) {
            on_scope_exit.push(
                [&] {
                    store.destroy_func(store.user_data);
                });
            throw SignalError{"failed setting session store"};
        }
        *session_store = static_cast<SessionStoreInterface*>(store.user_data);
    }
    on_scope_exit.cancel();
}


} // namespace


MemoryStoreContext::MemoryStoreContext(signal_context* context)
    : StoreContext{context}
{
    createStores(
        m_store_context,
        &m_identity_key_store,
        [=] { return makeMemoryIdentityKeyStore(context); },
        &m_pre_key_store,
        &makeMemoryPreKeyStore,
        &m_signed_pre_key_store,
        &makeMemorySignedPreKeyStore,
        &m_session_store,
        &makeMemorySessionStore);
}


SqliteStoreContext::SqliteStoreContext(gsl::not_null<sqlite::database*> database, signal_context* context)
    : StoreContext{context}
{
    createStores(
        m_store_context,
        &m_identity_key_store,
        [=] { return makeSqliteIdentityKeyStore(database, context); },
        &m_pre_key_store,
        [=] { return makeSqlitePreKeyStore(database); },
        &m_signed_pre_key_store,
        [=] { return makeSqliteSignedPreKeyStore(database); },
        &m_session_store,
        [=] { return makeSqliteSessionStore(database); });
}


} // namespace TigerMail::Signal
