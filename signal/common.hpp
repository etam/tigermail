/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_COMMON_HPP
#define TIGERMAIL_SIGNAL_COMMON_HPP

#include <exception>
#include <iostream>
#include <stdexcept>

#include <gsl/gsl_assert>

#include <signal_protocol.h>


namespace TigerMail::Signal {


using RegistrationIdT = std::uint32_t;
using PreKeyIdT = std::uint32_t;


class SignalError
    : public std::exception
{
private:
    const char* m_what;
    int m_result;

public:
    explicit
    SignalError(const char* what, int result = SG_ERR_UNKNOWN) noexcept
        : m_what{what}
        , m_result{result}
    {}

    ~SignalError() noexcept override = default;

    const char* what() const noexcept override
    {
        return m_what;
    }

    int result() const noexcept
    {
        return m_result;
    }
};


template <typename T>
class Ref
{
private:
    T* m_ptr = nullptr;

public:
    Ref() = default;

    explicit
    Ref(T* ptr)
        : m_ptr{ptr}
    {
        Expects(ptr != nullptr);
        SIGNAL_REF(m_ptr);
    }

    ~Ref() noexcept
    {
        SIGNAL_UNREF(m_ptr);
    }

    Ref(const Ref& other)
        : m_ptr{other.m_ptr}
    {
        SIGNAL_REF(m_ptr);
    }

    Ref(Ref&& other)
        : m_ptr{other.m_ptr}
    {
        other.m_ptr = nullptr;
    }

    Ref& operator=(const Ref& other)
    {
        if (m_ptr != other.m_ptr) {
            SIGNAL_UNREF(m_ptr);
            m_ptr = other.m_ptr;
            if (m_ptr) {
                SIGNAL_REF(m_ptr);
            }
        }
        return *this;
    }

    Ref& operator=(Ref&& other)
    {
        std::swap(m_ptr, other.m_ptr);
        return *this;
    }

    T* get() const
    {
        return m_ptr;
    }

    T** get2()
    {
        return &m_ptr;
    }
};


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_COMMON_HPP
