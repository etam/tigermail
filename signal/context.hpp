/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_CONTEXT_HPP
#define TIGERMAIL_SIGNAL_CONTEXT_HPP

#include <mutex>

#include <gsl/pointers>

struct signal_context;
struct signal_protocol_store_context;

namespace sqlite {
class database;
} // namespace sqlite

#include "crypto_provider.hpp"
#include "identity_key_store_impl/interface.hpp"
#include "pre_key_store_impl/interface.hpp"
#include "session_store_impl/interface.hpp"


namespace TigerMail::Signal {

using IdentityKeyStoreImpl::IdentityKeyStoreInterface;
using PreKeyStoreImpl::PreKeyStoreInterface;
using SessionStoreImpl::SessionStoreInterface;


class Context
{
public:
    // this type must be public to be accessible by free functions
    struct UserData
    {
        std::recursive_mutex mutex;
    };

private:
    class Context_
    {
    private:
        signal_context* m_context;

    public:
        explicit
        Context_(UserData* user_data);
        ~Context_() noexcept;

        signal_context* get() const
        {
            return m_context;
        }
    };

    UserData m_user_data;
    Context_ m_context;
    CryptoProvider m_crypto_provider;

public:
    Context();
    ~Context() noexcept = default;
    Context(const Context&) = delete;
    Context(Context&&) = delete;
    Context& operator=(const Context&) = delete;
    Context& operator=(Context&&) = delete;

    signal_context* get() const
    {
        return m_context.get();
    }

    CryptoProvider& get_crypto_provider()
    {
        return m_crypto_provider;
    }
};


class StoreContext
{
protected:
    signal_protocol_store_context* m_store_context;
    IdentityKeyStoreInterface* m_identity_key_store;
    PreKeyStoreInterface* m_pre_key_store;
    PreKeyStoreInterface* m_signed_pre_key_store;
    SessionStoreInterface* m_session_store;

public:
    explicit
    StoreContext(signal_context* context);
    ~StoreContext() noexcept;

    StoreContext(const StoreContext&) = delete;
    StoreContext(StoreContext&&) = delete;
    StoreContext& operator=(const StoreContext&) = delete;
    StoreContext& operator=(StoreContext&&) = delete;

    signal_protocol_store_context* get() const
    {
        return m_store_context;
    }

    IdentityKeyStoreInterface* get_identity_key_store() const
    {
        return m_identity_key_store;
    }

    PreKeyStoreInterface* get_pre_key_store() const
    {
        return m_pre_key_store;
    }

    PreKeyStoreInterface* get_signed_pre_key_store() const
    {
        return m_signed_pre_key_store;
    }

    SessionStoreInterface* get_session_store() const
    {
        return m_session_store;
    }
};


class MemoryStoreContext
    : public StoreContext
{
public:
    explicit
    MemoryStoreContext(signal_context* context);
};


class SqliteStoreContext
    : public StoreContext
{
public:
    SqliteStoreContext(gsl::not_null<sqlite::database*> database, signal_context* context);
};


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_CONTEXT_HPP
