/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "memory.hpp"

#include <iterator>
#include <stdexcept>
#include <tuple>

#include <signal_protocol.h>


namespace TigerMail::Signal::SessionStoreImpl {


namespace {


auto addressToTuple(const Address& address)
{
    return std::make_tuple(address.name(), address.device_id());
}


} // namespace


std::optional<Buffer> MemorySessionStore::load(const Address& address) const
{
    auto& index = m_container.get<AddressTag>();
    const auto it = index.find(addressToTuple(address));
    if (it == index.end()) {
        return std::nullopt;
    }
    return it->record;
}

std::set<DeviceIdT> MemorySessionStore::get_sub_devices(const std::string_view name) const
{
    auto& index = m_container.get<NameTag>();
    const auto range = index.equal_range(name);

    auto result = std::set<DeviceIdT>{};
    for (auto it = range.first; it != range.second; ++it) {
        result.insert(it->device_id);
    }
    return result;
}

void MemorySessionStore::store(const Address& address, const BufferView record)
{
    auto& index = m_container.get<AddressTag>();
    const auto it = index.find(addressToTuple(address));
    if (it == index.end()) {
        index.insert({
            std::string{address.name()},
            address.device_id(),
            {record.begin(), record.end()},
            {}
        });
    }
    else {
        it->record.assign(record.begin(), record.end());
    }
}

bool MemorySessionStore::contains(const Address& address) const
{
    auto& index = m_container.get<AddressTag>();
    const auto it = index.find(addressToTuple(address));
    return it != index.end();
}

bool MemorySessionStore::delete_(const Address& address)
{
    auto& index = m_container.get<AddressTag>();
    const auto it = index.find(addressToTuple(address));
    if (it == index.end()) {
        return false;
    }
    index.erase(it);
    return true;
}

int MemorySessionStore::delete_all(const std::string_view name)
{
    auto& index = m_container.get<NameTag>();
    const auto range = index.equal_range(name);
    if (range.first == index.end()) {
        return 0;
    }
    const auto distance = std::distance(range.first, range.second);
    index.erase(range.first, range.second);
    return distance;
}


void MemorySessionStore::store_keys_for_next_message_id(
    const Address& address, BufferView serialized_ratchet_pub_key, BufferView message_keys)
{
    auto& index = m_container.get<AddressTag>();
    const auto it = index.find(addressToTuple(address));
    if (it == index.end()) {
        throw std::out_of_range{"keys for message id not found"};
    }
    it->keys_for_message_id.serialized_ratchet_pub_key.assign(
        serialized_ratchet_pub_key.begin(), serialized_ratchet_pub_key.end());
    it->keys_for_message_id.message_keys.assign(message_keys.begin(), message_keys.end());
}

auto MemorySessionStore::get_keys_for_message_id(const Address& address) const -> KeysForMessageId
{
    auto& index = m_container.get<AddressTag>();
    const auto it = index.find(addressToTuple(address));
    if (it == index.end()) {
        throw std::out_of_range{"keys for message id not found"};
    }
    return it->keys_for_message_id;
}


} // namespace TigerMail::Signal::SessionStoreImpl
