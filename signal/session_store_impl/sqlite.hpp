/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_SESSION_STORE_IMPL_SQLITE_HPP
#define TIGERMAIL_SIGNAL_SESSION_STORE_IMPL_SQLITE_HPP

#include <gsl/pointers>

namespace sqlite {
class database;
} // namespace sqlite

#include "interface.hpp"


namespace TigerMail::Signal::SessionStoreImpl {


class SqliteSessionStore
    : public SessionStoreInterface
{
private:
    gsl::not_null<sqlite::database*> m_database;

public:
    SqliteSessionStore(gsl::not_null<sqlite::database*> database);
    ~SqliteSessionStore() noexcept override = default;

    std::optional<Buffer> load(const Address& address) const override;
    std::set<DeviceIdT> get_sub_devices(std::string_view name) const override;
    void store(const Address& address, BufferView record) override;
    bool contains(const Address& address) const override;
    bool delete_(const Address& address) override;
    int delete_all(std::string_view name) override;
    void store_keys_for_next_message_id(
        const Address& address, BufferView serialized_ratchet_pub_key, BufferView message_keys) override;
    KeysForMessageId get_keys_for_message_id(const Address& address) const override;
};


} // namespace TigerMail::Signal::SessionStoreImpl

#endif // TIGERMAIL_SIGNAL_SESSION_STORE_IMPL_SQLITE_HPP
