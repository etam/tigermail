/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_SESSION_STORE_IMPL_MEMORY_HPP
#define TIGERMAIL_SIGNAL_SESSION_STORE_IMPL_MEMORY_HPP

#include <vector>
#include <string_view>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/key.hpp>
#include <boost/multi_index/tag.hpp>

#include "interface.hpp"


namespace TigerMail::Signal::SessionStoreImpl {


class MemorySessionStore
    : public SessionStoreInterface
{
private:
    // see: https://www.boost.org/doc/libs/1_68_0/libs/multi_index/doc/tutorial/techniques.html
    struct Element {
        std::string name;
        DeviceIdT device_id;
        mutable Buffer record;
        mutable KeysForMessageId keys_for_message_id;

        std::string_view name_view() const
        {
            return name;
        }
    };

    struct AddressTag {};
    struct NameTag {};

    using Container = boost::multi_index_container<
        Element,
        boost::multi_index::indexed_by<
            boost::multi_index::hashed_unique<
                boost::multi_index::tag<AddressTag>,
                boost::multi_index::key<&Element::name_view, &Element::device_id>
            >,
            boost::multi_index::hashed_non_unique<
                boost::multi_index::tag<NameTag>,
                boost::multi_index::key<&Element::name_view>
            >
        >
    >;


    Container m_container;

public:
    ~MemorySessionStore() noexcept override = default;

    std::optional<Buffer> load(const Address& address) const override;
    std::set<DeviceIdT> get_sub_devices(std::string_view name) const override;
    void store(const Address& address, BufferView record) override;
    bool contains(const Address& address) const override;
    bool delete_(const Address& address) override;
    int delete_all(std::string_view name) override;
    void store_keys_for_next_message_id(
        const Address& address, BufferView serialized_ratchet_pub_key, BufferView message_keys) override;
    KeysForMessageId get_keys_for_message_id(const Address& address) const override;
};


} // namespace TigerMail::Signal::SessionStoreImpl

#endif // TIGERMAIL_SIGNAL_SESSION_STORE_IMPL_MEMORY_HPP
