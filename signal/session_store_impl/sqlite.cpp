/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sqlite.hpp"

#include <sqlite_modern_cpp.h>


namespace TigerMail::Signal::SessionStoreImpl {


SqliteSessionStore::SqliteSessionStore(gsl::not_null<sqlite::database*> database)
    : m_database{database}
{
    *m_database <<
        "create table if not exists "
        "signal_session ( "
        "  name text not null, "
        "  dev_id int not null, "
        "  record blob not null, "
        "  pub_key blob, "
        "  message_keys blob, "
        "  primary key (name, dev_id));";
}

std::optional<Buffer> SqliteSessionStore::load(const Address& address) const
{
    auto result = std::optional<Buffer>{};
    *m_database <<
        "select record "
        "from signal_session "
        "where "
        "  name = ? "
        "  and dev_id = ?;"
                << std::string{address.name()} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                << address.device_id()
                >> [&](Buffer record) {
                       result = std::move(record);
                   };
    return result;
}

std::set<DeviceIdT> SqliteSessionStore::get_sub_devices(std::string_view name) const
{
    auto result = std::set<DeviceIdT>{};
    *m_database <<
        "select dev_id "
        "from signal_session "
        "where "
        "  name = ?;"
                << std::string{name} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                >> [&](DeviceIdT dev_id) {
                       result.insert(dev_id);
                   };
    return result;
}

void SqliteSessionStore::store(const Address& address, BufferView record)
{
    *m_database <<
        "insert into signal_session(name, dev_id, record) "
        "values (?, ?, ?) "
        "on conflict (name, dev_id) do update set record=excluded.record;"
                << std::string{address.name()} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                << address.device_id()
                << Buffer(record.begin(), record.end()); // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
}

bool SqliteSessionStore::contains(const Address& address) const
{
    auto result = bool{};
    *m_database <<
        "select exists ( "
        "  select * "
        "  from signal_session "
        "  where "
        "    name = ? "
        "    and dev_id = ?);"
                << std::string{address.name()} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                << address.device_id()
                >> result;
    return result;
}

bool SqliteSessionStore::delete_(const Address& address)
{
    *m_database << "begin;";
    auto result = bool{};
    try {
        *m_database <<
            "select exists ( "
            "  select * "
            "  from signal_session "
            "  where "
            "    name = ? "
            "    and dev_id = ?);"
                    << std::string{address.name()} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                    << address.device_id()
                    >> result;
        if (!result) {
            return false;
        }
        *m_database <<
            "delete "
            "from signal_session "
            "where "
            "  name = ? "
            "  and dev_id = ?;"
                    << std::string{address.name()} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                    << address.device_id();
        *m_database << "commit;";
        return true;
    }
    catch (...) {
        *m_database << "rollback;";
        throw;
    }
}

int SqliteSessionStore::delete_all(std::string_view name)
{
    *m_database << "begin;";
    auto result = int{};
    try {
        *m_database <<
            "select count(*) "
            "from signal_session "
            "where "
            "  name = ?;"
                    << std::string{name} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                    >> result;
        if (result == 0) {
            return 0;
        }
        *m_database <<
            "delete "
            "from signal_session "
            "where "
            "  name = ?;"
                    << std::string{name}; // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
        *m_database << "commit;";
        return result;
    }
    catch (...) {
        *m_database << "rollback;";
        throw;
    }
}


void SqliteSessionStore::store_keys_for_next_message_id(
    const Address& address, BufferView serialized_ratchet_pub_key, BufferView message_keys)
{
    *m_database << "begin;";
    try {
        if (!contains(address)) {
            throw std::out_of_range{"keys for message id not found"};
        }
        *m_database <<
            "update signal_session "
            "set "
            "  pub_key = ?, "
            "  message_keys = ? "
            "where "
            "  name = ? "
            "  and dev_id = ?;"
                    << Buffer(serialized_ratchet_pub_key.begin(), serialized_ratchet_pub_key.end()) // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                    << Buffer(message_keys.begin(), message_keys.end()) // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                    << std::string{address.name()} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                    << address.device_id();
        *m_database << "commit;";
    }
    catch (...) {
        *m_database << "rollback;";
        throw;
    }
}

auto SqliteSessionStore::get_keys_for_message_id(const Address& address) const -> KeysForMessageId
{
    auto result = std::optional<KeysForMessageId>{};
    *m_database <<
        "select pub_key, message_keys "
        "from signal_session "
        "where "
        "  name = ? "
        "  and dev_id = ?;"
                << std::string{address.name()} // TODO https://github.com/SqliteModernCpp/sqlite_modern_cpp/issues/184
                << address.device_id()
                >> [&](Buffer pub_key, Buffer message_keys) {
                       result = KeysForMessageId{
                           .serialized_ratchet_pub_key = std::move(pub_key),
                           .message_keys = std::move(message_keys),
                       };
                   };
    if (!result) {
        throw std::out_of_range{"keys for message id not found"};
    }
    return *result;
}


} // namespace TigerMail::Signal::SessionStoreImpl
