/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_SESSION_STORE_IMPL_INTERFACE_HPP
#define TIGERMAIL_SIGNAL_SESSION_STORE_IMPL_INTERFACE_HPP

#include <optional>
#include <set>
#include <string_view>
#include <vector>

#include <gsl/span>

#include "../../common/buffer.hpp"
#include "../../common/buffer_view.hpp"

#include "../address.hpp"


namespace TigerMail::Signal::SessionStoreImpl {


class SessionStoreInterface
{
public:
    struct KeysForMessageId
    {
        Buffer serialized_ratchet_pub_key;
        Buffer message_keys;
    };

    virtual ~SessionStoreInterface() noexcept = default;

    virtual std::optional<Buffer> load(const Address& address) const = 0;
    virtual std::set<DeviceIdT> get_sub_devices(std::string_view name) const = 0;
    virtual void store(const Address& address, BufferView record) = 0;
    virtual bool contains(const Address& address) const = 0;
    virtual bool delete_(const Address& address) = 0;
    virtual int delete_all(std::string_view name) = 0;
    virtual void store_keys_for_next_message_id(
        const Address& address, BufferView serialized_ratchet_pub_key, BufferView message_keys) = 0;
    virtual KeysForMessageId get_keys_for_message_id(const Address& address) const = 0;
};


} // namespace TigerMail::Signal::SessionStoreImpl

#endif // TIGERMAIL_SIGNAL_SESSION_STORE_IMPL_INTERFACE_HPP
