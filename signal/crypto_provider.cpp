/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "crypto_provider.hpp"

#include <boost/log/trivial.hpp>

#include <gsl/gsl_util>

#include "common.hpp"

#include "crypto_provider_impl/aes.hpp"
#include "crypto_provider_impl/hmac_sha256.hpp"
#include "crypto_provider_impl/random.hpp"
#include "crypto_provider_impl/sha512.hpp"


namespace TigerMail::Signal {

namespace {


using HmacSha256 = CryptoProviderImpl::HmacSha256Interface;
using Random = CryptoProviderImpl::RandomInterface;
using Sha512 = CryptoProviderImpl::Sha512Interface;


template <typename F>
int handle_error_for_signal(F f, const char* func_name)
try {
    f();
    return SG_SUCCESS;
}
catch (std::bad_alloc& e) {
    BOOST_LOG_TRIVIAL(error) << func_name << " bad alloc: " << e.what();
    return SG_ERR_NOMEM;
}
catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(error) << func_name << " error: " << e.what();
    return SG_ERR_UNKNOWN;
}



int random(std::uint8_t* data, std::size_t len, void* user_data_)
{
    return handle_error_for_signal(
        [=](){
            auto* random = static_cast<Random*>(user_data_);
            random->generate({data, gsl::narrow<BufferView::index_type>(len)});
        },
        __func__);
}


int hmac_sha256_init(void** hmac, const std::uint8_t* key, std::size_t key_len, void* /*user_data*/)
{
    return handle_error_for_signal(
        [=](){
            *hmac = HmacSha256::create({key, gsl::narrow<BufferView::index_type>(key_len)});
        },
        __func__);
}

int hmac_sha256_update(void* hmac_, const std::uint8_t* data, std::size_t data_len, void* /*user_data*/)
{
    return handle_error_for_signal(
        [=](){
            auto* hmac = static_cast<HmacSha256*>(hmac_);
            hmac->update({data, gsl::narrow<BufferView::index_type>(data_len)});
        },
        __func__);
}

int hmac_sha256_final(void* hmac_, signal_buffer** output, void* /*user_data*/)
{
    return handle_error_for_signal(
        [=](){
            auto* hmac = static_cast<HmacSha256*>(hmac_);
            const auto buf = hmac->final();
            *output = signal_buffer_create(buf.data(), buf.size());
        },
        __func__);
}

void hmac_sha256_cleanup(void* hmac_, void* /*user_data*/)
{
    auto* hmac = static_cast<HmacSha256*>(hmac_);
    delete hmac;
}


int sha512_digest_init(void** digest, void* /*user_data*/)
{
    return handle_error_for_signal(
        [=](){
            *digest = Sha512::create();
        },
        __func__);
}

int sha512_digest_update(void* digest_, const std::uint8_t* data, std::size_t data_len, void* /*user_data*/)
{
    return handle_error_for_signal(
        [=](){
            auto* digest = static_cast<Sha512*>(digest_);
            digest->update({data, gsl::narrow<BufferView::index_type>(data_len)});
        },
        __func__);
}

int sha512_digest_final(void* digest_, signal_buffer** output, void* /*user_data*/)
{
    return handle_error_for_signal(
        [=](){
            auto* digest = static_cast<Sha512*>(digest_);
            const auto buf = digest->final();
            *output = signal_buffer_create(buf.data(), buf.size());
        },
        __func__);
}

void sha512_digest_cleanup(void* digest_, void* /*user_data*/)
{
    auto* digest = static_cast<Sha512*>(digest_);
    delete digest;
}


int encrypt(
    signal_buffer** output,
    int cipher,
    const std::uint8_t* key, std::size_t key_len,
    const std::uint8_t* iv, std::size_t iv_len,
    const std::uint8_t* plaintext, std::size_t plaintext_len,
    void* /*user_data*/)
{
    return handle_error_for_signal(
        [=]() {
            if (cipher == SG_CIPHER_AES_CTR_NOPADDING) {
                throw SignalError("unsupported old session version");
            }
            const auto buf = CryptoProviderImpl::encrypt(
                {key, gsl::narrow<BufferView::index_type>(key_len)},
                {iv, gsl::narrow<BufferView::index_type>(iv_len)},
                {plaintext, gsl::narrow<BufferView::index_type>(plaintext_len)});
            *output = signal_buffer_create(buf.data(), buf.size());
        },
        __func__);
}

int decrypt(
    signal_buffer** output,
    int cipher,
    const std::uint8_t* key, std::size_t key_len,
    const std::uint8_t* iv, std::size_t iv_len,
    const std::uint8_t* ciphertext, std::size_t ciphertext_len,
    void* /*user_data*/)
{
    return handle_error_for_signal(
        [=]() {
            if (cipher == SG_CIPHER_AES_CTR_NOPADDING) {
                throw SignalError("unsupported old session version");
            }
            const auto buf = CryptoProviderImpl::decrypt(
                {key, gsl::narrow<BufferView::index_type>(key_len)},
                {iv, gsl::narrow<BufferView::index_type>(iv_len)},
                {ciphertext, gsl::narrow<BufferView::index_type>(ciphertext_len)});
            *output = signal_buffer_create(buf.data(), buf.size());
        },
        __func__);
}


} // namespace


CryptoProvider::CryptoProvider()
    : m_provider{
        .random_func = TigerMail::Signal::random,
        .hmac_sha256_init_func = hmac_sha256_init,
        .hmac_sha256_update_func = hmac_sha256_update,
        .hmac_sha256_final_func = hmac_sha256_final,
        .hmac_sha256_cleanup_func = hmac_sha256_cleanup,
        .sha512_digest_init_func = sha512_digest_init,
        .sha512_digest_update_func = sha512_digest_update,
        .sha512_digest_final_func = sha512_digest_final,
        .sha512_digest_cleanup_func = sha512_digest_cleanup,
        .encrypt_func = encrypt,
        .decrypt_func = decrypt,
        #ifdef FUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION
        .user_data = Random::create_fake()
        #else
        .user_data = Random::create(),
        #endif
      }
{}

CryptoProvider::~CryptoProvider() noexcept
{
    delete static_cast<Random*>(m_provider.user_data);
}


void CryptoProvider::random(BufferSpan data)
{
    auto* random = static_cast<Random*>(m_provider.user_data);
    random->generate(data);
}


} // namespace TigerMail::Signal
