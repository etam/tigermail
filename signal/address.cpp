/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "address.hpp"

#include <tuple>


namespace TigerMail::Signal {


Address::Address(std::string_view name, DeviceIdT device_id)
    : m_name{name}
    , m_signal{m_name.data(), m_name.size(), device_id}
{}


Address Address::from_signal(const signal_protocol_address* address)
{
    return {
        {address->name, address->name_len},
        address->device_id,
    };
}

Address::Address(const Address& other)
    : m_name{other.m_name}
    , m_signal{m_name.data(), m_name.size(), other.m_signal.device_id}
{}

Address::Address(Address&& other)
    : m_name{std::move(other.m_name)}
    , m_signal{m_name.data(), m_name.size(), other.m_signal.device_id}
{}

Address& Address::operator=(const Address& other)
{
    m_name = other.m_name;
    m_signal.name = m_name.data();
    m_signal.name_len = m_name.size();
    m_signal.device_id = other.m_signal.device_id;
    return *this;
}

Address& Address::operator=(Address&& other)
{
    m_name = std::move(other.m_name);
    m_signal.name = m_name.data();
    m_signal.name_len = m_name.size();
    m_signal.device_id = other.m_signal.device_id;
    return *this;
}


const std::string_view Address::name() const
{
    return m_name;
}

DeviceIdT Address::device_id() const
{
    return m_signal.device_id;
}

const signal_protocol_address* Address::as_signal() const
{
    return &m_signal;
}


bool operator==(const Address& a1, const Address& a2)
{
    return std::tie(a1.m_name, a1.m_signal.device_id) == std::tie(a2.m_name, a2.m_signal.device_id);
}

bool operator!=(const Address& a1, const Address& a2)
{
    return !(a1 == a2);
}

bool operator<(const Address& a1, const Address& a2)
{
    return std::tie(a1.m_name, a1.m_signal.device_id) < std::tie(a2.m_name, a2.m_signal.device_id);
}

bool operator<=(const Address& a1, const Address& a2)
{
    return a1 < a2 || a1 == a2;
}

bool operator>(const Address& a1, const Address& a2)
{
    return !(a1 <= a2);
}

bool operator>=(const Address& a1, const Address& a2)
{
    return !(a1 < a2);
}


} // namespace TigerMail::Signal
