/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_ADDRESS_HPP
#define TIGERMAIL_SIGNAL_ADDRESS_HPP

#include <string>
#include <string_view>

#include <signal_protocol_types.h>


namespace TigerMail::Signal {


using DeviceIdT = std::int32_t;


class Address
{
private:
    std::string m_name;
    signal_protocol_address m_signal;

public:
    Address(std::string_view name, DeviceIdT device_id);
    static Address from_signal(const signal_protocol_address* address);

    Address(const Address& other);
    Address(Address&& other);
    Address& operator=(const Address& other);
    Address& operator=(Address&& other);

    const std::string_view name() const;
    DeviceIdT device_id() const;
    const signal_protocol_address* as_signal() const;

    friend bool operator==(const Address& a1, const Address& a2);
    friend bool operator<(const Address& a1, const Address& a2);
};


bool operator==(const Address& a1, const Address& a2);
bool operator!=(const Address& a1, const Address& a2);
bool operator<(const Address& a1, const Address& a2);
bool operator<=(const Address& a1, const Address& a2);
bool operator>(const Address& a1, const Address& a2);
bool operator>=(const Address& a1, const Address& a2);


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_ADDRESS_HPP
