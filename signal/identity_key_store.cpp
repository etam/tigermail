/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "identity_key_store.hpp"

#include <exception>

#include <boost/log/trivial.hpp>

#include <gsl/gsl_util>

#include <signal_protocol.h>

#include "address.hpp"
#include "identity_key_store_impl/interface.hpp"
#include "identity_key_store_impl/memory.hpp"
#include "identity_key_store_impl/sqlite.hpp"


namespace TigerMail::Signal {


namespace {

using IdentityKeyStoreImpl::IdentityKeyStoreInterface;


int get_identity_key_pair(signal_buffer** public_data, signal_buffer** private_data, void* user_data)
{
    const auto* store = static_cast<const IdentityKeyStoreInterface*>(user_data);
    const auto key_pair = store->get_identity_key_pair();
    *public_data = signal_buffer_copy(key_pair.pub_key->get());
    *private_data = signal_buffer_copy(key_pair.priv_key->get());
    return 0;
}

int get_local_registration_id(void* user_data, std::uint32_t* registration_id)
{
    const auto* store = static_cast<const IdentityKeyStoreInterface*>(user_data);
    *registration_id = store->get_local_registration_id();
    return 0;
}

int save_identity(const signal_protocol_address* address, std::uint8_t* key_data, std::size_t key_len, void* user_data)
try {
    auto* store = static_cast<IdentityKeyStoreInterface*>(user_data);
    store->save(Address::from_signal(address), {key_data, gsl::narrow<BufferView::index_type>(key_len)});
    return 0;
}
catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(error) << "save_identity error: " << e.what();
    return SG_ERR_UNKNOWN;
}

int is_trusted_identity(const signal_protocol_address* address, uint8_t* key_data, size_t key_len, void* user_data)
{
    const auto* store = static_cast<const IdentityKeyStoreInterface*>(user_data);
    return store->is_trusted(Address::from_signal(address), {key_data, gsl::narrow<BufferView::index_type>(key_len)});
}

void destroy(void* user_data)
{
    auto* store = static_cast<IdentityKeyStoreInterface*>(user_data);
    delete store;
}

signal_protocol_identity_key_store makeIdentityKeyStore(void* user_data)
{
    return {
        .get_identity_key_pair = get_identity_key_pair,
        .get_local_registration_id = get_local_registration_id,
        .save_identity = save_identity,
        .is_trusted_identity = is_trusted_identity,
        .destroy_func = destroy,
        .user_data = user_data,
    };
}


} // namespace


signal_protocol_identity_key_store makeMemoryIdentityKeyStore(signal_context* context)
{
    return makeIdentityKeyStore(new IdentityKeyStoreImpl::MemoryIdentityKeyStore{context});
}

signal_protocol_identity_key_store makeSqliteIdentityKeyStore(gsl::not_null<sqlite::database*> database, signal_context* context)
{
    return makeIdentityKeyStore(new IdentityKeyStoreImpl::SqliteIdentityKeyStore{database, context});
}


} // namespace TigerMail::Signal
