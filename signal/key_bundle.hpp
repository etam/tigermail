/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_KEY_BUNDLE_HPP
#define TIGERMAIL_SIGNAL_KEY_BUNDLE_HPP

#include <vector>

struct signal_context;
struct ec_public_key;

#include "../common/buffer.hpp"
#include "../common/buffer_view.hpp"
#include "common.hpp"


namespace TigerMail::Signal {


struct KeyBundle
{
    struct PreKey
    {
        PreKeyIdT id;
        Ref<ec_public_key> key;
    };

    RegistrationIdT registration_id;
    std::uint32_t device_id;
    Ref<ec_public_key> identity_key;
    PreKey signed_pre_key;
    Buffer signed_pre_key_signature;
    std::vector<PreKey> pre_keys;
};


Buffer serializeKeyBundle(const KeyBundle& key_bundle);
KeyBundle deserializeKeyBundle(BufferView serialized, signal_context* context);


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_KEY_BUNDLE_HPP
