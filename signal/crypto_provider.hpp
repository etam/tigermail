/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SIGNAL_CRYPTO_PROVIDER_HPP
#define TIGERMAIL_SIGNAL_CRYPTO_PROVIDER_HPP

#include <signal_protocol.h>

#include "../common/buffer_span.hpp"


namespace TigerMail::Signal {


class CryptoProvider
{
private:
    signal_crypto_provider m_provider;

public:
    CryptoProvider();
    ~CryptoProvider() noexcept;

    const signal_crypto_provider* get() const
    {
        return &m_provider;
    }

    void random(BufferSpan data);
};


} // namespace TigerMail::Signal

#endif // TIGERMAIL_SIGNAL_CRYPTO_PROVIDER_HPP
