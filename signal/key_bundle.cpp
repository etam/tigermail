/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "key_bundle.hpp"

#include <stdexcept>

#include <gsl/gsl_util>

#include <signal_protocol.h>

#include <range/v3/core.hpp>
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/transform.hpp>

#include "key_bundle_impl/key_bundle.pb.h"
#include "signalpp/curve.hpp"


namespace TigerMail::Signal {


namespace {


template <typename Setter>
void set_serialized_pub_key(ec_public_key* pub_key, Setter setter)
{
    const auto identity_key_serialized = ec_public_key_serialize(pub_key);
    setter(identity_key_serialized.data(),
           identity_key_serialized.size());
}


Ref<ec_public_key> deserialize_pub_key(const std::string& buff, signal_context* context)
{
    return curve_decode_point(
        {
            reinterpret_cast<const std::uint8_t*>(buff.data()),
            gsl::narrow<BufferView::index_type>(buff.size())
        },
        context);
}


void set_pre_key(const KeyBundle::PreKey& pre_key, KeyBundleImpl::KeyBundle_PreKey* pre_key_proto)
{
    pre_key_proto->set_id(pre_key.id);

    set_serialized_pub_key(
        pre_key.key.get(),
        [&](const void* data, std::size_t size) {
            pre_key_proto->set_pub(data, size);
        });
}


KeyBundle::PreKey deserialize_pre_key(const KeyBundleImpl::KeyBundle_PreKey& pre_key_proto, signal_context* context)
{
    auto pre_key = KeyBundle::PreKey{};
    pre_key.id = pre_key_proto.id();
    pre_key.key = deserialize_pub_key(pre_key_proto.pub(), context);
    return pre_key;
}


} // template


Buffer serializeKeyBundle(const KeyBundle& key_bundle)
{
    auto key_bundle_proto = KeyBundleImpl::KeyBundle{};

    key_bundle_proto.set_registration_id(key_bundle.registration_id);
    key_bundle_proto.set_device_id(key_bundle.device_id);

    set_serialized_pub_key(
        key_bundle.identity_key.get(),
        [&](const void* data, std::size_t size) {
            key_bundle_proto.set_identity_key_pub(data, size);
        });

    set_pre_key(key_bundle.signed_pre_key, key_bundle_proto.mutable_signed_pre_key());

    key_bundle_proto.set_signed_pre_key_signature(
        key_bundle.signed_pre_key_signature.data(),
        key_bundle.signed_pre_key_signature.size());

    for (const auto& pre_key : key_bundle.pre_keys) {
        set_pre_key(pre_key, key_bundle_proto.add_pre_keys());
    }

    auto serialized = Buffer(key_bundle_proto.ByteSizeLong(), '\0');
    key_bundle_proto.SerializeToArray(serialized.data(), serialized.size());
    return serialized;
}

KeyBundle deserializeKeyBundle(BufferView serialized, signal_context* context)
{
    const auto key_bundle_proto =
        [&serialized]() {
            KeyBundleImpl::KeyBundle key_bundle_proto;
            key_bundle_proto.ParseFromArray(serialized.data(), serialized.size());
            return key_bundle_proto;
        }();

    auto key_bundle = KeyBundle{};

    key_bundle.registration_id = key_bundle_proto.registration_id();
    key_bundle.device_id = key_bundle_proto.device_id();
    key_bundle.identity_key = deserialize_pub_key(key_bundle_proto.identity_key_pub(), context);
    key_bundle.signed_pre_key = deserialize_pre_key(key_bundle_proto.signed_pre_key(), context);
    key_bundle.signed_pre_key_signature = key_bundle_proto.signed_pre_key_signature() | ranges::to<Buffer>();
    key_bundle.pre_keys =
        key_bundle_proto.pre_keys()
        | ranges::views::transform(
            [&](const auto& pre_key_proto) {
                return deserialize_pre_key(pre_key_proto, context);
            })
        | ranges::to_vector;

    return key_bundle;
}


} // namespace TigerMail::Signal
