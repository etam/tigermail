/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_ZPAD_HPP
#define TIGERMAIL_COMMON_ZPAD_HPP

#include <algorithm>

#include <gsl/gsl_assert>
#include <gsl/gsl_util>

#include "buffer_view.hpp"
#include "fixed_size_buffer.hpp"


namespace TigerMail {


template <std::size_t size>
// constexpr since c++20
FixedSizeBuffer<size> zpad(BufferView buffer)
{
    Expects(gsl::narrow<std::size_t>(buffer.size()) <= size);
    auto result = FixedSizeBuffer<size>{};
    std::copy(buffer.begin(), buffer.end(), result.end() - buffer.size());
    return result;
}


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_ZPAD_HPP
