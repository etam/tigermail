/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_BUFFER_HPP
#define TIGERMAIL_COMMON_BUFFER_HPP

#include <vector>

#include "buffer_view.hpp"


namespace TigerMail {


class Buffer
    : public std::vector<std::uint8_t>
{
private:
    using BaseClass = std::vector<std::uint8_t>;

public:
    using BaseClass::BaseClass;

    Buffer(const BaseClass& base)
        : BaseClass(base)
    {}

    Buffer(BaseClass&& base)
        : BaseClass(std::move(base))
    {}
};

/*
  It may seem that it's not needed, because std::vector already has operator==,
  but without this, the compiler is confused whether it should use operator for
  vectors, or any of operators for comparing Buffer with BufferView.
 */
bool operator==(const Buffer& buf1, const Buffer& buf2);

inline
bool operator!=(const Buffer& buf1, const Buffer& buf2)
{
    return !(buf1 == buf2);
}


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_BUFFER_HPP
