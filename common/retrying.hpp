/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_RETRYING_HPP
#define TIGERMAIL_COMMON_RETRYING_HPP

#include <any>
#include <optional>
#include <type_traits>
#include <utility>


namespace TigerMail {


template <typename F>
auto retrying(F f, std::any yield) -> std::optional<typename std::invoke_result_t<F, std::any>::value_type>
{
    // TODO: wait with exponential backoff
    for (int tries = 3; tries > 0; --tries) {
        auto result = f(yield);
        if (result) {
            return std::move(*result);
        }
    }
    return std::nullopt;
}


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_RETRYING_HPP
