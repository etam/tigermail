/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../buffer.hpp"
#include "../retrying.hpp"

#include "fake_yield.hpp"
#include "out/buffer.hpp"
#include "out/std_optional.hpp"


BOOST_AUTO_TEST_SUITE(retrying)

using TigerMail::Buffer;
using TigerMail::retrying;
using TigerMail::Test::FakeYield;


BOOST_AUTO_TEST_CASE(success_on_third_try)
{
    auto yield = FakeYield(FakeYield::explicit_construction);
    const auto some_data = Buffer{0xde, 0xad, 0xbe, 0xef};
    const auto f = [i=0, &some_data](std::any) mutable -> std::optional<Buffer> {
        if (i < 2) {
            ++i;
            return std::nullopt;
        }
        return some_data;
    };
    const auto result = retrying(f, yield);
    BOOST_CHECK_EQUAL(result, some_data);
}

BOOST_AUTO_TEST_CASE(fail)
{
    auto yield = FakeYield(FakeYield::explicit_construction);
    const auto f = [](std::any) -> std::optional<Buffer> { return std::nullopt; };
    const auto result = retrying(f, yield);
    BOOST_CHECK_EQUAL(result, std::nullopt);
}


BOOST_AUTO_TEST_SUITE_END() // retrying
