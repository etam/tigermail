/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <vector>

#include <boost/test/unit_test.hpp>

#include "../on_scope_exit.hpp"


BOOST_AUTO_TEST_SUITE(on_scope_exit)


BOOST_AUTO_TEST_CASE(no_cancel)
{
    using TigerMail::OnScopeExit;
    auto v = std::vector<int>{};
    {
        auto on_scope_exit = OnScopeExit{};
        on_scope_exit.push(
            [&] {
                v.push_back(1);
            });
        on_scope_exit.push(
            [&] {
                v.push_back(2);
            });
    }
    const auto expected_v = std::vector{2, 1};
    BOOST_CHECK_EQUAL_COLLECTIONS(v.begin(), v.end(),
                                  expected_v.begin(), expected_v.end());
}

BOOST_AUTO_TEST_CASE(cancel)
{
    using TigerMail::OnScopeExit;
    auto v = std::vector<int>{};
    {
        auto on_scope_exit = OnScopeExit{};
        on_scope_exit.push(
            [&] {
                v.push_back(1);
            });
        on_scope_exit.push(
            [&] {
                v.push_back(2);
            });
        on_scope_exit.cancel();
    }
    BOOST_CHECK(v.empty());
}


BOOST_AUTO_TEST_SUITE_END() // on_scope_exit
