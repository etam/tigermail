/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_TEST_OUT_STD_TUPLE_HPP
#define TIGERMAIL_COMMON_TEST_OUT_STD_TUPLE_HPP

#include <ostream>
#include <tuple>


namespace std {


template <typename T1, typename T2>
static
ostream& operator<<(ostream& o, const tuple<T1, T2>& t)
{
    return o << "tuple{" << get<0>(t) << "," << get<1>(t) <<"}";
}

// add implementations for more types, or use some template-fu to handle them all at once


} // namespace std

#endif // TIGERMAIL_COMMON_TEST_OUT_STD_TUPLE_HPP
