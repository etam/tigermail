/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_TEST_OUT_FIXED_SIZE_BUFFER_HPP
#define TIGERMAIL_COMMON_TEST_OUT_FIXED_SIZE_BUFFER_HPP

#include <ostream>
#include <sstream>

#include "../../fixed_size_buffer.hpp"

#include "iterable.hpp"


namespace TigerMail {


template <std::size_t size>
static
std::ostream& operator<<(std::ostream& o, const FixedSizeBuffer<size>& buffer)
{
    auto name = std::ostringstream{};
    name << "FixedSizeBuffer<" << size << '>';
    return print_iterable(o, name.str(), buffer);
}


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_TEST_OUT_FIXED_SIZE_BUFFER_HPP
