/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../buffer.hpp"
#include "../buffer_view.hpp"

#include "out/buffer.hpp"
#include "out/buffer_view.hpp"

using TigerMail::Buffer;
using TigerMail::BufferView;


BOOST_AUTO_TEST_CASE(views_equal)
{
    const auto buf = Buffer{0x11, 0x22};
    BOOST_CHECK_EQUAL(BufferView{buf}, BufferView{buf});
}

BOOST_AUTO_TEST_CASE(buffer_equal_to_view)
{
    const auto buf = Buffer{0x11, 0x22};
    BOOST_CHECK_EQUAL(buf, BufferView{buf});
    BOOST_CHECK_EQUAL(BufferView{buf}, buf);
}
