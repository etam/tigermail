/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../buffer.hpp"
#include "../fixed_size_buffer.hpp"
#include "../fixed_size_buffer_view.hpp" // pull other operator==

#include "out/fixed_size_buffer.hpp"


BOOST_AUTO_TEST_SUITE(fixed_size_buffer)

using TigerMail::Buffer;
using TigerMail::FixedSizeBuffer;


BOOST_AUTO_TEST_CASE(construct_from_initializer_list)
{
    const auto buf = FixedSizeBuffer<5>{0x11, 0x22};
    const auto expected_buf = Buffer{0x11, 0x22, 0x00, 0x00, 0x00};
    BOOST_CHECK_EQUAL_COLLECTIONS(buf.begin(), buf.end(), expected_buf.begin(), expected_buf.end());
}

BOOST_AUTO_TEST_CASE(construct_from_buffer)
{
    const auto buf = FixedSizeBuffer<5>(Buffer{0x11, 0x22});
    const auto expected_buf = Buffer{0x11, 0x22, 0x00, 0x00, 0x00};
    BOOST_CHECK_EQUAL_COLLECTIONS(buf.begin(), buf.end(), expected_buf.begin(), expected_buf.end());
}

BOOST_AUTO_TEST_CASE(construct_from_iterator_pair)
{
    const auto data = Buffer{0x11, 0x22};
    const auto buf = FixedSizeBuffer<5>(data.begin(), data.end());
    const auto expected_buf = Buffer{0x11, 0x22, 0x00, 0x00, 0x00};
    BOOST_CHECK_EQUAL_COLLECTIONS(buf.begin(), buf.end(), expected_buf.begin(), expected_buf.end());
}

BOOST_AUTO_TEST_CASE(equal)
{
    // check if operator== is not ambiguous
    const auto buf = FixedSizeBuffer<2>{0x11, 0x22};
    BOOST_CHECK_EQUAL(buf, buf);
}


BOOST_AUTO_TEST_SUITE_END()
