/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include <gsl/gsl_assert>

#include "../buffer.hpp"
#include "../fixed_size_buffer.hpp"
#include "../zpad.hpp"


BOOST_AUTO_TEST_CASE(zpad)
{
    using TigerMail::Buffer;
    using TigerMail::FixedSizeBuffer;
    using TigerMail::zpad;

    BOOST_CHECK_THROW(zpad<1>(Buffer{0x11, 0x22}), gsl::fail_fast);
    BOOST_CHECK_NO_THROW(zpad<2>(Buffer{0x11, 0x22}));

    {
        const auto data = zpad<5>(Buffer{0xab, 0xcd});
        const auto expected_data = FixedSizeBuffer<5>{0x00, 0x00, 0x00, 0xab, 0xcd};
        BOOST_CHECK_EQUAL_COLLECTIONS(
            data.begin(), data.end(),
            expected_data.begin(), expected_data.end());
    }
}
