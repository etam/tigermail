/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../buffer.hpp"
#include "../fixed_size_buffer.hpp"
#include "../hex.hpp"


BOOST_AUTO_TEST_SUITE(hex)

using TigerMail::Buffer;


BOOST_AUTO_TEST_CASE(bin2hex)
{
    using TigerMail::bin2hex;
    const auto hex = bin2hex(Buffer{0x89,0xb2,0x41,0xf6,0x8c,0xb9,0x5e,0x47,0x06,0xa4,0xbe,0x92,0xbf,0xf2,0x82,0x24,0xab,0x90,0x88,0x6a,0x54,0xc6,0xb7,0x43,0xac,0xc2,0x94,0x22,0xe9,0x40,0x55,0xe1});
    const auto expected_hex = std::string{"89b241f68cb95e4706a4be92bff28224ab90886a54c6b743acc29422e94055e1"};
    BOOST_CHECK_EQUAL(hex, expected_hex);
}

BOOST_AUTO_TEST_CASE(bin2zxhex)
{
    using TigerMail::bin2zxhex;
    const auto zxhex = bin2zxhex(Buffer{0x12, 0x34, 0x56});
    const auto expected_zxhex = std::string{"0x123456"};
    BOOST_CHECK_EQUAL(zxhex, expected_zxhex);
}


BOOST_AUTO_TEST_SUITE(hex2bin)

using TigerMail::ConversionError;
using TigerMail::FixedSizeBuffer;
using TigerMail::hex2bin;


BOOST_AUTO_TEST_CASE(output_through_span)
{
    BOOST_CHECK_THROW(hex2bin("123"), ConversionError); // odd length
    BOOST_CHECK_THROW(hex2bin("wxyz"), ConversionError); // non-hex chars

    auto bin = FixedSizeBuffer<5>{};
    hex2bin("1122334455", bin);
    const auto expected_bin = FixedSizeBuffer<5>{0x11, 0x22, 0x33, 0x44, 0x55};
    BOOST_CHECK_EQUAL_COLLECTIONS(bin.begin(), bin.end(), expected_bin.begin(), expected_bin.end());
}

BOOST_AUTO_TEST_CASE(to_buffer)
{
    BOOST_CHECK_THROW(hex2bin("123"), ConversionError); // odd length
    BOOST_CHECK_THROW(hex2bin("wxyz"), ConversionError); // non-hex chars

    const auto bin = hex2bin("89b241f68cb95e4706a4be92bff28224ab90886a54c6b743acc29422e94055e1");
    const auto expected_bin = Buffer{0x89,0xb2,0x41,0xf6,0x8c,0xb9,0x5e,0x47,0x06,0xa4,0xbe,0x92,0xbf,0xf2,0x82,0x24,0xab,0x90,0x88,0x6a,0x54,0xc6,0xb7,0x43,0xac,0xc2,0x94,0x22,0xe9,0x40,0x55,0xe1};
    BOOST_CHECK_EQUAL_COLLECTIONS(bin.begin(), bin.end(), expected_bin.begin(), expected_bin.end());
}

BOOST_AUTO_TEST_CASE(to_fixed_size_buffer)
{
    BOOST_CHECK_THROW(hex2bin<FixedSizeBuffer<2>>("123"), ConversionError); // odd length
    BOOST_CHECK_THROW(hex2bin<FixedSizeBuffer<2>>("wxyz"), ConversionError); // non-hex chars

    const auto bin = hex2bin<FixedSizeBuffer<5>>("1122334455");
    const auto expected_bin = FixedSizeBuffer<5>{0x11, 0x22, 0x33, 0x44, 0x55};
    BOOST_CHECK_EQUAL_COLLECTIONS(bin.begin(), bin.end(), expected_bin.begin(), expected_bin.end());

    BOOST_CHECK_THROW(hex2bin<FixedSizeBuffer<2>>("112233"), ConversionError); // too long
}


BOOST_AUTO_TEST_SUITE_END() // hex2bin


BOOST_AUTO_TEST_CASE(zxhex2bin)
{
    using TigerMail::ConversionError;
    using TigerMail::zxhex2bin;

    BOOST_CHECK_THROW(zxhex2bin(""), ConversionError); // empty
    BOOST_CHECK_THROW(zxhex2bin("abc"), ConversionError); // no 0x
    BOOST_CHECK_THROW(zxhex2bin("0x123"), ConversionError); // odd length
    BOOST_CHECK_THROW(zxhex2bin("0xwxyz"), ConversionError); // non-hex chars

    BOOST_CHECK(zxhex2bin("0x").empty());

    const auto bin = zxhex2bin("0x123456");
    const auto expected_bin = Buffer{0x12, 0x34, 0x56};
    BOOST_CHECK_EQUAL_COLLECTIONS(bin.begin(), bin.end(), expected_bin.begin(), expected_bin.end());
}

BOOST_AUTO_TEST_CASE(int2hex)
{
    using TigerMail::int2hex;
    BOOST_CHECK_EQUAL(int2hex(3931798), "3bfe96");
}

BOOST_AUTO_TEST_CASE(int2zxhex)
{
    using TigerMail::int2zxhex;
    BOOST_CHECK_EQUAL(int2zxhex(3931798), "0x3bfe96");
}

BOOST_AUTO_TEST_CASE(hex2int)
{
    using TigerMail::ConversionError;
    using TigerMail::hex2int;

    BOOST_CHECK_THROW(hex2int(""), ConversionError); // empty
    BOOST_CHECK_THROW(hex2int("wxyz"), ConversionError); // non-hex chars

    BOOST_CHECK_EQUAL(hex2int("123"), 291); // odd length
    BOOST_CHECK_EQUAL(hex2int("3bfe96"), 3931798);
}

BOOST_AUTO_TEST_CASE(zxhex2int)
{
    using TigerMail::ConversionError;
    using TigerMail::zxhex2int;

    BOOST_CHECK_THROW(zxhex2int(""), ConversionError); // empty
    BOOST_CHECK_THROW(zxhex2int("abc"), ConversionError); // no 0x
    BOOST_CHECK_THROW(zxhex2int("0xwxyz"), ConversionError); // non-hex chars

    BOOST_CHECK_EQUAL(zxhex2int("0x123"), 291); // odd length
    BOOST_CHECK_EQUAL(zxhex2int("0x3bfe96"), 3931798);
}

BOOST_AUTO_TEST_CASE(zxhex2uint8)
{
    using TigerMail::zxhex2int;

    BOOST_CHECK_EQUAL(zxhex2int<std::uint8_t>("0x25"), 37);
}


BOOST_AUTO_TEST_SUITE_END()
