/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../string_view.hpp"


BOOST_AUTO_TEST_SUITE(string_view)


BOOST_AUTO_TEST_SUITE(starts_with)

using TigerMail::starts_with;


BOOST_AUTO_TEST_CASE(ok)
{
    BOOST_CHECK(starts_with("foo bar", "foo"));
}

BOOST_AUTO_TEST_CASE(wrong)
{
    BOOST_CHECK(!starts_with("foo bar", "bar"));
}

BOOST_AUTO_TEST_CASE(longer)
{
    BOOST_CHECK(!starts_with("foo", "foo bar"));
}

BOOST_AUTO_TEST_SUITE_END() // starts_with


BOOST_AUTO_TEST_SUITE(ends_with)

using TigerMail::ends_with;


BOOST_AUTO_TEST_CASE(ok)
{
    BOOST_CHECK(ends_with("foo bar", "bar"));
}

BOOST_AUTO_TEST_CASE(wrong)
{
    BOOST_CHECK(!ends_with("foo bar", "foo"));
}

BOOST_AUTO_TEST_CASE(longer)
{
    BOOST_CHECK(!ends_with("bar", "foo bar"));
}

BOOST_AUTO_TEST_SUITE_END() // starts_with


BOOST_AUTO_TEST_SUITE(case_insensitive_equal)

using TigerMail::case_insensitive_equal;


BOOST_AUTO_TEST_CASE(upper_equal)
{
    BOOST_CHECK(case_insensitive_equal("ABCD", "ABCD"));
}

BOOST_AUTO_TEST_CASE(upper_unequal)
{
    BOOST_CHECK(!case_insensitive_equal("ABCD", "GHIJ"));
}

BOOST_AUTO_TEST_CASE(mixed_case_equal)
{
    BOOST_CHECK(case_insensitive_equal("ABCD", "abCd"));
}

BOOST_AUTO_TEST_CASE(mixed_case_unequal)
{
    BOOST_CHECK(!case_insensitive_equal("ABCD", "ghij"));
}


BOOST_AUTO_TEST_SUITE_END() // case_insensitive_equal


BOOST_AUTO_TEST_SUITE_END() // string_view
