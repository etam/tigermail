/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../fixed_size_buffer.hpp"
#include "../fixed_size_buffer_view.hpp"

#include "out/fixed_size_buffer.hpp"
#include "out/fixed_size_buffer_view.hpp"


BOOST_AUTO_TEST_SUITE(fixed_size_buffer_view)

using TigerMail::FixedSizeBuffer;
using TigerMail::FixedSizeBufferView;


BOOST_AUTO_TEST_CASE(construct_from_fixed_size_buffer)
{
    const auto buf = FixedSizeBuffer<2>{0x11, 0x22};
    const auto view = FixedSizeBufferView<2>{buf};
    BOOST_CHECK_EQUAL_COLLECTIONS(buf.begin(), buf.end(), view.begin(), view.end());
}

BOOST_AUTO_TEST_CASE(construct_from_pointer)
{
    const auto buf = FixedSizeBuffer<2>{0x11, 0x22};
    const auto view = FixedSizeBufferView<2>{buf.data()};
    BOOST_CHECK_EQUAL_COLLECTIONS(buf.begin(), buf.end(), view.begin(), view.end());
}

BOOST_AUTO_TEST_CASE(equal)
{
    const auto buf = FixedSizeBuffer<2>{0x11, 0x22};
    BOOST_CHECK_EQUAL(FixedSizeBufferView{buf}, FixedSizeBufferView{buf});
}

BOOST_AUTO_TEST_CASE(view_equal_to_fixed_size_buffer)
{
    const auto buf = FixedSizeBuffer<2>{0x11, 0x22};
    BOOST_CHECK_EQUAL(FixedSizeBufferView{buf}, buf);
}

BOOST_AUTO_TEST_CASE(fixed_size_buffer_equal_to_view)
{
    const auto buf = FixedSizeBuffer<2>{0x11, 0x22};
    BOOST_CHECK_EQUAL(buf, FixedSizeBufferView{buf});
}

BOOST_AUTO_TEST_CASE(brackets)
{
    const auto buf = FixedSizeBuffer<2>{0x11, 0x22};
    const auto view = FixedSizeBufferView<2>{buf};
    for (std::size_t i = 0; i < 2; ++i) {
        BOOST_CHECK_EQUAL(buf[i], view[i]);
    }
}


BOOST_AUTO_TEST_SUITE_END()
