/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../buffer.hpp"
#include "../buffer_view.hpp" // pull other operator==

#include "out/buffer.hpp"


BOOST_AUTO_TEST_CASE(buffers_equal)
{
    // This seems obvious, but check comment near operator==
    using TigerMail::Buffer;
    const auto buf = Buffer{0x11, 0x22};
    BOOST_CHECK_EQUAL(buf, buf);
}
