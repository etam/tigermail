/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_HEX_HPP
#define TIGERMAIL_COMMON_HEX_HPP

#include <exception>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits>

#include <boost/algorithm/hex.hpp>
#include <boost/algorithm/string/case_conv.hpp>

#include <gsl/gsl_util>
#include <gsl/span>

#include "buffer.hpp"
#include "buffer_span.hpp"
#include "buffer_view.hpp"
#include "fixed_size_buffer_view.hpp"
#include "string_view.hpp"


namespace TigerMail {


class ConversionError
    : public std::exception
{
public:
    ~ConversionError() noexcept override = default;

    const char* what() const noexcept override
    {
        return "conversion failed";
    }
};


template <typename Container>
std::string bin2hex(const Container& bin)
{
    auto hex = std::string(bin.size()*sizeof(typename Container::value_type)*2, '\0');
    boost::algorithm::hex(bin, hex.begin());
    boost::algorithm::to_lower(hex);
    return hex;
}

template <typename T, std::size_t Size>
std::string bin2hex(const T (&array)[Size])
{
    return bin2hex(FixedSizeBufferView<Size>{array});
}

template <typename Container>
std::string bin2zxhex(const Container& bin)
{
    auto hex = std::string(2+bin.size()*sizeof(typename Container::value_type)*2, '\0');
    hex[0] = '0';
    hex[1] = 'x';
    boost::algorithm::hex(bin, hex.begin()+2);
    boost::algorithm::to_lower(hex);
    return hex;
}


void hex2bin(std::string_view hex, BufferSpan bin);

template <typename T = Buffer,
          typename std::enable_if_t<std::is_constructible_v<T, std::size_t>, int> = 0>
T hex2bin(std::string_view hex)
{
    auto bin = T(hex.size() / 2);
    hex2bin(hex, bin);
    return bin;
}

template <typename T,
          typename std::enable_if_t<!std::is_constructible_v<T, std::size_t>, int> = 0>
T hex2bin(std::string_view hex)
{
    auto bin = T{};
    hex2bin(hex, bin);
    return bin;
}

template <typename T = Buffer>
T zxhex2bin(std::string_view hex)
{
    if (!starts_with(hex, "0x")) {
        throw ConversionError{};
    }
    return hex2bin<T>(hex.substr(2));
}

template <typename Int = int>
std::string int2hex(Int i)
{
    auto ss = std::ostringstream{};
    ss << std::hex << i;
    return ss.str();
}

template <typename Int = int>
std::string int2zxhex(Int i)
{
    auto ss = std::ostringstream{};
    ss << "0x" << std::hex << i;
    return ss.str();
}


// based on https://stackoverflow.com/a/21389821
namespace numerical_chars {

inline
std::istream& operator>>(std::istream& i, signed char& c) {
    auto v = int{};
    i >> v;
    c = gsl::narrow<signed char>(v);
    return i;
}

inline
std::istream& operator>>(std::istream& i, unsigned char& c) {
    auto v = unsigned{};
    i >> v;
    c = gsl::narrow<unsigned char>(v);
    return i;
}

} // namespace numerical_chars


template <typename Int = int>
Int hex2int(std::string_view hex)
{
    using namespace numerical_chars;

    auto ss = std::stringstream{};
    ss << std::hex << hex;
    auto result = Int{};
    ss >> result;
    if (!ss) {
        throw ConversionError{};
    }
    return result;
}

template <typename Int = int>
Int zxhex2int(std::string_view hex)
{
    if (hex.size() <= 2 || hex[0] != '0' || hex[1] != 'x') {
        throw ConversionError{};
    }
    return hex2int<Int>(hex);
}


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_HEX_HPP
