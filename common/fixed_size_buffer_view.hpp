/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_FIXED_SIZE_BUFFER_VIEW_HPP
#define TIGERMAIL_COMMON_FIXED_SIZE_BUFFER_VIEW_HPP

#include <algorithm>
#include <cstdint>

#include "fixed_size_buffer.hpp"


namespace TigerMail {


template <std::size_t Size_>
class FixedSizeBufferView
{
public:
    using value_type = std::uint8_t;
    using pointer = const std::uint8_t*;
    using iterator = pointer;
    using const_iterator = pointer;

private:
    pointer m_data;

public:
    static constexpr const std::size_t Size = Size_;

    constexpr
    FixedSizeBufferView(const FixedSizeBuffer<Size_>& buf)
        : m_data{buf.data()}
    {}

    constexpr explicit
    FixedSizeBufferView(pointer data)
        : m_data{data}
    {}

    constexpr
    const_iterator begin() const
    {
        return m_data;
    }

    constexpr
    const_iterator end() const
    {
        return m_data + Size_;
    }

    constexpr
    pointer data() const
    {
        return m_data;
    }

    static constexpr
    std::size_t size()
    {
        return Size_;
    }

    constexpr
    value_type operator[](std::size_t i) const
    {
        return m_data[i];
    }
};

template <std::size_t Size_>
FixedSizeBufferView(const std::uint8_t (&array)[Size_]) -> FixedSizeBufferView<Size_>;


template <std::size_t size>
// constexpr since c++20
bool operator==(FixedSizeBufferView<size> buf1, FixedSizeBufferView<size> buf2)
{
    return std::equal(buf1.begin(), buf1.end(), buf2.begin());
}

template <std::size_t size>
bool operator!=(FixedSizeBufferView<size> buf1, FixedSizeBufferView<size> buf2)
{
    return !(buf1 == buf2);
}


template <std::size_t size>
bool operator==(const FixedSizeBuffer<size>& buf1, FixedSizeBufferView<size> buf2)
{
    return FixedSizeBufferView(buf1) == buf2;
}

template <std::size_t size>
bool operator!=(const FixedSizeBuffer<size>& buf1, FixedSizeBufferView<size> buf2)
{
    return !(buf1 == buf2);
}


template <std::size_t size>
bool operator==(FixedSizeBufferView<size> buf1, const FixedSizeBuffer<size>& buf2)
{
    return buf1 == FixedSizeBufferView(buf2);
}

template <std::size_t size>
bool operator!=(FixedSizeBufferView<size> buf1, const FixedSizeBuffer<size>& buf2)
{
    return !(buf1 == buf2);
}



} // namespace TigerMail

#endif // TIGERMAIL_COMMON_FIXED_SIZE_BUFFER_VIEW_HPP
