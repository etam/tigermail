/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_FIXED_SIZE_BUFFER_HPP
#define TIGERMAIL_COMMON_FIXED_SIZE_BUFFER_HPP

#include <algorithm>
#include <array>
#include <iterator>
#include <stdexcept>
#include <initializer_list>
#include <type_traits>

#include <gsl/gsl_util>

#include "type_traits.hpp"


namespace TigerMail {


template <std::size_t Size_>
class FixedSizeBuffer
    : public std::array<std::uint8_t, Size_>
{
private:
    using BaseClass = std::array<std::uint8_t, Size_>;

public:
    static constexpr const std::size_t Size = Size_;

    constexpr
    FixedSizeBuffer() = default;

    constexpr
    FixedSizeBuffer(std::initializer_list<std::uint8_t> l)
        : FixedSizeBuffer(l.begin(), l.end())
    {}

    template <typename Iterable,
              typename = std::enable_if_t<is_iterable_v<Iterable>>>
    constexpr explicit
    FixedSizeBuffer(const Iterable& i)
        : FixedSizeBuffer(i.begin(), i.end())
    {}

    template <typename Iterator,
              typename = std::enable_if_t<is_iterator_v<Iterator>>>
    constexpr
    FixedSizeBuffer(Iterator first, Iterator last)
        : BaseClass{}
    {
        if (std::distance(first, last) > gsl::narrow<std::ptrdiff_t>(Size_)) {
            throw std::length_error{"input range is too big"};
        }
        std::copy(first, last, this->begin());
    }
};

/*
  It may seem that it's not needed, because std::array already has operator==,
  but without this, the compiler is confused whether it should use operator for
  vectors, or any of operators for comparing FixedSizeBuffer with FixedSizeBufferView.
 */
template <std::size_t Size>
bool operator==(const FixedSizeBuffer<Size>& buf1, const FixedSizeBuffer<Size>& buf2)
{
    return static_cast<const std::array<std::uint8_t, Size>>(buf1)
        == static_cast<const std::array<std::uint8_t, Size>>(buf2);
}

template <std::size_t Size>
bool operator!=(const FixedSizeBuffer<Size>& buf1, const FixedSizeBuffer<Size>& buf2)
{
    return !(buf1 == buf2);
}


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_FIXED_SIZE_BUFFER_HPP
