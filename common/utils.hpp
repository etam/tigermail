/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_UTILS_HPP
#define TIGERMAIL_COMMON_UTILS_HPP

// a place for things that require no additional headers
#include <utility>

namespace TigerMail {


template <typename T>
T copy(const T& obj)
{
    return obj;
}


template <typename T>
struct Constructor
{
    template <typename ...Args>
    T operator()(Args&&... args) const
    {
        return T{std::forward<Args>(args)...};
    }
};

template <typename T>
const auto constructor = Constructor<T>{};


// from https://en.cppreference.com/w/cpp/utility/variant/visit
template <class... Ts>
struct overloaded
    : Ts...
{
    using Ts::operator()...;
};

template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;


/*
  Problem:
  If you have a function that takes a const reference as argument, you can pass
  there a temporary object, like this:

  void f(const Bar& bar);
  f(Bar{}); // ok

  But what to do if you want to have a vector of those?
  You can't make a vector of references. You could use std::reference_wrapper,
  but it can't bind to a temporary. This one can.
 */
template <typename T>
class temporary_reference_wrapper
{
private:
    T* m_ptr;

public:
    temporary_reference_wrapper(T& obj) noexcept
        : m_ptr{&obj}
    {}

    temporary_reference_wrapper(T&& obj) noexcept
        : m_ptr{&obj}
    {}

    operator T&() const noexcept
    {
        return *m_ptr;
    }

    T& get() const noexcept
    {
        return *m_ptr;
    }
};

template <typename T>
temporary_reference_wrapper(T&) -> temporary_reference_wrapper<T>;

template <typename T>
temporary_reference_wrapper(T&&) -> temporary_reference_wrapper<T>;

template <typename T>
bool operator==(temporary_reference_wrapper<T> obj1, temporary_reference_wrapper<T> obj2) { return obj1.get() == obj2.get(); }
template <typename T>
bool operator!=(temporary_reference_wrapper<T> obj1, temporary_reference_wrapper<T> obj2) { return obj1.get() != obj2.get(); }

template <typename T>
bool operator==(temporary_reference_wrapper<T> obj1, T& obj2) { return obj1.get() == obj2; }
template <typename T>
bool operator!=(temporary_reference_wrapper<T> obj1, T& obj2) { return obj1.get() != obj2; }
template <typename T>

bool operator==(T& obj1, temporary_reference_wrapper<T> obj2) { return obj1 == obj2.get(); }
template <typename T>
bool operator!=(T& obj1, temporary_reference_wrapper<T> obj2) { return obj1 != obj2.get(); }

template <typename T>
bool operator<(temporary_reference_wrapper<T> obj1, temporary_reference_wrapper<T> obj2) { return obj1.get() < obj2.get(); }
template <typename T>
bool operator<=(temporary_reference_wrapper<T> obj1, temporary_reference_wrapper<T> obj2) { return obj1.get() <= obj2.get(); }
template <typename T>
bool operator>(temporary_reference_wrapper<T> obj1, temporary_reference_wrapper<T> obj2) { return obj1.get() > obj2.get(); }
template <typename T>
bool operator>=(temporary_reference_wrapper<T> obj1, temporary_reference_wrapper<T> obj2) { return obj1.get() >= obj2.get(); }

template <typename T>
bool operator<(temporary_reference_wrapper<T> obj1, T& obj2) { return obj1.get() < obj2; }
template <typename T>
bool operator<=(temporary_reference_wrapper<T> obj1, T& obj2) { return obj1.get() <= obj2; }
template <typename T>
bool operator>(temporary_reference_wrapper<T> obj1, T& obj2) { return obj1.get() > obj2; }
template <typename T>
bool operator>=(temporary_reference_wrapper<T> obj1, T& obj2) { return obj1.get() >= obj2; }

template <typename T>
bool operator<(T& obj1, temporary_reference_wrapper<T> obj2) { return obj1 < obj2.get(); }
template <typename T>
bool operator<=(T& obj1, temporary_reference_wrapper<T> obj2) { return obj1 <= obj2.get(); }
template <typename T>
bool operator>(T& obj1, temporary_reference_wrapper<T> obj2) { return obj1 > obj2.get(); }
template <typename T>
bool operator>=(T& obj1, temporary_reference_wrapper<T> obj2) { return obj1 >= obj2.get(); }


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_UTILS_HPP
