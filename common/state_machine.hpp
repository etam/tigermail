/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_STATE_MACHINE_HPP
#define TIGERMAIL_COMMON_STATE_MACHINE_HPP

#include <boost/preprocessor/cat.hpp>

#include <boost/sml.hpp>


#define DECLARE_STATE(name) \
    constexpr auto name = ::boost::sml::state<class BOOST_PP_CAT(name,_cls)>


#define DECLARE_EVENT_WITH_DATA(name) \
    struct name; \
    namespace for_transition_table { \
    constexpr auto name = ::boost::sml::event<ev::name>; \
    } \
    struct name

#define DECLARE_EVENT(name) \
    DECLARE_EVENT_WITH_DATA(name) {}


#define DECLARE_FUNCTOR(name, ret_type, ...) \
    extern const struct BOOST_PP_CAT(name,_cls) { \
    ret_type operator()(__VA_ARGS__) const; \
    } name

#define DEFINE_FUNCTOR(name, ret_type, ...) \
    const BOOST_PP_CAT(name,_cls) name; \
    ret_type BOOST_PP_CAT(name,_cls)::operator()(__VA_ARGS__) const


#define DECLARE_ACTION(name, ...) \
    DECLARE_FUNCTOR(name, void, __VA_ARGS__)

#define DEFINE_ACTION(name, ...) \
    DEFINE_FUNCTOR(name, void, __VA_ARGS__)


#define DECLARE_GUARD(name, ...) \
    DECLARE_FUNCTOR(name, bool, __VA_ARGS__)

#define DEFINE_GUARD(name, ...) \
    DEFINE_FUNCTOR(name, bool, __VA_ARGS__)


namespace TigerMail {


struct Logger
{
    template <typename SM, typename Event>
    void log_process_event(const Event&);

    template <typename SM, typename Guard, typename Event>
    void log_guard(const Guard&, const Event&, bool);

    template <typename SM, typename Action, typename Event>
    void log_action(const Action&, const Event&);

    template <typename SM, typename SrcState, typename DstState>
    void log_state_change(const SrcState&, const DstState&);
};


} // namespace TigerMail


#endif // TIGERMAIL_COMMON_STATE_MACHINE_HPP
