/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_MONITOR_HPP
#define TIGERMAIL_COMMON_MONITOR_HPP

#include <shared_mutex>


namespace TigerMail {


template <class T>
class monitor
{
private:
    T obj;
    mutable std::shared_mutex shared_mutex;

public:
    monitor() = default;

    template <typename... Args>
    monitor(Args&&... args)
        : obj(std::forward<Args>(args)...)
    {}

    monitor(const monitor&) = delete;
    monitor& operator=(const monitor&) = delete;

    template <typename F>
    auto exec_ro(F f) const
    {
        std::shared_lock<std::shared_mutex> shared_lock{shared_mutex};
        return f(obj);
    }

    template <typename F>
    auto exec_rw(F f)
    {
        std::unique_lock<std::shared_mutex> unique_lock{shared_mutex};
        return f(obj);
    }
};


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_MONITOR_HPP
