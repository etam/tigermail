/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "string_view.hpp"

#include <algorithm>
#include <cctype>


namespace TigerMail {


bool starts_with(const std::string_view str, const std::string_view prefix)
{
    // https://en.cppreference.com/w/cpp/string/basic_string_view/starts_with
    return str.size() >= prefix.size()
        && str.compare(0, prefix.size(), prefix) == 0;
}

bool ends_with(std::string_view str, std::string_view suffix)
{
    // https://en.cppreference.com/w/cpp/string/basic_string_view/ends_with
    return str.size() >= suffix.size()
        && str.compare(str.size() - suffix.size(), std::string_view::npos, suffix) == 0;
}


bool case_insensitive_equal(const std::string_view str1, const std::string_view str2)
{
    return std::equal(
        str1.begin(), str1.end(),
        str2.begin(), str2.end(),
        // https://en.cppreference.com/w/cpp/string/byte/toupper
        [](unsigned char c1, unsigned char c2) {
            return std::toupper(c1) == std::toupper(c2);
        }
    );
}


} // namespace TigerMail
