/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "hex.hpp"

#include <gsl/gsl_util>


namespace TigerMail {


void hex2bin(std::string_view hex, BufferSpan bin)
{
    if (hex.size() % 2 != 0) {
        throw ConversionError{};
    }
    if (hex.size() / 2 != gsl::narrow<std::size_t>(bin.size())) {
        throw ConversionError{};
    }
    try {
        boost::algorithm::unhex(hex, bin.begin());
    }
    catch (boost::algorithm::non_hex_input&) {
        throw ConversionError{};
    }
}


} // namespace TigerMail
