/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_BUFFER_VIEW_HPP
#define TIGERMAIL_COMMON_BUFFER_VIEW_HPP

#include <gsl/span>


namespace TigerMail {

class Buffer;


class BufferView
    : public gsl::span<const std::uint8_t>
{
private:
    using BaseClass = gsl::span<const std::uint8_t>;

public:
    using BaseClass::BaseClass;
};

bool operator==(const Buffer& buf1, const BufferView buf2);
inline bool operator!=(const Buffer& buf1, const BufferView buf2) { return !(buf1 == buf2); }
bool operator==(const BufferView buf1, const Buffer& buf2);
inline bool operator!=(const BufferView buf1, const Buffer& buf2) { return !(buf1 == buf2); }


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_BUFFER_VIEW_HPP
