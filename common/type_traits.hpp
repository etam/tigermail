/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_TYPE_TRAITS_HPP
#define TIGERMAIL_COMMON_TYPE_TRAITS_HPP

#include <type_traits>


namespace TigerMail {


template <typename T, typename = void>
struct is_iterator
    : std::false_type
{};

template <typename T>
struct is_iterator<
    T,
    std::void_t<
        typename std::iterator_traits<T>::iterator_category
    >
>
    : std::true_type
{};

template <typename T>
inline constexpr bool is_iterator_v = is_iterator<T>::value;


template <typename T, typename = void>
struct is_iterable
    : std::false_type
{};

template <typename T>
struct is_iterable<
    T,
    std::void_t<
        decltype(std::begin(std::declval<T>())),
        decltype(std::end(std::declval<T>()))
    >
>
    : std::true_type
{};

template <typename T>
constexpr bool is_iterable_v = is_iterable<T>::value;


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_TYPE_TRAITS_HPP
