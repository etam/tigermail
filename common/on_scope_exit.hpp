/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_ON_SCOPE_EXIT_HPP
#define TIGERMAIL_COMMON_ON_SCOPE_EXIT_HPP

#include <forward_list>
#include <functional>


namespace TigerMail {


class OnScopeExit
{
public:
    using HandlerT = std::function<void()>;

private:
    std::forward_list<HandlerT> m_handlers;

public:
    OnScopeExit() = default;
    ~OnScopeExit() noexcept;

    OnScopeExit(const OnScopeExit&) = delete;
    OnScopeExit(OnScopeExit&&) = default;
    OnScopeExit& operator=(const OnScopeExit&) = delete;
    OnScopeExit& operator=(OnScopeExit&&) = default;

    void push(HandlerT&& f);
    void cancel() noexcept;
};


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_ON_SCOPE_EXIT_HPP
