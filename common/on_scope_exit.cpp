/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "on_scope_exit.hpp"

namespace TigerMail {


OnScopeExit::~OnScopeExit() noexcept
{
    for (const auto& f : m_handlers) {
        f();
    }
}

void OnScopeExit::push(HandlerT&& f)
{
    m_handlers.push_front(std::move(f));
}

void OnScopeExit::cancel() noexcept
{
    m_handlers.clear();
}


} // namespace TigerMail
