/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_COMMON_STRING_VIEW_HPP
#define TIGERMAIL_COMMON_STRING_VIEW_HPP

#include <string_view>


namespace TigerMail {


// TODO: remove this when updating to C++20
bool starts_with(std::string_view str, std::string_view prefix);
bool ends_with(std::string_view str, std::string_view suffix);

bool case_insensitive_equal(const std::string_view str1, const std::string_view str2);


} // namespace TigerMail

#endif // TIGERMAIL_COMMON_STRING_VIEW_HPP
