# This GDB script enhances debug logging by printing keys used inside signal library.
# example usage: gdb -batch -x this_file --args ./tigermail_test -t client/simple_message_exchange

set height 0
b main
r
disable breakpoints 1

python
class AsHex(gdb.Function):
  def __init__ (self):
    super().__init__ ("as_hex")

  def invoke(self, addr, len):
    ptr = addr.reinterpret_cast(gdb.lookup_type("unsigned char").pointer())
    return bytes(ptr[i] for i in range(len)).hex()

AsHex()
end


# ec_public/private_key length is in DJB_KEY_LEN in curve.c
# ratchet key length is hardcoded in ratchet_root_key_create_chain
# both are equal to 32 bytes


b session_state_get_receiver_chain_key
commands
echo sender_ephemeral (ec_public_key): 
p $as_hex(sender_ephemeral->data, 32)
set $chain = state->receiver_chain_head
while $chain != 0
    echo sender_ratchet_key (ec_public_key): 
    p $as_hex($chain->sender_ratchet_key->data, 32)
    echo chain_key (ratchet_chain_key): 
    p $as_hex($chain->chain_key->key, 32)
    set $chain = $chain->next
end
#bt
c
end

b session_state_add_receiver_chain
commands
echo sender_ratchet_key (ec_public_key): 
p $as_hex(sender_ratchet_key->data, 32)
echo chain_key (ratchet_chain_key): 
p $as_hex(chain_key->key, 32)
#bt
c
end

b session_state_set_receiver_chain_key
commands
echo sender_ephemeral (ec_public_key): 
p $as_hex(sender_ephemeral->data, 32)
echo chain_key (ratchet_chain_key): 
p $as_hex(chain_key->key, 32)
#bt
c
end

b session_state_set_sender_chain
commands
echo public key of sender_ratchet_key_pair (ec_public_key): 
p $as_hex(sender_ratchet_key_pair->public_key->data, 32)
echo chain_key (ratchet_chain_key): 
p $as_hex(chain_key->key, 32)
#bt
c
end

b session_state_set_sender_chain_key
commands
if state->has_sender_chain
    echo chain_key (ratchet_chain_key): 
    p $as_hex(chain_key->key, 32)
end
#bt
c
end

b session_state_get_sender_ratchet_key
commands
if state->sender_chain.sender_ratchet_key_pair
    echo sender ratchet key (ec_public_key): 
    p $as_hex(state->sender_chain.sender_ratchet_key_pair->public_key->data, 32)
end
#bt
c
end

b session_state_get_sender_ratchet_key_pair
commands
if state->sender_chain.sender_ratchet_key_pair
    echo public key of sender ratchet key (ec_public_key): 
    p $as_hex(state->sender_chain.sender_ratchet_key_pair->public_key->data, 32)
end
#bt
c
end


c
q
