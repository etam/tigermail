/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cerrno>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <filesystem>
#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>

#include <sys/stat.h>

#include <boost/asio/io_context.hpp>
#include <boost/asio/local/stream_protocol.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/asio/steady_timer.hpp>

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>

#include <gsl/pointers>
#include <gsl/gsl_util>

#include <libdevcrypto/SecretStore.h>

#include <sqlite_modern_cpp.h>

#include "../common/hex.hpp"
#include "../common/string_view.hpp"
#include "../common/utils.hpp"

#include "../ethereum/common.hpp"
#include "../ethereum/rpc_client.hpp"
#include "../ethereum/tigermail_contract.hpp"

#include "../pop3_smtp_common/server/server.hpp"
#include "../pop3_smtp_common/server/session_factory.hpp"

#include "../pop3/limits.hpp"
#include "../pop3/messages_store.hpp"
#include "../pop3/session_handler.hpp"

#include "../signal/context.hpp"

#include "../smtp/limits.hpp"
#include "../smtp/session_handler.hpp"

#include "../swarm/ipc_client.hpp"
#include "../swarm/tcp_client.hpp"

#include "client.hpp"
#include "client_state/sqlite.hpp"
#include "mime.hpp"
#include "options.hpp"

namespace asio = boost::asio;
using namespace TigerMail;


namespace {


dev::Secret get_ethereum_secret(
    const std::filesystem::path& ethereum_data_dir,
    const Ethereum::Address& user_address,
    const std::string& password)
{
    // TODO: remove .string() when there's compatibility
    const auto ethereum_secret_store = dev::SecretStore{(ethereum_data_dir / "keystore").string()};
    return dev::Secret{
        ethereum_secret_store.secret(
            dev::Address(user_address.data(), dev::Address::ConstructFromPointer),
            [&] { return password; })};
}


void log_error_and_send_system_message(
    POP3::MessagesStore& incoming_messages_store,
    const Ethereum::Address& user_address,
    std::string&& msg)
{
    BOOST_LOG_TRIVIAL(error) << msg;

    incoming_messages_store.push_message(
        format_system_message(user_address, std::move(msg)));
}


void smtp_message_received(
    asio::io_context& io_context,
    Client& tigermail_client,
    POP3::MessagesStore& incoming_messages_store,
    SMTP::Message&& smtp_message)
try {
    using namespace TigerMail;

    auto prepared_message = validate_and_prepare_outgoing_message(
        std::move(smtp_message),
        tigermail_client.ethereum_address());

    const auto message_content = std::make_shared<Buffer>(std::move(prepared_message.content));

    for (auto&& recipient_ethereum_address : prepared_message.recipients) {
        asio::spawn(
            io_context,
            [&tigermail_client,
             &incoming_messages_store,
             recipient_ethereum_address = std::move(recipient_ethereum_address),
             message_content]
            (asio::yield_context yield) {
                try {
                    tigermail_client.send_message(
                        recipient_ethereum_address,
                        *message_content,
                        yield);
                }
                catch (std::exception& e) {
                    log_error_and_send_system_message(
                        incoming_messages_store,
                        tigermail_client.ethereum_address(),
                        [&] {
                            auto msg = std::ostringstream{};
                            msg << "Failed sending message to \"" << bin2zxhex(recipient_ethereum_address) << "\", because \"" << e.what() << '"';
                            return msg.str();
                        }());
                }
            });
    }
}
catch (std::exception& e) {
    log_error_and_send_system_message(
        incoming_messages_store,
        tigermail_client.ethereum_address(),
        [&] {
            auto msg = std::ostringstream{};
            msg << "Failed sending message, because \"" << e.what() << '"';
            return msg.str();
        }());
}


void fetch_new_messages(
    asio::io_context& io_context,
    Client& tigermail_client,
    POP3::MessagesStore& incoming_messages_store,
    asio::steady_timer& timer,
    std::chrono::seconds interval,
    const boost::system::error_code& e)
{
    using namespace TigerMail;

    if (e == asio::error::operation_aborted) {
        return;
    }

    asio::spawn(
        io_context,
        [&, interval](asio::yield_context yield) {
            const auto messages =
                [&] {
                    try {
                        return tigermail_client.receive_messages(yield);
                    }
                    catch (std::exception& e) {
                        log_error_and_send_system_message(
                            incoming_messages_store,
                            tigermail_client.ethereum_address(),
                            [&] {
                                auto msg = std::ostringstream{};
                                msg << "Failed receiving messages, because \"" << e.what() << '"';
                                return msg.str();
                            }());
                        return std::vector<Message>{};
                    }
                }();

            for (const auto& message : messages) {
                try {
                    if (auto is_valid = is_incoming_message_valid(message); !is_valid.value) {
                        throw ParsingError{std::move(is_valid.why)};
                    }
                    incoming_messages_store.push_message(std::string{
                            reinterpret_cast<const char*>(message.content.data()),
                            message.content.size()});
                }
                catch (std::exception& e) {
                    log_error_and_send_system_message(
                        incoming_messages_store,
                        tigermail_client.ethereum_address(),
                        [&] {
                            auto msg = std::ostringstream{};
                            msg << "Message from " << bin2zxhex(message.from) << " rejected, because \"" << e.what() << '"';
                            return msg.str();
                        }());
                }
            }

            timer.expires_after(interval);
            timer.async_wait(
                [&, interval](const boost::system::error_code& e) {
                    fetch_new_messages(
                        io_context,
                        tigermail_client,
                        incoming_messages_store,
                        timer,
                        interval,
                        e);
                });
        });
}


void rotate_keys(
    gsl::not_null<sqlite::database*> database,
    asio::io_context& io_context,
    Client& tigermail_client,
    POP3::MessagesStore& incoming_messages_store,
    asio::steady_timer& timer,
    std::chrono::seconds interval,
    const boost::system::error_code& e)
{
    using namespace TigerMail;

    if (e == asio::error::operation_aborted) {
        return;
    }

    asio::spawn(
        io_context,
        [&, database, interval](asio::yield_context yield) {
            try {
                tigermail_client.rotate_keys(yield);

                *database <<
                    "update tigermail_keys_rotation "
                    "set value = ? "
                    "where name = \"last_rotation\";"
                          << std::chrono::duration_cast<std::chrono::seconds>(
                              std::chrono::system_clock::now().time_since_epoch()).count();
            }
            catch (std::exception& e) {
                log_error_and_send_system_message(
                    incoming_messages_store,
                    tigermail_client.ethereum_address(),
                    [&] {
                        auto msg = std::ostringstream{};
                        msg << "Failed to rotate keys: " << e.what() << '"';
                        return msg.str();
                    }());
            }

            timer.expires_after(interval);
            timer.async_wait(
                [&, interval](const boost::system::error_code& e) {
                    rotate_keys(
                        database,
                        io_context,
                        tigermail_client,
                        incoming_messages_store,
                        timer,
                        interval,
                        e);
                });
        });
}

void first_rotate_keys(
    gsl::not_null<sqlite::database*> database,
    asio::io_context& io_context,
    Client& tigermail_client,
    POP3::MessagesStore& incoming_messages_store,
    asio::steady_timer& timer,
    std::chrono::seconds interval)
{
    using system_clock = std::chrono::system_clock;

    *database <<
        "create table if not exists "
        "tigermail_keys_rotation( "
        "  name text not null primary key, "
        "  value int not null);";
    *database <<
        "insert or ignore "
        "into tigermail_keys_rotation(name, value) "
        "values (\"last_rotation\", 0);";

    const auto ts_of_last_rotation =
        [&] {
            auto ts = int{};
            *database <<
                "select value "
                "from tigermail_keys_rotation "
                "where name = \"last_rotation\";"
                      >> ts;
            return system_clock::time_point{std::chrono::seconds{ts}};
        }();

    const auto interval_since_last_rotation = system_clock::now() - ts_of_last_rotation;

    if (interval_since_last_rotation >= interval) {
        rotate_keys(
            database,
            io_context,
            tigermail_client,
            incoming_messages_store,
            timer,
            interval,
            {});
    }
    else {
        const auto interval_to_next_rotation = interval - interval_since_last_rotation;
        BOOST_LOG_TRIVIAL(info)
            << "Will rotate keys in "
            << std::chrono::duration_cast<std::chrono::seconds>(interval_to_next_rotation).count() << " seconds";
        timer.expires_after(interval_to_next_rotation);
        timer.async_wait(
            [&, interval](const boost::system::error_code& e) {
                rotate_keys(
                    database,
                    io_context,
                    tigermail_client,
                    incoming_messages_store,
                    timer,
                    interval,
                    e);
            });
    }
}


void setup_logging(boost::log::trivial::severity_level log_level)
{
    namespace log = boost::log;
    // https://stackoverflow.com/a/17473875
    log::add_common_attributes();
    log::register_simple_formatter_factory<log::trivial::severity_level, char>("Severity");
    log::add_console_log(std::cout, log::keywords::format = "[%TimeStamp%] %Severity%: %Message%");
    log::core::get()->set_filter
        (
            log::trivial::severity >= log_level
        );
}


} // namespace


int main(int argc, char* argv[])
try {
    using POP3_SMTP_common::server::LocalEndpoint;
    using POP3_SMTP_common::server::Server;
    using POP3_SMTP_common::server::make_session_factory;

    const auto options = parse_options(argc, argv);

    setup_logging(options.log_level);

    const auto tigermail_self_address = address_to_tigermail(options.user_ethereum_address);

    // data dir

    const auto received_mails_dir = options.tigermail_data_dir / "received";
    const auto sqlite_database_file = options.tigermail_data_dir / "tigermail.db";
    std::filesystem::create_directories(received_mails_dir);
    if (const int result = chmod(options.tigermail_data_dir.c_str(), S_IRUSR|S_IWUSR|S_IXUSR); result < 0) {
        auto msg = std::ostringstream{};
        msg << "chmod failed: " << std::strerror(errno);
        throw std::runtime_error{msg.str()};
    }

    // database

    auto database = sqlite::database{sqlite_database_file.string()};

    // asio

    auto io_context = asio::io_context{};

    // ethereum

    auto ethereum_transport_protocol_client = Ethereum::RawTransportProtocolClient{
        io_context,
        boost::asio::local::stream_protocol::endpoint{options.ethereum_data_dir / "geth.ipc"}};
    // auto ethereum_transport_protocol_client = Ethereum::HTTPTransportProtocolClient{
    //     io_context,
    //     boost::asio::ip::tcp::endpoint(boost::asio::ip::address_v4::loopback(), 7545)};
    auto ethereum_client = Ethereum::RPCClient{gsl::not_null{&ethereum_transport_protocol_client}};

    auto tigermail_contract = Ethereum::TigerMailContract{
        gsl::not_null{&ethereum_client},
        copy(options.contract_address),
        copy(options.user_ethereum_address),
        get_ethereum_secret(
            options.ethereum_data_dir,
            options.user_ethereum_address,
            options.ethereum_password)};

    // invalidate?

    if (options.invalidate) {
        std::cout << "You're about to invalidate your key.\n"
                  << "This means you won't be able to use this key to exchange messages on this contract.\n"
                  << "This operation is irreversible!\n"
                  << "If you're sure you want to do this, please enter your ethereum address here:\n";

        auto confirm_address = std::string{};
        std::cin >> confirm_address;

        if (zxhex2bin<Ethereum::Address>(confirm_address) != options.user_ethereum_address) {
            std::cout << "Provided address does not match\n";
            return 1;
        }

        asio::spawn(
            io_context,
            [&](asio::yield_context yield) {
                ethereum_transport_protocol_client.connect(yield);
                tigermail_contract.setup_nonce(yield);
                const auto tx_id = tigermail_contract.invalidate(yield);
                std::cout << "Key invalidated; tx id = " << bin2zxhex(tx_id) << '\n';
            });
        io_context.run();
        return 0;
    }

    // swarm

    auto swarm_client = Swarm::TCPClient{io_context};
    const auto swarm_endpoint = Swarm::get_swarm_tcp_endpoint(Swarm::get_bzz_info(options.swarm_socket.string()));

    // tigermail

    auto signal_context = Signal::Context{};
    auto signal_store_context = Signal::SqliteStoreContext{gsl::not_null{&database}, signal_context.get()};
    auto tigermail_client_state = SqliteClientState{gsl::not_null{&database}};
    auto tigermail_client = Client{
        gsl::not_null{&tigermail_contract},
        gsl::not_null{&swarm_client},
        copy(options.user_ethereum_address),
        copy(options.ethereum_password),
        gsl::not_null{&signal_context},
        gsl::not_null{&signal_store_context},
        gsl::not_null{&tigermail_client_state}
    };

    // pop3

    auto incoming_messages_store = POP3::MessagesStore{received_mails_dir};

    auto pop3_endpoint = LocalEndpoint{options.tigermail_data_dir / "pop3.socket"};

    const auto pop3_session_factory = make_session_factory<POP3::SessionHandler>(
        [&](std::string_view user, std::string_view) {
            return case_insensitive_equal(user, tigermail_self_address);
        },
        gsl::not_null{&incoming_messages_store},
        POP3::Limits{});

    auto pop3_server = Server{
        io_context,
        asio::local::stream_protocol::acceptor{io_context, pop3_endpoint},
        gsl::not_null{&pop3_session_factory}};

    // smtp

    auto smtp_endpoint = LocalEndpoint{options.tigermail_data_dir / "smtp.socket"};

    const auto smtp_session_factory = make_session_factory<SMTP::SessionHandler>(
        [&](std::string_view reverse_path) {
            return case_insensitive_equal(reverse_path, tigermail_self_address);
        },
        [&](SMTP::Message&& message) {
            ::smtp_message_received(
                io_context,
                tigermail_client,
                incoming_messages_store,
                std::move(message));
        },
        SMTP::Limits{});

    auto smtp_server = Server{
        io_context,
        asio::local::stream_protocol::acceptor{io_context, smtp_endpoint},
        gsl::not_null{&smtp_session_factory}};

    // timers

    auto fetch_new_messages_timer = asio::steady_timer{io_context};
    auto rotate_keys_timer = asio::steady_timer{io_context};

    // handle stopping

    auto signals = asio::signal_set{io_context, SIGINT, SIGTERM};
    signals.async_wait(
        [&](boost::system::error_code /*ec*/, int /*signal*/) {
            pop3_server.stop();
            smtp_server.stop();
            fetch_new_messages_timer.cancel();
            rotate_keys_timer.cancel();
        });

    // what to do first

    auto exit_code = int{0};

    asio::spawn(
        io_context,
        [&](asio::yield_context yield) {
            ethereum_transport_protocol_client.connect(yield);

            // Don't check old events, if we're freshly started
            if (tigermail_client_state.get_start_checking_blocks_at() == 0) {
                tigermail_client_state.set_start_checking_blocks_at(
                    ethereum_client.eth_blockNumber(yield));
            }

            if (tigermail_contract.am_I_invalidated(yield)) {
                BOOST_LOG_TRIVIAL(error) << "User address is invalidated";
                pop3_server.stop();
                smtp_server.stop();
                fetch_new_messages_timer.cancel();
                rotate_keys_timer.cancel();
                signals.cancel();
                exit_code = 1;
                return;
            }

            swarm_client.connect(swarm_endpoint, yield);

            tigermail_contract.setup_nonce(yield);

            first_rotate_keys(
                gsl::not_null{&database},
                io_context,
                tigermail_client,
                incoming_messages_store,
                rotate_keys_timer,
                options.rotate_keys_interval);

            fetch_new_messages(
                io_context,
                tigermail_client,
                incoming_messages_store,
                fetch_new_messages_timer,
                options.fetch_new_messages_interval,
                {});
        });

    // now really run it!

    // #pragma omp parallel
    io_context.run();

    return exit_code;
}
catch (std::exception& e) {
    BOOST_LOG_TRIVIAL(error) << "Unhandled exception: " << e.what();
    return 1;
}
