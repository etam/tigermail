/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_TIGERMAIL_CLIENT_HPP
#define TIGERMAIL_TIGERMAIL_CLIENT_HPP

#include <any>

#include <gsl/pointers>

#include "../common/buffer.hpp"
#include "../common/buffer_view.hpp"

#include "../ethereum/common.hpp"

#include "../signal/address.hpp"
#include "../signal/common.hpp"


namespace TigerMail {

namespace Ethereum {
class TigerMailContractInterface;
} // namespace Ethereum

namespace Signal {
class Context;
class StoreContext;
} // namespace Signal

namespace Swarm {
class SwarmClientInterface;
} // namespace Swarm

class ClientStateInterface;


struct Message
{
    Ethereum::Address from;
    Ethereum::Address to;
    Buffer content;
};

bool operator==(const Message& msg1, const Message& msg2);

inline
bool operator!=(const Message& msg1, const Message& msg2)
{
    return !(msg1 == msg2);
}


class ClientError
    : public std::exception
{
private:
    const char* m_what;

public:
    explicit
    ClientError(const char* what) noexcept
        : m_what{what}
    {}

    ~ClientError() noexcept override = default;

    const char* what() const noexcept override
    {
        return m_what;
    }
};


// This trick allows to have "private methods" without exposing them in header.
struct ClientData
{
    gsl::not_null<Ethereum::TigerMailContractInterface*> m_tigermail_contract;
    gsl::not_null<Swarm::SwarmClientInterface*> m_swarm;
    Ethereum::Address m_ethereum_address;
    std::string m_ethereum_password;
    gsl::not_null<Signal::Context*> m_signal_context;
    gsl::not_null<Signal::StoreContext*> m_signal_store_context;
    gsl::not_null<ClientStateInterface*> m_state;

    ClientData(
        gsl::not_null<Ethereum::TigerMailContractInterface*> tigermail_contract,
        gsl::not_null<Swarm::SwarmClientInterface*> swarm,
        Ethereum::Address&& ethereum_address,
        std::string&& ethereum_password,
        gsl::not_null<Signal::Context*> signal_context,
        gsl::not_null<Signal::StoreContext*> signal_store_context,
        gsl::not_null<ClientStateInterface*> state);
};

/*
  This class is responsible for encryption, decryption and transport of raw data.
  It could be used for other types of messaging standards, not related to e-mail.
 */
class Client
    : private ClientData
{
public:
    Client(
        gsl::not_null<Ethereum::TigerMailContractInterface*> tigermail_contract,
        gsl::not_null<Swarm::SwarmClientInterface*> swarm,
        Ethereum::Address&& ethereum_address,
        std::string&& ethereum_password,
        gsl::not_null<Signal::Context*> signal_context,
        gsl::not_null<Signal::StoreContext*> signal_store_context,
        gsl::not_null<ClientStateInterface*> state
    );

    const Ethereum::Address& ethereum_address() const
    {
        return m_ethereum_address;
    }

    void rotate_keys(std::any yield);
    void send_message(const Ethereum::Address& to, BufferView content, std::any yield);
    std::vector<Message> receive_messages(std::any yield);
};


} // namespace TigerMail

#endif // TIGERMAIL_TIGERMAIL_CLIENT_HPP
