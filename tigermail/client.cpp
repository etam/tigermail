/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "client.hpp"

#include <chrono>
#include <tuple>
#include <type_traits>

#include <boost/log/trivial.hpp>

#include <session_state.h>

#include "../common/fixed_size_buffer.hpp"
#include "../common/hex.hpp"
#include "../common/retrying.hpp"

#include "../ethereum/tigermail_contract_interface.hpp"

#include "../signal/common.hpp"
#include "../signal/context.hpp"
#include "../signal/crypto_provider.hpp"
#include "../signal/crypto_provider_impl/aes.hpp"
#include "../signal/crypto_provider_impl/hmac_sha256.hpp"
#include "../signal/key_bundle.hpp"
#include "../signal/signalpp/curve.hpp"
#include "../signal/signalpp/hkdf.hpp"
#include "../signal/signalpp/key_helper.hpp"
#include "../signal/signalpp/protocol.hpp"
#include "../signal/signalpp/ratchet.hpp"
#include "../signal/signalpp/session_builder.hpp"
#include "../signal/signalpp/session_cipher.hpp"
#include "../signal/signalpp/session_pre_key.hpp"
#include "../signal/signalpp/signal_protocol.hpp"

#include "../swarm/swarm_client_interface.hpp"

#include "client_state/interface.hpp"


namespace TigerMail {


bool operator==(const Message& msg1, const Message& msg2)
{
    return std::tie(msg1.from, msg1.to, msg1.content) == std::tie(msg2.from, msg2.to, msg2.content);
}


Signal::Address ethereum_address_to_signal(const Ethereum::Address& ethereum_address)
{
    return {
        bin2zxhex(ethereum_address),
        0
    };
}


namespace {


const auto g_pre_keys_in_bundle = 10;


auto get_timestamp()
{
    using std::chrono::system_clock;
    using std::chrono::duration_cast;
    using std::chrono::milliseconds;

    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

template <typename IntT = int>
IntT good_enough_random_int(IntT min, IntT max, Signal::CryptoProvider& crypto_provider)
{
    auto buff = FixedSizeBuffer<sizeof(IntT)>{};
    crypto_provider.random(buff);
    auto result = IntT{};
    std::memcpy(&result, buff.data(), sizeof(IntT));
    return (result % (max - min + 1)) + min;
}

constexpr const auto version_size = std::size_t{1};
constexpr const auto encrypted_id_size = std::size_t{48};
constexpr const auto ratchet_pub_key_size = std::size_t{33};
constexpr const auto hmac_size = std::size_t{32};

const auto swarm_key_bundle_feed_name = "TigerMail_key_bundle";
const auto message_id_version = std::uint8_t{0x01};


void generate_signed_pre_key(ClientData& client)
{
    const auto key_pair = Signal::signal_protocol_identity_get_key_pair(client.m_signal_store_context->get());

    const auto key_id = client.m_state->get_start_signed_pre_key_generation_at();
    #ifdef FUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION
    using timestamp_t = std::invoke_result_t<decltype(get_timestamp)>;
    const auto timestamp = timestamp_t{0};
    #else
    const auto timestamp = get_timestamp();
    #endif

    const auto signed_pre_key = Signal::signal_protocol_key_helper_generate_signed_pre_key(
        key_pair.get(), key_id, timestamp, client.m_signal_context->get());

    Signal::signal_protocol_signed_pre_key_store_key(client.m_signal_store_context->get(), signed_pre_key.get());

    client.m_state->start_signed_pre_key_generation_at_increase();
}

void generate_pre_key(ClientData& client)
{
    const auto key_id = client.m_state->get_start_pre_key_generation_at();
    const auto key_pair = Signal::curve_generate_key_pair(client.m_signal_context->get());
    const auto pre_key = Signal::session_pre_key_create(key_id, key_pair.get());
    Signal::signal_protocol_pre_key_store_key(client.m_signal_store_context->get(), pre_key.get());
    client.m_state->start_pre_key_generation_at_increase();
}

void generate_new_keys(ClientData& client)
{
    generate_signed_pre_key(client);
    for (int i = 0, end = g_pre_keys_in_bundle; i < end; ++i) {
        generate_pre_key(client);
    }
}

void delete_old_keys(ClientData& client)
{
    Signal::signal_protocol_signed_pre_key_remove_key(
        client.m_signal_store_context->get(), client.m_state->get_start_signed_pre_key_generation_at() - 3);

    const auto start_pre_key_id = Signal::PreKeyIdT{client.m_state->get_start_pre_key_generation_at() - 3*g_pre_keys_in_bundle};
    const auto end_pre_key_id = start_pre_key_id + g_pre_keys_in_bundle;
    for (auto pre_key_id = start_pre_key_id; pre_key_id != end_pre_key_id; ++pre_key_id) {
        // don't use '<' in end condition, because numbers might wrap
        Signal::signal_protocol_pre_key_remove_key(client.m_signal_store_context->get(), pre_key_id);
    }
}

Signal::KeyBundle generate_key_bundle(const ClientData& client)
{
    auto key_bundle = Signal::KeyBundle{};

    key_bundle.registration_id = Signal::signal_protocol_identity_get_local_registration_id(client.m_signal_store_context->get());
    key_bundle.device_id = 0;

    {
        const auto key_pair = Signal::signal_protocol_identity_get_key_pair(client.m_signal_store_context->get());
        key_bundle.identity_key = Signal::Ref{ratchet_identity_key_pair_get_public(key_pair.get())};
    }

    {
        const Signal::PreKeyIdT key_id = client.m_state->get_start_signed_pre_key_generation_at() - 1;
        const auto signed_pre_key = Signal::signal_protocol_signed_pre_key_load_key(client.m_signal_store_context->get(), key_id);
        const ec_key_pair* key_pair = session_signed_pre_key_get_key_pair(signed_pre_key.get());

        key_bundle.signed_pre_key.id = key_id;
        key_bundle.signed_pre_key.key = Signal::Ref{ec_key_pair_get_public(key_pair)};

        const auto* signature_ptr = session_signed_pre_key_get_signature(signed_pre_key.get());
        const auto signature_size = session_signed_pre_key_get_signature_len(signed_pre_key.get());

        key_bundle.signed_pre_key_signature = Buffer(signature_ptr, signature_ptr + signature_size);
    }

    // TODO: ranges?
    key_bundle.pre_keys.reserve(g_pre_keys_in_bundle);
    for (int i = 0, end = g_pre_keys_in_bundle; i < end; ++i) {
        const Signal::PreKeyIdT key_id = client.m_state->get_start_pre_key_generation_at() - g_pre_keys_in_bundle + i;
        const auto pre_key = Signal::signal_protocol_pre_key_load_key(client.m_signal_store_context->get(), key_id);
        const ec_key_pair* key_pair = session_pre_key_get_key_pair(pre_key.get());

        key_bundle.pre_keys.push_back({
                .id = key_id,
                .key = Signal::Ref{ec_key_pair_get_public(key_pair)}
            });
    }

    return key_bundle;
}

void publish_key_bundle(ClientData& client, std::any yield)
{
    const auto serialized = serializeKeyBundle(generate_key_bundle(client));

    client.m_swarm->feed_update(client.m_ethereum_address, client.m_ethereum_password, swarm_key_bundle_feed_name, serialized, yield);
}

Signal::Ref<session_pre_key_bundle> process_pre_key_bundle(
    ClientData& client, const Ethereum::Address& ether_address, const Signal::Address& signal_address, std::any yield)
{
    const auto opt_key_bundle_serialized = retrying(
        [&](std::any yield) {
            return client.m_swarm->feed_get(ether_address, swarm_key_bundle_feed_name, yield);
        }, yield);
    if (opt_key_bundle_serialized == std::nullopt) {
        throw ClientError{"key bundle not found"};
    }

    const auto key_bundle = Signal::deserializeKeyBundle(*opt_key_bundle_serialized, client.m_signal_context->get());

    const bool has_pre_key = key_bundle.pre_keys.size() > 0;
    const auto pre_key_idx =
        has_pre_key
        ? good_enough_random_int<std::size_t>(0, key_bundle.pre_keys.size()-1, client.m_signal_context->get_crypto_provider())
        : std::size_t{0};

    auto builder = Signal::session_builder_create(
        client.m_signal_store_context->get(), signal_address.as_signal(), client.m_signal_context->get());

    const auto signal_bundle = Signal::session_pre_key_bundle_create(
        key_bundle.registration_id,
        key_bundle.device_id,
        has_pre_key ? key_bundle.pre_keys[pre_key_idx].id : 0,
        has_pre_key ? key_bundle.pre_keys[pre_key_idx].key.get() : nullptr,
        key_bundle.signed_pre_key.id,
        key_bundle.signed_pre_key.key.get(),
        key_bundle.signed_pre_key_signature,
        key_bundle.identity_key.get()
    );

    Signal::session_builder_process_pre_key_bundle(builder.get(), signal_bundle.get());

    return signal_bundle;
}

Swarm::SwarmHash encrypt_and_store_message(
    ClientData& client, const Signal::Address& signal_address, BufferView message, std::any yield)
{
    const auto cipher = Signal::session_cipher_create(
        client.m_signal_store_context->get(), signal_address.as_signal(), client.m_signal_context->get());
    const auto encrypted_message = Signal::session_cipher_encrypt(cipher.get(), message);
    const signal_buffer* serialized = ciphertext_message_get_serialized(encrypted_message.get());
    BOOST_LOG_TRIVIAL(debug) << "Storing message on swarm...";
    const auto hash = client.m_swarm->bzz_post({signal_buffer_const_data(serialized),
                                                gsl::narrow<BufferView::index_type>(signal_buffer_len(serialized))},
                                               yield);
    BOOST_LOG_TRIVIAL(debug) << "Stored message on swarm, hash=" << bin2hex(hash);
    return hash;
}

ratchet_message_keys get_message_keys_for_new_session(
    ClientData& client, const ec_public_key* pub_key, const ec_private_key* priv_key)
{
    BOOST_LOG_TRIVIAL(debug)
        << "Creating message keys for new sesssion.\n"
        << "pub_key (ec_public_key): " << bin2hex(BufferView{Signal::ec_public_key_serialize(pub_key)}) << '\n'
        << "public key of priv_key (ec_public_key): " << bin2hex(BufferView{Signal::ec_public_key_serialize(Signal::curve_generate_public_key(priv_key).get())});

    const auto shared_key = Signal::curve_calculate_agreement(pub_key, priv_key);
    const auto hkdf = Signal::hkdf_create(3, client.m_signal_context->get());
    const auto chain_key = Signal::ratchet_chain_key_create(
        hkdf.get(), shared_key, 1, client.m_signal_context->get());

    BOOST_LOG_TRIVIAL(debug)
        << "Created ratchet chain key (ratchet_chain_key): " << bin2hex(BufferView{Signal::ratchet_chain_key_get_key(chain_key.get())});

    return Signal::ratchet_chain_key_get_message_keys(chain_key.get());
}

void publish_message_id_on_ethereum(
    ClientData& client,
    Swarm::SwarmHashView swarm_id,
    const ratchet_message_keys* message_keys,
    const ec_public_key* ratchet_pub_key,
    std::any yield)
{
    const auto encrypted_id = Signal::CryptoProviderImpl::encrypt(
        {message_keys->cipher_key, sizeof(message_keys->cipher_key)},
        {message_keys->iv, sizeof(message_keys->iv)},
        swarm_id);
    Ensures(encrypted_id.size() == encrypted_id_size);

    const auto serialized_ratchet_pub_key = Signal::ec_public_key_serialize(ratchet_pub_key);
    Ensures(serialized_ratchet_pub_key.size() == ratchet_pub_key_size);

    BOOST_LOG_TRIVIAL(debug)
        << "Calculating HMAC for publishing.\n"
        << "mac_key: " << bin2hex(message_keys->mac_key) << '\n'
        << "encrypted_id: " << bin2hex(encrypted_id) << '\n'
        << "serialized_ratchet_pub_key (ec_public_key): " << bin2hex(BufferView{serialized_ratchet_pub_key});

    const auto hmac =
        [&] {
            auto hmac_counter = std::unique_ptr<Signal::CryptoProviderImpl::HmacSha256Interface>{
                Signal::CryptoProviderImpl::HmacSha256Interface::create(
                    {message_keys->mac_key, sizeof(message_keys->mac_key)})};
            hmac_counter->update(encrypted_id);
            hmac_counter->update(serialized_ratchet_pub_key);
            return hmac_counter->final();
        }();
    static_assert(hmac.size() == hmac_size);

    BOOST_LOG_TRIVIAL(debug) << "Calculated HMAC for publishing: " << bin2hex(hmac);

    auto message_id = FixedSizeBuffer<version_size + encrypted_id_size + ratchet_pub_key_size + hmac_size>{};
    message_id[0] = message_id_version;
    std::copy(encrypted_id.begin(), encrypted_id.end(),
              message_id.begin() + version_size);
    std::copy(serialized_ratchet_pub_key.begin(), serialized_ratchet_pub_key.end(),
              message_id.begin() + version_size + encrypted_id_size);
    std::copy(hmac.begin(), hmac.end(),
              message_id.begin() + version_size + encrypted_id_size + ratchet_pub_key_size);

    BOOST_LOG_TRIVIAL(debug) << "Posting message id on ethereum...";
    const auto tx_id = client.m_tigermail_contract->postMessageId(message_id, yield);
    BOOST_LOG_TRIVIAL(debug) << "Posted message id on ethereum; tx id = " << bin2zxhex(tx_id);
}

std::optional<Swarm::SwarmHash> verify_and_decrypt_message_id(
    const FixedSizeBufferView<encrypted_id_size> encrypted_id,
    const FixedSizeBufferView<ratchet_pub_key_size> serialized_ratchet_pub_key,
    const FixedSizeBufferView<hmac_size> hmac,
    const ratchet_message_keys* message_keys)
{
    BOOST_LOG_TRIVIAL(debug)
        << "Calculating HMAC for verification.\n"
        << "mac_key: " << bin2hex(message_keys->mac_key) << '\n'
        << "encrypted_id: " << bin2hex(encrypted_id) << '\n'
        << "serialized_ratchet_pub_key (ec_public_key): " << bin2hex(serialized_ratchet_pub_key);

    const auto calculated_hmac =
        [&] {
            auto hmac_counter = std::unique_ptr<Signal::CryptoProviderImpl::HmacSha256Interface>{
                Signal::CryptoProviderImpl::HmacSha256Interface::create(
                    {message_keys->mac_key, sizeof(message_keys->mac_key)})};
            hmac_counter->update(encrypted_id);
            hmac_counter->update(serialized_ratchet_pub_key);
            return hmac_counter->final();
        }();

    #ifdef FUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION
    if (hmac[0] == 1) {
    #else
    if (hmac != calculated_hmac) {
    #endif
        BOOST_LOG_TRIVIAL(debug)
            << "HMAC doesn't match.\n"
            << "calculated " << bin2hex(calculated_hmac) << '\n'
            << "received " << bin2hex(hmac);
        return std::nullopt;
    }

    const auto decrypted_id = Signal::CryptoProviderImpl::decrypt(
        {message_keys->cipher_key, sizeof(message_keys->cipher_key)},
        {message_keys->iv, sizeof(message_keys->iv)},
        encrypted_id);

    #ifdef FUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION
    if (false) {
    #else
    if (decrypted_id.size() != Swarm::SwarmHash::Size) {
    #endif
        BOOST_LOG_TRIVIAL(warning)
            << "decrypted id has wrong size.\n"
            << "expected " << Swarm::SwarmHash::Size << '\n'
            << "received " << decrypted_id.size();
        return std::nullopt;
    }

    return Swarm::SwarmHash{decrypted_id};
}

// based on function with the same name from libsignal-protocol-c/src/session_cipher.c
Signal::Ref<ratchet_chain_key> session_cipher_get_or_create_chain_key(
    session_state* state, ec_public_key* their_ephemeral_pub_key)
{
    if (auto* result = ::session_state_get_receiver_chain_key(state, their_ephemeral_pub_key); result) {
        return Signal::Ref{result};
    }

    BOOST_LOG_TRIVIAL(debug) << "Chain key not found; Calculating from ephemeral.";

    auto* root_key = ::session_state_get_root_key(state);
    if (!root_key) {
        throw Signal::SignalError("missing root key");
    }

    const auto* our_ephemeral_key_pair = ::session_state_get_sender_ratchet_key_pair(state);
    if (!our_ephemeral_key_pair) {
        throw Signal::SignalError("missing sender ratchet key pair");
    }

    const auto [receiver_root_key, receiver_chain_key] =
        Signal::ratchet_root_key_create_chain(
            root_key, their_ephemeral_pub_key,
            ec_key_pair_get_private(our_ephemeral_key_pair));

    return receiver_chain_key;
}

std::optional<Swarm::SwarmHash> try_decrypt_message_id(
    ClientData& client, const Ethereum::Address& sender_address, const BufferView data)
{
    if (data.empty()) {
        throw ClientError{"empty encrypted message id"};
    }
    if (data[0] != message_id_version) {
        throw ClientError{"message id version mismatch"};
    }
    if (data.size() != version_size + encrypted_id_size + ratchet_pub_key_size + hmac_size) {
        throw ClientError{"message id has wrong size"};
    }
    const auto encrypted_id = FixedSizeBufferView<encrypted_id_size>(
        data.data() + version_size);
    const auto serialized_ratchet_pub_key = FixedSizeBufferView<ratchet_pub_key_size>(
        data.data() + version_size + encrypted_id_size);
    const auto hmac = FixedSizeBufferView<hmac_size>(
        data.data() + version_size + encrypted_id_size + ratchet_pub_key_size);

    BOOST_LOG_TRIVIAL(debug) << "Message ID ratchet pub key (ec_public_key): " << bin2hex(serialized_ratchet_pub_key);
    const auto ratchet_pub_key = Signal::curve_decode_point(serialized_ratchet_pub_key, client.m_signal_context->get());

    if (const auto signal_address = ethereum_address_to_signal(sender_address);
            Signal::signal_protocol_session_contains_session(client.m_signal_store_context->get(), signal_address.as_signal())) {
        BOOST_LOG_TRIVIAL(debug) << "Trying session with: " << bin2zxhex(sender_address);
        try {
            const auto record = Signal::signal_protocol_session_load_session(
                client.m_signal_store_context->get(), signal_address.as_signal());
            auto* state = session_record_get_state(record.get());
            auto receiver_chain_key = session_cipher_get_or_create_chain_key(state, ratchet_pub_key.get());

            for (int i = 0; i < 2000; ++i) {
                BOOST_LOG_TRIVIAL(debug)
                    << "Receiver ratchet chain key (ratchet_chain_key): "
                    << bin2hex(BufferView{Signal::ratchet_chain_key_get_key(receiver_chain_key.get())});

                const auto message_keys = Signal::ratchet_chain_key_get_message_keys(receiver_chain_key.get());

                const auto opt_swarm_id = verify_and_decrypt_message_id(encrypted_id, serialized_ratchet_pub_key, hmac, &message_keys);
                if (opt_swarm_id != std::nullopt) {
                    BOOST_LOG_TRIVIAL(debug) << "Match!";
                    return opt_swarm_id;
                }

                BOOST_LOG_TRIVIAL(debug) << "Trying " << (i+1) << " skipped key";
                receiver_chain_key = Signal::ratchet_chain_key_create_next(receiver_chain_key.get());
            }
        }
        catch (Signal::SignalError& e) {
            BOOST_LOG_TRIVIAL(error) << "failed decrypting message id using session key: " << e.what() << ", signal result: " << e.result();
        }
        catch (std::exception& e) {
            BOOST_LOG_TRIVIAL(error) << "failed decrypting message id using session key: " << e.what();
        }
        BOOST_LOG_TRIVIAL(debug) << "No match";
        return std::nullopt;
    }

    for (const auto signed_pre_key_id : client.m_signal_store_context->get_signed_pre_key_store()->key_ids()) {
        BOOST_LOG_TRIVIAL(debug) << "Trying signed pre key with id: " << signed_pre_key_id;

        try {
            const auto signed_pre_key = Signal::signal_protocol_signed_pre_key_load_key(
                client.m_signal_store_context->get(), signed_pre_key_id);
            const auto* signed_key_pair = session_signed_pre_key_get_key_pair(signed_pre_key.get());
            const auto* signed_priv_key = ec_key_pair_get_private(signed_key_pair);

            BOOST_LOG_TRIVIAL(debug) << "Trying signed pre key (ec_public_key): " <<
                bin2hex(BufferView{Signal::ec_public_key_serialize(ec_key_pair_get_public(signed_key_pair))});

            const auto message_keys = get_message_keys_for_new_session(client, ratchet_pub_key.get(), signed_priv_key);

            const auto opt_swarm_id = verify_and_decrypt_message_id(encrypted_id, serialized_ratchet_pub_key, hmac, &message_keys);
            if (opt_swarm_id != std::nullopt) {
                BOOST_LOG_TRIVIAL(debug) << "Match!";
                return opt_swarm_id;
            }
        }
        catch (Signal::SignalError& e) {
            BOOST_LOG_TRIVIAL(error) << "failed decrypting message id using signed pre key: " << e.what() << ", signal result: " << e.result();
        }
        catch (std::exception& e) {
            BOOST_LOG_TRIVIAL(error) << "failed decrypting message id using signed pre key: " << e.what();
        }

        BOOST_LOG_TRIVIAL(debug) << "No match";
    }

    return std::nullopt;
}

void save_keys_for_next_message_id(ClientData& client, const Signal::Address& signal_address, session_record* record)
{
    const auto* state = session_record_get_state(record);
    const auto* sender_ratchet_pub_key = session_state_get_sender_ratchet_key(state);

    const auto serialized_ratchet_pub_key = Signal::ec_public_key_serialize(sender_ratchet_pub_key);

    auto* sender_chain_key = session_state_get_sender_chain_key(state);

    BOOST_LOG_TRIVIAL(debug) << "Sender chain key (ratchet_chain_key): " << bin2hex(BufferView{Signal::ratchet_chain_key_get_key(sender_chain_key)});

    const auto message_id_keys = Signal::ratchet_chain_key_get_message_keys(sender_chain_key);

    BOOST_LOG_TRIVIAL(debug)
        << "Saving keys for next message ID to " << signal_address.name() << '\n'
        << "serialized_ratchet_pub_key (ec_public_key): " << bin2hex(BufferView{serialized_ratchet_pub_key}) << '\n'
        << "message_id_keys.cipher_key: " << bin2hex(message_id_keys.cipher_key) << '\n'
        << "message_id_keys.mac_key: " << bin2hex(message_id_keys.mac_key) << '\n'
        << "message_id_keys.iv: " << bin2hex(message_id_keys.iv) << '\n'
        << "message_id_keys.counter: " << message_id_keys.counter;

    client.m_signal_store_context->get_session_store()->store_keys_for_next_message_id(
        signal_address, serialized_ratchet_pub_key,
        {reinterpret_cast<const std::uint8_t*>(&message_id_keys), sizeof(message_id_keys)});
}

Signal::SignalBufferRef decrypt_pre_key_signal_message(
    ClientData& client, const Signal::Address& sender_signal_address, session_cipher* cipher, pre_key_signal_message* message)
{
    // We are duplicating session_cipher_decrypt_pre_key_signal_message here,
    // but we need to jump in to save the key for message id.
    // And we're not removing pre key, so it can be reused.

    // this works for new sessions, by creating them
    const auto record = Signal::signal_protocol_session_load_session(
        client.m_signal_store_context->get(), sender_signal_address.as_signal());

    Signal::session_builder_process_pre_key_signal_message(
        Signal::session_cipher_get_builder(cipher), record.get(), message);

    Signal::signal_protocol_session_store_session(
        client.m_signal_store_context->get(), sender_signal_address.as_signal(), record.get());

    save_keys_for_next_message_id(client, sender_signal_address, record.get());

    return Signal::session_cipher_decrypt_signal_message(
        cipher, pre_key_signal_message_get_signal_message(message));
}


std::optional<Buffer> try_decrypt_message(ClientData& client, const Ethereum::MessageIdLog& message_id_log, std::any yield)
{
    const auto opt_id = try_decrypt_message_id(client, message_id_log.sender_address, message_id_log.data);
    if (opt_id == std::nullopt) {
        // Decryption failure is not an error.
        // The message can be for someone else.
        BOOST_LOG_TRIVIAL(debug) << "Message ID not decrypted";
        return std::nullopt;
    }

    const auto is_sender_invalidated = client.m_tigermail_contract->is_invalidated(
        message_id_log.sender_address, message_id_log.block_number, yield);
    if (is_sender_invalidated) {
        BOOST_LOG_TRIVIAL(debug) << "Message decrypted, but sender is invalidated";
        return std::nullopt;
    }

    const auto opt_encrypted_message = retrying(
        [&](std::any yield) {
            return client.m_swarm->bzz_get(*opt_id, yield);
        }, yield);
    if (opt_encrypted_message == std::nullopt) {
        throw ClientError{"failed getting encrypted message from swarm"};
    }

    const auto sender_signal_address = ethereum_address_to_signal(message_id_log.sender_address);

    const auto cipher = Signal::session_cipher_create(
        client.m_signal_store_context->get(), sender_signal_address.as_signal(), client.m_signal_context->get());

    const auto decrypt_pre_key_signal_message_ =
        [&] {
            const auto message = Signal::pre_key_signal_message_deserialize(
                *opt_encrypted_message, client.m_signal_context->get());
            return decrypt_pre_key_signal_message(client, sender_signal_address, cipher.get(), message.get());
        };
    const auto decrypt_signal_message_ =
        [&] {
            auto message = Signal::Ref<signal_message>{};
            try {
                message = Signal::signal_message_deserialize(
                    *opt_encrypted_message, client.m_signal_context->get());
            }
            catch (Signal::SignalError& e) {
                if (e.result() == SG_ERR_INVALID_PROTO_BUF) {
                    BOOST_LOG_TRIVIAL(debug) << "Failed deserializing message. Maybe it's a pre key message?";

                    // don't use decrypt_pre_key_signal_message_ here,
                    // because we don't want to overwrite stored keys for message id
                    const auto message = Signal::pre_key_signal_message_deserialize(
                        *opt_encrypted_message, client.m_signal_context->get());
                    return Signal::session_cipher_decrypt_pre_key_signal_message(cipher.get(), message.get());
                }
                throw;
            }
            return Signal::session_cipher_decrypt_signal_message(cipher.get(), message.get());
        };

    const auto plaintext =
        signal_protocol_session_contains_session(client.m_signal_store_context->get(), sender_signal_address.as_signal())
        ? decrypt_signal_message_()
        : decrypt_pre_key_signal_message_();

    return Buffer(plaintext.begin(), plaintext.end());
}


} // namespace


ClientData::ClientData(
    gsl::not_null<Ethereum::TigerMailContractInterface*> tigermail_contract,
    gsl::not_null<Swarm::SwarmClientInterface*> swarm,
    Ethereum::Address&& ethereum_address,
    std::string&& ethereum_password,
    gsl::not_null<Signal::Context*> signal_context,
    gsl::not_null<Signal::StoreContext*> signal_store_context,
    gsl::not_null<ClientStateInterface*> state
)
    : m_tigermail_contract{tigermail_contract}
    , m_swarm{swarm}
    , m_ethereum_address{std::move(ethereum_address)}
    , m_ethereum_password{std::move(ethereum_password)}
    , m_signal_context{signal_context}
    , m_signal_store_context{signal_store_context}
    , m_state{state}
{}

Client::Client(
    gsl::not_null<Ethereum::TigerMailContractInterface*> tigermail_contract,
    gsl::not_null<Swarm::SwarmClientInterface*> swarm,
    Ethereum::Address&& ethereum_address,
    std::string&& ethereum_password,
    gsl::not_null<Signal::Context*> signal_context,
    gsl::not_null<Signal::StoreContext*> signal_store_context,
    gsl::not_null<ClientStateInterface*> state
)
    : ClientData{
        tigermail_contract,
        swarm,
        std::move(ethereum_address),
        std::move(ethereum_password),
        signal_context,
        signal_store_context,
        state
      }
{}

void Client::rotate_keys(std::any yield)
{
    BOOST_LOG_TRIVIAL(info) << "Rotating keys...";
    generate_new_keys(*this);
    publish_key_bundle(*this, yield);
    delete_old_keys(*this);
    BOOST_LOG_TRIVIAL(info) << "Rotating keys finished";
}

void Client::send_message(const Ethereum::Address& to, BufferView content, std::any yield)
{
    if (to == m_ethereum_address) {
        throw ClientError{"sending messages to yourself is unsupported"};
    }

    BOOST_LOG_TRIVIAL(info) << "Sending message to " << bin2zxhex(to);

    const auto recipient_signal_address = ethereum_address_to_signal(to);

    const auto make_new_message_id_keys =
        [&] {
            BOOST_LOG_TRIVIAL(debug) << "Creating new Signal session";
            const auto key_bundle = process_pre_key_bundle(*this, to, recipient_signal_address, yield);
            const auto message_id_ephemeral_key_pair = Signal::curve_generate_key_pair(m_signal_context->get());

            return std::make_tuple(
                get_message_keys_for_new_session(
                    *this,
                    session_pre_key_bundle_get_signed_pre_key(key_bundle.get()),
                    ec_key_pair_get_private(message_id_ephemeral_key_pair.get())),
                Signal::Ref{ec_key_pair_get_public(message_id_ephemeral_key_pair.get())}
            );
        };

    const auto get_message_id_keys =
        [&] {
            BOOST_LOG_TRIVIAL(debug) << "Using existing Signal session";
            const auto keys_for_message_id =
                m_signal_store_context->get_session_store()->get_keys_for_message_id(recipient_signal_address);

            auto message_id_keys = ratchet_message_keys{};
            std::copy(keys_for_message_id.message_keys.begin(), keys_for_message_id.message_keys.end(),
                      reinterpret_cast<std::uint8_t*>(&message_id_keys));

            BOOST_LOG_TRIVIAL(debug)
                << "Got stored keys for message ID.\n"
                << "serialized_ratchet_pub_key (ec_public_key): " << bin2hex(keys_for_message_id.serialized_ratchet_pub_key) << '\n'
                << "message_id_keys.cipher_key: " << bin2hex(message_id_keys.cipher_key) << '\n'
                << "message_id_keys.mac_key: " << bin2hex(message_id_keys.mac_key) << '\n'
                << "message_id_keys.iv: " << bin2hex(message_id_keys.iv) << '\n'
                << "message_id_keys.counter: " << message_id_keys.counter;

            return std::make_tuple(
                std::move(message_id_keys),
                Signal::curve_decode_point(
                    keys_for_message_id.serialized_ratchet_pub_key,
                    m_signal_context->get())
            );
        };

    const auto [message_id_keys, ratchet_pub_key] =
        signal_protocol_session_contains_session(m_signal_store_context->get(), recipient_signal_address.as_signal())
        ? get_message_id_keys()
        : make_new_message_id_keys();

    const auto swarm_id = encrypt_and_store_message(*this, recipient_signal_address, content, yield);

    {
        const auto record = Signal::signal_protocol_session_load_session(
            m_signal_store_context->get(), recipient_signal_address.as_signal());
        save_keys_for_next_message_id(*this, recipient_signal_address, record.get());
    }

    publish_message_id_on_ethereum(*this, swarm_id, &message_id_keys, ratchet_pub_key.get(), yield);

    BOOST_LOG_TRIVIAL(info) << "Message sent successfully";
}

std::vector<Message> Client::receive_messages(std::any yield)
{
    BOOST_LOG_TRIVIAL(debug) << "Receiving messages...";

    const auto current_block = m_tigermail_contract->get_block_number(yield);

    const auto message_id_logs = m_tigermail_contract->get_message_id_logs(
        m_state->get_start_checking_blocks_at(), current_block, yield);

    auto result = std::vector<Message>{};
    for (const auto& message_id_log : message_id_logs) {
        BOOST_LOG_TRIVIAL(debug) << "Trying to decrypt message ID from " << bin2zxhex(message_id_log.sender_address);
        // const auto start = std::chrono::steady_clock::now();
        try {
            auto plaintext = try_decrypt_message(*this, message_id_log, yield);

            if (!plaintext) {
                continue;
            }

            // TODO: ranges?
            result.push_back({
                    message_id_log.sender_address,
                    m_ethereum_address,
                    std::move(*plaintext)
                });

            BOOST_LOG_TRIVIAL(info) << "Received message from " << bin2zxhex(message_id_log.sender_address);
        }
        catch (Signal::SignalError& e) {
            BOOST_LOG_TRIVIAL(error) << "failed to decrypt message from " << bin2zxhex(message_id_log.sender_address) << ": " << e.what() << ", signal result: " << e.result();
        }
        catch (std::exception& e) {
            BOOST_LOG_TRIVIAL(error) << "failed to decrypt message from " << bin2zxhex(message_id_log.sender_address) << ": " << e.what();
        }
        // const auto end = std::chrono::steady_clock::now();
        // const auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        // BOOST_LOG_TRIVIAL(info) << "receiving message took " << milliseconds.count() << " milliseconds";
    }

    m_state->set_start_checking_blocks_at(current_block + 1);

    BOOST_LOG_TRIVIAL(debug) << "Receiving messages finished";
    return result;
}


} // namespace TigerMail
