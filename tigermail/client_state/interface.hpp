/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_TIGERMAIL_CLIENT_STATE_INTERFACE_HPP
#define TIGERMAIL_TIGERMAIL_CLIENT_STATE_INTERFACE_HPP

#include "../../signal/common.hpp"


namespace TigerMail {


class ClientStateInterface
{
public:
    virtual ~ClientStateInterface() = default;

    virtual Signal::PreKeyIdT get_start_signed_pre_key_generation_at() const = 0;
    virtual void start_signed_pre_key_generation_at_increase() = 0;

    virtual Signal::PreKeyIdT get_start_pre_key_generation_at() const = 0;
    virtual void start_pre_key_generation_at_increase() = 0;

    virtual int get_start_checking_blocks_at() const = 0;
    virtual void set_start_checking_blocks_at(int block) = 0;
};


} // namespace TigerMail

#endif // TIGERMAIL_TIGERMAIL_CLIENT_STATE_INTERFACE_HPP
