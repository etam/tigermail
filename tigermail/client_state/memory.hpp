/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_TIGERMAIL_CLIENT_STATE_MEMORY_HPP
#define TIGERMAIL_TIGERMAIL_CLIENT_STATE_MEMORY_HPP

#include "interface.hpp"


namespace TigerMail {


class MemoryClientState
    : public ClientStateInterface
{
protected:
    Signal::PreKeyIdT m_start_signed_pre_key_generation_at{0};
    Signal::PreKeyIdT m_start_pre_key_generation_at{0};
    int m_start_checking_blocks_at{0};

public:
    Signal::PreKeyIdT get_start_signed_pre_key_generation_at() const override
    {
        return m_start_signed_pre_key_generation_at;
    }

    void start_signed_pre_key_generation_at_increase() override
    {
        ++m_start_signed_pre_key_generation_at;
    }

    Signal::PreKeyIdT get_start_pre_key_generation_at() const override
    {
        return m_start_pre_key_generation_at;
    }

    void start_pre_key_generation_at_increase() override
    {
        ++m_start_pre_key_generation_at;
    }

    int get_start_checking_blocks_at() const override
    {
        return m_start_checking_blocks_at;
    }

    void set_start_checking_blocks_at(int block) override
    {
        m_start_checking_blocks_at = block;
    }
};


} // namespace TigerMail

#endif // TIGERMAIL_TIGERMAIL_CLIENT_STATE_MEMORY_HPP
