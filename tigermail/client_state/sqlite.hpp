/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_TIGERMAIL_CLIENT_STATE_SQLITE_HPP
#define TIGERMAIL_TIGERMAIL_CLIENT_STATE_SQLITE_HPP

#include "interface.hpp"

#include <gsl/pointers>

namespace sqlite {
class database;
} // namespace sqlite


namespace TigerMail {


class SqliteClientState
    : public ClientStateInterface
{
protected:
    gsl::not_null<sqlite::database*> m_database;

public:
    explicit
    SqliteClientState(gsl::not_null<sqlite::database*> database);

    Signal::PreKeyIdT get_start_signed_pre_key_generation_at() const override;
    void start_signed_pre_key_generation_at_increase() override;

    Signal::PreKeyIdT get_start_pre_key_generation_at() const override;
    void start_pre_key_generation_at_increase() override;

    int get_start_checking_blocks_at() const override;
    void set_start_checking_blocks_at(int block) override;
};


} // namespace TigerMail

#endif // TIGERMAIL_TIGERMAIL_CLIENT_STATE_SQLITE_HPP
