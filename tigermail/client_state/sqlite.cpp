/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sqlite.hpp"

#include <limits>

#include <sqlite_modern_cpp.h>


namespace TigerMail {


static
constexpr const auto max_id = std::numeric_limits<TigerMail::Signal::PreKeyIdT>::max();


SqliteClientState::SqliteClientState(gsl::not_null<sqlite::database*> database)
    : m_database{database}
{
    // this is a key-value store
    *m_database <<
        "create table if not exists "
        "tigermail_client_state ( "
        "  name text not null primary key, "
        "  value int not null);";
    *m_database <<
        "insert or ignore "
        "into tigermail_client_state(name, value) "
        "values "
        "  (\"start_signed_pre_key_generation_at\", 0), "
        "  (\"start_pre_key_generation_at\", 0), "
        "  (\"start_checking_blocks_at\", 0);";
}

Signal::PreKeyIdT SqliteClientState::get_start_signed_pre_key_generation_at() const
{
    auto result = Signal::PreKeyIdT{};
    *m_database <<
        "select value "
        "from tigermail_client_state "
        "where name = \"start_signed_pre_key_generation_at\";"
                >> result;
    return result;
}

void SqliteClientState::start_signed_pre_key_generation_at_increase()
{
    *m_database <<
        "update tigermail_client_state "
        "set value = (value+1) % (?+1) "
        "where name = \"start_signed_pre_key_generation_at\";"
                << max_id;
}

Signal::PreKeyIdT SqliteClientState::get_start_pre_key_generation_at() const
{
    auto result = Signal::PreKeyIdT{};
    *m_database <<
        "select value "
        "from tigermail_client_state "
        "where name = \"start_pre_key_generation_at\";"
                >> result;
    return result;
}

void SqliteClientState::start_pre_key_generation_at_increase()
{
    *m_database <<
        "update tigermail_client_state "
        "set value = (value+1) % (?+1) "
        "where name = \"start_pre_key_generation_at\";"
                << max_id;
}

int SqliteClientState::get_start_checking_blocks_at() const
{
    auto result = int{};
    *m_database <<
        "select value "
        "from tigermail_client_state "
        "where name = \"start_checking_blocks_at\";"
                >> result;
    return result;
}

void SqliteClientState::set_start_checking_blocks_at(int block)
{
    *m_database <<
        "update tigermail_client_state "
        "set value = ? "
        "where name = \"start_checking_blocks_at\";"
                << block;
}


} // namespace TigerMail
