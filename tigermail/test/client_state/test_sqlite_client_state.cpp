/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "test_sqlite_client_state.hpp"

#include <sqlite_modern_cpp.h>


namespace TigerMail {


void TestSqliteClientState::set_start_signed_pre_key_generation_at(Signal::PreKeyIdT id)
{
    *m_database <<
        "update tigermail_client_state "
        "set value = ? "
        "where name = \"start_signed_pre_key_generation_at\";"
                << id;
}

void TestSqliteClientState::set_start_pre_key_generation_at(Signal::PreKeyIdT id)
{
    *m_database <<
        "update tigermail_client_state "
        "set value = ? "
        "where name = \"start_pre_key_generation_at\";"
                << id;
}


} // namespace TigerMail
