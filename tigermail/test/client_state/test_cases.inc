/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

BOOST_AUTO_TEST_CASE(id_increase)
{
    BOOST_CHECK_EQUAL(client_state.get_start_signed_pre_key_generation_at(), 0);
    BOOST_CHECK_EQUAL(client_state.get_start_pre_key_generation_at(), 0);

    client_state.start_signed_pre_key_generation_at_increase();
    client_state.start_pre_key_generation_at_increase();

    BOOST_CHECK_EQUAL(client_state.get_start_signed_pre_key_generation_at(), 1);
    BOOST_CHECK_EQUAL(client_state.get_start_pre_key_generation_at(), 1);
}

BOOST_AUTO_TEST_CASE(id_overflow)
{
    constexpr const auto max_id = std::numeric_limits<TigerMail::Signal::PreKeyIdT>::max();
    client_state.set_start_signed_pre_key_generation_at(max_id);
    client_state.set_start_pre_key_generation_at(max_id);

    BOOST_CHECK_EQUAL(client_state.get_start_signed_pre_key_generation_at(), max_id);
    BOOST_CHECK_EQUAL(client_state.get_start_pre_key_generation_at(), max_id);

    client_state.start_signed_pre_key_generation_at_increase();
    client_state.start_pre_key_generation_at_increase();

    BOOST_CHECK_EQUAL(client_state.get_start_signed_pre_key_generation_at(), 0);
    BOOST_CHECK_EQUAL(client_state.get_start_pre_key_generation_at(), 0);
}

BOOST_AUTO_TEST_CASE(block)
{
    client_state.set_start_checking_blocks_at(5);
    BOOST_CHECK_EQUAL(client_state.get_start_checking_blocks_at(), 5);
}