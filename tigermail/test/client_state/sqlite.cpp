/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <limits>

#include <boost/test/unit_test.hpp>

#include <gsl/pointers>

#include <sqlite_modern_cpp.h>

#include "test_sqlite_client_state.hpp"

using TigerMail::TestSqliteClientState;


namespace {


struct Fixture
{
    sqlite::database database{":memory:"};
    TestSqliteClientState client_state{gsl::not_null{&database}};
};


} // namespace


BOOST_AUTO_TEST_SUITE(client_state)
BOOST_FIXTURE_TEST_SUITE(sqlite, Fixture)


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsubobject-linkage"
#include "test_cases.inc"
#pragma GCC diagnostic pop


BOOST_AUTO_TEST_SUITE_END() // memory
BOOST_AUTO_TEST_SUITE_END() // client_state
