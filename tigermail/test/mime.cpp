/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <memory>
#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

#include <gsl/pointers>

#include <range/v3/core.hpp>
#include <range/v3/action/sort.hpp>

#include "../../common/buffer.hpp"
#include "../../common/buffer_view.hpp"

#include "../../ethereum/common.hpp"
#include "../../ethereum/test/out/common.hpp"

#include "../../smtp/session_handler.hpp"

#include "../client.hpp"
#include "../mime.hpp"

namespace Ethereum = TigerMail::Ethereum;


BOOST_AUTO_TEST_CASE(address_to_tigermail)
{
    using TigerMail::address_to_tigermail;

    const auto ethereum_address = Ethereum::Address{
        0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10,
        0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
    };

    BOOST_CHECK_EQUAL(address_to_tigermail(ethereum_address),
                      "0102030405060708091011121314151617181920@tigermail.eth");
}


BOOST_AUTO_TEST_SUITE(parse_tigermail_address)

using TigerMail::ParsingError;
using TigerMail::parse_tigermail_address;


BOOST_AUTO_TEST_CASE(ok)
{
    const auto address = parse_tigermail_address("0102030405060708091011121314151617181920@tigermail.eth");

    const auto expected_ethereum_address = Ethereum::Address{
        0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10,
        0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
    };

    BOOST_CHECK_EQUAL_COLLECTIONS(
        address.begin(), address.end(),
        expected_ethereum_address.begin(), expected_ethereum_address.end());
}

BOOST_AUTO_TEST_CASE(wrong_domain)
{
    BOOST_CHECK_THROW(
        parse_tigermail_address("0102030405060708091011121314151617181920@example.com"),
        ParsingError);
}

BOOST_AUTO_TEST_CASE(wrong_hex)
{
    BOOST_CHECK_THROW(
        parse_tigermail_address("010304050607080910111213x4151617181920@tigermail.eth"),
        ParsingError);
}

BOOST_AUTO_TEST_CASE(not_even_an_address)
{
    // In this case vmime asks dns for localhost name. TODO: avoid doing that.
    // See also: https://github.com/kisli/vmime/issues/235
    BOOST_CHECK_THROW(
        parse_tigermail_address("foobar"),
        ParsingError);
}


BOOST_AUTO_TEST_SUITE_END() // parse_tigermail_address


BOOST_AUTO_TEST_SUITE(is_incoming_message_valid)

namespace Ethereum = TigerMail::Ethereum;
using TigerMail::Buffer;
using TigerMail::Message;
using TigerMail::is_incoming_message_valid;


BOOST_AUTO_TEST_CASE(ok)
{
    const auto message_content_str = std::string_view{
        "From: One Two Three <0102030405060708091011121314151617181920@tigermail.eth>\r\n"
        "\r\n"
        "Hi\r\n"
        ".\r\n"
    };
    const auto message = Message{
        .from = Ethereum::Address{
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10,
            0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
        },
        .to = Ethereum::Address{},
        .content = Buffer{message_content_str.begin(), message_content_str.end()},
    };

    BOOST_CHECK(is_incoming_message_valid(message).value);
}

BOOST_AUTO_TEST_CASE(sender_address_mismatch)
{
    const auto message_content_str = std::string_view{
        "From: One Two Three <0102030405060708091011121314151617181920@tigermail.eth>\r\n"
        "\r\n"
        "Hi\r\n"
        ".\r\n"
    };
    const auto message = Message{
        .from = Ethereum::Address{
            0xff, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10,
            0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
        },
        .to = Ethereum::Address{},
        .content = Buffer{message_content_str.begin(), message_content_str.end()},
    };

    BOOST_CHECK(!is_incoming_message_valid(message).value);
}

BOOST_AUTO_TEST_CASE(some_address_not_parsable)
{
    const auto message_content_str = std::string_view{
        "From: One Two Three <0102030405060708091011121314151617181920@tigermail.eth>\r\n"
        "To: fhtagn <abcdefghijklmnopqrstuvwxz@tigermail.eth>\r\n"
        "\r\n"
        "Hi\r\n"
        ".\r\n"
    };
    const auto message = Message{
        .from = Ethereum::Address{
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10,
            0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
        },
        .to = Ethereum::Address{},
        .content = Buffer{message_content_str.begin(), message_content_str.end()},
    };

    BOOST_CHECK(!is_incoming_message_valid(message).value);
}


BOOST_AUTO_TEST_SUITE_END() // is_incoming_message_valid


BOOST_AUTO_TEST_SUITE(is_outgoing_message_valid)

namespace Ethereum = TigerMail::Ethereum;
using TigerMail::Buffer;
using TigerMail::Message;
using TigerMail::is_outgoing_message_valid;
using TigerMail::parse_message;


BOOST_AUTO_TEST_CASE(ok)
{
    const auto header = gsl::not_null{parse_message(
        "From: One Two Three <0102030405060708091011121314151617181920@tigermail.eth>\r\n"
        "To: ABC <a1a2a3a4a5a6a7a8a9b0b1b2b3b4b5b6b7b8b9c0@tigermail.eth>\r\n"
        "Cc: DEF <d1d2d3d4d5d6d7d8d9e0e1e2e3e4e5e6e7e8e9f0@tigermail.eth>\r\n"
        "Bcc: 789 <7172737475767778798081828384858687888990@tigermail.eth>\r\n"
        "\r\n"
        "Hi\r\n"
        ".\r\n"
    ).getHeader()};

    const auto from = Ethereum::Address{
        0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10,
        0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
    };
    const auto to = Ethereum::Address{
        0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xb0,
        0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xc0,
    };
    const auto cc = Ethereum::Address{
        0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xe0,
        0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xf0,
    };
    const auto bcc = Ethereum::Address{
        0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x80,
        0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x90,
    };

    BOOST_CHECK(is_outgoing_message_valid(from, {to, cc, bcc}, header).value);
}

BOOST_AUTO_TEST_CASE(no_recipient)
{
    const auto header = gsl::not_null{parse_message(
        "From: One Two Three <0102030405060708091011121314151617181920@tigermail.eth>\r\n"
        "\r\n"
        "Hi\r\n"
        ".\r\n"
    ).getHeader()};

    const auto from = Ethereum::Address{
        0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10,
        0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
    };

    BOOST_CHECK(!is_outgoing_message_valid(from, {}, header).value);
}

BOOST_AUTO_TEST_CASE(sender_address_mismatch)
{
    const auto header = gsl::not_null{parse_message(
        "From: One Two Three <0102030405060708091011121314151617181920@tigermail.eth>\r\n"
        "\r\n"
        "Hi\r\n"
        ".\r\n"
    ).getHeader()};

    const auto from = Ethereum::Address{
        0xff, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10,
        0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
    };

    BOOST_CHECK(!is_outgoing_message_valid(from, {}, header).value);
}


BOOST_AUTO_TEST_SUITE_END() // is_outgoing_message_valid


BOOST_AUTO_TEST_SUITE(validate_and_prepare_outgoing_message)

namespace Ethereum = TigerMail::Ethereum;
namespace SMTP = TigerMail::SMTP;
using TigerMail::Buffer;
using TigerMail::BufferView;
using TigerMail::validate_and_prepare_outgoing_message;


BOOST_AUTO_TEST_CASE(ok)
{
    const auto prepared_message = validate_and_prepare_outgoing_message(
        {
            .recipients = {
                "a1a2a3a4a5a6a7a8a9b0b1b2b3b4b5b6b7b8b9c0@tigermail.eth",
                "d1d2d3d4d5d6d7d8d9e0e1e2e3e4e5e6e7e8e9f0@tigermail.eth",
                "7172737475767778798081828384858687888990@tigermail.eth",
            },
            .message = gsl::not_null{std::make_shared<const std::string>(
                "From: One Two Three <0102030405060708091011121314151617181920@tigermail.eth>\r\n"
                "To: ABC <a1a2a3a4a5a6a7a8a9b0b1b2b3b4b5b6b7b8b9c0@tigermail.eth>\r\n"
                "Cc: DEF <d1d2d3d4d5d6d7d8d9e0e1e2e3e4e5e6e7e8e9f0@tigermail.eth>\r\n"
                "Bcc: 789 <7172737475767778798081828384858687888990@tigermail.eth>\r\n"
                "\r\n"
                "Hi\r\n"
                ".\r\n"
            )},
        },
        Ethereum::Address{
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10,
            0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
        }
    );
    const auto prepared_message_content_str = std::string_view(
        reinterpret_cast<const char*>(prepared_message.content.data()),
        prepared_message.content.size());

    const auto expected_recipients = std::vector{
        Ethereum::Address{
            0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xb0,
            0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xc0,
        },
        Ethereum::Address{
            0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xe0,
            0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xf0,
        },
        Ethereum::Address{
            0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x80,
            0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x90,
        },
    } | ranges::actions::sort;

    const auto expected_message = std::string_view{
        "From: \"One Two Three\" <0102030405060708091011121314151617181920@tigermail.eth>\r\n"
        "To: \"ABC\" <a1a2a3a4a5a6a7a8a9b0b1b2b3b4b5b6b7b8b9c0@tigermail.eth>\r\n"
        "Cc: \"DEF\" <d1d2d3d4d5d6d7d8d9e0e1e2e3e4e5e6e7e8e9f0@tigermail.eth>\r\n"
        "\r\n"
        "Hi\r\n"
        ".\r\n"
    };

    BOOST_CHECK_EQUAL_COLLECTIONS(
        prepared_message.recipients.begin(), prepared_message.recipients.end(),
        expected_recipients.begin(), expected_recipients.end());

    BOOST_CHECK_EQUAL_COLLECTIONS(
        prepared_message_content_str.begin(), prepared_message_content_str.end(),
        expected_message.begin(), expected_message.end());
}

BOOST_AUTO_TEST_CASE(empty_bcc)
{
    const auto prepared_message = validate_and_prepare_outgoing_message(
        {
            .recipients = {
                "a1a2a3a4a5a6a7a8a9b0b1b2b3b4b5b6b7b8b9c0@tigermail.eth",
                "d1d2d3d4d5d6d7d8d9e0e1e2e3e4e5e6e7e8e9f0@tigermail.eth",
            },
            .message = gsl::not_null{std::make_shared<const std::string>(
                "From: One Two Three <0102030405060708091011121314151617181920@tigermail.eth>\r\n"
                "To: ABC <a1a2a3a4a5a6a7a8a9b0b1b2b3b4b5b6b7b8b9c0@tigermail.eth>\r\n"
                "Cc: DEF <d1d2d3d4d5d6d7d8d9e0e1e2e3e4e5e6e7e8e9f0@tigermail.eth>\r\n"
                "\r\n"
                "Hi\r\n"
                ".\r\n"
            )},
        },
        Ethereum::Address{
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10,
            0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20,
        }
    );
    const auto prepared_message_content_str = std::string_view(
        reinterpret_cast<const char*>(prepared_message.content.data()),
        prepared_message.content.size());

    const auto expected_recipients = std::vector{
        Ethereum::Address{
            0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xb0,
            0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xc0,
        },
        Ethereum::Address{
            0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xe0,
            0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xf0,
        },
    } | ranges::actions::sort;

    const auto expected_message = std::string_view{
        "From: \"One Two Three\" <0102030405060708091011121314151617181920@tigermail.eth>\r\n"
        "To: \"ABC\" <a1a2a3a4a5a6a7a8a9b0b1b2b3b4b5b6b7b8b9c0@tigermail.eth>\r\n"
        "Cc: \"DEF\" <d1d2d3d4d5d6d7d8d9e0e1e2e3e4e5e6e7e8e9f0@tigermail.eth>\r\n"
        "\r\n"
        "Hi\r\n"
        ".\r\n"
    };

    BOOST_CHECK_EQUAL_COLLECTIONS(
        prepared_message.recipients.begin(), prepared_message.recipients.end(),
        expected_recipients.begin(), expected_recipients.end());

    BOOST_CHECK_EQUAL_COLLECTIONS(
        prepared_message_content_str.begin(), prepared_message_content_str.end(),
        expected_message.begin(), expected_message.end());
}


BOOST_AUTO_TEST_SUITE_END() // validate_and_prepare_outgoing_message
