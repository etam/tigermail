/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_TIGERMAIL_TEST_FAKE_SWARM_HPP
#define TIGERMAIL_TIGERMAIL_TEST_FAKE_SWARM_HPP

#include <any>
#include <memory>
#include <string>
#include <unordered_map>

#include <boost/container_hash/hash.hpp>

#include <gsl/gsl_assert>

#include "../../../common/buffer.hpp"
#include "../../../common/buffer_view.hpp"

#include "../../../swarm/swarm_client_interface.hpp"

#include "fake_ethereum.hpp"
#include "turbulent.hpp"


namespace TigerMail::Test {

using Swarm::SwarmHash;


class FakeSwarm
    : public Swarm::SwarmClientInterface
    , public Turbulent
{
private:
    using FeedKey = std::pair<std::string, Ethereum::Address>;

    std::unordered_map<SwarmHash, Buffer, boost::hash<SwarmHash>> m_store;
    std::unordered_map<FeedKey, Buffer, boost::hash<FeedKey>> m_feeds;

public:
    ~FakeSwarm() noexcept override = default;

    std::optional<Buffer> bzz_get(const SwarmHash& id, std::any continuation) override;
    SwarmHash bzz_post(BufferView data, std::any continuation) override;

    std::optional<Buffer> feed_get(const Ethereum::Address& address, std::string_view name,
                                   std::any continuation) override;
    void feed_update(const Ethereum::Address& address, std::string_view password, std::string_view name,
                     BufferView data, std::any continuation) override;
};


} // namespace TigerMail::Test

#endif // TIGERMAIL_TIGERMAIL_TEST_FAKE_SWARM_HPP
