/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "fake_swarm.hpp"

#include <boost/test/unit_test.hpp>

#include "../../../common/hex.hpp"

#include "../../../common/test/fake_yield.hpp"

#include "../../../common/test/out/buffer.hpp"
#include "../../../common/test/out/buffer_view.hpp"
#include "../../../common/test/out/std_optional.hpp"

#include "../../../signal/crypto_provider_impl/sha512.hpp"

using TigerMail::Buffer;
using TigerMail::Test::FakeEthereum;
using TigerMail::Test::FakeSwarm;
using TigerMail::Test::FakeYield;


namespace TigerMail::Test {


namespace {


SwarmHash sha512(BufferView data)
{
    using Signal::CryptoProviderImpl::Sha512Interface;
    auto hash = std::unique_ptr<Sha512Interface>{Sha512Interface::create()};
    hash->update(data);
    const auto result = hash->final();
    static_assert(result.size() >= 32);
    return SwarmHash(result.begin(), result.begin() + 32);
}


} // namespace


std::optional<Buffer> FakeSwarm::bzz_get(const SwarmHash& id, std::any yield)
{
    std::any_cast<FakeYield>(yield);

    const auto it = m_store.find(id);
    if (it == m_store.end()) {
        return std::nullopt;
    }
    return it->second;
}

SwarmHash FakeSwarm::bzz_post(BufferView data, std::any yield)
{
    std::any_cast<FakeYield>(yield);

    const auto key = sha512(data);
    if (m_turbulence == Turbulence::None) {
        m_store.emplace(key, Buffer(data.begin(), data.end()));
    }
    return key;
}

std::optional<Buffer> FakeSwarm::feed_get(const Ethereum::Address& address, std::string_view name,
                                          std::any yield)
{
    std::any_cast<FakeYield>(yield);

    // drop creating objects from views when updating to C++20
    const auto it = m_feeds.find({std::string(name), Ethereum::Address{address}});
    if (it == m_feeds.end()) {
        return std::nullopt;
    }
    return it->second;
}

void FakeSwarm::feed_update(const Ethereum::Address& address, std::string_view password, std::string_view name,
                            BufferView data, std::any yield)
{
    std::any_cast<FakeYield>(yield);
    Expects(bin2hex(address) == password);
    if (m_turbulence == Turbulence::ConnectionLost) {
        return;
    }

    m_feeds.insert_or_assign({std::string(name), Ethereum::Address{address}}, Buffer{data.begin(), data.end()});
}


} // namespace TigerMail::Test


namespace {


struct Fixture
{
    FakeSwarm swarm{};
    FakeYield yield{FakeYield::explicit_construction};
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(fake_swarm, Fixture)


BOOST_AUTO_TEST_CASE(fake_swarm)
{
    const auto data1 = Buffer{'d', 'a', 't', 'a'};
    const auto data2 = Buffer{'f', 'o', 'o'};

    const auto key1 = swarm.bzz_post(data1, yield);
    const auto key2 = swarm.bzz_post(data2, yield);

    BOOST_CHECK_NE(key1, key2);

    const auto got_data1 = swarm.bzz_get(key1, yield);
    const auto got_data2 = swarm.bzz_get(key2, yield);

    BOOST_REQUIRE_NE(got_data1, std::nullopt);
    BOOST_REQUIRE_NE(got_data2, std::nullopt);
    BOOST_CHECK_EQUAL_COLLECTIONS(data1.begin(), data1.end(), got_data1->begin(), got_data1->end());
    BOOST_CHECK_EQUAL_COLLECTIONS(data2.begin(), data2.end(), got_data2->begin(), got_data2->end());

    auto key3 = key2;
    ++key3[0];
    const auto got_data3 = swarm.bzz_get(key3, yield);
    BOOST_CHECK_EQUAL(got_data3, std::nullopt);
}

BOOST_AUTO_TEST_CASE(fake_swarm_feed)
{
    const auto name1 = std::string{"name1"};
    const auto name2 = std::string{"name2"};

    const auto address1 = FakeEthereum::Address{3};
    const auto password1 = bin2hex(address1);
    const auto address2 = FakeEthereum::Address{5};
    const auto password2 = bin2hex(address2);
    const auto address3 = FakeEthereum::Address{7};

    const auto data1 = Buffer{'d', 'a', 't', 'a'};
    const auto data2 = Buffer{'f', 'o', 'o'};
    const auto data3 = Buffer{'b', 'a', 'r'};

    swarm.feed_update(address1, password1, name1, data1, yield);
    swarm.feed_update(address2, password2, name1, data2, yield);

    const auto got_data1 = swarm.feed_get(address1, name1, yield);
    const auto got_data2 = swarm.feed_get(address2, name1, yield);
    BOOST_REQUIRE_NE(got_data1, std::nullopt);
    BOOST_REQUIRE_NE(got_data2, std::nullopt);
    BOOST_CHECK_EQUAL_COLLECTIONS(data1.begin(), data1.end(), got_data1->begin(), got_data1->end());
    BOOST_CHECK_EQUAL_COLLECTIONS(data2.begin(), data2.end(), got_data2->begin(), got_data2->end());

    swarm.feed_update(address1, password1, name1, data3, yield);
    const auto got_data3 = swarm.feed_get(address1, name1, yield);
    BOOST_REQUIRE_NE(got_data3, std::nullopt);
    BOOST_CHECK_EQUAL_COLLECTIONS(data3.begin(), data3.end(), got_data3->begin(), got_data3->end());

    BOOST_REQUIRE_EQUAL(swarm.feed_get(address1, name2, yield), std::nullopt);
    BOOST_REQUIRE_EQUAL(swarm.feed_get(address3, name1, yield), std::nullopt);
}

BOOST_AUTO_TEST_CASE(connection_lost)
{
    const auto data1 = Buffer{'d', 'a', 't', 'a'};
    const auto key1 = swarm.bzz_post(data1, yield);

    const auto address = FakeEthereum::Address{3};
    const auto password = bin2hex(address);
    const auto name = std::string{"name1"};
    swarm.feed_update(address, password, name, data1, yield);

    swarm.connection_lost();

    const auto data2 = Buffer{'f', 'o', 'o'};
    const auto key2 = swarm.bzz_post(data2, yield);

    swarm.feed_update(address, password, name, data2, yield);

    swarm.no_problems();

    BOOST_CHECK_EQUAL(swarm.bzz_get(key1, yield), data1);
    BOOST_CHECK_EQUAL(swarm.bzz_get(key2, yield), std::nullopt);
    BOOST_CHECK_EQUAL(swarm.feed_get(address, name, yield), data1);
}


BOOST_AUTO_TEST_SUITE_END() // fake_swarm
