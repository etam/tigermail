/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_TIGERMAIL_TEST_TURBULENT_HPP
#define TIGERMAIL_TIGERMAIL_TEST_TURBULENT_HPP


namespace TigerMail::Test {


class Turbulent
{
protected:
    enum class Turbulence {
        None,
        ConnectionLost,
        Throws,
    } m_turbulence;

public:
    Turbulent()
        : m_turbulence{Turbulence::None}
    {}

    void no_problems()
    {
        m_turbulence = Turbulence::None;
    }

    void connection_lost()
    {
        m_turbulence = Turbulence::ConnectionLost;
    }

    void throws()
    {
        m_turbulence = Turbulence::Throws;
    }
};


} // namespace TigerMail::Test

#endif // TIGERMAIL_TIGERMAIL_TEST_TURBULENT_HPP
