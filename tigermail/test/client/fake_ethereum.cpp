/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "fake_ethereum.hpp"

#include <algorithm>
#include <ostream>
#include <stdexcept>

#include <boost/test/unit_test.hpp>

#include <range/v3/core.hpp>
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/transform.hpp>

#include "../../../common/utils.hpp"
#include "../../../common/zpad.hpp"
#include "../../../common/test/fake_yield.hpp"

#include "../../../ethereum/abi.hpp"

#include "../../../common/test/out/buffer.hpp"
#include "../../../ethereum/test/out/tigermail_contract_interface.hpp"

using TigerMail::Buffer;
using TigerMail::copy;
using TigerMail::temporary_reference_wrapper;
using TigerMail::zpad;
using TigerMail::Ethereum::BlockNumberOrTag;
using TigerMail::Ethereum::BlockTag;
using TigerMail::Ethereum::Topic;
using TigerMail::Test::FakeEthereum;
using TigerMail::Test::FakeTigerMailContract;
using TigerMail::Test::FakeYield;
namespace Ethereum = TigerMail::Ethereum;


namespace TigerMail::Test {


FakeEthereum::FakeEthereum()
    : m_block_number{0}
    , m_events{}
{}

unsigned FakeEthereum::get_block_number(std::any yield) const
{
    std::any_cast<FakeYield>(yield);

    return m_block_number;
}

FixedSizeBuffer<32> FakeEthereum::post_event(
    Address&& sender_address,
    Buffer&& data,
    std::vector<Ethereum::Topic>&& topics,
    std::any yield)
{
    std::any_cast<FakeYield>(yield);

    m_events[m_block_number].push_back(
        Event{
            .block_number = m_block_number,
            .sender_address = std::move(sender_address),
            .data = std::move(data),
            .topics = std::move(topics),
        }
    );

    return {};
}

auto FakeEthereum::filter_events(
    Ethereum::BlockNumberOrTag from,
    Ethereum::BlockNumberOrTag to,
    std::optional<temporary_reference_wrapper<const Ethereum::Address>> address,
    std::vector<temporary_reference_wrapper<const Ethereum::Topic>> topics,
    std::any yield
    ) const -> std::vector<Event>
{
    std::any_cast<FakeYield>(yield);

    const auto block_tag_to_it =
        [&](Ethereum::BlockTag tag) {
            switch (tag) {
            case Ethereum::BlockTag::earliest: return m_events.begin();
            case Ethereum::BlockTag::latest: return m_events.end();
            default: std::abort();
            }
        };

    const auto from_it = std::visit(overloaded{
            block_tag_to_it,
            [&](unsigned number) {
                return m_events.lower_bound(number);
            }
        }, from);
    const auto to_it = std::visit(overloaded{
            block_tag_to_it,
            [&](unsigned number) {
                return m_events.upper_bound(number);
            }
        }, to);

    // TODO: ranges?
    auto result = std::vector<Event>{};
    std::for_each(
        from_it, to_it,
        [&](const auto& block) {
            for (const auto& event : block.second) {
                if (address != std::nullopt && event.sender_address != *address) {
                    continue;
                }
                if (!topics.empty()) {
                    if (topics.size() > event.topics.size()) {
                        continue;
                    }
                    if (!std::equal(topics.begin(), topics.end(), event.topics.begin())) {
                        continue;
                    }
                }
                result.push_back(event);
            }
        });
    return result;
}


namespace {


const auto MessageId_event_id = Ethereum::get_event_id("MessageId(bytes)");
const auto Invalidated_event_id = Ethereum::get_event_id("Invalidated(address)");


} // namespace


FakeTigerMailContract::FakeTigerMailContract(
    gsl::not_null<FakeEthereum*> ethereum,
    Address&& user_address
)
    : Ethereum::TigerMailContractInterface{std::move(user_address)}
    , m_ethereum{ethereum}
{}

FixedSizeBuffer<32> FakeTigerMailContract::postMessageId(BufferView id, std::any yield)
{
    if (m_turbulence == Turbulence::ConnectionLost) {
        return {};
    }
    return m_ethereum->post_event(
        copy(m_user_address),
        Buffer{id.begin(), id.end()},
        std::vector<Ethereum::Topic>{MessageId_event_id},
        yield);
}

std::vector<Ethereum::MessageIdLog> FakeTigerMailContract::get_message_id_logs(
    unsigned from_block, unsigned to_block, std::any yield)
{
    if (m_turbulence == Turbulence::Throws) {
        throw std::runtime_error{"could not get transaction"};
    }

    auto events = m_ethereum->filter_events(
        from_block, to_block, std::nullopt, {MessageId_event_id}, yield);

    // TODO: ranges?
    auto message_ids = std::vector<Ethereum::MessageIdLog>{};
    message_ids.reserve(events.size());
    for (auto& event : events) {
        message_ids.emplace_back(
            event.block_number,
            std::move(event.sender_address),
            std::move(event.data)
        );
    }
    return message_ids;
}

FixedSizeBuffer<32> FakeTigerMailContract::invalidate(std::any yield)
{
    if (m_turbulence == Turbulence::ConnectionLost) {
        return {};
    }
    return m_ethereum->post_event(
        copy(m_user_address),
        {},
        std::vector<Ethereum::Topic>{
            Invalidated_event_id,
            zpad<32>(m_user_address)
        },
        yield);
}

bool FakeTigerMailContract::is_invalidated(const Address& address, Ethereum::BlockNumberOrTag to_block, std::any yield)
{
    const auto events = m_ethereum->filter_events(
        Ethereum::BlockTag::earliest, to_block, std::nullopt,
        {
            Invalidated_event_id,
            zpad<32>(address)
        },
        yield);
    return !events.empty();
}

unsigned FakeTigerMailContract::get_block_number(std::any yield)
{
    return m_ethereum->get_block_number(yield);
}


} // namespace TigerMail::Test


using TigerMail::Test::Invalidated_event_id;
using TigerMail::Test::MessageId_event_id;


namespace {


struct FakeEthereumEventsFixture
{
    Ethereum::Address address1{1};
    Ethereum::Address address2{2};
    FakeYield yield{FakeYield::explicit_construction};
    FakeEthereum ethereum;

    FakeEthereumEventsFixture()
    {
        // block 0
        ethereum.post_event(copy(address1), {'m', 's', 'g', '0'},
                            {MessageId_event_id}, yield);
        ethereum.post_event(copy(address1), {'i', 'n', 'v', '0'},
                            {Invalidated_event_id, zpad<32>(address1)}, yield);
        ethereum.tick(); // block 1
        ethereum.post_event(copy(address2), {'m', 's', 'g', '1'},
                            {MessageId_event_id}, yield);
        ethereum.post_event(copy(address2), {'i', 'n', 'v', '1'},
                            {Invalidated_event_id, zpad<32>(address2)}, yield);
        ethereum.tick(); // block 2
        ethereum.post_event(copy(address1), {'m', 's', 'g', '2'},
                            {MessageId_event_id}, yield);
        ethereum.post_event(copy(address1), {'i', 'n', 'v', '2'},
                            {Invalidated_event_id, zpad<32>(address1)}, yield);
        ethereum.tick(); // block 3
        ethereum.post_event(copy(address2), {'m', 's', 'g', '3'},
                            {MessageId_event_id}, yield);
        ethereum.post_event(copy(address2), {'i', 'n', 'v', '3'},
                            {Invalidated_event_id, zpad<32>(address2)}, yield);
        ethereum.tick();
    }

    std::vector<Buffer> filter_events_data(
        BlockNumberOrTag from,
        BlockNumberOrTag to,
        std::optional<temporary_reference_wrapper<const Ethereum::Address>> address,
        std::vector<temporary_reference_wrapper<const Topic>> topics) const
    {
        const auto events = ethereum.filter_events(from, to, address, topics, yield);
        return events
            | ranges::views::transform(&FakeEthereum::Event::data)
            | ranges::to_vector;
    }
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(fake_ethereum_events, FakeEthereumEventsFixture)


BOOST_AUTO_TEST_CASE(no_constraints)
{
    const auto events_data = filter_events_data(
        BlockTag::earliest, BlockTag::latest, std::nullopt, {});
    const auto expected_events_data = std::vector<Buffer>{
        {'m', 's', 'g', '0'},
        {'i', 'n', 'v', '0'},
        {'m', 's', 'g', '1'},
        {'i', 'n', 'v', '1'},
        {'m', 's', 'g', '2'},
        {'i', 'n', 'v', '2'},
        {'m', 's', 'g', '3'},
        {'i', 'n', 'v', '3'},
    };
    BOOST_CHECK_EQUAL_COLLECTIONS(events_data.begin(), events_data.end(),
                                  expected_events_data.begin(), expected_events_data.end());
}

BOOST_AUTO_TEST_CASE(blocks_range)
{
    const auto events_data = filter_events_data(1u, 2u, std::nullopt, {});
    const auto expected_events_data = std::vector<Buffer>{
        {'m', 's', 'g', '1'},
        {'i', 'n', 'v', '1'},
        {'m', 's', 'g', '2'},
        {'i', 'n', 'v', '2'},
    };
    BOOST_CHECK_EQUAL_COLLECTIONS(events_data.begin(), events_data.end(),
                                  expected_events_data.begin(), expected_events_data.end());
}

BOOST_AUTO_TEST_CASE(address)
{
    const auto events_data = filter_events_data(
        BlockTag::earliest, BlockTag::latest, address1, {});
    const auto expected_events_data = std::vector<Buffer>{
        {'m', 's', 'g', '0'},
        {'i', 'n', 'v', '0'},
        {'m', 's', 'g', '2'},
        {'i', 'n', 'v', '2'},
    };
    BOOST_CHECK_EQUAL_COLLECTIONS(events_data.begin(), events_data.end(),
                                  expected_events_data.begin(), expected_events_data.end());
}

BOOST_AUTO_TEST_CASE(topic)
{
    const auto events_data = filter_events_data(
        BlockTag::earliest, BlockTag::latest, std::nullopt,
        {Invalidated_event_id, zpad<32>(address2)});
    const auto expected_events_data = std::vector<Buffer>{
        {'i', 'n', 'v', '1'},
        {'i', 'n', 'v', '3'},
    };
    BOOST_CHECK_EQUAL_COLLECTIONS(events_data.begin(), events_data.end(),
                                  expected_events_data.begin(), expected_events_data.end());
}

BOOST_AUTO_TEST_CASE(partial_topic)
{
    const auto events_data = filter_events_data(
        BlockTag::earliest, BlockTag::latest, std::nullopt,
        {Invalidated_event_id});
    const auto expected_events_data = std::vector<Buffer>{
        {'i', 'n', 'v', '0'},
        {'i', 'n', 'v', '1'},
        {'i', 'n', 'v', '2'},
        {'i', 'n', 'v', '3'},
    };
    BOOST_CHECK_EQUAL_COLLECTIONS(events_data.begin(), events_data.end(),
                                  expected_events_data.begin(), expected_events_data.end());
}

BOOST_AUTO_TEST_CASE(everything)
{
    const auto events_data = filter_events_data(
        1u, 2u, address2, {MessageId_event_id});
    const auto expected_events_data = std::vector<Buffer>{
        {'m', 's', 'g', '1'},
    };
    BOOST_CHECK_EQUAL_COLLECTIONS(events_data.begin(), events_data.end(),
                                  expected_events_data.begin(), expected_events_data.end());
}


BOOST_AUTO_TEST_SUITE_END() // fake_ethereum_events


namespace {


struct FakeTigerMailContractFixture
{
    Ethereum::Address address1{1};
    Ethereum::Address address2{2};

    FakeYield yield{FakeYield::explicit_construction};

    FakeEthereum ethereum;

    FakeTigerMailContract tigermail_contract1{gsl::not_null{&ethereum}, copy(address1)};
    FakeTigerMailContract tigermail_contract2{gsl::not_null{&ethereum}, copy(address2)};
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(fake_tigermail_contract, FakeTigerMailContractFixture)


BOOST_AUTO_TEST_CASE(message_ids)
{
    tigermail_contract1.postMessageId(Buffer{'f', 'o', 'o'}, yield);
    ethereum.tick();
    const auto from = ethereum.get_block_number(yield);
    tigermail_contract2.postMessageId(Buffer{'b', 'a', 'r'}, yield);
    tigermail_contract1.postMessageId(Buffer{'b', 'a', 'z'}, yield);
    ethereum.tick();
    tigermail_contract2.postMessageId(Buffer{'c', 'u', 'x'}, yield);
    ethereum.tick();
    tigermail_contract1.postMessageId(Buffer{'b', 'l', 'a'}, yield);
    const auto to = ethereum.get_block_number(yield);
    ethereum.tick();
    tigermail_contract2.postMessageId(Buffer{'b', 'l', 'e'}, yield);

    const auto expected_message_ids = std::vector<Ethereum::MessageIdLog>{
        {1, copy(address2), {'b', 'a', 'r'}},
        {1, copy(address1), {'b', 'a', 'z'}},
        {2, copy(address2), {'c', 'u', 'x'}},
        {3, copy(address1), {'b', 'l', 'a'}},
    };

    {
        const auto found_message_ids1 = tigermail_contract1.get_message_id_logs(from, to, yield);
        BOOST_CHECK_EQUAL_COLLECTIONS(
            found_message_ids1.begin(),
            found_message_ids1.end(),
            expected_message_ids.begin(),
            expected_message_ids.end()
        );
    }
    {
        const auto found_message_ids2 = tigermail_contract2.get_message_id_logs(from, to, yield);
        BOOST_CHECK_EQUAL_COLLECTIONS(
            found_message_ids2.begin(),
            found_message_ids2.end(),
            expected_message_ids.begin(),
            expected_message_ids.end()
        );
    }
}

BOOST_AUTO_TEST_CASE(invalidate)
{
    BOOST_CHECK(!tigermail_contract1.am_I_invalidated(yield));
    BOOST_CHECK(!tigermail_contract2.am_I_invalidated(yield));
    BOOST_CHECK(!tigermail_contract1.is_invalidated(address2, BlockTag::latest, yield));
    BOOST_CHECK(!tigermail_contract2.is_invalidated(address1, BlockTag::latest, yield));

    ethereum.tick();
    tigermail_contract1.invalidate(yield);

    BOOST_CHECK(tigermail_contract1.am_I_invalidated(yield));
    BOOST_CHECK(!tigermail_contract2.am_I_invalidated(yield));
    BOOST_CHECK(!tigermail_contract1.is_invalidated(address2, BlockTag::latest, yield));
    BOOST_CHECK(tigermail_contract2.is_invalidated(address1, BlockTag::latest, yield));
    BOOST_CHECK(!tigermail_contract2.is_invalidated(address1, 0u, yield));

    ethereum.tick();
    tigermail_contract1.invalidate(yield);

    BOOST_CHECK(tigermail_contract1.am_I_invalidated(yield));
    BOOST_CHECK(!tigermail_contract2.am_I_invalidated(yield));
    BOOST_CHECK(!tigermail_contract1.is_invalidated(address2, BlockTag::latest, yield));
    BOOST_CHECK(tigermail_contract2.is_invalidated(address1, BlockTag::latest, yield));
    BOOST_CHECK(tigermail_contract2.is_invalidated(address1, 1u, yield));
    BOOST_CHECK(!tigermail_contract2.is_invalidated(address1, 0u, yield));
}

BOOST_AUTO_TEST_CASE(connection_lost)
{
    tigermail_contract1.connection_lost();
    tigermail_contract1.postMessageId(Buffer{'b', 'l', 'a'}, yield);
    tigermail_contract1.invalidate(yield);
    tigermail_contract1.no_problems();
    tigermail_contract1.postMessageId(Buffer{'b', 'l', 'e'}, yield);

    ethereum.tick();

    {
        const auto expected_message_ids = std::vector<Ethereum::MessageIdLog>{
            {0, copy(address1), {'b', 'l', 'e'}},
        };
        const auto received_message_ids = tigermail_contract2.get_message_id_logs(0, 1, yield);
        BOOST_CHECK_EQUAL_COLLECTIONS(
            received_message_ids.begin(),
            received_message_ids.end(),
            expected_message_ids.begin(),
            expected_message_ids.end());
    }
    BOOST_CHECK(!tigermail_contract2.is_invalidated(address1, 1u, yield));
}

BOOST_AUTO_TEST_CASE(throws)
{
    tigermail_contract1.throws();
    BOOST_CHECK_THROW(tigermail_contract1.get_message_id_logs(0, 1, yield), std::runtime_error);
}


BOOST_AUTO_TEST_SUITE_END() // fake_tigermail_contract
