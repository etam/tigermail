/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_TIGERMAIL_TEST_FAKE_ETHEREUM_HPP
#define TIGERMAIL_TIGERMAIL_TEST_FAKE_ETHEREUM_HPP

#include <any>
#include <functional>
#include <map>
#include <optional>
#include <vector>

#include <gsl/pointers>

#include "../../../common/buffer.hpp"
#include "../../../common/fixed_size_buffer.hpp"
#include "../../../common/utils.hpp"

#include "../../../ethereum/common.hpp"
#include "../../../ethereum/tigermail_contract_interface.hpp"

#include "turbulent.hpp"


namespace TigerMail::Test {


class FakeEthereum
{
public:
    using Address = Ethereum::Address;

    struct Event
    {
        unsigned block_number;
        Address sender_address;
        Buffer data;
        std::vector<Ethereum::Topic> topics;
    };

private:
    unsigned m_block_number;
    std::map<unsigned, std::vector<Event>> m_events;

public:
    FakeEthereum();

    unsigned get_block_number(std::any continuation) const;

    void tick()
    {
        ++m_block_number;
    }

    FixedSizeBuffer<32> post_event(
        Address&& sender_address,
        Buffer&& data,
        std::vector<Ethereum::Topic>&& topics,
        std::any continuation);

    std::vector<Event> filter_events(
        Ethereum::BlockNumberOrTag from,
        Ethereum::BlockNumberOrTag to,
        std::optional<temporary_reference_wrapper<const Ethereum::Address>> address,
        std::vector<temporary_reference_wrapper<const Ethereum::Topic>> topics,
        std::any continuation) const;
};


class FakeTigerMailContract
    : public Ethereum::TigerMailContractInterface
    , public Turbulent
{
public:
    using Address = Ethereum::Address;
    using MessageIdLog = Ethereum::MessageIdLog;

private:
    gsl::not_null<FakeEthereum*> m_ethereum;

public:
    FakeTigerMailContract(
        gsl::not_null<FakeEthereum*> ethereum,
        Address&& user_address
    );

    ~FakeTigerMailContract() noexcept override = default;

    FixedSizeBuffer<32> postMessageId(BufferView id, std::any continuation) override;
    std::vector<MessageIdLog> get_message_id_logs(unsigned from_block, unsigned to_block, std::any continuation) override;

    FixedSizeBuffer<32> invalidate(std::any continuation) override;
    bool is_invalidated(const Address& address, Ethereum::BlockNumberOrTag to_block, std::any continuation) override;

    unsigned get_block_number(std::any continuation) override;
};


} // namespace TigerMail::Test

#endif // TIGERMAIL_TIGERMAIL_TEST_FAKE_ETHEREUM_HPP
