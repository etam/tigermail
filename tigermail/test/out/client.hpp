/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_TIGERMAIL_TEST_OUT_CLIENT_HPP
#define TIGERMAIL_TIGERMAIL_TEST_OUT_CLIENT_HPP

#include <ostream>

#include "../../client.hpp"

#include "../../../common/test/out/buffer.hpp"
#include "../../../ethereum/test/out/common.hpp"


namespace TigerMail {


static
std::ostream& operator<<(std::ostream& o, const Message& message) {
    return o << "Message{from: " << message.from << ", to " << message.to << ", content: " << message.content << "}";
}


} // namespace TigerMail

#endif // TIGERMAIL_TIGERMAIL_TEST_OUT_CLIENT_HPP
