/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_TIGERMAIL_MIME_HPP
#define TIGERMAIL_TIGERMAIL_MIME_HPP

#include <exception>
#include <memory>
#include <string>
#include <vector>

#include <gsl/pointers>

#include <vmime/emailAddress.hpp>
#include <vmime/message.hpp>

#include "../common/buffer.hpp"
#include "../common/buffer_view.hpp"
#include "../common/utils.hpp"


namespace TigerMail {

namespace Ethereum {
class Address;
} // namespace Ethereum

namespace SMTP {
struct Message;
} // namespace SMTP

struct Message;


class ParsingError
    : public std::exception
{
private:
    std::string m_what;

public:
    explicit
    ParsingError(const char* what) noexcept
        : m_what{what}
    {}

    explicit
    ParsingError(std::string&& what) noexcept
        : m_what{std::move(what)}
    {}

    ~ParsingError() noexcept override = default;

    const char* what() const noexcept override
    {
        return m_what.data();
    }
};


Ethereum::Address parse_tigermail_address(const vmime::emailAddress address);
std::string address_to_tigermail(const Ethereum::Address& ethereum_address);


vmime::message parse_message(const std::string& raw_message);
vmime::message parse_message(BufferView raw_message);

struct BoolAndWhy
{
    bool value;
    std::string why;
};

/*
  Incoming from TigerMail, not SMTP.
  Checks if:
  - "From", "To", "Cc" and "Bcc" have proper tigermail addresses
  - sender ethereum address matches "From" flied.
 */
BoolAndWhy is_incoming_message_valid(const Message& message);

/*
  Checks if:
  - "From", "To", "Cc" and "Bcc" have proper tigermail addresses
  - sender ethereum address matches "From" field.
  - recipients ethereum addresses are in "To:", "Cc" or "Bcc"
 */
BoolAndWhy is_outgoing_message_valid(
    const Ethereum::Address& from,
    std::vector<temporary_reference_wrapper<const Ethereum::Address>> to,
    gsl::not_null<std::shared_ptr<const vmime::header>> mime_message_header);


struct PreparedMessage
{
    std::vector<Ethereum::Address> recipients;
    Buffer content;
};

PreparedMessage validate_and_prepare_outgoing_message(
    SMTP::Message&& smtp_message,
    const Ethereum::Address& my_address);


std::string format_system_message(const Ethereum::Address& user_address, std::string&& msg);


} // namespace TigerMail

#endif // TIGERMAIL_TIGERMAIL_MIME_HPP
