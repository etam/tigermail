/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "mime.hpp"

#include <algorithm>
#include <functional>
#include <memory>
#include <sstream>

#include <range/v3/core.hpp>
#include <range/v3/action/sort.hpp>
#include <range/v3/action/unique.hpp>
#include <range/v3/algorithm/all_of.hpp>
#include <range/v3/experimental/view/shared.hpp>
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/concat.hpp>
#include <range/v3/view/join.hpp>
#include <range/v3/view/remove_if.hpp>
#include <range/v3/view/single.hpp>
#include <range/v3/view/transform.hpp>

#include <vmime/addressList.hpp>
#include <vmime/emailAddress.hpp>
#include <vmime/mailbox.hpp>
#include <vmime/mailboxGroup.hpp>
#include <vmime/message.hpp>
#include <vmime/messageBuilder.hpp>
#include <vmime/plainTextPart.hpp>
#include <vmime/stringContentHandler.hpp>
#include <vmime/text.hpp>
#include <vmime/types.hpp>
#include <vmime/utility/inputStreamByteBufferAdapter.hpp>
#include <vmime/utility/outputStreamByteArrayAdapter.hpp>

#include "../common/hex.hpp"
#include "../common/string_view.hpp"

#include "../ethereum/common.hpp"

#include "../smtp/session_handler.hpp"

#include "client.hpp"


namespace TigerMail {


namespace {


const auto tigermail_domain = std::string{"tigermail.eth"};


template <typename T>
T parse_bytes_to(BufferView bytes)
{
    auto obj = T{};
    auto buffer_adapter = std::make_shared<vmime::utility::inputStreamByteBufferAdapter>(
        bytes.data(), bytes.size());
    obj.parse(buffer_adapter, bytes.size());
    return obj;
}


template <typename T>
T parse_string_to(const std::string& string)
{
    auto obj = T{};
    obj.parse(string);
    return obj;
}


BoolAndWhy is_sender_address_matching(const Ethereum::Address& from, const vmime::header& mime_message_header)
try {
    const auto mime_from = parse_tigermail_address(mime_message_header.From()->getValue<vmime::mailbox>()->getEmail());
    if (mime_from != from) {
        auto msg = std::ostringstream{};
        msg << "sender address mismatch: mime_from=" << bin2hex(mime_from) << ", ethereum_from=" << bin2hex(from);
        return {false, msg.str()};
    }
    return {true, ""};
}
catch (std::exception& e) {
    auto msg = std::ostringstream{};
    msg << "parsing sender address error: " << e.what();
    return {false, msg.str()};
}


auto parse_mime_recipients(const vmime::header& mime_message_header)
{
    namespace views = ranges::views;
    return
        views::concat(
            views::single(mime_message_header.To()),
            views::single(mime_message_header.Cc()),
            views::single(mime_message_header.Bcc()))
        | views::remove_if(std::logical_not{})
        | views::transform(
            [](const std::shared_ptr<const vmime::headerField> field) {
                return field->getValue<vmime::addressList>()->getAddressList();
            })
        | views::transform(ranges::experimental::views::shared)
        | views::join
        | views::transform(
            [](const std::shared_ptr<const vmime::address> address) {
                if (address->isGroup()) {
                    return std::dynamic_pointer_cast<const vmime::mailboxGroup>(address)->getMailboxList();
                }
                else {
                    return std::vector{std::dynamic_pointer_cast<const vmime::mailbox>(address)};
                }
            })
        | views::transform(ranges::experimental::views::shared)
        | views::join
        | views::transform(
            [](const std::shared_ptr<const vmime::mailbox> mailbox) {
                return mailbox->getEmail();
            })
        | views::transform(parse_tigermail_address);
}


} // namespace


Ethereum::Address parse_tigermail_address(const vmime::emailAddress address)
{
    if (address.getDomainName().getBuffer() != tigermail_domain) {
        throw ParsingError{"incorrect domain"};
    }

    try {
        return hex2bin<Ethereum::Address>(address.getLocalName().getBuffer());
    }
    catch (ConversionError&) {
        throw ParsingError{"hex conversion failed"};
    }
}


std::string address_to_tigermail(const Ethereum::Address& ethereum_address)
{
    return vmime::emailAddress{bin2hex(ethereum_address), tigermail_domain}.toString();
}


vmime::message parse_message(const std::string& raw_message)
{
    return parse_string_to<vmime::message>(raw_message);
}

vmime::message parse_message(BufferView raw_message)
{
    return parse_bytes_to<vmime::message>(raw_message);
}


BoolAndWhy is_incoming_message_valid(const Message& message)
try {
    const auto mime_message_header = parse_bytes_to<vmime::message>(message.content).getHeader();

    if (auto is_matching = is_sender_address_matching(message.from, *mime_message_header); !is_matching.value) {
        auto msg = std::ostringstream{};
        msg << "incoming message " << std::move(is_matching.why);
        return {false, msg.str()};
    }

    // check if addresses in "To", "Cc" and "Bcc" are parsable
    try {
        ranges::all_of(
            parse_mime_recipients(*mime_message_header),
            [](const auto&) { return true; });
    }
    catch (std::exception& e) {
        auto msg = std::ostringstream{};
        msg << "incoming message recipient is invalid: " << e.what();
        return {false, msg.str()};
    }

    return {true, ""};
}
catch (std::exception& e) {
    auto msg = std::ostringstream{};
    msg << "other error: " << e.what();
    return {false, msg.str()};
}

BoolAndWhy is_outgoing_message_valid(
    const Ethereum::Address& from,
    std::vector<temporary_reference_wrapper<const Ethereum::Address>> to,
    gsl::not_null<std::shared_ptr<const vmime::header>> mime_message_header)
try {
    if (auto is_matching = is_sender_address_matching(from, *mime_message_header); !is_matching.value) {
        auto msg = std::ostringstream{};
        msg << "outgoing message " << std::move(is_matching.why);
        return {false, msg.str()};
    }

    const auto parsed_mime_recipients =
        parse_mime_recipients(*mime_message_header)
        | ranges::to_vector
        | ranges::actions::sort
        | ranges::actions::unique;

    if (parsed_mime_recipients.size() == 0) {
        return {false, "no mime recipients"};
    }

    to |= ranges::actions::sort
        | ranges::actions::unique;

    if (!std::equal(parsed_mime_recipients.begin(), parsed_mime_recipients.end(),
                   to.begin(), to.end())) {
        return {false, "mime recipients doesn't match ethereum"};
    }

    return {true, ""};
}
catch (std::exception& e) {
    auto msg = std::ostringstream{};
    msg << "other error: " << e.what();
    return {false, msg.str()};
}


PreparedMessage validate_and_prepare_outgoing_message(
    SMTP::Message&& smtp_message,
    const Ethereum::Address& my_address)
{
    auto ethereum_recipients_addresses =
        smtp_message.recipients
        | ranges::views::transform(parse_tigermail_address)
        | ranges::to_vector
        | ranges::actions::sort
        | ranges::actions::unique;

    auto message_content =
        [&] {
            auto mime_message = parse_message(*smtp_message.message);

            if (auto is_valid = is_outgoing_message_valid(
                    my_address,
                    ethereum_recipients_addresses
                        | ranges::views::transform(constructor<temporary_reference_wrapper<const Ethereum::Address>>)
                        | ranges::to_vector,
                    gsl::not_null{mime_message.getHeader()});
                    !is_valid.value) {
                throw ParsingError{std::move(is_valid.why)};
            }

            mime_message.getHeader()->removeAllFields(vmime::fields::BCC);

            auto output = Buffer{};
            {
                auto output_stream = vmime::utility::outputStreamByteArrayAdapter{output};
                mime_message.generate(output_stream);
            }
            return output;
        }();

    return {
        .recipients = std::move(ethereum_recipients_addresses),
        .content = message_content,
    };
}


std::string format_system_message(const Ethereum::Address& user_address, std::string&& msg)
{
    auto builder = vmime::messageBuilder{};
    builder.setSubject(vmime::text{"system message"});
    builder.setExpeditor(vmime::mailbox{vmime::text{"TigerMail"}, vmime::emailAddress{"noreply", "tigermail.eth"}});
    builder.getRecipients().appendAddress(
        std::make_shared<vmime::mailbox>(
            address_to_tigermail(user_address)));
    builder.getTextPart()->setText(std::make_shared<vmime::stringContentHandler>(msg));

    return builder.construct()->generate();
}


} // namespace TigerMail
