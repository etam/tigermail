/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_TIGERMAIL_OPTIONS_HPP
#define TIGERMAIL_TIGERMAIL_OPTIONS_HPP

#include <chrono>
#include <string>
#include <filesystem>

#include <boost/log/trivial.hpp>

#include "../ethereum/common.hpp"


namespace TigerMail {


struct Options
{
    Ethereum::Address contract_address;
    Ethereum::Address user_ethereum_address;
    std::string ethereum_password;
    std::filesystem::path tigermail_data_dir;
    std::filesystem::path ethereum_data_dir;
    std::filesystem::path swarm_socket;
    std::chrono::seconds fetch_new_messages_interval;
    std::chrono::seconds rotate_keys_interval;
    bool invalidate;
    boost::log::trivial::severity_level log_level;
};

Options parse_options(int argc, char* argv[]);


} // namespace TigerMail

#endif // TIGERMAIL_TIGERMAIL_OPTIONS_HPP
