/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "options.hpp"

#include <cstdlib>
#include <iostream>
#include <string_view>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>

#include "../common/fs.hpp"
#include "../common/hex.hpp"

namespace po = boost::program_options;


namespace TigerMail {


namespace {

// some inspiration: https://gist.github.com/ksimek/4a2814ba7d74f778bbee


void print_usage(
    std::ostream& o,
    const std::string& program_name,
    const po::options_description& desc,
    po::positional_options_description& p)
{
    o << "Usage: " << program_name << ' ';

    const auto N = p.max_total_count();
    for (size_t i = 0; i < N; ++i) {
        o << p.name_for_position(i) << ' ';
    }

    if (desc.options().size() > 0) {
        o << "[options]";
    }

    o << '\n' << desc;
}


void validate_interval(std::string_view name, std::chrono::seconds min, std::chrono::seconds interval)
{
    if (interval < min) {
        std::cout << name << " is below allowed minimum\n";
        std::exit(1);
    }
}


} // namespace


Options parse_options(int argc, char* argv[])
{
    auto options = Options{};

    const auto home = std::filesystem::path{std::getenv("HOME")};
    const auto tigermail_data_dir =
        [&] {
            // https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
            const auto data_home_env = std::getenv("XDG_DATA_HOME");
            if (!data_home_env || std::string_view{data_home_env}.empty()) {
                return home / ".local" / "share" / "tigermail";
            }
            return std::filesystem::path{data_home_env} / "tigermail";
        }();

    auto opts_desc = po::options_description{"Options"};
    opts_desc.add_options()
        ("help,h", "print this message")
        ("data_dir", po::value(&options.tigermail_data_dir)->default_value(tigermail_data_dir))
        ("ethereum_data_dir", po::value(&options.ethereum_data_dir)->default_value(home / ".ethereum"))
        ("swarm_socket", po::value(&options.swarm_socket)->default_value(home / ".ethereum/bzzd.ipc"))
        ("fetch_new_messages_interval", po::value<int>()->default_value(60), "(in seconds; min=15)")
        ("rotate_keys_interval", po::value<int>()->default_value(60*60*24*14), "(in seconds; min=60; default=14 days)")
        ("invalidate", "")
        ("log_level", po::value(&options.log_level)->default_value(boost::log::trivial::info),
            "one of: debug, info, error")
    ;

    auto hidden_opts_desc = po::options_description{};
    hidden_opts_desc.add_options()
        ("contract_address", po::value<std::string>()->required())
        ("user_address", po::value<std::string>()->required())
        ("password_file", po::value<std::string>()->required())
    ;

    auto pos_opts_desc = po::positional_options_description{};
    pos_opts_desc
        .add("contract_address", 1)
        .add("user_address", 1)
        .add("password_file", 1)
    ;

    auto all_options = po::options_description{};
    all_options.add(opts_desc);
    all_options.add(hidden_opts_desc);

    auto vm = po::variables_map{};
    po::store(po::command_line_parser{argc, argv}.options(all_options).positional(pos_opts_desc).run(), vm);

    if (vm.count("help")) {
        print_usage(
            std::cout,
            std::filesystem::path(argv[0]).stem().string(),
            opts_desc,
            pos_opts_desc);
        std::exit(0);
    }

    po::notify(vm);

    options.contract_address = zxhex2bin<Ethereum::Address>(vm["contract_address"].as<std::string>());
    options.user_ethereum_address = zxhex2bin<Ethereum::Address>(vm["user_address"].as<std::string>());
    options.ethereum_password = read_file_contents(vm["password_file"].as<std::string>());
    options.fetch_new_messages_interval = std::chrono::seconds{vm["fetch_new_messages_interval"].as<int>()};
    options.rotate_keys_interval = std::chrono::seconds{vm["rotate_keys_interval"].as<int>()};
    options.invalidate = !!vm.count("invalidate");

    validate_interval("fetch_new_messages_interval", std::chrono::seconds{15}, options.fetch_new_messages_interval);
    validate_interval("rotate_keys_interval", std::chrono::seconds{60}, options.rotate_keys_interval);

    return options;
}


} // namespace TigerMail
