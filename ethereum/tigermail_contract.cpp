/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tigermail_contract.hpp"
#include "tigermail_contract_internal.hpp"

#include <algorithm>
#include <sstream>
#include <stdexcept>

#include <boost/asio/spawn.hpp>

#include <gsl/gsl_assert>
#include <gsl/gsl_util>

#include <libdevcore/Common.h>
#include <libdevcrypto/Common.h>
#include <libethereum/Transaction.h>

#include "../common/fixed_size_buffer_view.hpp"
#include "../common/hex.hpp"
#include "../common/retrying.hpp"
#include "../common/utils.hpp"
#include "../common/zpad.hpp"

#include "abi.hpp"
#include "rpc_client.hpp"


namespace TigerMail::Ethereum {


namespace {

// events
const auto MessageId_event_id = get_event_id("MessageId(bytes)");
const auto Invalidated_event_id = get_event_id("Invalidated(address)");

// methods
const auto postMessageId_method_id = get_method_id("postMessageId(bytes)");
const auto invalidate_method_id = get_method_id("invalidate()");


} // namespace


Buffer encode_postMessageId_data(BufferView id)
{
    static const auto arg_data_location = zpad<32>(Buffer{0x20});

    const auto encoded_arg_size = encode_uint(id.size());

    auto result = Buffer(
        postMessageId_method_id.size() + arg_data_location.size() + encoded_arg_size.size() + size_with_padding(id.size()),
        0);

    std::copy(postMessageId_method_id.begin(), postMessageId_method_id.end(), result.begin());
    std::copy(arg_data_location.begin(), arg_data_location.end(),
              result.begin() + postMessageId_method_id.size());
    std::copy(encoded_arg_size.begin(), encoded_arg_size.end(),
              result.begin() + postMessageId_method_id.size() + arg_data_location.size());
    std::copy(id.begin(), id.end(),
              result.begin() + postMessageId_method_id.size() + arg_data_location.size() + encoded_arg_size.size());

    return result;
}

std::optional<Buffer> decode_MessageId_data(BufferView data)
{
    if (data.size() % 32 != 0) {
        return std::nullopt;
    }

    const auto blocks_num = data.size() / 32;
    if (blocks_num < 3) {
        return std::nullopt;
    }

    const auto data_location = FixedSizeBufferView<32>{data.data()};
    const auto data_size = FixedSizeBufferView<32>{data.data() + 32};
    const auto data_data = BufferView{data.data() + 32*2, (blocks_num-2)*32};

    // check location
    {
        static const auto expected_data_location = zpad<32>(Buffer{0x20});
        if (data_location != expected_data_location) {
            return std::nullopt;
        }
    }

    // check size
    if (std::any_of(data_size.begin(), data_size.begin() + 24, [](const auto i) { return i != 0; })) {
        return std::nullopt;
    }
    const auto opt_size = decode_uint(data_size);
    if (!opt_size || size_with_padding(*opt_size) != gsl::narrow<std::size_t>(data_data.size())) {
        return std::nullopt;
    }

    return Buffer(data_data.begin(), data_data.begin() + *opt_size);
}


Buffer make_MessageId_transaction(
    const dev::u256& gas_price,
    const Address& contract_address,
    BufferView id,
    const dev::u256& nonce,
    const dev::Secret& secret)
{
    const auto tx = dev::eth::Transaction{
        dev::u256{0}, // value
        gas_price,
        dev::u256{0x8060}, // gas limit
        dev::Address{contract_address},
        encode_postMessageId_data(id),
        nonce,
        secret
    };

    return tx.rlp();
}


Buffer make_invalidate_transaction(
    const dev::u256& gas_price,
    const Address& contract_address,
    const dev::u256& nonce,
    const dev::Secret& secret)
{
    const auto tx = dev::eth::Transaction{
        dev::u256{0}, // value
        gas_price,
        dev::u256{0x8060}, // gas limit
        dev::Address{contract_address},
        std::vector(invalidate_method_id.begin(),
                    invalidate_method_id.end()),
        nonce,
        secret
    };

    return tx.rlp();
}


TigerMailContract::TigerMailContract(
    gsl::not_null<RPCClient*> rpc_client,
    Address&& contract_address,
    Address&& user_address,
    dev::Secret&& secret
)
    : TigerMailContractInterface{std::move(user_address)}
    , m_rpc_client{rpc_client}
    , m_contract_address{std::move(contract_address)}
    , m_secret{std::move(secret)}
    , m_nonce{std::nullopt}
{}


void TigerMailContract::setup_nonce(std::any yield)
{
    m_nonce = m_rpc_client->eth_getTransactionCount(m_user_address, yield);
}

FixedSizeBuffer<32> TigerMailContract::postMessageId(BufferView id, std::any yield)
{
    Expects(m_nonce != std::nullopt);

    const auto gas_price = m_rpc_client->eth_gasPrice(yield);

    const auto tx = make_MessageId_transaction(
        gas_price,
        m_contract_address,
        id,
        (*m_nonce)++,
        m_secret);

    return m_rpc_client->eth_sendRawTransaction(tx, yield);
}

std::vector<MessageIdLog> TigerMailContract::get_message_id_logs(
    unsigned from_block, unsigned to_block, std::any yield)
{
    const auto logs = m_rpc_client->eth_getLogs(
        from_block,
        to_block,
        {m_contract_address},
        {MessageId_event_id},
        yield);

    // TODO: ranges?
    auto result = std::vector<MessageIdLog>{};
    for (const auto& log : logs) {
        auto message_id = decode_MessageId_data(log.data);
        if (!message_id) {
            continue;
        }

        // it happens that it returns null, even if we got an event log
        auto tx = retrying(
            [&](std::any yield) {
                return m_rpc_client->eth_getTransactionByHash(*log.transactionHash, yield);
            },
            yield);

        if (!tx) {
            auto msg = std::ostringstream{};
            msg << "could not get the transaction " << bin2zxhex(*log.transactionHash)
                << " (this is not fatal; receiving messages will be retried later)";
            throw std::runtime_error{msg.str()};
        }

        result.emplace_back(
            *log.blockNumber,
            std::move(tx->from),
            std::move(*message_id)
        );
    }
    return result;
}


FixedSizeBuffer<32> TigerMailContract::invalidate(std::any yield)
{
    Expects(m_nonce != std::nullopt);

    const auto gas_price = m_rpc_client->eth_gasPrice(yield);

    const auto tx = make_invalidate_transaction(
        gas_price,
        m_contract_address,
        (*m_nonce)++,
        m_secret);

    return m_rpc_client->eth_sendRawTransaction(tx, yield);
}

bool TigerMailContract::is_invalidated(const Address& address, BlockNumberOrTag to_block, std::any yield)
{
    const auto logs = m_rpc_client->eth_getLogs(
        BlockTag::earliest,
        to_block,
        {m_contract_address},
        {
            Invalidated_event_id,
            zpad<32>(address)
        },
        yield);
    return !logs.empty();
}

unsigned TigerMailContract::get_block_number(std::any yield)
{
    return m_rpc_client->eth_blockNumber(yield);
}


} // namespace TigerMail::Ethereum
