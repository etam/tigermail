/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_ETHEREUM_TIGERMAIL_CONTRACT_INTERFACE_HPP
#define TIGERMAIL_ETHEREUM_TIGERMAIL_CONTRACT_INTERFACE_HPP

#include <any>
#include <vector>

#include "../common/buffer.hpp"
#include "../common/buffer_view.hpp"
#include "../common/fixed_size_buffer.hpp"

#include "common.hpp"


namespace TigerMail::Ethereum {


struct MessageIdLog
{
    unsigned block_number;
    Address sender_address;
    Buffer data;

    MessageIdLog() = default;
    MessageIdLog(unsigned block_number_,
                 Address&& sender_address_,
                 Buffer&& data_)
        : block_number{block_number_}
        , sender_address{std::move(sender_address_)}
        , data{std::move(data_)}
    {}
};

bool operator==(const MessageIdLog& log1, const MessageIdLog& log2);

inline
bool operator!=(const MessageIdLog& log1, const MessageIdLog& log2)
{
    return !(log1 == log2);
}


class TigerMailContractInterface
{
protected:
    Address m_user_address;

public:
    TigerMailContractInterface(Address&& user_address)
        : m_user_address{std::move(user_address)}
    {}
    virtual ~TigerMailContractInterface() noexcept = default;

    // return transaction id
    virtual FixedSizeBuffer<32> postMessageId(BufferView id, std::any continuation) = 0;
    virtual std::vector<MessageIdLog> get_message_id_logs(
        unsigned from_block, unsigned to_block, std::any continuation) = 0;

    // return transaction id
    virtual FixedSizeBuffer<32> invalidate(std::any continuation) = 0;
    virtual bool is_invalidated(const Address& address, BlockNumberOrTag to_block, std::any continuation) = 0;
    bool am_I_invalidated(std::any continuation)
    {
        return is_invalidated(m_user_address, BlockTag::latest, continuation);
    }

    // just for convencience
    virtual unsigned get_block_number(std::any continuation) = 0;
};


} // namespace TigerMail::Ethereum

#endif // TIGERMAIL_ETHEREUM_TIGERMAIL_CONTRACT_INTERFACE_HPP
