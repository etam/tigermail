
var tigermail_solc_output = solc_output["contracts"]["../tigermail_contract.sol:TigerMail"]
var contract = web3.eth.contract(JSON.parse(tigermail_solc_output["abi"]))

web3.eth.defaultAccount = web3.personal.listAccounts[0]
web3.personal.unlockAccount(web3.eth.defaultAccount)

var instance = contract.at("0xaddress")
instance.kill()
