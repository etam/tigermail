
var tigermail_solc_output = solc_output["contracts"]["../tigermail_contract.sol:TigerMail"]
var contract = web3.eth.contract(JSON.parse(tigermail_solc_output["abi"]))
var bytecode = "0x" + tigermail_solc_output["bin"]

var my_address = web3.personal.listAccounts[0]
web3.personal.unlockAccount(my_address, "", 10)

var gas_estimate = web3.eth.estimateGas({data: bytecode});

contract.new(
    {
        from: my_address,
        data: bytecode,
        gas: gas_estimate,
    },
    function(err, instance) {
        if (!err) {
            if (!instance.address) {
                console.log(myContract.transactionHash)
            }
            else {
                console.log(myContract.address)
            }
        }
    }
)
