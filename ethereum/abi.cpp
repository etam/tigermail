/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "abi.hpp"

#include <algorithm>

#include <boost/endian/buffers.hpp>

#include <gsl/gsl_assert>
#include <gsl/gsl_util>

#include "crypto.hpp"

#include "../common/zpad.hpp"

// I REALLY WISH THERE WAS A LIBRARY FOR THAT!!!


namespace TigerMail::Ethereum {


std::size_t size_with_padding(std::size_t size)
{
    return ((size+31) / 32) * 32;
}


FixedSizeBuffer<32> encode_uint(std::uint64_t i)
{
    const auto big_endian = boost::endian::big_uint64_buf_t{i};
    return zpad<32>(BufferView(reinterpret_cast<const std::uint8_t*>(big_endian.data()), 8));
}

std::optional<std::uint64_t> decode_uint(FixedSizeBufferView<32> buf)
{
    if (!std::all_of(buf.begin(), buf.begin() + 24, [](const auto i) { return i == 0; })) {
        return std::nullopt;
    }
    auto big_endian = boost::endian::big_uint64_buf_t{};
    // use data() when exposed as non-const https://github.com/boostorg/endian/pull/25
    std::copy(buf.begin() + 24, buf.end(), const_cast<char*>(big_endian.data()));
    return big_endian.value();
}


FixedSizeBuffer<32> get_event_id(std::string_view signature)
{
    return keccak({reinterpret_cast<const std::uint8_t*>(signature.data()),
                   gsl::narrow<BufferView::index_type>(signature.size())});
}

FixedSizeBuffer<4> get_method_id(std::string_view signature)
{
    const auto hash = keccak({reinterpret_cast<const std::uint8_t*>(signature.data()),
                              gsl::narrow<BufferView::index_type>(signature.size())});
    return FixedSizeBuffer<4>(hash.begin(), hash.begin() + 4);
}


} // namespace TigerMail::Ethereum
