/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_ETHEREUM_RPC_CLIENT_INTERNAL_HPP
#define TIGERMAIL_ETHEREUM_RPC_CLIENT_INTERNAL_HPP
// those headers are only for unit tests

#include <optional>
#include <string>
#include <vector>

#include <json/value.h>

#include "../common/buffer_view.hpp"
#include "../common/utils.hpp"

#include "common.hpp"
#include "rpc_client.hpp"


namespace TigerMail::Ethereum {


std::string serialize(BlockTag tag);
std::string serialize(BlockNumberOrTag nr);


Json::Value jsonize_eth_newFilter_params(
    std::optional<BlockNumberOrTag> fromBlock,
    std::optional<BlockNumberOrTag> toBlock,
    std::vector<temporary_reference_wrapper<const Address>> address,
    std::vector<temporary_reference_wrapper<const Topic>> topics);


} // namespace TigerMail::Ethereum

#endif // TIGERMAIL_ETHEREUM_RPC_CLIENT_INTERNAL_HPP
