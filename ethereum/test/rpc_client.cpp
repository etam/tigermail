/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <any>
#include <optional>
#include <ostream>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <gsl/pointers>

#include <json/value.h>
#include <json/writer.h>

#include "../../common/buffer.hpp"
#include "../../common/buffer_view.hpp"
#include "../../common/zpad.hpp"
#include "../../common/test/fake_yield.hpp"

#include "../../common/test/out/buffer.hpp"
#include "../../common/test/out/fixed_size_buffer.hpp"
#include "../../common/test/out/std_optional.hpp"
#include "../../common/test/out/std_vector.hpp"

#include "../../json_rpc/json.hpp"

#include "../common.hpp"
#include "../rpc_client_internal.hpp"

#include "out/rpc_client.hpp"


namespace TigerMail::Ethereum {


static
bool operator==(const RPCClient::Transaction& t1,
                const RPCClient::Transaction& t2)
{
    return
        std::tie(
            t1.blockHash,
            t1.blockNumber,
            t1.from,
            t1.gas,
            t1.gasPrice,
            t1.hash,
            t1.input,
            t1.nonce,
            t1.to,
            t1.transactionIndex,
            t1.value,
            t1.v,
            t1.r,
            t1.s
        ) == std::tie(
            t2.blockHash,
            t2.blockNumber,
            t2.from,
            t2.gas,
            t2.gasPrice,
            t2.hash,
            t2.input,
            t2.nonce,
            t2.to,
            t2.transactionIndex,
            t2.value,
            t2.v,
            t2.r,
            t2.s
        );
}

// static
// bool operator!=(const RPCClient::Transaction& t1,
//                 const RPCClient::Transaction& t2)
// {
//     return !(t1 == t2);
// }

static
bool operator==(const RPCClient::FilterLog& l1,
                const RPCClient::FilterLog& l2)
{
    return
        std::tie(
            l1.removed,
            l1.logIndex,
            l1.transactionIndex,
            l1.transactionHash,
            l1.blockHash,
            l1.blockNumber,
            l1.address,
            l1.data,
            l1.topics
        ) == std::tie(
            l2.removed,
            l2.logIndex,
            l2.transactionIndex,
            l2.transactionHash,
            l2.blockHash,
            l2.blockNumber,
            l2.address,
            l2.data,
            l2.topics
        );
}

static
bool operator!=(const RPCClient::FilterLog& l1,
                const RPCClient::FilterLog& l2)
{
    return !(l1 == l2);
}


} // namespace TigerMail::Ethereum


BOOST_AUTO_TEST_SUITE(rpc_client_internals)


BOOST_AUTO_TEST_CASE(serialize_block_tag)
{
    using TigerMail::Ethereum::BlockTag;
    using TigerMail::Ethereum::serialize;
    BOOST_CHECK_EQUAL(serialize(BlockTag::earliest), "earliest");
    BOOST_CHECK_EQUAL(serialize(BlockTag::latest), "latest");
    BOOST_CHECK_EQUAL(serialize(BlockTag::pending), "pending");
}

BOOST_AUTO_TEST_CASE(serialize_block_number)
{
    using TigerMail::Ethereum::BlockNumberOrTag;
    using TigerMail::Ethereum::BlockTag;
    using TigerMail::Ethereum::serialize;
    BOOST_CHECK_EQUAL(serialize(BlockNumberOrTag{BlockTag::latest}), "latest");
    BOOST_CHECK_EQUAL(serialize(BlockNumberOrTag{3931798u}), "0x3bfe96");
}


BOOST_AUTO_TEST_SUITE(jsonize_eth_newFilter_params)

using TigerMail::Ethereum::jsonize_eth_newFilter_params;
using TigerMail::JsonRpc::make_array;
using TigerMail::JsonRpc::make_object;


BOOST_AUTO_TEST_CASE(no_data)
{
    const auto params = jsonize_eth_newFilter_params(std::nullopt, std::nullopt, {}, {});
    const auto expected_params = make_array({make_object({})});
    BOOST_CHECK_EQUAL(params, expected_params);
}

BOOST_AUTO_TEST_CASE(asAddressFromZxhex)
{
    using TigerMail::Ethereum::Address;
    using TigerMail::JsonRpc::asDataFromZxhex;

    const auto json_address = Json::Value{"0x0001020304050607080910111213141516171819"};
    const auto address = asDataFromZxhex<Address>([]{return "";}, json_address);

    const auto expected_address = Address{
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
    };
    BOOST_CHECK_EQUAL_COLLECTIONS(address.begin(), address.end(), expected_address.begin(), expected_address.end());
}

BOOST_AUTO_TEST_CASE(some_data)
{
    using TigerMail::Buffer;
    using TigerMail::BufferView;
    using TigerMail::zpad;
    using TigerMail::Ethereum::Address;
    using TigerMail::Ethereum::BlockTag;
    using TigerMail::Ethereum::Topic;

    const auto params = jsonize_eth_newFilter_params(
        3931798u,
        BlockTag::pending,
        {Address{0xde, 0xad, 0xbe, 0xef}},
        {
            zpad<32>(Buffer{0xca, 0xfe}),
            zpad<32>(Buffer{0xba, 0xbe}),
        }
    );

    const auto expected_params =
        make_array({
                make_object({
                        {"fromBlock", "0x3bfe96"},
                        {"toBlock", "pending"},
                        {"address", "0xdeadbeef00000000000000000000000000000000"},
                        {"topics", make_array({
                                    "0x000000000000000000000000000000000000000000000000000000000000cafe",
                                    "0x000000000000000000000000000000000000000000000000000000000000babe"
                                })},
                    })
            });

    BOOST_CHECK_EQUAL(params, expected_params);
}


BOOST_AUTO_TEST_SUITE_END() // jsonize_eth_newFilter_params


BOOST_AUTO_TEST_SUITE_END() // rpc_client_internals

using TigerMail::Ethereum::RPCClient;
using TigerMail::Ethereum::TransportProtocolClientInterface;
using TigerMail::Test::FakeYield;


namespace {


class FakeTransportProtocolClient
    : public TransportProtocolClientInterface
{
private:
    std::string m_expected_request;
    std::string m_response;

public:
    FakeTransportProtocolClient() = default;

    void connect(std::any) override {}

    void expect(
        std::string&& expected_request,
        std::string&& response)
    {
        m_expected_request = std::move(expected_request);
        m_response = std::move(response);
    }

    std::string call(std::string_view request, std::any yield) override
    {
        std::any_cast<FakeYield>(yield);
        BOOST_CHECK_EQUAL(request, m_expected_request);
        return m_response;
    }
};


struct RPCClientFixture
{
    FakeYield yield{FakeYield::explicit_construction};
    FakeTransportProtocolClient fake_transport_protocol_client{};
    RPCClient rpc_client{gsl::not_null{&fake_transport_protocol_client}};
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(rpc_client, RPCClientFixture)


BOOST_AUTO_TEST_CASE(eth_blockNumber)
{
    // given
    fake_transport_protocol_client.expect(
        R"json({"id":0,"jsonrpc":"2.0","method":"eth_blockNumber"}
)json",
        R"json({"jsonrpc":"2.0","id":0,"result":"0xc94"})json");

    // when
    const auto result = rpc_client.eth_blockNumber(yield);

    // then
    BOOST_CHECK_EQUAL(result, 3220);
}

BOOST_AUTO_TEST_CASE(eth_gasPrice)
{
    // given
    fake_transport_protocol_client.expect(
        R"json({"id":0,"jsonrpc":"2.0","method":"eth_gasPrice"}
)json",
        R"json({"jsonrpc":"2.0","id":0,"result":"0x09184e72a000"})json");

    // when
    const auto result = rpc_client.eth_gasPrice(yield);

    // then
    BOOST_CHECK_EQUAL(result, 10000000000000);
}

BOOST_AUTO_TEST_CASE(eth_getTransactionByHash)
{
    using TigerMail::Buffer;
    using TigerMail::FixedSizeBuffer;
    using TigerMail::Ethereum::Address;
    using TigerMail::Ethereum::BlockHash;
    using TigerMail::Ethereum::RPCClient;
    using TigerMail::Ethereum::TransactionHash;
    using TigerMail::JsonRpc::parse_json;

    // given
    fake_transport_protocol_client.expect(
        R"json({"id":0,"jsonrpc":"2.0","method":"eth_getTransactionByHash","params":["0x88df016429689c079f3b2f6ad39fa052532c56795b733da78a91ebe6a713944b"]}
)json",
        R"json({"jsonrpc":"2.0","id":0,"result":
{
    "blockHash": "0x1d59ff54b1eb26b013ce3cb5fc9dab3705b415a67127a003c3e61eb445bb8df2",
    "blockNumber": "0x5daf3b",
    "from": "0xa7d9ddbe1f17865597fbd27ec712455208b6b76d",
    "gas": "0xc350",
    "gasPrice": "0x4a817c800",
    "hash": "0x88df016429689c079f3b2f6ad39fa052532c56795b733da78a91ebe6a713944b",
    "input": "0x68656c6c6f21",
    "nonce": "0x15",
    "to": "0xf02c1c8e6114b1dbe8937a39260b5b0a374432bb",
    "transactionIndex": "0x41",
    "value": "0xf3dbb76162000",
    "v": "0x25",
    "r": "0x1b5e176d927f8e9ab405058b2d2457392da3e20f328b16ddabcebc33eaac5fea",
    "s": "0x4ba69724e8f69de52f0125ad8b3c5c2cef33019bac3249e2c0a2192766d1721c"
}})json");

    // when
    const auto result = rpc_client.eth_getTransactionByHash(
        TransactionHash{0x88,0xdf,0x01,0x64,0x29,0x68,0x9c,0x07,0x9f,0x3b,0x2f,0x6a,0xd3,0x9f,0xa0,0x52,0x53,0x2c,0x56,0x79,0x5b,0x73,0x3d,0xa7,0x8a,0x91,0xeb,0xe6,0xa7,0x13,0x94,0x4b}, yield);

    // then
    const auto expected_result = std::optional{RPCClient::Transaction{
        .blockHash = BlockHash{0x1d,0x59,0xff,0x54,0xb1,0xeb,0x26,0xb0,0x13,0xce,0x3c,0xb5,0xfc,0x9d,0xab,0x37,0x05,0xb4,0x15,0xa6,0x71,0x27,0xa0,0x03,0xc3,0xe6,0x1e,0xb4,0x45,0xbb,0x8d,0xf2},
        .blockNumber = 6139707,
        .from = Address{0xa7,0xd9,0xdd,0xbe,0x1f,0x17,0x86,0x55,0x97,0xfb,0xd2,0x7e,0xc7,0x12,0x45,0x52,0x08,0xb6,0xb7,0x6d},
        .gas = 50000,
        .gasPrice = 20000000000,
        .hash = FixedSizeBuffer<32>{0x88,0xdf,0x01,0x64,0x29,0x68,0x9c,0x07,0x9f,0x3b,0x2f,0x6a,0xd3,0x9f,0xa0,0x52,0x53,0x2c,0x56,0x79,0x5b,0x73,0x3d,0xa7,0x8a,0x91,0xeb,0xe6,0xa7,0x13,0x94,0x4b},
        .input = Buffer{0x68,0x65,0x6c,0x6c,0x6f,0x21},
        .nonce = 21,
        .to = Address{0xf0,0x2c,0x1c,0x8e,0x61,0x14,0xb1,0xdb,0xe8,0x93,0x7a,0x39,0x26,0x0b,0x5b,0x0a,0x37,0x44,0x32,0xbb},
        .transactionIndex = 65,
        .value = 4290000000000000,
        .v = 37,
        .r = dev::u256{"0x1b5e176d927f8e9ab405058b2d2457392da3e20f328b16ddabcebc33eaac5fea"},
        .s = dev::u256{"0x4ba69724e8f69de52f0125ad8b3c5c2cef33019bac3249e2c0a2192766d1721c"},
    }};

    BOOST_CHECK_EQUAL(result, expected_result);
}

BOOST_AUTO_TEST_CASE(eth_getTransactionCount)
{
    using TigerMail::Ethereum::Address;

    // given
    fake_transport_protocol_client.expect(
        R"json({"id":0,"jsonrpc":"2.0","method":"eth_getTransactionCount","params":["0xc94770007dda54cf92009bff0de90c06f603a09f","pending"]}
)json",
        R"json({"jsonrpc":"2.0","id":0,"result":"0x1"})json");

    // when
    const auto result = rpc_client.eth_getTransactionCount(
        Address{0xc9,0x47,0x70,0x00,0x7d,0xda,0x54,0xcF,0x92,0x00,0x9B,0xFF,0x0d,0xE9,0x0c,0x06,0xF6,0x03,0xa0,0x9f}, yield);

    // then
    BOOST_CHECK_EQUAL(result, 1);
}

BOOST_AUTO_TEST_CASE(eth_getLogs)
{
    using TigerMail::Buffer;
    using TigerMail::zpad;
    using TigerMail::Ethereum::Address;
    using TigerMail::Ethereum::BlockHash;
    using TigerMail::Ethereum::RPCClient;
    using TigerMail::Ethereum::Topic;
    using TigerMail::Ethereum::TransactionHash;
    using TigerMail::JsonRpc::parse_json;

    // given
    fake_transport_protocol_client.expect(
        R"json({"id":0,"jsonrpc":"2.0","method":"eth_getLogs","params":[{"topics":["0x000000000000000000000000a94f5374fce5edbc8e2a8697c15331677e6ebf0b"]}]}
)json",
        R"json({"jsonrpc":"2.0","id":0,"result":[
{
    "removed": false,
    "logIndex": "0x1",
    "blockNumber": "0x1b4",
    "blockHash": "0x008216c5785ac562ff41e2dcfdf5785ac562ff41e2dcfdf829c5a142f1fccd7d",
    "transactionHash": "0x00df829c5a142f1fccd7d8216c5785ac562ff41e2dcfdf5785ac562ff41e2dcf",
    "transactionIndex": "0x0", // 0
    "address": "0x16c5785ac562ff41e2dcfdf829c5a142f1fccd7d",
    "data": "0x0000000000000000000000000000000000000000000000000000000000000000",
    "topics": [
        "0x59ebeb90bc63057b6515673c3ecf9438e5058bca0f92585014eced636878c9a5"
    ]
},{
    "removed": false,
    "logIndex": null,
    "blockNumber": null,
    "blockHash": null,
    "transactionHash": null,
    "transactionIndex": null,
    "address": "0xdeadbeef00000000000000000000000000000000",
    "data":"0xcafebabe",
    "topics": [
        "0x0000000000000000000000000000000000000000000000000000000012345678",
        "0x0000000000000000000000000000000000000000000000000000000000abcdef"
    ]
}
]})json");

    // when
    const auto result = rpc_client.eth_getLogs(
        std::nullopt, std::nullopt, {},
        {Topic{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xa9,0x4f,0x53,0x74,0xfc,0xe5,0xed,0xbc,0x8e,0x2a,0x86,0x97,0xc1,0x53,0x31,0x67,0x7e,0x6e,0xbf,0x0b}},
        yield);

    // then
    const auto expected_result = std::vector<RPCClient::FilterLog>{
        {
            .removed = false,
            .logIndex = 1,
            .transactionIndex = 0,
            .transactionHash = TransactionHash{0x00,0xdf,0x82,0x9c,0x5a,0x14,0x2f,0x1f,0xcc,0xd7,0xd8,0x21,0x6c,0x57,0x85,0xac,0x56,0x2f,0xf4,0x1e,0x2d,0xcf,0xdf,0x57,0x85,0xac,0x56,0x2f,0xf4,0x1e,0x2d,0xcf},
            .blockHash = BlockHash{0x00,0x82,0x16,0xc5,0x78,0x5a,0xc5,0x62,0xff,0x41,0xe2,0xdc,0xfd,0xf5,0x78,0x5a,0xc5,0x62,0xff,0x41,0xe2,0xdc,0xfd,0xf8,0x29,0xc5,0xa1,0x42,0xf1,0xfc,0xcd,0x7d},
            .blockNumber = 436,
            .address = Address{0x16,0xc5,0x78,0x5a,0xc5,0x62,0xff,0x41,0xe2,0xdc,0xfd,0xf8,0x29,0xc5,0xa1,0x42,0xf1,0xfc,0xcd,0x7d},
            .data = Buffer(32),
            .topics = std::vector<Topic>{
                {0x59,0xeb,0xeb,0x90,0xbc,0x63,0x05,0x7b,0x65,0x15,0x67,0x3c,0x3e,0xcf,0x94,0x38,0xe5,0x05,0x8b,0xca,0x0f,0x92,0x58,0x50,0x14,0xec,0xed,0x63,0x68,0x78,0xc9,0xa5},
            },
        },
        {
            .removed = false,
            .logIndex = std::nullopt,
            .transactionIndex = std::nullopt,
            .transactionHash = std::nullopt,
            .blockHash = std::nullopt,
            .blockNumber = std::nullopt,
            .address = Address{0xde,0xad,0xbe,0xef},
            .data = Buffer{0xca,0xfe,0xba,0xbe},
            .topics = std::vector<Topic>{
                zpad<32>(Buffer{0x12,0x34,0x56,0x78}),
                zpad<32>(Buffer{0xab,0xcd,0xef}),
            },
        },
    };

    BOOST_CHECK_EQUAL_COLLECTIONS(
        result.begin(), result.end(),
        expected_result.begin(), expected_result.end());
}

BOOST_AUTO_TEST_CASE(eth_sendRawTransaction)
{
    using TigerMail::Buffer;
    using TigerMail::FixedSizeBuffer;

    // given
    fake_transport_protocol_client.expect(
        R"json({"id":0,"jsonrpc":"2.0","method":"eth_sendRawTransaction","params":["0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675"]}
)json",
        R"json({"jsonrpc":"2.0","id":0,"result":"0x0e670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331"})json");

    // when
    const auto result = rpc_client.eth_sendRawTransaction(
        Buffer{0xd4,0x6e,0x8d,0xd6,0x7c,0x5d,0x32,0xbe,0x8d,0x46,0xe8,0xdd,0x67,0xc5,0xd3,0x2b,0xe8,0x05,0x8b,0xb8,0xeb,0x97,0x08,0x70,0xf0,0x72,0x44,0x56,0x75,0x05,0x8b,0xb8,0xeb,0x97,0x08,0x70,0xf0,0x72,0x44,0x56,0x75},
        yield);

    // then
    const auto expected_result = FixedSizeBuffer<32>{0x0e,0x67,0x0e,0xc6,0x43,0x41,0x77,0x16,0x06,0xe5,0x5d,0x6b,0x4c,0xa3,0x5a,0x1a,0x6b,0x75,0xee,0x3d,0x51,0x45,0xa9,0x9d,0x05,0x92,0x10,0x26,0xd1,0x52,0x73,0x31};
    BOOST_CHECK_EQUAL(result, expected_result);
}


BOOST_AUTO_TEST_SUITE_END() // rpc_client
