/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include <gsl/gsl_assert>

#include "../../common/buffer.hpp"
#include "../../common/fixed_size_buffer.hpp"
#include "../../common/test/out/std_optional.hpp"

#include "../abi.hpp"

using TigerMail::Buffer;
using TigerMail::FixedSizeBuffer;


// c++ does not like using just "abi"
BOOST_AUTO_TEST_SUITE(ethereum_abi)


BOOST_AUTO_TEST_CASE(size_with_padding)
{
    using TigerMail::Ethereum::size_with_padding;

    BOOST_CHECK_EQUAL(size_with_padding(0), 0);
    BOOST_CHECK_EQUAL(size_with_padding(1), 32);
    BOOST_CHECK_EQUAL(size_with_padding(31), 32);
    BOOST_CHECK_EQUAL(size_with_padding(32), 32);
    BOOST_CHECK_EQUAL(size_with_padding(33), 32*2);
}


BOOST_AUTO_TEST_CASE(encode_uint)
{
    using TigerMail::Ethereum::encode_uint;

    const auto encoded_size = encode_uint(0x1234567890abcdef);
    const auto expected = FixedSizeBuffer<32>{
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef};
    BOOST_CHECK_EQUAL_COLLECTIONS(encoded_size.begin(), encoded_size.end(), expected.begin(), expected.end());
}


BOOST_AUTO_TEST_SUITE(decode_uint)

using TigerMail::Ethereum::decode_uint;


BOOST_AUTO_TEST_CASE(too_much_data)
{
    const auto data = FixedSizeBuffer<32>{
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11,
        0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99,
    };
    BOOST_CHECK_EQUAL(decode_uint(data), std::nullopt);
}

BOOST_AUTO_TEST_CASE(decode)
{
    const auto data = FixedSizeBuffer<32>{
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef};
    const auto expected_value = 0x1234567890abcdef;
    BOOST_CHECK_EQUAL(decode_uint(data), expected_value);
}


BOOST_AUTO_TEST_SUITE_END() // decode_uint


BOOST_AUTO_TEST_CASE(get_event_id)
{
    using TigerMail::Ethereum::get_event_id;
    const auto id = get_event_id("MessageId(bytes)");
    const auto expected_id = Buffer{0x00,0xb1,0xcf,0x5f,0xb5,0xdc,0x4f,0xb6,0x95,0x12,0x24,0xb8,0x58,0x61,0x47,0xeb,0x34,0x0b,0x69,0x8c,0x82,0x9c,0xe3,0xb6,0xf4,0x27,0x5e,0xb9,0x31,0x61,0xbb,0xfa};
    BOOST_CHECK_EQUAL_COLLECTIONS(id.begin(), id.end(),
                                  expected_id.begin(), expected_id.end());
}


BOOST_AUTO_TEST_SUITE(get_method_id)

// data from https://solidity.readthedocs.io/en/v0.5.4/abi-spec.html
using TigerMail::Ethereum::get_method_id;


BOOST_AUTO_TEST_CASE(bar)
{
    const auto id = get_method_id("bar(bytes3[2])");
    const auto expected_id = Buffer{0xfc, 0xe3, 0x53, 0xf6};
    BOOST_CHECK_EQUAL_COLLECTIONS(id.begin(), id.end(), expected_id.begin(), expected_id.end());
}

BOOST_AUTO_TEST_CASE(baz)
{
    const auto id = get_method_id("baz(uint32,bool)");
    const auto expected_id = Buffer{0xcd, 0xcd, 0x77, 0xc0};
    BOOST_CHECK_EQUAL_COLLECTIONS(id.begin(), id.end(), expected_id.begin(), expected_id.end());
}

BOOST_AUTO_TEST_CASE(sam)
{
    const auto id = get_method_id("sam(bytes,bool,uint256[])");
    const auto expected_id = Buffer{0xa5, 0x64, 0x3b, 0xf2};
    BOOST_CHECK_EQUAL_COLLECTIONS(id.begin(), id.end(), expected_id.begin(), expected_id.end());
}


BOOST_AUTO_TEST_SUITE_END() // get_short_method_id


BOOST_AUTO_TEST_SUITE_END() // ethereum_abi
