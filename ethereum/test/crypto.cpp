/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../../common/buffer.hpp"
#include "../../common/fixed_size_buffer.hpp"

#include "../crypto.hpp"

using TigerMail::Buffer;
using TigerMail::FixedSizeBuffer;


BOOST_AUTO_TEST_SUITE(keccak)

using TigerMail::Ethereum::keccak;

// source: https://github.com/ethereum/solidity/blob/develop/test/libsolutil/Keccak256.cpp


BOOST_AUTO_TEST_CASE(known_answer0)
{
    const auto data = Buffer{};

    const auto output = keccak(data);

    const auto expected_output = FixedSizeBuffer<32>{
        0xc5,0xd2,0x46,0x01,0x86,0xf7,0x23,0x3c,0x92,0x7e,0x7d,0xb2,0xdc,0xc7,0x03,0xc0,
        0xe5,0x00,0xb6,0x53,0xca,0x82,0x27,0x3b,0x7b,0xfa,0xd8,0x04,0x5d,0x85,0xa4,0x70};

    BOOST_CHECK_EQUAL_COLLECTIONS(
        output.begin(), output.end(),
        expected_output.begin(), expected_output.end());
}

BOOST_AUTO_TEST_CASE(known_answer1)
{
    const auto data = Buffer{'l','o','n','g','e','r',' ','t','e','s','t',' ','s','t','r','i','n','g'};

    const auto output = keccak(data);

    const auto expected_output = FixedSizeBuffer<32>{
        0x47,0xbe,0xd1,0x7b,0xfb,0xbc,0x08,0xd6,0xb5,0xa0,0xf6,0x03,0xef,0xf1,0xb3,0xe9,
        0x32,0xc3,0x7c,0x10,0xb8,0x65,0x84,0x7a,0x7b,0xc7,0x3d,0x55,0xb2,0x60,0xf3,0x2a};

    BOOST_CHECK_EQUAL_COLLECTIONS(
        output.begin(), output.end(),
        expected_output.begin(), expected_output.end());
}


BOOST_AUTO_TEST_SUITE_END() // keccak
