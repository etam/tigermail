/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_ETHEREUM_TEST_OUT_TIGERMAIL_CONTRACT_INTERFACE_HPP
#define TIGERMAIL_ETHEREUM_TEST_OUT_TIGERMAIL_CONTRACT_INTERFACE_HPP

#include <ostream>

#include "../../../common/test/out/buffer.hpp"

#include "../../tigermail_contract_interface.hpp"

#include "common.hpp"


namespace TigerMail::Ethereum {


static
std::ostream& operator<<(std::ostream& o, const MessageIdLog& log)
{
    return o << "MessageIdLog{"
             << "sender_address = " << log.sender_address << ','
             << "data = " << log.data
             << "}";
}


} // namespace TigerMail::Ethereum

#endif // TIGERMAIL_ETHEREUM_TEST_OUT_TIGERMAIL_CONTRACT_INTERFACE_HPP
