/*
Copyright 2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_ETHEREUM_TEST_OUT_RPC_CLIENT_HPP
#define TIGERMAIL_ETHEREUM_TEST_OUT_RPC_CLIENT_HPP

#include <ostream>

#include "../../rpc_client.hpp"


namespace TigerMail::Ethereum {


static
std::ostream& operator<<(std::ostream& o, const RPCClient::Transaction& t)
{
    return
        o << "Transaction{"
          << ".blockHash = " << t.blockHash << ", "
          << ".blockNumber = " << t.blockNumber << ", "
          << ".from = " << t.from << ", "
          << ".gas = " << t.gas << ", "
          << ".gasPrice = " << t.gasPrice << ", "
          << ".hash = " << t.hash << ", "
          << ".input = " << t.input << ", "
          << ".nonce = " << t.nonce << ", "
          << ".to = " << t.to << ", "
          << ".transactionIndex = " << t.transactionIndex << ", "
          << ".value = " << t.value << ", "
          << ".v = " << t.v << ", "
          << ".r = " << t.r << ", "
          << ".s = " << t.s << '}';
}

static
std::ostream& operator<<(std::ostream& o, const RPCClient::FilterLog& l)
{
    return
        o << "FilterLog{"
          << ".removed = " << l.removed << ", "
          << ".logIndex = " << l.logIndex << ", "
          << ".transactionIndex = " << l.transactionIndex << ", "
          << ".transactionHash = " << l.transactionHash << ", "
          << ".blockHash = " << l.blockHash << ", "
          << ".blockNumber = " << l.blockNumber << ", "
          << ".address = " << l.address << ", "
          << ".data = " << l.data << ", "
          << ".topics = " << l.topics << '}';
}


} // namespace TigerMail::Ethereum

#endif // TIGERMAIL_ETHEREUM_TEST_OUT_RPC_CLIENT_HPP
