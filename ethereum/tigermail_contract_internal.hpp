/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_ETHEREUM_TIGERMAIL_CONTRACT_INTERNAL_HPP
#define TIGERMAIL_ETHEREUM_TIGERMAIL_CONTRACT_INTERNAL_HPP
// those headers are only for unit tests

#include <optional>

#include <libdevcore/Common.h>
#include <libdevcrypto/Common.h>

#include "../common/buffer.hpp"
#include "../common/buffer_view.hpp"

#include "common.hpp"

namespace TigerMail::Ethereum {


Buffer encode_postMessageId_data(BufferView id);
std::optional<Buffer> decode_MessageId_data(BufferView data);

Buffer make_MessageId_transaction(
    const dev::u256& gas_price,
    const Address& contract_address,
    BufferView id,
    const dev::u256& nonce,
    const dev::Secret& secret);
Buffer make_invalidate_transaction(
    const dev::u256& gas_price,
    const Address& contract_address,
    const dev::u256& nonce,
    const dev::Secret& secret);


} // namespace TigerMail::Ethereum

#endif // TIGERMAIL_ETHEREUM_TIGERMAIL_CONTRACT_INTERNAL_HPP
