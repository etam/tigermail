/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_ETHEREUM_TIGERMAIL_CONTRACT_HPP
#define TIGERMAIL_ETHEREUM_TIGERMAIL_CONTRACT_HPP

#include <any>
#include <optional>
#include <vector>

#include <gsl/pointers>

#include <libdevcore/Common.h>
#include <libdevcrypto/Common.h>

#include "../common/buffer.hpp"
#include "../common/buffer_view.hpp"
#include "../common/fixed_size_buffer.hpp"

#include "common.hpp"
#include "tigermail_contract_interface.hpp"


namespace TigerMail::Ethereum {

class RPCClient;


class TigerMailContract
    : public TigerMailContractInterface
{
private:
    gsl::not_null<RPCClient*> m_rpc_client;
    Address m_contract_address;
    dev::Secret m_secret;
    std::optional<dev::u256> m_nonce;

public:
    TigerMailContract(
        gsl::not_null<RPCClient*> rpc_client,
        Address&& contract_address,
        Address&& user_address,
        dev::Secret&& secret
    );
    ~TigerMailContract() noexcept override = default;

    void setup_nonce(std::any yield);

    FixedSizeBuffer<32> postMessageId(BufferView id, std::any yield) override;
    std::vector<MessageIdLog> get_message_id_logs(unsigned from_block, unsigned to_block, std::any yield) override;

    FixedSizeBuffer<32> invalidate(std::any yield) override;
    bool is_invalidated(const Address& address, BlockNumberOrTag to_block, std::any yield) override;

    unsigned get_block_number(std::any yield) override;
};


} // namespace TigerMail::Ethereum

#endif // TIGERMAIL_ETHEREUM_TIGERMAIL_CONTRACT_HPP
