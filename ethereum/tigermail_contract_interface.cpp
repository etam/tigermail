/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tigermail_contract_interface.hpp"

#include <tuple>


namespace TigerMail::Ethereum {


bool operator==(const MessageIdLog& log1, const MessageIdLog& log2)
{
    return std::tie(log1.sender_address, log1.data)
        == std::tie(log2.sender_address, log2.data);
}


} // namespace TigerMail::Ethereum
