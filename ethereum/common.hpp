/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_ETHEREUM_COMMON_HPP
#define TIGERMAIL_ETHEREUM_COMMON_HPP

#include <variant>

#include "../common/fixed_size_buffer.hpp"

namespace dev {
template <unsigned N>
class FixedHash;
using h160 = FixedHash<20>;
using Address = h160;
} // namespace dev


namespace TigerMail::Ethereum {


class Address
    : public FixedSizeBuffer<20>
{
private:
    using BaseClass = FixedSizeBuffer<20>;

public:
    using BaseClass::BaseClass;

    // Maybe I should drop this class and use dev::Address directry?
    operator dev::Address() const;
};


class TransactionHash
    : public FixedSizeBuffer<32>
{
private:
    using BaseClass = FixedSizeBuffer<32>;

public:
    using BaseClass::BaseClass;
};


class BlockHash
    : public FixedSizeBuffer<32>
{
private:
    using BaseClass = FixedSizeBuffer<32>;

public:
    using BaseClass::BaseClass;
};


using Topic = FixedSizeBuffer<32>;


enum class BlockTag
{
    earliest,
    latest,
    pending,
};


using BlockNumberOrTag = std::variant<BlockTag, unsigned>;


} // namespace TigerMail::Ethereum

#endif // TIGERMAIL_ETHEREUM_COMMON_HPP
