/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_ETHEREUM_RPC_CLIENT_HPP
#define TIGERMAIL_ETHEREUM_RPC_CLIENT_HPP

#include <any>
#include <cstdint>
#include <optional>
#include <string_view>
#include <vector>

#include <boost/asio/io_context.hpp>
#include <boost/asio/generic/stream_protocol.hpp>

#include <boost/beast/core/basic_stream.hpp>
#include <boost/beast/core/flat_buffer.hpp>

#include <gsl/pointers>

#include <json/value.h>

#include <libdevcore/Common.h>

#include "../common/buffer.hpp"
#include "../common/buffer_view.hpp"
#include "../common/utils.hpp"

#include "common.hpp"


namespace TigerMail::Ethereum {


class TransportProtocolClientInterface
{
public:
    virtual
    ~TransportProtocolClientInterface() noexcept = default;

    virtual void connect(std::any yield) = 0;
    virtual std::string call(std::string_view request, std::any yield) = 0;
};


class RawTransportProtocolClient
    : public TransportProtocolClientInterface
{
private:
    boost::asio::generic::stream_protocol::endpoint m_endpoint;
    boost::asio::generic::stream_protocol::socket m_socket;
    bool m_connected;
    std::string m_buffer;

public:
    RawTransportProtocolClient(
        boost::asio::io_context& io,
        boost::asio::generic::stream_protocol::endpoint&& endpoint);
    ~RawTransportProtocolClient() noexcept override;

    void connect(std::any yield) override;
    std::string call(std::string_view request, std::any yield) override;
};


class HTTPTransportProtocolClient
    : public TransportProtocolClientInterface
{
private:
    boost::asio::generic::stream_protocol::endpoint m_endpoint;
    boost::beast::basic_stream<boost::asio::generic::stream_protocol> m_stream;
    bool m_connected;
    boost::beast::flat_buffer m_buffer;

public:
    HTTPTransportProtocolClient(
        boost::asio::io_context& io,
        boost::asio::generic::stream_protocol::endpoint&& endpoint);
    ~HTTPTransportProtocolClient() noexcept override;

    void connect(std::any yield) override;
    std::string call(std::string_view request, std::any yield) override;
};


// class FilterId
// {
// private:
//     int m_id;
// public:
//     explicit
//     FilterId(int id)
//         : m_id{id}
//     {}

//     int get() const
//     {
//         return m_id;
//     }
// };


class RPCClient
{
private:
    gsl::not_null<TransportProtocolClientInterface*> m_transport_protocol_client;
    int m_next_request_id;

    Json::Value call(std::string_view method, const Json::Value& params, std::any yield);

public:
    explicit
    RPCClient(gsl::not_null<TransportProtocolClientInterface*> transport_protocol_client);

    RPCClient(const RPCClient&) = delete;
    RPCClient(RPCClient&&) = delete;
    RPCClient& operator=(const RPCClient&) = delete;
    RPCClient& operator=(RPCClient&&) = delete;

    unsigned eth_blockNumber(std::any yield);
    dev::u256 eth_gasPrice(std::any yield);

    struct Transaction
    {
        std::optional<BlockHash> blockHash;
        std::optional<unsigned> blockNumber;
        Address from;
        dev::u256 gas;
        dev::u256 gasPrice;
        FixedSizeBuffer<32> hash;
        Buffer input;
        dev::u256 nonce;
        std::optional<Address> to;
        std::optional<unsigned> transactionIndex;
        dev::u256 value;
        std::uint8_t v;
        dev::u256 r;
        dev::u256 s;
    };
    std::optional<Transaction> eth_getTransactionByHash(
        const TransactionHash& hash, std::any yield);

    dev::u256 eth_getTransactionCount(const Address& address, std::any yield);

    // TODO: topics could be a more complicated structure
    // FilterId eth_newFilter(std::optional<BlockNumberOrTag> fromBlock,
    //                        std::optional<BlockNumberOrTag> toBlock,
    //                        std::vector<temporary_reference_wrapper<const Address>> address,
    //                        std::vector<temporary_reference_wrapper<const Topic>> topics,
    //                        std::any yield);
    // bool eth_uninstallFilter(FilterId id, std::any yield);

    struct FilterLog
    {
        std::optional<bool> removed; // wiki doesn't say about it being optional
        std::optional<unsigned> logIndex;
        std::optional<unsigned> transactionIndex;
        std::optional<TransactionHash> transactionHash;
        std::optional<BlockHash> blockHash;
        std::optional<unsigned> blockNumber;
        Address address;
        Buffer data;
        std::vector<Topic> topics;
    };
    // std::vector<FilterLog> eth_getFilterChanges(FilterId id, std::any yield);
    std::vector<FilterLog> eth_getLogs(std::optional<BlockNumberOrTag> fromBlock,
                                       std::optional<BlockNumberOrTag> toBlock,
                                       std::vector<temporary_reference_wrapper<const Address>> address,
                                       std::vector<temporary_reference_wrapper<const Topic>> topics,
                                       std::any yield);

    FixedSizeBuffer<32> eth_sendRawTransaction(BufferView data, std::any yield);
};


} // namespace TigerMail::Ethereum

#endif // TIGERMAIL_ETHEREUM_RPC_CLIENT_HPP
