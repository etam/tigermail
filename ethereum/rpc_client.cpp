/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "rpc_client.hpp"
#include "rpc_client_internal.hpp"

#include <chrono>
#include <functional>

#include <boost/asio/read_until.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/write.hpp>

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>

#include <boost/log/trivial.hpp>

#include <gsl/gsl_assert>

#include "../common/hex.hpp"
#include "../common/utils.hpp"
#include "../common/zpad.hpp"

#include "../json_rpc/json.hpp"
#include "../json_rpc/json_rpc.hpp"


namespace TigerMail::Ethereum {

// I REALLY WISH THERE WAS A LIBRARY FOR THAT!!!

namespace asio = boost::asio;
namespace beast = boost::beast;
namespace http = beast::http;


namespace {


void shutdown_and_close_socket(asio::generic::stream_protocol::socket& socket) noexcept
{
    try {
        socket.shutdown(asio::socket_base::shutdown_both);
    }
    catch (std::exception& e) {
        BOOST_LOG_TRIVIAL(error) << "failed shutting down ethereum socket: " << e.what();
    }

    try {
        socket.close();
    }
    catch (std::exception& e) {
        BOOST_LOG_TRIVIAL(error) << "failed closing ethereum socket: " << e.what();
    }
}


} // namespace


RawTransportProtocolClient::RawTransportProtocolClient(
        asio::io_context& io,
        asio::generic::stream_protocol::endpoint&& endpoint)
    : m_endpoint{std::move(endpoint)}
    , m_socket{io}
    , m_connected{false}
    , m_buffer{}
{}

RawTransportProtocolClient::~RawTransportProtocolClient() noexcept
{
    if (m_connected) {
        shutdown_and_close_socket(m_socket);
    }
}

void RawTransportProtocolClient::connect(std::any yield_)
{
    Expects(!m_connected);
    auto yield = std::any_cast<asio::yield_context>(yield_);
    m_connected = true;
    m_socket.async_connect(m_endpoint, yield);
}

std::string RawTransportProtocolClient::call(std::string_view request, std::any yield_)
{
    Expects(m_connected);
    auto yield = std::any_cast<asio::yield_context>(yield_);

    BOOST_LOG_TRIVIAL(debug) << "Ethereum raw call request: " << request;
    asio::async_write(m_socket, asio::buffer(request), yield);
    const auto size = asio::async_read_until(m_socket, asio::dynamic_buffer(m_buffer), '\n', yield);
    const auto response =
        [&] {
            if (size == m_buffer.size()) {
                auto response = std::string{};
                response.swap(m_buffer);
                return response;
            }
            else {
                const auto response = m_buffer.substr(0, size);
                m_buffer.erase(0, size);
                return response;
            }
        }();
    BOOST_LOG_TRIVIAL(debug) << "Ethereum raw call response: " << response;
    return response;
}


HTTPTransportProtocolClient::HTTPTransportProtocolClient(
    asio::io_context& io,
    asio::generic::stream_protocol::endpoint&& endpoint)
    : m_endpoint{std::move(endpoint)}
    , m_stream{asio::make_strand(io)}
    , m_connected{false}
    , m_buffer{}
{}

HTTPTransportProtocolClient::~HTTPTransportProtocolClient() noexcept
{
    if (m_connected) {
        shutdown_and_close_socket(m_stream.socket());
    }
}

void HTTPTransportProtocolClient::connect(std::any yield_)
{
    Expects(!m_connected);
    auto yield = std::any_cast<asio::yield_context>(yield_);
    m_connected = true;
    m_stream.async_connect(m_endpoint, yield);
}

std::string HTTPTransportProtocolClient::call(std::string_view request, std::any yield_)
{
    Expects(m_connected);
    auto yield = std::any_cast<asio::yield_context>(yield_);

    BOOST_LOG_TRIVIAL(debug) << "Ethereum http call request: " << request;

    auto req = http::request<http::string_body>{http::verb::post, "/", 11};
    req.set(http::field::content_type, "application/json");
    req.body() = request;
    req.prepare_payload();
    http::async_write(m_stream, req, yield);

    #ifdef FUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION
    m_stream.expires_after(std::chrono::milliseconds{100});
    #else
    m_stream.expires_after(std::chrono::seconds{10});
    #endif

    auto res = http::response<http::string_body>{};
    auto ec = boost::system::error_code{};
    http::async_read(m_stream, m_buffer, res, yield[ec]);
    m_stream.expires_never();

    if (ec == http::error::end_of_stream) {
        BOOST_LOG_TRIVIAL(debug) << "Ethereum remote closed connection. Retrying";
        m_stream.close();
        m_connected = false;
        connect(yield);
        http::async_write(m_stream, req, yield);
        http::async_read(m_stream, m_buffer, res, yield);
    }

    const auto result = res.body();
    BOOST_LOG_TRIVIAL(debug) << "Ethereum http call response: " << result;
    return result;
}


std::string serialize(BlockTag tag)
{
    switch (tag) {
    case BlockTag::earliest: return "earliest";
    case BlockTag::latest: return "latest";
    case BlockTag::pending: return "pending";
    }
    std::abort();
}

std::string serialize(BlockNumberOrTag nr)
{
    return std::visit(overloaded{
            [](BlockTag tag) {
                return serialize(tag);
            },
            [](int i) {
                return int2zxhex(i);
            }
        }, nr);
}

Json::Value RPCClient::call(std::string_view method, const Json::Value& params, std::any yield)
{
    const auto request_id = m_next_request_id++;
    const auto request = JsonRpc::format_request(method, params, request_id) + "\n";

    const auto string_response = m_transport_protocol_client->call(request, yield);

    const auto json_response = JsonRpc::parse_json(string_response);
    JsonRpc::check_response(json_response);
    return json_response["result"];
}

RPCClient::RPCClient(gsl::not_null<TransportProtocolClientInterface*> transport_protocol_client)
    : m_transport_protocol_client{transport_protocol_client}
    , m_next_request_id{0}
{}

unsigned RPCClient::eth_blockNumber(std::any yield)
{
    return zxhex2int<unsigned>(
        JsonRpc::asString(
            [] { return std::string{"eth_blockNumber response"}; },
            call("eth_blockNumber", Json::nullValue, yield)));
}

dev::u256 RPCClient::eth_gasPrice(std::any yield)
{
    return zxhex2int<dev::u256>(
        JsonRpc::asString(
            [] { return std::string{"eth_gasPrice response"}; },
            call("eth_gasPrice", Json::nullValue, yield)));
}

static
RPCClient::Transaction dejsonize_transaction_(std::function<std::string()> name, const Json::Value& json_transaction)
{
    if (!json_transaction.isObject()) {
        throw JsonRpc::JsonError{name() + " is not an object"};
    }

    return {
        .blockHash = JsonRpc::asOptional(JsonRpc::asDataFromZxhex<BlockHash>, [&]{ return name() + ".blockHash"; }, json_transaction["blockHash"]),
        .blockNumber = JsonRpc::asOptional(JsonRpc::asIntFromZxhex<unsigned>, [&]{ return name() + ".blockNumber"; }, json_transaction["blockNumber"]),
        .from = JsonRpc::asDataFromZxhex<Address>([&]{ return name() + ".from"; }, json_transaction["from"]),
        .gas = JsonRpc::asIntFromZxhex<dev::u256>([&] { return name() + ".gas"; }, json_transaction["gas"]),
        .gasPrice = JsonRpc::asIntFromZxhex<dev::u256>([&] { return name() + ".gasPrice"; }, json_transaction["gasPrice"]),
        .hash = JsonRpc::asDataFromZxhex<FixedSizeBuffer<32>>([&] { return name() + ".hash"; }, json_transaction["hash"]),
        .input = JsonRpc::asDataFromZxhex<Buffer>([&] { return name() + ".input"; }, json_transaction["input"]),
        .nonce = JsonRpc::asIntFromZxhex<dev::u256>([&] { return name() + ".nonce"; }, json_transaction["nonce"]),
        .to = JsonRpc::asOptional(JsonRpc::asDataFromZxhex<Address>, [&] { return name() + ".to"; }, json_transaction["to"]),
        .transactionIndex = JsonRpc::asOptional(JsonRpc::asIntFromZxhex<unsigned>, [&] { return name() + ".transactionIndex"; }, json_transaction["transactionIndex"]),
        .value = JsonRpc::asIntFromZxhex<dev::u256>([&] { return name() + ".value"; }, json_transaction["value"]),
        .v = JsonRpc::asIntFromZxhex<std::uint8_t>([&] { return name() + ".v"; }, json_transaction["v"]),
        .r = JsonRpc::asIntFromZxhex<dev::u256>([&] { return name() + ".r"; }, json_transaction["r"]),
        .s = JsonRpc::asIntFromZxhex<dev::u256>([&] { return name() + ".s"; }, json_transaction["s"]),
    };
}

std::optional<RPCClient::Transaction> dejsonize_transaction(const Json::Value& json_transaction)
{
    return JsonRpc::asOptional(dejsonize_transaction_, [] { return "transaction"; }, json_transaction);
}

std::optional<RPCClient::Transaction> RPCClient::eth_getTransactionByHash(
    const TransactionHash& hash, std::any yield)
{
    // TODO: check if returned transaction hash mathes the given hash
    return dejsonize_transaction(
        call("eth_getTransactionByHash",
             JsonRpc::make_array({bin2zxhex(hash)}),
             yield));
}

dev::u256 RPCClient::eth_getTransactionCount(const Address& address, std::any yield)
{
    return zxhex2int<dev::u256>(
        JsonRpc::asString(
            [] { return std::string{"eth_getTransactionCount response"}; },
            call("eth_getTransactionCount",
                 JsonRpc::make_array({bin2zxhex(address), "pending"}),
                 yield)));
}

Json::Value jsonize_eth_newFilter_params(
    std::optional<BlockNumberOrTag> fromBlock,
    std::optional<BlockNumberOrTag> toBlock,
    std::vector<temporary_reference_wrapper<const Address>> address,
    std::vector<temporary_reference_wrapper<const Topic>> topics)
{
    auto params_obj = Json::Value{Json::objectValue};
    if (fromBlock != std::nullopt) {
        params_obj["fromBlock"] = serialize(*fromBlock);
    }
    if (toBlock != std::nullopt) {
        params_obj["toBlock"] = serialize(*toBlock);
    }

    switch (address.size()) {
    case 0: break;
    case 1:
        params_obj["address"] = bin2zxhex(address[0].get());
        break;
    default:
        params_obj["address"] = JsonRpc::make_array(
            address,
            [](const Address& addr) {
                return bin2zxhex(addr);
            });
        break;
    }
    if (!topics.empty()) {
        params_obj["topics"] = JsonRpc::make_array(
            topics,
            [](const Topic& topic) {
                return bin2zxhex(zpad<32>(topic));
            }
        );
    }

    return JsonRpc::make_array({std::move(params_obj)});
}

// FilterId RPCClient::eth_newFilter(std::optional<BlockNumberOrTag> fromBlock,
//                                   std::optional<BlockNumberOrTag> toBlock,
//                                   std::vector<temporary_reference_wrapper<const Address>> address,
//                                   std::vector<temporary_reference_wrapper<const Topic>> topics,
//                                   std::any yield)
// {
//     return FilterId{
//         zxhex2int(
//             JsonRpc::asString(
//                 [] { return std::string{"eth_newFilter response"}; },
//                 call("eth_newFilter",
//                      jsonize_eth_newFilter_params(fromBlock, toBlock, address, topics),
//                      yield)))};
// }

// bool RPCClient::eth_uninstallFilter(FilterId id, std::any yield)
// {
//     return JsonRpc::asBool(
//         [] { return std::string{"eth_uninstallFilter response"}; },
//         call("eth_uninstallFilter",
//              JsonRpc::make_array({int2zxhex(id.get())}),
//              yield));
// }

static
RPCClient::FilterLog dejsonize_filterlog(std::function<std::string()> name, const Json::Value& json_filterlog)
{
    if (!json_filterlog.isObject()) {
        throw JsonRpc::JsonError{name() + " is not an object"};
    }
    return {
        .removed = JsonRpc::asOptional(JsonRpc::asBool, [&]{ return name() + ".removed"; }, json_filterlog["removed"]),
        .logIndex = JsonRpc::asOptional(JsonRpc::asIntFromZxhex<unsigned>, [&]{ return name() + ".logIndex"; }, json_filterlog["logIndex"]),
        .transactionIndex = JsonRpc::asOptional(JsonRpc::asIntFromZxhex<unsigned>, [&]{ return name() + ".transactionIndex"; }, json_filterlog["transactionIndex"]),
        .transactionHash = JsonRpc::asOptional(JsonRpc::asDataFromZxhex<TransactionHash>, [&]{ return name() + ".transactionHash"; }, json_filterlog["transactionHash"]),
        .blockHash = JsonRpc::asOptional(JsonRpc::asDataFromZxhex<BlockHash>, [&]{ return name() + ".blockHash"; }, json_filterlog["blockHash"]),
        .blockNumber = JsonRpc::asOptional(JsonRpc::asIntFromZxhex<unsigned>, [&]{ return name() + ".blockNumber"; }, json_filterlog["blockNumber"]),
        .address = JsonRpc::asDataFromZxhex<Address>([&]{ return name() + ".address"; }, json_filterlog["address"]),
        .data = JsonRpc::asDataFromZxhex([&]{ return name() + ".data"; }, json_filterlog["data"]),
        .topics = JsonRpc::asVector(JsonRpc::asDataFromZxhex<Topic>, [&]{ return name() + ".topic"; }, json_filterlog["topics"]),
    };
}

std::vector<RPCClient::FilterLog> dejsonize_filterlogs(const Json::Value& json_filterlogs)
{
    return JsonRpc::asVector(
        dejsonize_filterlog,
        [] { return std::string{"filterlog"}; },
        json_filterlogs);
}

// std::vector<RPCClient::FilterLog> RPCClient::eth_getFilterChanges(FilterId id, std::any yield)
// {
//     return dejsonize_filterlogs(
//         call("eth_getFilterChanges",
//              JsonRpc::make_array({int2zxhex(id.get())}),
//              yield));
// }

std::vector<RPCClient::FilterLog> RPCClient::eth_getLogs(
    std::optional<BlockNumberOrTag> fromBlock,
    std::optional<BlockNumberOrTag> toBlock,
    std::vector<temporary_reference_wrapper<const Address>> address,
    std::vector<temporary_reference_wrapper<const Topic>> topics,
    std::any yield)
{
    // TODO: check if returned FilterLogs match given addresses and topics
    return dejsonize_filterlogs(
        call("eth_getLogs",
             jsonize_eth_newFilter_params(fromBlock, toBlock, address, topics),
             yield));
}


FixedSizeBuffer<32> RPCClient::eth_sendRawTransaction(BufferView data, std::any yield)
{
    return JsonRpc::asDataFromZxhex<FixedSizeBuffer<32>>(
        [] { return std::string{"eth_sendRawTransaction response"}; },
        call("eth_sendRawTransaction",
             JsonRpc::make_array({bin2zxhex(data)}),
             yield));
}


} // namespace TigerMail::Ethereum
