/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <exception>
#include <filesystem>
#include <iostream>
#include <optional>
#include <string>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/local/stream_protocol.hpp>
#include <boost/asio/spawn.hpp>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>

#include <libdevcrypto/SecretStore.h>

#include "../common/buffer.hpp"
#include "../common/fs.hpp"
#include "../common/hex.hpp"
#include "../common/utils.hpp"

#include "common.hpp"
#include "rpc_client.hpp"
#include "tigermail_contract.hpp"


namespace {

namespace po = boost::program_options;
namespace Ethereum = TigerMail::Ethereum;
using TigerMail::read_file_contents;
using TigerMail::zxhex2bin;


struct Options
{
    Ethereum::Address contract_address;
    Ethereum::Address user_ethereum_address;
    std::string ethereum_password;
    std::filesystem::path ethereum_data_dir;
};


void print_usage(
    std::ostream& o,
    const std::string& program_name,
    const po::options_description& desc,
    po::positional_options_description& p)
{
    o << "Usage: " << program_name << ' ';

    const auto N = p.max_total_count();
    for (size_t i = 0; i < N; ++i) {
        o << p.name_for_position(i) << ' ';
    }

    if (desc.options().size() > 0) {
        o << "[options]";
    }

    o << '\n' << desc;
}


Options parse_options(int argc, char* argv[])
{
    auto options = Options{};

    const auto home = std::filesystem::path{std::getenv("HOME")};

    auto opts_desc = po::options_description{"Options"};
    opts_desc.add_options()
        ("help,h", "print this message")
        ("ethereum_data_dir", po::value(&options.ethereum_data_dir)->default_value(home / ".ethereum"))
    ;

    auto hidden_opts_desc = po::options_description{};
    hidden_opts_desc.add_options()
        ("contract_address", po::value<std::string>()->required())
        ("user_address", po::value<std::string>()->required())
        ("password_file", po::value<std::string>()->required())
    ;

    auto pos_opts_desc = po::positional_options_description{};
    pos_opts_desc
        .add("contract_address", 1)
        .add("user_address", 1)
        .add("password_file", 1)
    ;

    auto all_options = po::options_description{};
    all_options.add(opts_desc);
    all_options.add(hidden_opts_desc);

    auto vm = po::variables_map{};
    po::store(po::command_line_parser{argc, argv}.options(all_options).positional(pos_opts_desc).run(), vm);

    if (vm.count("help")) {
        print_usage(
            std::cout,
            std::filesystem::path(argv[0]).stem().string(),
            opts_desc,
            pos_opts_desc);
        std::exit(0);
    }

    po::notify(vm);

    options.contract_address = zxhex2bin<Ethereum::Address>(vm["contract_address"].as<std::string>());
    options.user_ethereum_address = zxhex2bin<Ethereum::Address>(vm["user_address"].as<std::string>());
    options.ethereum_password = read_file_contents(vm["password_file"].as<std::string>());

    return options;
}


} // namespace


int main(int argc, char* argv[])
try {
    using namespace TigerMail;

    const auto options = parse_options(argc, argv);

    const auto secret_store = dev::SecretStore{(options.ethereum_data_dir / "keystore").string()};
    const auto secret = secret_store.secret(
        dev::Address(options.user_ethereum_address.data(), dev::Address::ConstructFromPointer),
        [&] { return options.ethereum_password; });

    auto io = boost::asio::io_context{};

    auto transport_protocol_client = Ethereum::RawTransportProtocolClient{
        io,
        boost::asio::local::stream_protocol::endpoint{options.ethereum_data_dir / "geth.ipc"}};
    // auto transport_protocol_client = Ethereum::HTTPTransportProtocolClient{
    //     io,
    //     boost::asio::ip::tcp::endpoint(boost::asio::ip::address_v4::loopback(), 7545)};
    auto client = Ethereum::RPCClient{gsl::not_null{&transport_protocol_client}};
    auto tigermail_contract = Ethereum::TigerMailContract{
        gsl::not_null{&client},
        copy(options.contract_address),
        copy(options.user_ethereum_address),
        dev::Secret{secret}
    };

    boost::asio::spawn(
        io,
        [&](boost::asio::yield_context yield) {
            transport_protocol_client.connect(yield);

            const auto block_number = client.eth_blockNumber(yield);
            std::cout << "block number: " << block_number << '\n';

            {
                const auto gas_price = client.eth_gasPrice(yield);
                std::cout << "gas price: " << gas_price << '\n';
            }
            {
                const auto count = client.eth_getTransactionCount(options.user_ethereum_address, yield);
                std::cout << "transaction count: " << count << '\n';
            }

            tigermail_contract.setup_nonce(yield);
            {
                const auto id = TigerMail::Buffer{0xde,0xad,0xbe,0xef};
                const auto tx_id = tigermail_contract.postMessageId(id, yield);
                std::cout << "posted msg id; tx id = " << bin2zxhex(tx_id) << '\n';
            }
            {
                const auto ids_log = tigermail_contract.get_message_id_logs(0, block_number, yield);
                std::cout << "ids:\n";
                for (const auto& id_log : ids_log) {
                    std::cout << "block number: " << id_log.block_number << ", "
                              << "from: " << bin2zxhex(id_log.sender_address) << ", "
                              << "data: " << bin2zxhex(id_log.data) << '\n';
                }
            }
            std::cout << "am I invalidated? " << tigermail_contract.am_I_invalidated(yield) << '\n';
            {
                const auto tx_id = tigermail_contract.invalidate(yield);
                std::cout << "invalidated; tx id = " << bin2zxhex(tx_id) << '\n';
            }
            std::cout << "and now? " << tigermail_contract.am_I_invalidated(yield) << '\n';

            {
                const auto filterlogs = client.eth_getLogs(
                    TigerMail::Ethereum::BlockTag::earliest, TigerMail::Ethereum::BlockTag::latest,
                    {options.contract_address}, {}, yield);
                std::cout << "filtered " << filterlogs.size() << " logs:\n";
                for (const auto& filterlog : filterlogs) {
                    if (filterlog.transactionHash == std::nullopt) {
                        std::cout << "unexpected empty transaction hash\n";
                        continue;
                    }

                    std::cout << TigerMail::bin2zxhex(*filterlog.transactionHash) << '\n';

                    const auto tx = client.eth_getTransactionByHash(*filterlog.transactionHash, yield);
                    if (tx == std::nullopt) {
                        std::cout << "unexpected empty transaction object\n";
                        continue;
                    }
                    std::cout << "    was sent from " << TigerMail::bin2zxhex(tx->from) << '\n';
                }
            }
        });

    io.run();
    return 0;
}
catch (std::exception& e) {
    std::cerr << "Error: " << e.what() << '\n';
    return 1;
}
