/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_ETHEREUM_ABI_HPP
#define TIGERMAIL_ETHEREUM_ABI_HPP

#include <optional>
#include <string_view>

#include "../common/fixed_size_buffer.hpp"
#include "../common/fixed_size_buffer_view.hpp"


namespace TigerMail::Ethereum {


std::size_t size_with_padding(std::size_t size);

FixedSizeBuffer<32> encode_uint(std::uint64_t i);
std::optional<std::uint64_t> decode_uint(FixedSizeBufferView<32> buf);

FixedSizeBuffer<32> get_event_id(std::string_view signature);
FixedSizeBuffer<4> get_method_id(std::string_view signature);


} // namespace TigerMail::Ethereum

#endif // TIGERMAIL_ETHEREUM_ABI_HPP
