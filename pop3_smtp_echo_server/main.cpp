/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <csignal>
#include <exception>
#include <iostream>
#include <sstream>
#include <string>
#include <string_view>

#include <boost/asio/io_context.hpp>
#include <boost/asio/local/stream_protocol.hpp>
#include <boost/asio/signal_set.hpp>

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>

#include <gsl/pointers>

#include "../pop3_smtp_common/server/server.hpp"
#include "../pop3_smtp_common/server/session_factory.hpp"

#include "../pop3/limits.hpp"
#include "../pop3/messages_store.hpp"
#include "../pop3/session_handler.hpp"

#include "../smtp/limits.hpp"
#include "../smtp/session_handler.hpp"

namespace asio = boost::asio;
namespace POP3 = TigerMail::POP3;
namespace SMTP = TigerMail::SMTP;

using TigerMail::POP3_SMTP_common::server::LocalEndpoint;
using TigerMail::POP3_SMTP_common::server::Server;
using TigerMail::POP3_SMTP_common::server::make_session_factory;


namespace {


void setup_logging()
{
    namespace log = boost::log;
    // https://stackoverflow.com/a/17473875
    log::add_common_attributes();
    log::add_console_log(std::cout, log::keywords::format = "[%TimeStamp%]: %Message%");
    log::core::get()->set_filter
        (
            log::trivial::severity >= log::trivial::debug
        );

}


} // namespace


int main()
try {
    setup_logging();

    const auto self_address = std::string{"me@localhost"};

    auto io_context = asio::io_context{};

    auto messages_store = POP3::MessagesStore{};

    auto pop3_endpoint = LocalEndpoint{"pop3.socket"};
    const auto pop3_session_factory = make_session_factory<POP3::SessionHandler>(
        [&](std::string_view user, std::string_view) {
            return user == self_address;
        },
        gsl::not_null{&messages_store},
        POP3::Limits{});
    auto pop3_server = Server{
        io_context,
        asio::local::stream_protocol::acceptor{io_context, pop3_endpoint},
        gsl::not_null{&pop3_session_factory}};

    const auto message_received =
        [&](SMTP::Message&& message) {
            auto msg = std::ostringstream{};
            for (const auto& recipient : message.recipients) {
                msg << "X-TigerMail-To: " << recipient << "\r\n";
            }
            msg << *message.message;
            messages_store.push_message(msg.str());
        };

    auto smtp_endpoint = LocalEndpoint{"smtp.socket"};
    const auto smtp_session_factory = make_session_factory<SMTP::SessionHandler>(
        [&](std::string_view reverse_path) {
            return reverse_path == self_address;
        },
        message_received,
        SMTP::Limits{});
    auto smtp_server = Server{
        io_context,
        asio::local::stream_protocol::acceptor{io_context, smtp_endpoint},
        gsl::not_null{&smtp_session_factory}};

    auto signals = asio::signal_set{io_context, SIGINT, SIGTERM};
    signals.async_wait(
        [&](boost::system::error_code /*ec*/, int /*signal*/) {
            pop3_server.stop();
            smtp_server.stop();
        });

    // #pragma omp parallel
    io_context.run();
}
catch (std::exception& e) {
    std::cerr << "Unhandled exception: " << e.what() << "\n";
}
