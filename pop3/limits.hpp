/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_LIMITS_HPP
#define TIGERMAIL_POP3_LIMITS_HPP

#include <cstddef>


namespace TigerMail::POP3 {


struct Limits
{
    std::size_t max_command_len = 512; // it's not in standard
    std::size_t max_response_line_len = 512;
};


} // namespace TigerMail::POP3

#endif // TIGERMAIL_POP3_LIMITS_HPP
