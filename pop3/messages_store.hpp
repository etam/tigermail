/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_MESSAGES_STORE_HPP
#define TIGERMAIL_POP3_MESSAGES_STORE_HPP

#include <filesystem>
#include <list>
#include <mutex>
#include <optional>
#include <string>
#include <variant>
#include <vector>

#include "../common/monitor.hpp"


namespace TigerMail::POP3 {


class MessagesStore
{
public:
    class Message
    {
    private:
        std::variant<
            std::string, // data in memory
            std::filesystem::path // data on disk
        > m_data;

    public:
        explicit
        Message(std::string&& data);

        explicit
        Message(std::filesystem::path path);

        std::size_t size() const;
        std::string contents() const;
        std::optional<std::filesystem::path> path() const;
    };

private:
    std::optional<std::filesystem::path> m_directory;
    // list has no problem of iterator invalidation
    monitor<std::list<Message>> m_messages;

public:
    std::mutex m_pop3_mutex;

    MessagesStore();

    explicit
    MessagesStore(std::filesystem::path directory);

    void push_message(std::string&& msg);
    std::vector<std::list<Message>::const_iterator> make_snapshot();
    void delete_messages(const std::vector<std::list<Message>::const_iterator>& iterators);
};


} // namespace TigerMail::POP3

#endif // TIGERMAIL_POP3_MESSAGES_STORE_HPP
