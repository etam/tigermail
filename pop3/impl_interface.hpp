/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_IMPL_INTERFACE_HPP
#define TIGERMAIL_POP3_IMPL_INTERFACE_HPP

#include <optional>
#include <string_view>

#include "msg_id.hpp"


namespace TigerMail::POP3 {


/**
 * Implementation must take a pointer or reference to a global mutex
 * and unlock in destructor.
 */
class ImplInterface
{
public:
    virtual ~ImplInterface() noexcept = default;

    virtual bool is_login_valid(std::string_view user, std::string_view digest) const = 0;
    virtual bool is_locked() const = 0;
    virtual bool is_valid_msg_id(MsgId msg_id) const = 0;

    virtual void send_err() = 0;
    virtual void send_ok() = 0;
    virtual void send_ok_with_banner() = 0;
    virtual void try_lock_and_snapshot_inbox() = 0;
    virtual void send_response_to_STAT() = 0;
    virtual void send_response_to_LIST(std::optional<MsgId> msg_id) = 0;
    virtual void send_response_to_RETR(MsgId msg_id) = 0;
    virtual void mark_message_as_deleted(MsgId msg_id) = 0;
    virtual void unmark_all_messages() = 0;
    virtual void delete_marked_messages() = 0;
    virtual void disconnect() = 0;
};


} // namespace TigerMail::POP3

#endif // TIGERMAIL_POP3_IMPL_INTERFACE_HPP
