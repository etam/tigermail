/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_FORMAT_RESPONSE_HPP
#define TIGERMAIL_POP3_FORMAT_RESPONSE_HPP

#include <cstddef>
#include <iosfwd>
#include <string_view>
#include <vector>

#include "msg_id.hpp"


namespace TigerMail::POP3::format {


std::ostream& err(std::ostream&);


std::ostream& ok(std::ostream&);


struct ok_with_banner
{
    std::string_view banner;
};

std::ostream& operator<<(std::ostream&, ok_with_banner);


struct response_to_STAT
{
    std::size_t messages_count;
    std::size_t messages_size;
};

std::ostream& operator<<(std::ostream&, response_to_STAT);


struct multiline
{
    std::vector<std::string> lines;
};

std::ostream& operator<<(std::ostream&, const multiline&);


struct LIST_msg_info
{
    MsgId id;
    std::size_t size;
};


struct response_to_LIST_single
{
    LIST_msg_info message_info;
};

std::ostream& operator<<(std::ostream&, response_to_LIST_single);


struct response_to_LIST_multi
{
    std::vector<LIST_msg_info> messages_info;
};

std::ostream& operator<<(std::ostream&, const response_to_LIST_multi&);


struct response_to_RETR
{
    std::string_view message_contents;
};

std::ostream& operator<<(std::ostream&, response_to_RETR);


} // namespace TigerMail::POP3::format

#endif // TIGERMAIL_POP3_FORMAT_RESPONSE_HPP
