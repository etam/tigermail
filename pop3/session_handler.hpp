/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_SESSION_HANDLER_HPP
#define TIGERMAIL_POP3_SESSION_HANDLER_HPP

#include <functional>
#include <list>
#include <mutex>
#include <set>
#include <sstream>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

#include <gsl/pointers>

#include "limits.hpp"
#include "messages_store.hpp"
#include "msg_id.hpp"
#include "parser.hpp"

#include "../pop3_smtp_common/server/session_handler_interface.hpp"


namespace TigerMail::POP3 {

using POP3_SMTP_common::server::SessionHandlerInterface;

class MessagesStore;


std::string default_banner_generator();


class Impl
    : public ImplInterface
{
private:
    std::function<bool(std::string_view, std::string_view)> m_login_verifier;
    gsl::not_null<MessagesStore*> m_messages_store;
    std::unique_lock<std::mutex> m_messages_store_lock;
    std::vector<std::list<MessagesStore::Message>::const_iterator> m_messages_snapshot;
    std::set<MsgId> m_messages_marked_to_delete;
    std::ostream& m_output;
    bool& m_should_disconnect;
    std::function<std::string()> m_banner_generator;

public:
    Impl(
        std::function<bool(std::string_view, std::string_view)>&& login_verifier,
        gsl::not_null<MessagesStore*> messages_store,
        std::ostream& output,
        bool& should_disconnect,
        std::function<std::string()>&& banner_generator = default_banner_generator);

    bool is_login_valid(std::string_view user, std::string_view digest) const override;
    bool is_locked() const override;
    bool is_valid_msg_id(POP3::MsgId msg_id) const override;

    void send_err() override;
    void send_ok() override;
    void send_ok_with_banner() override;
    void try_lock_and_snapshot_inbox() override;
    void send_response_to_STAT() override;
    void send_response_to_LIST(std::optional<POP3::MsgId> msg_id) override;
    void send_response_to_RETR(POP3::MsgId msg_id) override;
    void mark_message_as_deleted(POP3::MsgId msg_id) override;
    void unmark_all_messages() override;
    void delete_marked_messages() override;
    void disconnect() override;
};


class SessionHandler
    : public SessionHandlerInterface
{
private:
    const Limits m_limits;
    std::ostringstream m_output_buffer;
    bool m_should_disconnect;
    Impl m_pop3_impl;
    Parser m_parser;

public:
    SessionHandler(
        std::function<bool(std::string_view, std::string_view)>&& login_verifier,
        gsl::not_null<MessagesStore*> messages_store,
        const Limits& limits);

    std::string handle_hello() override;
    std::tuple<std::string, bool> handle_input(std::string_view input) override;
    std::string handle_stop() override;
};


} // namespace TigerMail::POP3

#endif // TIGERMAIL_POP3_SESSION_HANDLER_HPP
