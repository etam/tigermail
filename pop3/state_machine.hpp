/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_STATE_MACHINE_HPP
#define TIGERMAIL_POP3_STATE_MACHINE_HPP

#include <boost/sml.hpp>

#include "../common/state_machine.hpp"

#include "events.hpp"
#include "impl_interface.hpp"


namespace TigerMail::POP3 {

namespace sml = boost::sml;


namespace st { // state

DECLARE_STATE(Start);
DECLARE_STATE(Authorization);
DECLARE_STATE(Locking);
DECLARE_STATE(Transaction);
DECLARE_STATE(AnyState);

} // namespace st


namespace a { // action

DECLARE_ACTION(send_err, ImplInterface& pop3);
DECLARE_ACTION(send_ok, ImplInterface& pop3);
DECLARE_ACTION(send_ok_with_banner, ImplInterface& pop3);
DECLARE_ACTION(try_lock_and_snapshot_inbox, ImplInterface& pop3);
DECLARE_ACTION(send_response_to_STAT, ImplInterface& pop3);
DECLARE_ACTION(send_response_to_LIST, ImplInterface& pop3, const ev::LIST& ev);
DECLARE_ACTION(send_response_to_RETR, ImplInterface& pop3, const ev::RETR& ev);
DECLARE_ACTION(mark_message_as_deleted, ImplInterface& pop3, const ev::DELE& ev);
DECLARE_ACTION(unmark_all_messages, ImplInterface& pop3);
DECLARE_ACTION(delete_marked_messages, ImplInterface& pop3);
DECLARE_ACTION(disconnect, ImplInterface& pop3);

} // namespace a


namespace g { // guard

DECLARE_GUARD(is_self, const ImplInterface& pop3, const ev::APOP& ev);
DECLARE_GUARD(is_locked, const ImplInterface& pop3);
DECLARE_GUARD(LIST_has_valid_msg_id, const ImplInterface& pop3, const ev::LIST& ev);
DECLARE_GUARD(RETR_has_valid_msg_id, const ImplInterface& pop3, const ev::RETR& ev);
DECLARE_GUARD(DELE_has_valid_msg_id, const ImplInterface& pop3, const ev::DELE& ev);

} // namespace g


struct TransitionTable
{
    auto operator()() const
    {
        namespace ev_raw = ev;
        namespace ev = ev::for_transition_table;
        using sml::X;
        using sml::operator!;
        using sml::operator&&;
        using sml::operator,;

        return sml::make_transition_table(
            *st::Start + ev::connected / a::send_ok_with_banner = st::Authorization

            ,st::Authorization + ev::APOP [ !g::is_self ] / a::send_err
            ,st::Authorization + ev::APOP [ g::is_self ] / a::try_lock_and_snapshot_inbox = st::Locking
            ,st::Authorization + ev::STAT / a::send_err
            ,st::Authorization + ev::LIST / a::send_err
            ,st::Authorization + ev::RETR / a::send_err
            ,st::Authorization + ev::DELE / a::send_err
            ,st::Authorization + ev::NOOP / a::send_err
            ,st::Authorization + ev::RSET / a::send_err
            ,st::Authorization + ev::QUIT / (a::send_ok, a::disconnect) = X

            ,st::Locking [ g::is_locked ] / a::send_ok = st::Transaction
            ,st::Locking [ !g::is_locked ] / a::send_err = st::Authorization

            ,st::Transaction + ev::APOP / a::send_err
            ,st::Transaction + ev::STAT / a::send_response_to_STAT
            ,st::Transaction + ev::LIST [ !g::LIST_has_valid_msg_id ] / a::send_err
            ,st::Transaction + ev::LIST [ g::LIST_has_valid_msg_id ] / a::send_response_to_LIST
            ,st::Transaction + ev::RETR [ !g::RETR_has_valid_msg_id ] / a::send_err
            ,st::Transaction + ev::RETR [ g::RETR_has_valid_msg_id ] / a::send_response_to_RETR
            ,st::Transaction + ev::DELE [ !g::DELE_has_valid_msg_id ] / a::send_err
            ,st::Transaction + ev::DELE [ g::DELE_has_valid_msg_id ] / (a::mark_message_as_deleted, a::send_ok)
            ,st::Transaction + ev::NOOP / a::send_ok
            ,st::Transaction + ev::RSET / (a::unmark_all_messages, a::send_ok)
            ,st::Transaction + ev::QUIT / (a::delete_marked_messages, a::send_ok, a::disconnect) = X

            ,
            *st::AnyState + ev::unknown_command_or_syntax_error / a::send_err
        );
    }
};


using StateMachine = boost::sml::sm<TransitionTable, boost::sml::logger<Logger>>;


} // namespace TigerMail::POP3

#endif // TIGERMAIL_POP3_STATE_MACHINE_HPP
