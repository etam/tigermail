/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_EVENTS_HPP
#define TIGERMAIL_POP3_EVENTS_HPP

#include <string>
#include <optional>

#include "../common/state_machine.hpp"

#include "msg_id.hpp"


namespace TigerMail::POP3::ev {


DECLARE_EVENT(connected);
DECLARE_EVENT_WITH_DATA(APOP) {
    std::string user;
    std::string digest;
};
DECLARE_EVENT(STAT);
DECLARE_EVENT_WITH_DATA(LIST) {
    std::optional<MsgId> msg_id;
};
DECLARE_EVENT_WITH_DATA(RETR) {
    MsgId msg_id;
};
DECLARE_EVENT_WITH_DATA(DELE) {
    MsgId msg_id;
};
DECLARE_EVENT(NOOP);
DECLARE_EVENT(RSET);
DECLARE_EVENT(QUIT);
DECLARE_EVENT(unknown_command_or_syntax_error);


} // namespace TigerMail::POP3::ev

#endif // TIGERMAIL_POP3_EVENTS_HPP
