/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "format_response.hpp"

#include <algorithm>
#include <ostream>
#include <sstream>
#include <string_view>

#include <gsl/gsl_assert>

#include <range/v3/core.hpp>
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/concat.hpp>
#include <range/v3/view/single.hpp>
#include <range/v3/view/transform.hpp>

#include "../common/string_view.hpp"


namespace TigerMail::POP3::format {


namespace {


constexpr const auto eod_size = std::string_view{"\r\n.\r\n"}.size();


} // namespace


std::ostream& err(std::ostream& o)
{
    return o << "-ERR\r\n";
}

std::ostream& ok(std::ostream& o)
{
    return o << "+OK\r\n";
}

std::ostream& operator<<(std::ostream& o, ok_with_banner data)
{
    Expects(
        data.banner.front() == '<'
        && data.banner.back() == '>'
        && data.banner.find('@') != std::string_view::npos
    );

    return o << "+OK " << data.banner << "\r\n";
}

std::ostream& operator<<(std::ostream& o, response_to_STAT data)
{
    return o << "+OK " << data.messages_count
             << ' ' << (data.messages_size + data.messages_count * eod_size) << "\r\n";
}

std::ostream& operator<<(std::ostream& o, const multiline& data)
{
    Expects(std::all_of(data.lines.begin(), data.lines.end(),
                         [](const std::string& line) {
                             return line.find("\r\n") == std::string::npos;
                         })
            && std::none_of(data.lines.begin(), data.lines.end(),
                            [](const std::string& line) {
                                return line == ".";
                            })
        );

    for (const auto& line : data.lines) {
        o << line << "\r\n";
    }
    return o << ".\r\n";
}

std::ostream& operator<<(std::ostream& o, response_to_LIST_single data)
{
    return o << "+OK " << data.message_info.id.value()
             << ' ' << (data.message_info.size + eod_size) << "\r\n";
}

std::ostream& operator<<(std::ostream& o, const response_to_LIST_multi& data)
{
    auto lines =
        ranges::views::concat(
            ranges::views::single(std::string{"+OK"}),
            data.messages_info | ranges::views::transform(
                [] (const auto& msg_info) {
                    auto line = std::ostringstream{};
                    line << msg_info.id.value() << ' ' << (msg_info.size + eod_size);
                    return line.str();
                })
        )
        | ranges::to_vector;

    return o << multiline{std::move(lines)};
}

std::ostream& operator<<(std::ostream& o, response_to_RETR data)
{
    Expects(data.message_contents.find("\r\n.\r\n") == std::string_view::npos);
    Expects(!ends_with(data.message_contents, "\r\n."));

    return o << "+OK\r\n" << data.message_contents << "\r\n.\r\n";
}


} // namespace TigerMail::POP3::format
