/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <gsl/pointers>

#include <range/v3/core.hpp>
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/indirect.hpp>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/remove_if.hpp>
#include <range/v3/view/transform.hpp>

#include "../limits.hpp"
#include "../messages_store.hpp"
#include "../msg_id.hpp"
#include "../parser.hpp"
#include "../session_handler.hpp"

using TigerMail::POP3::Impl;
using TigerMail::POP3::Limits;
using TigerMail::POP3::MessagesStore;
using TigerMail::POP3::MsgId;
using TigerMail::POP3::Parser;


namespace {


std::string test_banner_generator()
{
    return "<foo@bar>";
}


class TestImpl
    : public Impl
{
private:
    std::ostringstream m_output;
    bool m_disconnected;

public:
    TestImpl(gsl::not_null<MessagesStore*> messages_store)
        : Impl{
            [](std::string_view user, std::string_view) {
                return user == "me";
            },
            messages_store,
            m_output,
            m_disconnected,
            test_banner_generator
        }
        , m_output{}
        , m_disconnected{false}
    {}

    void send_err() override
    {
        if (m_disconnected) {
            return;
        }
        Impl::send_err();
    }

    void send_ok() override
    {
        if (m_disconnected) {
            return;
        }
        Impl::send_ok();
    }

    void send_ok_with_banner() override
    {
        if (m_disconnected) {
            return;
        }
        Impl::send_ok_with_banner();
    }

    void send_response_to_STAT() override
    {
        if (m_disconnected) {
            return;
        }
        Impl::send_response_to_STAT();
    }

    void send_response_to_LIST(std::optional<MsgId> msg_id) override
    {
        if (m_disconnected) {
            return;
        }
        Impl::send_response_to_LIST(msg_id);
    }

    void send_response_to_RETR(MsgId msg_id) override
    {
        if (m_disconnected) {
            return;
        }
        Impl::send_response_to_RETR(msg_id);
    }

    std::string output() const
    {
        return m_output.str();
    }

    bool disconnected() const
    {
        return m_disconnected;
    }
};


} // namespace


BOOST_AUTO_TEST_CASE(integration)
{
    // given
    const auto limits = Limits{};
    auto messages_store = MessagesStore{};
    messages_store.push_message("foo\r\nbar"); // len: 13
    const auto msg2 = "abcdefgh\r\nijklmno\r\npqrstuvwxyz"; // len: 35
    messages_store.push_message(msg2);

    auto pop3 = TestImpl{gsl::not_null{&messages_store}};
    auto parser = Parser{pop3, limits};

    const auto input = std::vector<std::string_view>{
        "APOP me c4c9334bac560ecc979e58001b3e22fb\r\n",
        "NOOP\r\n",
        "STAT\r\n",
        "LIST\r\n",
        "RETR 1\r\n",
        "DELE 1\r\n",
        "RETR 2\r\n",
        "DELE 2\r\n",
        "DELE 1\r\n",
        "RSET\r\n",
        "DELE 1\r\n",
        "STAT\r\n",
        "LIST\r\n",
        "LIST 1\r\n",
        "LIST 2\r\n",
        "QUIT\r\n",
    };

    // when
    for (auto input_chunk : input) {
        parser.process_input(input_chunk);
    }

    // then
    constexpr const auto expected_output = std::string_view{
        "+OK <foo@bar>\r\n" // connected
        "+OK\r\n" // APOP
        "+OK\r\n" // NOOP
        "+OK 2 48\r\n" // STAT
        "+OK\r\n" // LIST
        "1 13\r\n"
        "2 35\r\n"
        ".\r\n"
        "+OK\r\n" // RETR 1
        "foo\r\n"
        "bar\r\n"
        ".\r\n"
        "+OK\r\n" // DELE 1
        "+OK\r\n" // RETR 2
        "abcdefgh\r\n"
        "ijklmno\r\n"
        "pqrstuvwxyz\r\n"
        ".\r\n"
        "+OK\r\n" // DELE 2
        "-ERR\r\n" // DELE 1
        "+OK\r\n" // RSET
        "+OK\r\n" // DELE 1
        "+OK 2 48\r\n" // STAT
        "+OK\r\n" // LIST
        "2 35\r\n"
        ".\r\n"
        "-ERR\r\n" // LIST 1
        "+OK 2 35\r\n" // LIST 2
        "+OK\r\n" // QUIT
    };

    BOOST_CHECK_EQUAL(pop3.output(), expected_output);
    BOOST_CHECK(pop3.disconnected());

    const auto messages_left =
        [&] {
            const auto snapshot = messages_store.make_snapshot();
            return
                snapshot
                | ranges::views::indirect
                | ranges::views::transform(
                    [](const MessagesStore::Message& msg) {
                        return msg.contents();
                    })
                | ranges::to_vector;
        }();
    const auto expected_messages_left = std::vector{std::string{msg2}};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        messages_left.begin(), messages_left.end(),
        expected_messages_left.begin(), expected_messages_left.end());
}
