/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <any>
#include <typeinfo>
#include <vector>

#include <boost/test/unit_test.hpp>

#include "../../parser.cpp"


namespace ev = TigerMail::POP3::ev;

using TigerMail::POP3::detail::parse_command;


namespace {


class StateMachine
{
private:
    std::vector<std::any> m_events;

public:
    StateMachine()
        : m_events{}
    {}

    template <typename Event>
    void process_event(Event event)
    {
        m_events.push_back(event);
    }

    auto& events() const
    {
        return m_events;
    }
};


struct Fixture
{
    StateMachine sm;
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(test_parse_command, Fixture)

BOOST_AUTO_TEST_CASE(too_short)
{
    parse_command("hi", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::unknown_command_or_syntax_error));
}

// more detailed testing is in parse_APOP.cpp
BOOST_AUTO_TEST_CASE(APOP)
{
    parse_command("APOP me c4c9334bac560ecc979e58001b3e22fb", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::APOP));
}

BOOST_AUTO_TEST_CASE(STAT)
{
    parse_command("STAT", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::STAT));
}

// more detailed testing is in parse_LIST.cpp
BOOST_AUTO_TEST_CASE(LIST)
{
    parse_command("LIST", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::LIST));
}

// more detailed testing is in parse_RETR.cpp
BOOST_AUTO_TEST_CASE(RETR)
{
    parse_command("RETR 1", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::RETR));
}

// more detailed testing is in parse_DELE.cpp
BOOST_AUTO_TEST_CASE(DELE)
{
    parse_command("DELE 1", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::DELE));
}

BOOST_AUTO_TEST_CASE(NOOP)
{
    parse_command("NOOP", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::NOOP));
}

BOOST_AUTO_TEST_CASE(RSET)
{
    parse_command("RSET", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::RSET));
}

BOOST_AUTO_TEST_CASE(QUIT)
{
    parse_command("QUIT", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::QUIT));
}

BOOST_AUTO_TEST_CASE(unknown)
{
    parse_command("aoeuidhtns", &sm);
    BOOST_CHECK_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::unknown_command_or_syntax_error));
}

BOOST_AUTO_TEST_SUITE_END()
