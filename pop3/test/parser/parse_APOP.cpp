/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>

#include <boost/test/unit_test.hpp>

#include "../../parser.hpp"

using TigerMail::POP3::detail::SyntaxError;
using TigerMail::POP3::detail::parse_APOP;


BOOST_AUTO_TEST_SUITE(test_parse_APOP)

const auto correct_digest =
    std::string{"c4c9334bac560ecc979e58001b3e22fb"};


BOOST_AUTO_TEST_CASE(correct)
{
    const auto ev = parse_APOP(" me " + correct_digest);

    BOOST_CHECK_EQUAL(ev.user, "me");
    BOOST_CHECK_EQUAL(ev.digest, correct_digest);
}

BOOST_AUTO_TEST_CASE(one_arg)
{
    BOOST_CHECK_THROW(parse_APOP(" me"), SyntaxError);
}

BOOST_AUTO_TEST_CASE(too_much_space)
{
    BOOST_CHECK_THROW(parse_APOP(" me  " + correct_digest), SyntaxError);
}

BOOST_AUTO_TEST_CASE(space_at_end)
{
    BOOST_CHECK_THROW(parse_APOP(" me " + correct_digest + " "), SyntaxError);
}

BOOST_AUTO_TEST_CASE(too_short_digest)
{
    const auto too_short_digest = correct_digest.substr(5);
    BOOST_CHECK_THROW(parse_APOP(" me " + too_short_digest), SyntaxError);
}

BOOST_AUTO_TEST_CASE(too_long_digest)
{
    const auto too_long_digest = correct_digest + "aoeui";
    BOOST_CHECK_THROW(parse_APOP(" me " + too_long_digest), SyntaxError);
}

BOOST_AUTO_TEST_SUITE_END()
