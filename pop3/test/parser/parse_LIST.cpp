/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../../parser.hpp"

using TigerMail::POP3::detail::SyntaxError;
using TigerMail::POP3::detail::parse_LIST;


BOOST_AUTO_TEST_SUITE(test_parse_LIST)

BOOST_AUTO_TEST_CASE(correct_no_args)
{
    const auto ev = parse_LIST("");

    BOOST_CHECK(!ev.msg_id);
}

BOOST_AUTO_TEST_CASE(correct_arg)
{
    const auto ev = parse_LIST(" 123");

    BOOST_REQUIRE(ev.msg_id);
    BOOST_CHECK_EQUAL(*ev.msg_id, 123);
}

BOOST_AUTO_TEST_CASE(negative)
{
    BOOST_CHECK_THROW(parse_LIST(" -3"), SyntaxError);
}

BOOST_AUTO_TEST_CASE(too_much_space)
{
    BOOST_CHECK_THROW(parse_LIST("  123"), SyntaxError);
}

BOOST_AUTO_TEST_CASE(space_at_end)
{
    BOOST_CHECK_THROW(parse_LIST(" 123 "), SyntaxError);
}

BOOST_AUTO_TEST_CASE(rubbish)
{
    BOOST_CHECK_THROW(parse_LIST("aoeuidhtns"), SyntaxError);
}

BOOST_AUTO_TEST_SUITE_END()
