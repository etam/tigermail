/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <string_view>
#include <tuple>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <gsl/pointers>

#include <range/v3/core.hpp>
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/indirect.hpp>
#include <range/v3/view/transform.hpp>

#include "../../common/string_view.hpp"
#include "../../common/test/out/std_tuple.hpp"

#include "../messages_store.hpp"
#include "../session_handler.hpp"


// Based on integration.cpp
BOOST_AUTO_TEST_CASE(pop3_session_handler)
{
    using TigerMail::POP3::MessagesStore;
    using TigerMail::POP3::SessionHandler;
    using TigerMail::ends_with;
    using TigerMail::starts_with;

    auto messages_store = MessagesStore{};
    auto session_handler = SessionHandler{
        [](std::string_view user, std::string_view) {
            return user == "me";
        },
        gsl::not_null{&messages_store},
        {}};

    const auto init_messages = std::vector<std::string_view>{
        "foo\r\nbar", // 13
        "abcdefgh\r\nijklmno\r\npqrstuvwxyz", // 35
    };
    for (const auto msg : init_messages) {
        messages_store.push_message(std::string{msg});
    }

    const auto input = std::vector<std::string_view>{
        "APOP me c4c9334bac560ecc979e58001b3e22fb\r\n",
        "NOOP\r\n",
        "STAT\r\n",
        "LIST\r\n",
        "RETR 1\r\n",
        "DELE 1\r\n",
        "RETR 2\r\n",
        "DELE 2\r\n",
        "DELE 1\r\n",
        "RSET\r\n",
        "DELE 1\r\n",
        "STAT\r\n",
        "LIST\r\n",
        "LIST 1\r\n",
        "LIST 2\r\n",
        "QUIT\r\n",
    };

    const auto expected_output = std::vector<std::tuple<std::string_view, bool>>{
        {"+OK\r\n", false}, // APOP
        {"+OK\r\n", false}, // NOOP
        {"+OK 2 48\r\n", false}, // STAT
        {"+OK\r\n" // LIST
         "1 13\r\n"
         "2 35\r\n"
         ".\r\n", false},
        {"+OK\r\n" // RETR 1
         "foo\r\n"
         "bar\r\n"
         ".\r\n", false},
        {"+OK\r\n", false}, // DELE 1
        {"+OK\r\n" // RETR 2
         "abcdefgh\r\n"
         "ijklmno\r\n"
         "pqrstuvwxyz\r\n"
         ".\r\n", false},
        {"+OK\r\n", false}, // DELE 2
        {"-ERR\r\n", false}, // DELE 1
        {"+OK\r\n", false}, // RSET
        {"+OK\r\n", false}, // DELE 1
        {"+OK 2 48\r\n", false}, // STAT
        {"+OK\r\n" // LIST
         "2 35\r\n"
         ".\r\n", false},
        {"-ERR\r\n", false}, // LIST 1
        {"+OK 2 35\r\n", false}, // LIST 2
        {"+OK\r\n", true}, // QUIT
    };

    const auto expected_messages_left = std::vector<std::string_view>{
        init_messages[1],
    };

    const auto hello_msg = session_handler.handle_hello();
    const auto output =
        input
        | ranges::views::transform(
            [&](const auto& input_line) {
                return session_handler.handle_input(input_line);
            })
        | ranges::to_vector;
    const auto stop_msg = session_handler.handle_stop();

    const auto messages_left =
        [&] {
            const auto snapshot = messages_store.make_snapshot();
            return
                snapshot
                | ranges::views::indirect
                | ranges::views::transform(
                    [](const MessagesStore::Message& msg) {
                        return msg.contents();
                    })
                | ranges::to_vector;
        }();

    BOOST_CHECK(starts_with(hello_msg, "+OK <"));
    BOOST_CHECK(ends_with(hello_msg, "@localhost>\r\n"));
    BOOST_CHECK_EQUAL_COLLECTIONS(
        output.begin(), output.end(),
        expected_output.begin(), expected_output.end());
    BOOST_CHECK_EQUAL(stop_msg, "");

    BOOST_CHECK_EQUAL_COLLECTIONS(
        messages_left.begin(), messages_left.end(),
        expected_messages_left.begin(), expected_messages_left.end());
}
