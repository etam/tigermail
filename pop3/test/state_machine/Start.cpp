/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../../state_machine.hpp"

#include "common.hpp"

namespace ev = TigerMail::POP3::ev;
namespace st = TigerMail::POP3::st;

using TigerMail::POP3::test::StateMachine::BaseFixture;
using TigerMail::POP3::test::StateMachine::Impl;


BOOST_FIXTURE_TEST_SUITE(Start, BaseFixture)

BOOST_AUTO_TEST_CASE(connected)
{
    sm.process_event(ev::connected{});

    BOOST_CHECK(sm.is(st::Authorization));
    BOOST_CHECK(!pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::OkWithBanner);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_SUITE_END()
