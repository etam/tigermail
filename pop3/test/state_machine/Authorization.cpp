/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/sml.hpp>

#include <boost/test/unit_test.hpp>

#include "../../state_machine.hpp"

#include "common.hpp"

namespace sml = boost::sml;
namespace ev = TigerMail::POP3::ev;
namespace st = TigerMail::POP3::st;

using TigerMail::POP3::test::StateMachine::BaseFixture;
using TigerMail::POP3::test::StateMachine::self_address;
using TigerMail::POP3::test::StateMachine::not_self_address;
using TigerMail::POP3::test::StateMachine::Impl;


namespace {


struct AuthorizationFixture
    : BaseFixture
{
    AuthorizationFixture()
        : BaseFixture{}
    {
        sm.set_current_states(st::Authorization, st::AnyState);
    }
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(Authorization, AuthorizationFixture)

// BOOST_AUTO_TEST_CASE(connected)

BOOST_AUTO_TEST_CASE(APOP_self_address_lock_success)
{
    sm.process_event(ev::APOP{self_address, "aoeuidhtns"});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Ok);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(APOP_self_address_lock_failure)
{
    pop3.is_lock_possible(false);

    sm.process_event(ev::APOP{self_address, "aoeuidhtns"});

    BOOST_CHECK(sm.is(st::Authorization));
    BOOST_CHECK(!pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(APOP_not_self_address)
{
    sm.process_event(ev::APOP{not_self_address, "aoeuidhtns"});

    BOOST_CHECK(sm.is(st::Authorization));
    BOOST_CHECK(!pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(STAT)
{
    sm.process_event(ev::STAT{});

    BOOST_CHECK(sm.is(st::Authorization));
    BOOST_CHECK(!pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(LIST)
{
    sm.process_event(ev::LIST{});

    BOOST_CHECK(sm.is(st::Authorization));
    BOOST_CHECK(!pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(RETR)
{
    sm.process_event(ev::RETR{});
    BOOST_CHECK(sm.is(st::Authorization));
    BOOST_CHECK(!pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(DELE)
{
    sm.process_event(ev::DELE{});

    BOOST_CHECK(sm.is(st::Authorization));
    BOOST_CHECK(!pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(NOOOP)
{
    sm.process_event(ev::NOOP{});

    BOOST_CHECK(sm.is(st::Authorization));
    BOOST_CHECK(!pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(RSET)
{
    sm.process_event(ev::RSET{});

    BOOST_CHECK(sm.is(st::Authorization));
    BOOST_CHECK(!pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(QUIT)
{
    sm.process_event(ev::QUIT{});

    BOOST_CHECK(sm.is(sml::X));
    BOOST_CHECK(!pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Ok);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(unknown_command_or_syntax_error)
{
    sm.process_event(ev::unknown_command_or_syntax_error{});

    BOOST_CHECK(sm.is(st::Authorization));
    BOOST_CHECK(!pop3.is_locked());
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_SUITE_END()
