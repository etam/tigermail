/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <optional>

#include <boost/sml.hpp>

#include <boost/test/unit_test.hpp>

#include "../../state_machine.hpp"

#include "common.hpp"

namespace sml = boost::sml;
namespace ev = TigerMail::POP3::ev;
namespace st = TigerMail::POP3::st;

using TigerMail::POP3::test::StateMachine::BaseFixture;
using TigerMail::POP3::test::StateMachine::Impl;


namespace {


struct TransactionFixture
    : BaseFixture
{
    TransactionFixture()
        : BaseFixture{}
    {
        sm.set_current_states(st::Transaction, st::AnyState);
    }
};


} // namespace



BOOST_FIXTURE_TEST_SUITE(Transaction, TransactionFixture)

// connected

BOOST_AUTO_TEST_CASE(APOP)
{
    sm.process_event(ev::APOP{});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(STAT)
{
    sm.process_event(ev::STAT{});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::ResponseToSTAT);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(LIST_no_msg_id)
{
    sm.process_event(ev::LIST{std::nullopt});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::ResponseToLISTAll);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(LIST_valid_msg_id)
{
    sm.process_event(ev::LIST{max_valid_msg_id});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::ResponseToLISTSingle);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(LIST_invalid_msg_id)
{
    sm.process_event(ev::LIST{max_valid_msg_id.value()+1});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(RETR_valid_msg_id)
{
    sm.process_event(ev::RETR{max_valid_msg_id});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::ResponseToRETR);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(RETR_invalid_msg_id)
{
    sm.process_event(ev::RETR{max_valid_msg_id.value()+1});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(DELE_valid_msg_id)
{
    sm.process_event(ev::DELE{max_valid_msg_id});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Ok);
    BOOST_REQUIRE(pop3.msg_id_marked_as_deleted());
    BOOST_CHECK_EQUAL(*pop3.msg_id_marked_as_deleted(), max_valid_msg_id);
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(DELE_invalid_msg_id)
{
    sm.process_event(ev::DELE{max_valid_msg_id.value()+1});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(NOOP)
{
    sm.process_event(ev::NOOP{});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Ok);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(RSET)
{
    sm.process_event(ev::RSET{});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Ok);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(pop3.unmark_all_messages_called());
    BOOST_CHECK(!pop3.delete_marked_messages_called());
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(QUIT)
{
    sm.process_event(ev::QUIT{});

    BOOST_CHECK(sm.is(sml::X));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Ok);
    BOOST_CHECK(!pop3.msg_id_marked_as_deleted());
    BOOST_CHECK(!pop3.unmark_all_messages_called());
    BOOST_CHECK(pop3.delete_marked_messages_called());
    BOOST_CHECK(pop3.is_disconnected());
}

BOOST_AUTO_TEST_CASE(unknown_command_or_syntax_error)
{
    sm.process_event(ev::unknown_command_or_syntax_error{});

    BOOST_CHECK(sm.is(st::Transaction));
    BOOST_CHECK(!pop3.is_locked()); // there was no attempt to lock again
    BOOST_CHECK_EQUAL(pop3.last_sent_response(), Impl::SentResponse::Err);
    BOOST_CHECK(!pop3.is_disconnected());
}

BOOST_AUTO_TEST_SUITE_END()
