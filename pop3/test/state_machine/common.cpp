/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "common.hpp"

#include <ostream>

#include "../../../common/utils.hpp"


namespace TigerMail::POP3::test::StateMachine {


Impl::Impl(
    std::function<bool(std::string_view, std::string_view)>&& login_verifier,
    MsgId max_valid_msg_id
)
    : m_login_verifier{std::move(login_verifier)}
    , m_is_lock_possible{true}
    , m_is_locked{false}
    , m_max_valid_msg_id{max_valid_msg_id}
    , m_last_sent_response{SentResponse::None}
    , m_msg_id_marked_as_deleted{std::nullopt}
    , m_unmark_all_messages_called{false}
    , m_delete_marked_messages_called{false}
    , m_is_disconnected{false}
{}

bool Impl::is_login_valid(std::string_view user, std::string_view digest) const
{
    return m_login_verifier(user, digest);
}

bool Impl::is_locked() const
{
    return m_is_locked;
}

bool Impl::is_valid_msg_id(MsgId msg_id) const
{
    return msg_id <= m_max_valid_msg_id;
}

void Impl::send_err()
{
    m_last_sent_response = SentResponse::Err;
}

void Impl::send_ok()
{
    m_last_sent_response = SentResponse::Ok;
}

void Impl::send_ok_with_banner()
{
    m_last_sent_response = SentResponse::OkWithBanner;
}

void Impl::try_lock_and_snapshot_inbox()
{
    if (m_is_lock_possible) {
        m_is_locked = true;
    }
}

void Impl::send_response_to_STAT()
{
    m_last_sent_response = SentResponse::ResponseToSTAT;
}

void Impl::send_response_to_LIST(std::optional<MsgId> msg_id)
{
    if (!msg_id) {
        m_last_sent_response = SentResponse::ResponseToLISTAll;
    }
    else if (*msg_id > m_max_valid_msg_id) {
        m_last_sent_response = SentResponse::Err;
    }
    else {
        m_last_sent_response = SentResponse::ResponseToLISTSingle;
    }
}

void Impl::send_response_to_RETR(MsgId msg_id)
{
    m_last_sent_response =
        (msg_id <= m_max_valid_msg_id)
        ? SentResponse::ResponseToRETR
        : SentResponse::Err;
}

void Impl::mark_message_as_deleted(MsgId msg_id)
{
    m_msg_id_marked_as_deleted = msg_id;
}

void Impl::unmark_all_messages()
{
    m_unmark_all_messages_called = true;
}

void Impl::delete_marked_messages()
{
    m_delete_marked_messages_called = true;
}

void Impl::disconnect()
{
    m_is_disconnected = true;
}

void Impl::is_lock_possible(bool is_lock_possible)
{
    m_is_lock_possible = is_lock_possible;
}

Impl::SentResponse Impl::last_sent_response() const
{
    return m_last_sent_response;
}

const std::optional<MsgId>& Impl::msg_id_marked_as_deleted() const
{
    return m_msg_id_marked_as_deleted;
}

bool Impl::unmark_all_messages_called() const
{
    return m_unmark_all_messages_called;
}

bool Impl::delete_marked_messages_called() const
{
    return m_delete_marked_messages_called;
}

bool Impl::is_disconnected() const
{
    return m_is_disconnected;
}


std::ostream& operator<<(std::ostream& o, Impl::SentResponse response)
{
    switch (response) {
    case Impl::SentResponse::None:                 o << "None"; break;
    case Impl::SentResponse::Err:                  o << "Err"; break;
    case Impl::SentResponse::Ok:                   o << "Ok"; break;
    case Impl::SentResponse::OkWithBanner:         o << "OkWithBanner"; break;
    case Impl::SentResponse::ResponseToSTAT:       o << "ResponseToSTAT"; break;
    case Impl::SentResponse::ResponseToLISTAll:    o << "ResponseToLISTAll"; break;
    case Impl::SentResponse::ResponseToLISTSingle: o << "ResponseToLISTSingle"; break;
    case Impl::SentResponse::ResponseToRETR:       o << "ResponseToRETR"; break;
    }
    return o;
}


const std::string self_address{"this_is_a_self@address"};
const std::string not_self_address{"this_is_not_a_self@address"};


BaseFixture::BaseFixture()
    : max_valid_msg_id{5}
    , pop3{
        [](std::string_view user, std::string_view) {
            return user == self_address;
        },
        max_valid_msg_id
    }
    , sm{static_cast<ImplInterface&>(pop3)}
{}

} // namespace TigerMail::POP3::test::StateMachine
