/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_TEST_STATE_MACHINE_COMMON_HPP
#define TIGERMAIL_POP3_TEST_STATE_MACHINE_COMMON_HPP

#include <functional>
#include <iosfwd>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

#include <boost/sml.hpp>

#include "../../impl_interface.hpp"
#include "../../msg_id.hpp"
#include "../../state_machine.hpp"


namespace TigerMail::POP3::test::StateMachine {


class Impl
    : public ImplInterface
{
public:
    enum class SentResponse
    {
        None,
        Err,
        Ok,
        OkWithBanner,
        ResponseToSTAT,
        ResponseToLISTAll,
        ResponseToLISTSingle,
        ResponseToRETR
    };

private:
    std::function<bool(std::string_view, std::string_view)> m_login_verifier;
    bool m_is_lock_possible;
    bool m_is_locked;
    MsgId m_max_valid_msg_id;
    SentResponse m_last_sent_response;
    std::optional<MsgId> m_msg_id_marked_as_deleted;
    bool m_unmark_all_messages_called;
    bool m_delete_marked_messages_called;
    bool m_is_disconnected;

public:
    Impl(
        std::function<bool(std::string_view, std::string_view)>&& login_verifier,
        MsgId max_valid_msg_id);

    bool is_login_valid(std::string_view user, std::string_view digest) const override;
    bool is_locked() const override;
    bool is_valid_msg_id(MsgId msg_id) const override;

    void send_err() override;
    void send_ok() override;
    void send_ok_with_banner() override;
    void try_lock_and_snapshot_inbox() override;
    void send_response_to_STAT() override;
    void send_response_to_LIST(std::optional<MsgId> msg_id) override;
    void send_response_to_RETR(MsgId msg_id) override;
    void mark_message_as_deleted(MsgId msg_id) override;
    void unmark_all_messages() override;
    void delete_marked_messages() override;
    void disconnect() override;

    void is_lock_possible(bool is_lock_possible);

    SentResponse last_sent_response() const;
    const std::optional<MsgId>& msg_id_marked_as_deleted() const;
    bool unmark_all_messages_called() const;
    bool delete_marked_messages_called() const;
    bool is_disconnected() const;
};


std::ostream& operator<<(std::ostream& o, Impl::SentResponse response);


extern const std::string self_address;
extern const std::string not_self_address;


struct BaseFixture
{
    MsgId max_valid_msg_id;
    Impl pop3;
    sml::sm<TransitionTable, sml::testing> sm;

    BaseFixture();
};


} // namespace TigerMail::POP3::test::StateMachine

#endif // TIGERMAIL_POP3_TEST_STATE_MACHINE_COMMON_HPP
