/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>
#include <boost/test/tools/output_test_stream.hpp>

#include <gsl/gsl_assert>

#include "../../format_response.hpp"

using boost::test_tools::output_test_stream;
using TigerMail::POP3::format::multiline;


BOOST_AUTO_TEST_SUITE(test_format_multiline)

BOOST_AUTO_TEST_CASE(ok)
{
    auto output = output_test_stream{};
    output << multiline{{"foo", "bar"}};
    BOOST_CHECK(output.is_equal("foo\r\nbar\r\n.\r\n"));
}

BOOST_AUTO_TEST_CASE(has_newline)
{
    auto output = output_test_stream{};
    const auto format_obj = multiline{{"foo", "b\r\nar"}};
    BOOST_CHECK_THROW(output << format_obj, gsl::fail_fast);
}

BOOST_AUTO_TEST_CASE(has_dot)
{
    auto output = output_test_stream{};
    const auto format_obj = multiline{{"foo", ".", "bar"}};
    BOOST_CHECK_THROW(output << format_obj, gsl::fail_fast);
}

BOOST_AUTO_TEST_SUITE_END()
