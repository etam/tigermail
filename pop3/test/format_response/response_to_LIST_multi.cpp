/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>
#include <boost/test/tools/output_test_stream.hpp>

#include "../../format_response.hpp"
#include "../../msg_id.hpp"

using boost::test_tools::output_test_stream;
using TigerMail::POP3::MsgId;
using TigerMail::POP3::format::response_to_LIST_multi;


BOOST_AUTO_TEST_CASE(test_format_response_to_LIST_multi)
{
    auto output = output_test_stream{};
    output << response_to_LIST_multi{{
            {1, 10},
            {2, 123},
            {5, 77},
        }};
    BOOST_CHECK(output.is_equal(
                    "+OK\r\n"
                    "1 15\r\n"
                    "2 128\r\n"
                    "5 82\r\n"
                    ".\r\n"));
}
