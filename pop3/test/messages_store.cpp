/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <filesystem>
#include <iterator>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/log/trivial.hpp>
#include <boost/test/unit_test.hpp>

#include <range/v3/core.hpp>
#include <range/v3/action/sort.hpp>
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/indirect.hpp>
#include <range/v3/view/transform.hpp>

#include "../messages_store.hpp"

using TigerMail::POP3::MessagesStore;


BOOST_AUTO_TEST_SUITE(messages_store)


BOOST_AUTO_TEST_CASE(memory)
{
    auto messages_store = MessagesStore{};
    messages_store.push_message("foo");
    messages_store.push_message("bar");
    auto snapshot = messages_store.make_snapshot();
    messages_store.push_message("cux");

    {
        const auto deref_snapshot =
            snapshot
            | ranges::views::indirect
            | ranges::views::transform(
                [](const MessagesStore::Message& msg) {
                    return msg.contents();
                });
        const auto expected_snapshot = std::vector{"foo", "bar"};
        BOOST_CHECK_EQUAL_COLLECTIONS(
            deref_snapshot.begin(), deref_snapshot.end(),
            expected_snapshot.begin(), expected_snapshot.end());
    }

    snapshot.erase(snapshot.begin()+1);
    messages_store.delete_messages(snapshot);
    snapshot = messages_store.make_snapshot();

    {
        const auto deref_snapshot =
            snapshot
            | ranges::views::indirect
            | ranges::views::transform(
                [](const MessagesStore::Message& msg) {
                    return msg.contents();
                });
        const auto expected_snapshot = std::vector{"bar", "cux"};
        BOOST_CHECK_EQUAL_COLLECTIONS(
            deref_snapshot.begin(), deref_snapshot.end(),
            expected_snapshot.begin(), expected_snapshot.end());
    }
}


namespace {


struct TempDirFixture
{
    std::filesystem::path temp_dir;

    TempDirFixture()
        : temp_dir{}
    {
        do {
            temp_dir = std::filesystem::temp_directory_path() / boost::filesystem::unique_path().string();
        } while (std::filesystem::exists(temp_dir));
        BOOST_LOG_TRIVIAL(debug) << "Creating temp directory " << temp_dir;
        std::filesystem::create_directory(temp_dir);
    }

    ~TempDirFixture()
    {
        try {
            BOOST_LOG_TRIVIAL(debug) << "Removing temp directory " << temp_dir;
            std::filesystem::remove_all(temp_dir);
        }
        catch (std::exception& e) {
            BOOST_LOG_TRIVIAL(error) << "Failed to remove temp_dir " << temp_dir << ", because " << e.what();
        }
    }
};


}


BOOST_FIXTURE_TEST_CASE(filesystem, TempDirFixture)
{
    {
        auto messages_store = MessagesStore{temp_dir};
        messages_store.push_message("foo");
        messages_store.push_message("bar");
        auto snapshot = messages_store.make_snapshot();
        messages_store.push_message("cux");

        {
            const auto deref_snapshot =
                snapshot
                | ranges::views::indirect
                | ranges::views::transform(
                    [](const MessagesStore::Message& msg) {
                        return msg.contents();
                    });
            const auto expected_snapshot = std::vector{"foo", "bar"};
            BOOST_CHECK_EQUAL_COLLECTIONS(
                deref_snapshot.begin(), deref_snapshot.end(),
                expected_snapshot.begin(), expected_snapshot.end());
        }

        snapshot.erase(snapshot.begin()+1);
        messages_store.delete_messages(snapshot);
    }

    BOOST_CHECK_EQUAL(std::distance(std::filesystem::directory_iterator{temp_dir},
                                    std::filesystem::directory_iterator{}),
                      2);

    {
        auto messages_store = MessagesStore{temp_dir};
        auto snapshot = messages_store.make_snapshot();
        {
            const auto deref_snapshot =
                snapshot
                | ranges::views::indirect
                | ranges::views::transform(
                    [](const MessagesStore::Message& msg) {
                        return msg.contents();
                    })
                | ranges::to_vector
                | ranges::actions::sort;
            const auto expected_snapshot = std::vector{"bar", "cux"};
            BOOST_CHECK_EQUAL_COLLECTIONS(
                deref_snapshot.begin(), deref_snapshot.end(),
                expected_snapshot.begin(), expected_snapshot.end());
        }
    }
}


BOOST_AUTO_TEST_SUITE_END() // messages_store
