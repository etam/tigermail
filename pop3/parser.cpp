/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "parser.hpp"

#include <boost/fusion/include/adapt_struct.hpp>

#include <boost/spirit/home/x3.hpp>

#include "../pop3_smtp_common/parser_x3.hpp"


BOOST_FUSION_ADAPT_STRUCT(
    TigerMail::POP3::ev::APOP,
    user, digest
);

BOOST_FUSION_ADAPT_STRUCT(
    TigerMail::POP3::ev::LIST,
    msg_id
);

BOOST_FUSION_ADAPT_STRUCT(
    TigerMail::POP3::ev::RETR,
    msg_id
);

BOOST_FUSION_ADAPT_STRUCT(
    TigerMail::POP3::ev::DELE,
    msg_id
);


namespace TigerMail::POP3 {
namespace detail {

using POP3_SMTP_common::parse_type;


namespace {


namespace x3 = boost::spirit::x3;
namespace ascii = boost::spirit::x3::ascii;
using x3::eoi;
using x3::no_case;
using x3::no_skip;
using x3::repeat;
using x3::uint_;
using ascii::char_;

const x3::rule<class APOP_ID, ev::APOP> APOP;
const x3::rule<class LIST_ID, ev::LIST> LIST;
const x3::rule<class RETR_ID, ev::RETR> RETR;
const x3::rule<class DELE_ID, ev::DELE> DELE;

const auto APOP_def =
    no_skip[' ' >> +(char_ - ' ') >> ' ' >> repeat(32)[char_ - ' '] >> eoi];
const auto LIST_def =
    no_skip[-(' ' >> -uint_) >> eoi];
const auto RETR_def =
    no_skip[' ' >> uint_ >> eoi];
const auto DELE_def =
    RETR_def;

BOOST_SPIRIT_DEFINE(APOP)
BOOST_SPIRIT_DEFINE(LIST)
BOOST_SPIRIT_DEFINE(RETR)
BOOST_SPIRIT_DEFINE(DELE)


} // namespace


ev::APOP parse_APOP(std::string_view args)
{
    return parse_type<ev::APOP>(APOP, args);
}

ev::LIST parse_LIST(std::string_view args)
{
    return parse_type<ev::LIST>(LIST, args);
}

ev::RETR parse_RETR(std::string_view args)
{
    return parse_type<ev::RETR>(RETR, args);
}

ev::DELE parse_DELE(std::string_view args)
{
    return parse_type<ev::DELE>(DELE, args);
}


} // namespace detail


namespace {


class CommandProcessorClient
    : public POP3_SMTP_common::CommandProcessorClientInterface
{
private:
    StateMachine* m_sm;

public:
    explicit
    CommandProcessorClient(StateMachine* sm)
        : m_sm{sm}
    {}

    void command_too_long() override
    {
        m_sm->process_event(ev::unknown_command_or_syntax_error{});
    }

    void parse_command(std::string_view command) override
    {
        detail::parse_command(command, m_sm);
    }
};


} // namespace


Parser::Parser(ImplInterface& pop3, const Limits& limits)
    : m_logger{}
    , m_sm{pop3, m_logger}
    , m_limits{limits}
    , m_input_buffer{}
    , m_ignoring_input_until_end{false}
{
    m_sm.process_event(ev::connected{});
}

void Parser::process_input(std::string_view new_input)
{
    m_input_buffer.append(new_input);
    bool cont = true;
    auto command_processor_client = CommandProcessorClient{&m_sm};
    while (cont) {
        std::tie(m_input_buffer, cont) = POP3_SMTP_common::process_command(
            std::move(m_input_buffer),
            &command_processor_client,
            m_limits.max_command_len,
            &m_ignoring_input_until_end);
    }
}


} // namespace TigerMail::POP3
