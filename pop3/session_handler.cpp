/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "session_handler.hpp"

#include <chrono>
#include <numeric>
#include <thread>

#include <range/v3/core.hpp>
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/remove_if.hpp>
#include <range/v3/view/transform.hpp>

#include "format_response.hpp"
#include "messages_store.hpp"


namespace TigerMail::POP3 {


std::string default_banner_generator()
{
    auto banner = std::ostringstream{};
    banner << '<'
           << std::this_thread::get_id()
           << '.'
           << std::chrono::duration_cast<std::chrono::seconds>(
               std::chrono::system_clock::now().time_since_epoch()).count()
           << "@localhost>";
    return banner.str();
}


Impl::Impl(
    std::function<bool(std::string_view, std::string_view)>&& login_verifier,
    gsl::not_null<MessagesStore*> messages_store,
    std::ostream& output,
    bool& should_disconnect,
    std::function<std::string()>&& banner_generator)
    : m_login_verifier{std::move(login_verifier)}
    , m_messages_store{messages_store}
    , m_messages_store_lock{messages_store->m_pop3_mutex, std::defer_lock}
    , m_messages_snapshot{}
    , m_messages_marked_to_delete{}
    , m_output{output}
    , m_should_disconnect{should_disconnect}
    , m_banner_generator{std::move(banner_generator)}
{}

bool Impl::is_login_valid(std::string_view user, std::string_view digest) const
{
    return m_login_verifier(user, digest);
}

bool Impl::is_locked() const
{
    return m_messages_store_lock.owns_lock();
}

bool Impl::is_valid_msg_id(MsgId msg_id) const
{
    return (msg_id.as_idx() < m_messages_snapshot.size())
        && m_messages_marked_to_delete.count(msg_id) == 0;
}

void Impl::send_err()
{
    m_output << format::err;
}

void Impl::send_ok()
{
    m_output << format::ok;
}

void Impl::send_ok_with_banner()
{
    m_output << format::ok_with_banner{m_banner_generator()};
}

void Impl::try_lock_and_snapshot_inbox()
{
    if (m_messages_store_lock.try_lock()) {
        m_messages_snapshot = m_messages_store->make_snapshot();
    }
}

void Impl::send_response_to_STAT()
{
    const auto messages_count = m_messages_snapshot.size();
    const auto messages_size = std::accumulate(
        m_messages_snapshot.cbegin(),
        m_messages_snapshot.cend(),
        std::size_t{0},
        [](std::size_t count, const auto& msg) {
            return count + msg->size();
        }
    );
    m_output << format::response_to_STAT{messages_count, messages_size};
}

void Impl::send_response_to_LIST(std::optional<MsgId> msg_id)
{
    if (msg_id) {
        const auto msg_info = format::LIST_msg_info{
            *msg_id,
            m_messages_snapshot[msg_id->as_idx()]->size()
        };
        m_output << format::response_to_LIST_single{msg_info};
        return;
    }

    auto msgs_info =
        ranges::views::ints(std::size_t{0}, m_messages_snapshot.size())
        | ranges::views::transform(&MsgId::from_idx)
        | ranges::views::remove_if(
            [&](const auto i_msg_id) {
                return m_messages_marked_to_delete.count(i_msg_id) != 0;
            })
        | ranges::views::transform(
            [&](const auto i_msg_id) {
                return format::LIST_msg_info{
                    .id = i_msg_id,
                    .size = m_messages_snapshot[i_msg_id.as_idx()]->size(),
                };
            })
        | ranges::to_vector;
    m_output << format::response_to_LIST_multi{std::move(msgs_info)};
}

void Impl::send_response_to_RETR(MsgId msg_id)
{
    m_output << format::response_to_RETR{m_messages_snapshot[msg_id.as_idx()]->contents()};
}

void Impl::mark_message_as_deleted(MsgId msg_id)
{
    m_messages_marked_to_delete.insert(msg_id);
}

void Impl::unmark_all_messages()
{
    m_messages_marked_to_delete.clear();
}

void Impl::delete_marked_messages()
{
    m_messages_store->delete_messages(
        m_messages_marked_to_delete
        | ranges::views::transform(
            [&](const auto msg_id) {
                return m_messages_snapshot[msg_id.as_idx()];
            })
        | ranges::to_vector
    );
}

void Impl::disconnect()
{
    m_should_disconnect = true;
}



SessionHandler::SessionHandler(
    std::function<bool(std::string_view, std::string_view)>&& login_verifier,
    gsl::not_null<MessagesStore*> messages_store,
    const Limits& limits)
    : m_limits{limits}
    , m_output_buffer{}
    , m_should_disconnect{false}
    , m_pop3_impl{
        std::move(login_verifier),
        messages_store,
        m_output_buffer,
        m_should_disconnect}
    , m_parser{m_pop3_impl, m_limits}
{}

std::string SessionHandler::handle_hello()
{
    auto result = m_output_buffer.str();
    m_output_buffer.str("");
    return result;
}

std::tuple<std::string, bool> SessionHandler::handle_input(std::string_view input)
{
    m_parser.process_input(input);
    auto result = std::make_tuple(m_output_buffer.str(), m_should_disconnect);
    m_output_buffer.str("");
    return result;
}

std::string SessionHandler::handle_stop()
{
    return {};
}


} // namespace TigerMail::POP3
