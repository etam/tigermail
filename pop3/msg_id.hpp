/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_MSG_ID_HPP
#define TIGERMAIL_POP3_MSG_ID_HPP

#include <ostream>

#include <gsl/gsl_assert>


namespace TigerMail::POP3 {


class MsgId
{
private:
    unsigned int m_msg_id;

public:
    MsgId(unsigned int msg_id)
        : m_msg_id{msg_id}
    {}

    static
    MsgId from_idx(unsigned int msg_idx)
    {
        return MsgId(msg_idx+1);
    }

    MsgId() = default;
    MsgId(const MsgId& other) = default;

    unsigned int value() const
    {
        return m_msg_id;
    }

    unsigned int as_idx() const
    {
        return m_msg_id - 1;
    }
};


inline
bool operator==(const MsgId id1, const MsgId id2)
{
    return id1.value() == id2.value();
}

inline
bool operator!=(const MsgId id1, const MsgId id2)
{
    return !(id1 == id2);
}

inline
bool operator<(const MsgId id1, const MsgId id2)
{
    return id1.value() < id2.value();
}

inline
bool operator<=(const MsgId id1, const MsgId id2)
{
    return (id1 == id2) || (id1 < id2);
}

inline
bool operator>(const MsgId id1, const MsgId id2)
{
    return !(id1 <= id2);
}

inline
bool operator>=(const MsgId id1, const MsgId id2)
{
    return !(id1 < id2);
}


inline
std::ostream& operator<<(std::ostream& o, const MsgId id)
{
    return o << id.value();
}


} // namespace TigerMail::POP3

#endif // TIGERMAIL_POP3_MSG_ID_HPP
