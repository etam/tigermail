/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_PARSER_HPP
#define TIGERMAIL_POP3_PARSER_HPP

#include <string>
#include <string_view>
#include <tuple>

#include "../common/string_view.hpp"

#include "../pop3_smtp_common/parser.hpp"

#include "events.hpp"
#include "limits.hpp"
#include "state_machine.hpp"


namespace TigerMail::POP3 {

class ImplInterface;


namespace detail {

using POP3_SMTP_common::SyntaxError;
using POP3_SMTP_common::disallow_args;


ev::APOP parse_APOP(std::string_view args);
ev::LIST parse_LIST(std::string_view args);
ev::RETR parse_RETR(std::string_view args);
ev::DELE parse_DELE(std::string_view args);


template <typename StateMachine>
void parse_command(const std::string_view input, StateMachine* sm)
try {
    if (input.size() < 4) {
        throw SyntaxError{};
    }
    const auto command = input.substr(0, 4);
    const auto args = input.substr(4);

    if (case_insensitive_equal(command, "APOP")) {
        sm->process_event(parse_APOP(args));
    }
    else if (case_insensitive_equal(command, "STAT")) {
        sm->process_event(disallow_args<ev::STAT>(args));
    }
    else if (case_insensitive_equal(command, "LIST")) {
        sm->process_event(parse_LIST(args));
    }
    else if (case_insensitive_equal(command, "RETR")) {
        sm->process_event(parse_RETR(args));
    }
    else if (case_insensitive_equal(command, "DELE")) {
        sm->process_event(parse_DELE(args));
    }
    else if (case_insensitive_equal(command, "NOOP")) {
        sm->process_event(disallow_args<ev::NOOP>(args));
    }
    else if (case_insensitive_equal(command, "RSET")) {
        sm->process_event(disallow_args<ev::RSET>(args));
    }
    else if (case_insensitive_equal(command, "QUIT")) {
        sm->process_event(disallow_args<ev::QUIT>(args));
    }
    else {
        throw SyntaxError{};
    }
}
catch (SyntaxError&) {
    sm->process_event(ev::unknown_command_or_syntax_error{});
}


} // namespace detail


class Parser
{
private:
    Logger m_logger;
    StateMachine m_sm;
    const Limits& m_limits;
    std::string m_input_buffer;
    bool m_ignoring_input_until_end;

public:
    Parser(ImplInterface& pop3, const Limits& limits);

    void process_input(std::string_view new_input);
};


} // namespace TigerMail::POP3

#endif // TIGERMAIL_POP3_PARSER_HPP
