/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "state_machine.hpp"

#include <string>

#include <boost/log/trivial.hpp>


namespace TigerMail {
namespace POP3 {


namespace a { // action


DEFINE_ACTION(send_err, ImplInterface& pop3)
{
    pop3.send_err();
}

DEFINE_ACTION(send_ok, ImplInterface& pop3)
{
    pop3.send_ok();
}

DEFINE_ACTION(send_ok_with_banner, ImplInterface& pop3)
{
    pop3.send_ok_with_banner();
}

DEFINE_ACTION(try_lock_and_snapshot_inbox, ImplInterface& pop3)
{
    pop3.try_lock_and_snapshot_inbox();
}

DEFINE_ACTION(send_response_to_STAT, ImplInterface& pop3)
{
    pop3.send_response_to_STAT();
}

DEFINE_ACTION(send_response_to_LIST, ImplInterface& pop3, const ev::LIST& ev)
{
    pop3.send_response_to_LIST(ev.msg_id);
}

DEFINE_ACTION(send_response_to_RETR, ImplInterface& pop3, const ev::RETR& ev)
{
    pop3.send_response_to_RETR(ev.msg_id);
}

DEFINE_ACTION(mark_message_as_deleted, ImplInterface& pop3, const ev::DELE& ev)
{
    pop3.mark_message_as_deleted(ev.msg_id);
}

DEFINE_ACTION(unmark_all_messages, ImplInterface& pop3)
{
    pop3.unmark_all_messages();
}

DEFINE_ACTION(delete_marked_messages, ImplInterface& pop3)
{
    pop3.delete_marked_messages();
}

DEFINE_ACTION(disconnect, ImplInterface& pop3)
{
    pop3.disconnect();
}


} // namespace a


namespace g { // guard


DEFINE_GUARD(is_self, const ImplInterface& pop3, const ev::APOP& ev)
{
    return pop3.is_login_valid(ev.user, ev.digest);
}

DEFINE_GUARD(is_locked, const ImplInterface& pop3)
{
    return pop3.is_locked();
}

DEFINE_GUARD(LIST_has_valid_msg_id, const ImplInterface& pop3, const ev::LIST& ev)
{
    if (!ev.msg_id) {
        return true;
    }
    return pop3.is_valid_msg_id(*ev.msg_id);
}

DEFINE_GUARD(RETR_has_valid_msg_id, const ImplInterface& pop3, const ev::RETR& ev)
{
    return pop3.is_valid_msg_id(ev.msg_id);
}

DEFINE_GUARD(DELE_has_valid_msg_id, const ImplInterface& pop3, const ev::DELE& ev)
{
    return pop3.is_valid_msg_id(ev.msg_id);
}


} // namespace g


} // namespace POP3


template <>
void Logger::log_process_event<POP3::TransitionTable>(const POP3::ev::connected&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev connected";
}

template <>
void Logger::log_process_event<POP3::TransitionTable>(const POP3::ev::APOP& ev)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev APOP user=\"" << ev.user << "\" digest=\"" << ev.digest << "\"";
}

template <>
void Logger::log_process_event<POP3::TransitionTable>(const POP3::ev::STAT&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev STAT";
}

template <>
void Logger::log_process_event<POP3::TransitionTable>(const POP3::ev::LIST& ev)
{
    BOOST_LOG_TRIVIAL(debug)
        << "POP3 ev LIST"
        << (ev.msg_id
            ? std::string{" msg_id="} + std::to_string(ev.msg_id->value())
            : "");
}

template <>
void Logger::log_process_event<POP3::TransitionTable>(const POP3::ev::RETR& ev)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev RETR msg_id=" << ev.msg_id.value();
}

template <>
void Logger::log_process_event<POP3::TransitionTable>(const POP3::ev::DELE& ev)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev DELE msg_id=" << ev.msg_id.value();
}

template <>
void Logger::log_process_event<POP3::TransitionTable>(const POP3::ev::NOOP&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev NOOP";
}

template <>
void Logger::log_process_event<POP3::TransitionTable>(const POP3::ev::RSET&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev RSET";
}

template <>
void Logger::log_process_event<POP3::TransitionTable>(const POP3::ev::QUIT&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev QUIT";
}

template <>
void Logger::log_process_event<POP3::TransitionTable>(const POP3::ev::unknown_command_or_syntax_error&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev unknown_command_or_syntax_error";
}

template <>
void Logger::log_process_event<POP3::TransitionTable>(const boost::sml::back::anonymous&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev [anonymous]";
}


template <>
void Logger::log_guard<POP3::TransitionTable>(const POP3::g::DELE_has_valid_msg_id_cls&, const POP3::ev::DELE&, bool result)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev DELE g has_valid_msg_id result=" << result;
}

template <>
void Logger::log_guard<POP3::TransitionTable>(const POP3::g::LIST_has_valid_msg_id_cls&, const POP3::ev::LIST&, bool result)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev LIST g has_valid_msg_id result=" << result;
}

template <>
void Logger::log_guard<POP3::TransitionTable>(const POP3::g::RETR_has_valid_msg_id_cls&, const POP3::ev::RETR&, bool result)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev RETR g has_valid_msg_id result=" << result;
}

template <>
void Logger::log_guard<POP3::TransitionTable>(const POP3::g::is_locked_cls&, const boost::sml::back::anonymous&, bool result)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev [anonymous] g is_locked result=" << result;
}

template <>
void Logger::log_guard<POP3::TransitionTable>(const POP3::g::is_self_cls&, const POP3::ev::APOP&, bool result)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev APOP g is_self result=" << result;
}


template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::delete_marked_messages_cls&, const POP3::ev::QUIT&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev QUIT a delete_marked_messages";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::disconnect_cls&, const POP3::ev::QUIT&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev QUIT a disconnect";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::mark_message_as_deleted_cls&, const POP3::ev::DELE&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev DELE a mark_message_as_deleted";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_err_cls&, const POP3::ev::APOP&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev APOP a send_err";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_err_cls&, const POP3::ev::DELE&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev DELE a send_err";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_err_cls&, const POP3::ev::LIST&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev LIST a send_err";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_err_cls&, const POP3::ev::NOOP&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev NOOP a send_err";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_err_cls&, const POP3::ev::RETR&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev RETR a send_err";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_err_cls&, const POP3::ev::RSET&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev RSET a send_err";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_err_cls&, const POP3::ev::STAT&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev STAT a send_err";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_err_cls&, const POP3::ev::unknown_command_or_syntax_error&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev unknown_command_or_syntax_error a send_err";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_err_cls&, const boost::sml::back::anonymous&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev [anonymous] a send_err";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_ok_cls&, const POP3::ev::DELE&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev DELE a send_ok";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_ok_cls&, const POP3::ev::NOOP&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev NOOP a send_ok";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_ok_cls&, const POP3::ev::QUIT&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev QUIT a send_ok";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_ok_cls&, const POP3::ev::RSET&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev RSET a send_ok";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_ok_cls&, const boost::sml::back::anonymous&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev [anonymous] a send_ok";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_ok_with_banner_cls&, const POP3::ev::connected&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev connected a send_ok_with_banner";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_response_to_LIST_cls&, const POP3::ev::LIST&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev LIST a send_response_to_LIST";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_response_to_RETR_cls&, const POP3::ev::RETR&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev RETR a send_response_to_RETR";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::send_response_to_STAT_cls&, const POP3::ev::STAT&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev STAT a send_response_to_STAT";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::try_lock_and_snapshot_inbox_cls&, const POP3::ev::APOP&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev APOP a try_lock_and_snapshot_inbox";
}

template <>
void Logger::log_action<POP3::TransitionTable>(const POP3::a::unmark_all_messages_cls&, const POP3::ev::RSET&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 ev RSET a unmark_all_messages";
}


template <>
void Logger::log_state_change<POP3::TransitionTable>(const boost::sml::aux::string<POP3::st::Authorization_cls>&, const boost::sml::aux::string<POP3::st::Locking_cls>&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 st Authorization -> Locking";
}

template <>
void Logger::log_state_change<POP3::TransitionTable>(const boost::sml::aux::string<POP3::st::Authorization_cls>&, const boost::sml::aux::string<boost::sml::back::terminate_state>&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 st Authorization -> X";
}

template <>
void Logger::log_state_change<POP3::TransitionTable>(const boost::sml::aux::string<POP3::st::Locking_cls>&, const boost::sml::aux::string<POP3::st::Authorization_cls>&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 st Locking -> Authorization";
}

template <>
void Logger::log_state_change<POP3::TransitionTable>(const boost::sml::aux::string<POP3::st::Locking_cls>&, const boost::sml::aux::string<POP3::st::Transaction_cls>&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 st Locking -> Transaction";
}

template <>
void Logger::log_state_change<POP3::TransitionTable>(const boost::sml::aux::string<POP3::st::Start_cls>&, const boost::sml::aux::string<POP3::st::Authorization_cls>&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 st Start -> Authorization";
}

template <>
void Logger::log_state_change<POP3::TransitionTable>(const boost::sml::aux::string<POP3::st::Transaction_cls>&, const boost::sml::aux::string<boost::sml::back::terminate_state>&)
{
    BOOST_LOG_TRIVIAL(debug) << "POP3 st Transaction -> X";
}


} // namespace TigerMail
