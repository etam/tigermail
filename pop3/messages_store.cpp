/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "messages_store.hpp"

#include <fstream>

#include <boost/filesystem.hpp>
#include <boost/log/trivial.hpp>

#include "../common/fs.hpp"
#include "../common/utils.hpp"


namespace TigerMail::POP3 {


MessagesStore::Message::Message(std::string&& data)
    : m_data{std::move(data)}
{}

MessagesStore::Message::Message(std::filesystem::path path)
    : m_data(path)
{}

std::size_t MessagesStore::Message::size() const
{
    return std::visit(overloaded{
            [](const std::string& data) {
                return data.size();
            },
            [](const std::filesystem::path& path) {
                return std::filesystem::file_size(path);
            }},
        m_data);
}

std::string MessagesStore::Message::contents() const
{
    return std::visit(overloaded{
            [](const std::string& data) {
                return data;
            },
            [](const std::filesystem::path& path) {
                return read_file_contents(path);
            }},
        m_data);
}

std::optional<std::filesystem::path> MessagesStore::Message::path() const
{
    return std::visit(overloaded{
            [](const std::string&) -> std::optional<std::filesystem::path> {
                return std::nullopt;
            },
            [](const std::filesystem::path& path) -> std::optional<std::filesystem::path> {
                return path;
            }},
        m_data);
}


namespace {


std::filesystem::path make_unique_path(const std::filesystem::path& directory)
{
    auto path = std::filesystem::path{};
    do {
        path = directory / boost::filesystem::unique_path("%%%%%%%%%%%%%%%%.eml").string();
    } while (std::filesystem::exists(path)); // should never happen
    return path;
}


} // namespace


MessagesStore::MessagesStore()
    : m_directory{}
    , m_messages{}
    , m_pop3_mutex{}
{}

MessagesStore::MessagesStore(std::filesystem::path directory)
    : m_directory{directory}
    , m_messages{}
    , m_pop3_mutex{}
{
    m_messages.exec_rw(
        [&](auto& messages) {
            for (auto& path : std::filesystem::directory_iterator(directory)) {
                messages.push_back(Message{path});
            }
        });
}


void MessagesStore::push_message(std::string&& msg)
{
    m_messages.exec_rw(
        [&](auto& messages) {
            if (m_directory) {
                const auto path = make_unique_path(*m_directory);
                BOOST_LOG_TRIVIAL(debug) << "Saving email in " << path;
                std::ofstream{path} << std::move(msg);
                messages.push_back(Message{path});
            }
            else {
                messages.push_back(Message{std::move(msg)});
            }
        });
}

auto MessagesStore::make_snapshot() -> std::vector<std::list<Message>::const_iterator>
{
    return m_messages.exec_rw(
        [](auto& messages) {
            // TODO: ranges?
            auto snapshot = std::vector<std::list<Message>::const_iterator>{};
            snapshot.reserve(messages.size());
            for (auto it = messages.cbegin();
                 it != messages.cend();
                 ++it) {
                snapshot.push_back(it);
            }
            return snapshot;
        });
}

void MessagesStore::delete_messages(const std::vector<std::list<Message>::const_iterator>& iterators)
{
    m_messages.exec_rw(
        [&](auto& messages) {
            for (const auto iterator : iterators) {
                if (const auto opt_path = iterator->path(); opt_path) {
                    try {
                        BOOST_LOG_TRIVIAL(debug) << "Removing email in " << *opt_path;
                        std::filesystem::remove(*opt_path);
                    }
                    catch (std::exception& e) {
                        BOOST_LOG_TRIVIAL(error) << "Cannot remove file " << *opt_path << ", because " << e.what();
                    }
                }
                messages.erase(iterator);
            }
        });
}


} // namespace TigerMail::POP3
