/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SMTP_PARSER_HPP
#define TIGERMAIL_SMTP_PARSER_HPP

#include <memory>
#include <string>
#include <string_view>
#include <tuple>

#include <gsl/pointers>

#include "../common/string_view.hpp"

#include "../pop3_smtp_common/parser.hpp"

#include "events.hpp"
#include "limits.hpp"
#include "parser_control_interface.hpp"
#include "state_machine.hpp"


namespace TigerMail::SMTP {


class ImplInterface;


namespace detail {

using POP3_SMTP_common::SyntaxError;
using POP3_SMTP_common::disallow_args;


ev::MAIL parse_MAIL(std::string_view args);
ev::RCPT parse_RCPT(std::string_view args);
ev::VRFY parse_VRFY(std::string_view args);


template <typename StateMachine>
void parse_command(const std::string_view input, StateMachine* sm)
try {
    // https://tools.ietf.org/html/rfc5321#section-4.1

    if (input.size() < 4) {
        throw SyntaxError{};
    }
    const auto command = input.substr(0, 4);
    const auto args = input.substr(4);

    if (case_insensitive_equal(command, "HELO")) {
        sm->process_event(ev::HELO{}); // ignore args
    }
    else if (case_insensitive_equal(command, "EHLO")) {
        sm->process_event(ev::EHLO{}); // ignore args
    }
    else if (case_insensitive_equal(command, "MAIL")) {
        sm->process_event(parse_MAIL(args));
    }
    else if (case_insensitive_equal(command, "RCPT")) {
        sm->process_event(parse_RCPT(args));
    }
    else if (case_insensitive_equal(command, "DATA")) {
        sm->process_event(disallow_args<ev::DATA>(args));
    }
    else if (case_insensitive_equal(command, "RSET")) {
        sm->process_event(disallow_args<ev::RSET>(args));
    }
    else if (case_insensitive_equal(command, "QUIT")) {
        sm->process_event(disallow_args<ev::QUIT>(args));
    }
    else if (case_insensitive_equal(command, "NOOP")) {
        sm->process_event(ev::NOOP{}); // ignore args
    }
    else if (case_insensitive_equal(command, "VRFY")) {
        sm->process_event(parse_VRFY(args));
    }
    else {
        sm->process_event(ev::command_unknown_or_too_long{});
    }
}
catch (SyntaxError&) {
    sm->process_event(ev::syntax_error{});
}


/**
 * return: {unparsed input, should continue parsing}
 *
 * We're not searching for and removing "byte-stuffing", 
 * because we would have to add them again in pop3.
 */
template <typename StateMachine>
std::tuple<std::string, bool> process_data(
    std::string&& input,
    StateMachine* sm,
    const Limits& limits,
    std::string* buffer,
    bool* ignoring_input_until_end)
{
    const auto last_size = buffer->size();
    buffer->append(input);

    static constexpr const auto immediate_eod = std::string_view{".\r\n"};
    if (last_size < immediate_eod.size()
            && buffer->size() >= immediate_eod.size()
            && starts_with(*buffer, immediate_eod)) {
        // cut and return tail
        sm->process_event(ev::data_contents{
                .data = gsl::not_null{std::make_shared<const std::string>("")},
            });
        auto unparsed_input = std::move(*buffer);
        *buffer = std::string{};
        unparsed_input.erase(0, immediate_eod.size());
        if (unparsed_input.empty()) {
            return {"", false};
        }
        return {std::move(unparsed_input), true};
    }

    static constexpr const auto eod = std::string_view{"\r\n.\r\n"};
    const auto start_searching_pos =
        // std::max(std::size_t{0}, last_size - eod.size())
        // does not work, because unsigned
        (last_size > eod.size())
        ? last_size - eod.size()
        : std::size_t{0};
    const auto eod_pos = buffer->find(eod, start_searching_pos);

    if (eod_pos == std::string::npos) {
        if (buffer->size() > limits.max_data_len) {
            sm->process_event(ev::data_too_big{});
            *ignoring_input_until_end = true;
        }
        if (*ignoring_input_until_end) {
            // keep some of last chars to be able to find eod
            *buffer = buffer->substr(buffer->size() - eod.size() + 1);
            return {"", false};
        }
        else {
            return {"", false};
        }
    }

    auto extra_input = buffer->substr(eod_pos + eod.size());

    if (*ignoring_input_until_end) {
        *ignoring_input_until_end = false;
        buffer->clear();
    }
    else {
        buffer->erase(eod_pos);
        if (buffer->size() > limits.max_data_len) {
            buffer->clear();
            sm->process_event(ev::data_too_big{});
        }
        else {
            const auto data_ptr =
                std::make_shared<const std::string>(std::move(*buffer));
            *buffer = std::string{};
            sm->process_event(ev::data_contents{
                    .data = gsl::not_null{data_ptr},
                });
        }
    }

    if (extra_input.empty()) {
        return {"", false};
    }
    return {std::move(extra_input), true};
}


} // namespace detail


class Parser
    : public ParserControlInterface
{
private:
    Logger m_logger;
    StateMachine m_sm;
    const Limits& m_limits;
    std::string m_input_buffer;
    std::string m_data_buffer;
    bool m_parsing_commands; // else: parsing data
    bool m_ignoring_input_until_end;

public:
    Parser(ImplInterface& smtp, const Limits& limits);

    void parse_commands() override;
    void parse_data() override;

    void process_input(std::string_view new_input);

    void stop();
};


} // namespace TigerMail::SMTP

#endif // TIGERMAIL_SMTP_PARSER_HPP
