/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "state_machine.hpp"

#include <boost/log/trivial.hpp>

#include "../common/utils.hpp"


namespace TigerMail {
namespace SMTP {


namespace a {


void response::operator()(ImplInterface& smtp) const
{
    smtp.send_response(code, msg);
}

DEFINE_ACTION(clear_buffers, ImplInterface& smtp)
{
    smtp.clear_buffers();
}

DEFINE_ACTION(store_recipient, ImplInterface& smtp, const ev::RCPT& ev)
{
    smtp.store_recipient(ev.forward_path);
}

DEFINE_ACTION(store_data_contents, ImplInterface& smtp, const ev::data_contents& ev)
{
    smtp.store_data_contents(ev.data);
}

DEFINE_ACTION(finish_message, ImplInterface& smtp)
{
    smtp.finish_message();
}

DEFINE_ACTION(parser_parse_commands, ParserControlInterface& parser)
{
    parser.parse_commands();
}

DEFINE_ACTION(parser_parse_data, ParserControlInterface& parser)
{
    parser.parse_data();
}

DEFINE_ACTION(disconnect, ImplInterface& smtp)
{
    smtp.disconnect();
}


} // namespace a


namespace g {


DEFINE_GUARD(is_self, const ImplInterface& smtp, const ev::MAIL& ev)
{
    return smtp.is_self(ev.reverse_path);
}

DEFINE_GUARD(MAIL_has_parameters, const ev::MAIL& ev)
{
    return !ev.parameters.empty();
}

DEFINE_GUARD(RCPT_has_parameters, const ev::RCPT& ev)
{
    return !ev.parameters.empty();
}

DEFINE_GUARD(has_recipients, const ImplInterface& smtp)
{
    return smtp.has_recipients();
}

DEFINE_GUARD(has_max_recipients, const ImplInterface& smtp)
{
    return smtp.has_max_recipients();
}

DEFINE_GUARD(is_recipient_valid, const ImplInterface& smtp, const ev::RCPT& ev)
{
    return smtp.is_recipient_valid(ev.forward_path);
}


} // namespace g


} // namespace SMTP


template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::DATA&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev DATA";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::EHLO&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev EHLO";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::HELO&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev HELO";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::MAIL& ev)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev MAIL reverse_path=\"" << ev.reverse_path << '"';
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::NOOP&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev NOOP";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::QUIT&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev QUIT";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::RCPT& ev)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev RCPT forward_path=\"" << ev.forward_path << '"';
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::RSET&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev RSET";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::VRFY&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev VRFY";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::command_unknown_or_too_long&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev command_unknown_or_too_long";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::connected&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev connected";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::data_contents& ev)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev data_contents bytes=" << ev.data->size();
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::data_too_big&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev data_too_big";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::server_closing&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev server_closing";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const SMTP::ev::syntax_error&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev syntax_error";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::DATA>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry DATA";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::EHLO>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry EHLO";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::HELO>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry HELO";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::MAIL>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry MAIL";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::QUIT>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry QUIT";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::RSET>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry RSET";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::connected>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry connected";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::data_contents>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry data_contents";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::data_too_big>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry data_too_big";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::server_closing>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry server_closing";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, boost::sml::back::initial>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry [initial]";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::DATA>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit DATA";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::EHLO>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit EHLO";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::HELO>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit HELO";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::MAIL>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit MAIL";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::QUIT>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit QUIT";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::RSET>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit RSET";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::connected>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit connected";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::data_contents>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit data_contents";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::data_too_big>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit data_too_big";
}

template <>
void Logger::log_process_event<SMTP::SessionTransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::server_closing>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit server_closing";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::DATA&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev DATA";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::EHLO&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev EHLO";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::HELO&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev HELO";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::MAIL& ev)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev MAIL reverse_path=\"" << ev.reverse_path << '"';
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::NOOP&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev NOOP";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::QUIT&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev QUIT";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::RCPT& ev)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev RCPT forward_path=\"" << ev.forward_path << '"';
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::RSET&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev RSET";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::VRFY&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev VRFY";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::command_unknown_or_too_long&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev command_unknown_or_too_long";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::connected&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev connected";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::data_contents& ev)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev data_contents bytes=" << ev.data->size();
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::data_too_big&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev data_too_big";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::server_closing&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev server_closing";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const SMTP::ev::syntax_error&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev syntax_error";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::QUIT>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev on_entry QUIT";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::connected>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev on_entry connected";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::server_closing>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev on_entry server_closing";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const boost::sml::back::on_entry<boost::sml::back::_, boost::sml::back::initial>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev on_entry [initial]";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::QUIT>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev on_exit QUIT";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::connected>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev on_exit connected";
}

template <>
void Logger::log_process_event<SMTP::TransitionTable>(const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::server_closing>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev on_exit server_closing";
}


template <>
void Logger::log_guard<SMTP::SessionTransitionTable>(const SMTP::g::MAIL_has_parameters_cls&, const SMTP::ev::MAIL&, bool result)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev MAIL g has_parameters result=" << result;
}

template <>
void Logger::log_guard<SMTP::SessionTransitionTable>(const SMTP::g::RCPT_has_parameters_cls&, const SMTP::ev::RCPT&, bool result)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev RCPT g has_parameters result=" << result;
}

template <>
void Logger::log_guard<SMTP::SessionTransitionTable>(const SMTP::g::has_max_recipients_cls&, const SMTP::ev::RCPT&, bool result)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev RCPT g has_max_recipients result=" << result;
}

template <>
void Logger::log_guard<SMTP::SessionTransitionTable>(const SMTP::g::has_recipients_cls&, const SMTP::ev::DATA&, bool result)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev DATA g has_recipients result=" << result;
}

template <>
void Logger::log_guard<SMTP::SessionTransitionTable>(const SMTP::g::is_recipient_valid_cls&, const SMTP::ev::RCPT&, bool result)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev RCPT g is_recipient_valid result=" << result;
}

template <>
void Logger::log_guard<SMTP::SessionTransitionTable>(const SMTP::g::is_self_cls&, const SMTP::ev::MAIL&, bool result)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev MAIL g is_self result=" << result;
}


template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::clear_buffers_cls&, const SMTP::ev::MAIL&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev MAIL a clear_buffers";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::finish_message_cls&, const SMTP::ev::data_contents&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev data_contents a finish_message";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_commands_cls&, const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::DATA>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit DATA a parser_parse_commands";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_commands_cls&, const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::EHLO>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit EHLO a parser_parse_commands";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_commands_cls&, const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::HELO>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit HELO a parser_parse_commands";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_commands_cls&, const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::MAIL>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit MAIL a parser_parse_commands";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_commands_cls&, const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::QUIT>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit QUIT a parser_parse_commands";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_commands_cls&, const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::RSET>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit RSET a parser_parse_commands";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_commands_cls&, const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::connected>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit connected a parser_parse_commands";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_commands_cls&, const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::data_contents>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit data_contents a parser_parse_commands";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_commands_cls&, const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::data_too_big>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit data_too_big a parser_parse_commands";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_commands_cls&, const boost::sml::back::on_exit<boost::sml::back::_, SMTP::ev::server_closing>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_exit server_closing a parser_parse_commands";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_data_cls&, const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::DATA>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry DATA a parser_parse_data";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_data_cls&, const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::EHLO>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry EHLO a parser_parse_data";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_data_cls&, const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::HELO>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry HELO a parser_parse_data";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_data_cls&, const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::MAIL>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry MAIL a parser_parse_data";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_data_cls&, const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::QUIT>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry QUIT a parser_parse_data";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_data_cls&, const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::RSET>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry RSET a parser_parse_data";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_data_cls&, const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::connected>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry connected a parser_parse_data";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_data_cls&, const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::data_contents>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry data_contents a parser_parse_data";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_data_cls&, const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::data_too_big>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry data_too_big a parser_parse_data";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_data_cls&, const boost::sml::back::on_entry<boost::sml::back::_, SMTP::ev::server_closing>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry server_closing a parser_parse_data";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::parser_parse_data_cls&, const boost::sml::back::on_entry<boost::sml::back::_, boost::sml::back::initial>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev on_entry [initial] a parser_parse_data";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::store_data_contents_cls&, const SMTP::ev::data_contents&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev data_contents a store_data_contents";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::store_recipient_cls&, const SMTP::ev::RCPT&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev RCPT a store_recipient";
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::DATA&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev DATA a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::EHLO&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev EHLO a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::HELO&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev HELO a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::MAIL&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev MAIL a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::NOOP&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev NOOP a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::RCPT&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev RCPT a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::RSET&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev RSET a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::VRFY&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev VRFY a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::command_unknown_or_too_long&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev command_unknown_or_too_long a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::data_contents&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev data_contents a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::data_too_big&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev data_too_big a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::SessionTransitionTable>(const SMTP::a::response& a, const SMTP::ev::syntax_error&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session ev syntax_error a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::TransitionTable>(const SMTP::a::disconnect_cls&, const SMTP::ev::QUIT&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev QUIT a disconnect";
}

template <>
void Logger::log_action<SMTP::TransitionTable>(const SMTP::a::disconnect_cls&, const SMTP::ev::server_closing&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev server_closing a disconnect";
}

template <>
void Logger::log_action<SMTP::TransitionTable>(const SMTP::a::response& a, const SMTP::ev::QUIT&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev QUIT a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::TransitionTable>(const SMTP::a::response& a, const SMTP::ev::connected&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev connected a response code=" << a.code << " msg=\"" << a.msg << '"';
}

template <>
void Logger::log_action<SMTP::TransitionTable>(const SMTP::a::response& a, const SMTP::ev::server_closing&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP ev server_closing a response code=" << a.code << " msg=\"" << a.msg << '"';
}


template <>
void Logger::log_state_change<SMTP::SessionTransitionTable>(const boost::sml::aux::string<SMTP::st::Idle_cls>&, const boost::sml::aux::string<SMTP::st::ReceivingRecipients_cls>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session st Idle -> ReceivingRecipients";
}

template <>
void Logger::log_state_change<SMTP::SessionTransitionTable>(const boost::sml::aux::string<SMTP::st::ReceivingData_cls>&, const boost::sml::aux::string<SMTP::st::Idle_cls>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session st ReceivingData -> Idle";
}

template <>
void Logger::log_state_change<SMTP::SessionTransitionTable>(const boost::sml::aux::string<SMTP::st::ReceivingRecipients_cls>&, const boost::sml::aux::string<SMTP::st::Idle_cls>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session st ReceivingRecipients -> Idle";
}

template <>
void Logger::log_state_change<SMTP::SessionTransitionTable>(const boost::sml::aux::string<SMTP::st::ReceivingRecipients_cls>&, const boost::sml::aux::string<SMTP::st::ReceivingData_cls>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session st ReceivingRecipients -> ReceivingData";
}

template <>
void Logger::log_state_change<SMTP::SessionTransitionTable>(const boost::sml::aux::string<SMTP::st::WaitForHelo_cls>&, const boost::sml::aux::string<SMTP::st::Idle_cls>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP Session st WaitForHelo -> Idle";
}

template <>
void Logger::log_state_change<SMTP::TransitionTable>(const boost::sml::aux::string<SMTP::SessionTransitionTable>&, const boost::sml::aux::string<boost::sml::back::terminate_state>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP st Session -> X";
}

template <>
void Logger::log_state_change<SMTP::TransitionTable>(const boost::sml::aux::string<SMTP::st::Start_cls>&, const boost::sml::aux::string<SMTP::SessionTransitionTable>&)
{
    BOOST_LOG_TRIVIAL(debug) << "SMTP st Start -> Session";
}


} // namespace TigerMail
