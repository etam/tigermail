/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SMTP_STATE_MACHINE_HPP
#define TIGERMAIL_SMTP_STATE_MACHINE_HPP

#include <functional>
#include <string_view>

#include <boost/sml.hpp>

#include "../common/state_machine.hpp"

#include "events.hpp"
#include "impl_interface.hpp"
#include "parser_control_interface.hpp"


namespace TigerMail::SMTP {

namespace sml = boost::sml;


namespace st { // state

DECLARE_STATE(Start);
DECLARE_STATE(WaitForHelo);
DECLARE_STATE(Idle);
DECLARE_STATE(ReceivingRecipients);
DECLARE_STATE(ReceivingData);

DECLARE_STATE(AnyState);

} // namespace st


namespace a { // action

struct response
{
    int code;
    std::string_view msg = "";

    void operator()(ImplInterface&) const;
};
DECLARE_ACTION(clear_buffers, ImplInterface& smtp);
DECLARE_ACTION(store_recipient, ImplInterface& smtp, const ev::RCPT& ev);
DECLARE_ACTION(store_data_contents, ImplInterface& smtp, const ev::data_contents& ev);
DECLARE_ACTION(finish_message, ImplInterface& smtp);
DECLARE_ACTION(parser_parse_commands, ParserControlInterface& parser);
DECLARE_ACTION(parser_parse_data, ParserControlInterface& parser);
DECLARE_ACTION(disconnect, ImplInterface& smtp);

} // namespace a


namespace g { // guard

DECLARE_GUARD(is_self, const ImplInterface& smtp, const ev::MAIL& ev);
DECLARE_GUARD(MAIL_has_parameters, const ev::MAIL& ev);
DECLARE_GUARD(RCPT_has_parameters, const ev::RCPT& ev);
DECLARE_GUARD(has_recipients, const ImplInterface& smtp);
DECLARE_GUARD(has_max_recipients, const ImplInterface& smtp);
DECLARE_GUARD(is_recipient_valid, const ImplInterface& smtp, const ev::RCPT& ev);

} // namespace g


constexpr const auto hostname = std::string_view{"localhost"};

// must have msg: 220, 221, 251, 421, 551

struct SessionTransitionTable
{
    auto operator()() const
    {
        namespace ev = ev::for_transition_table;
        using sml::_;
        using sml::operator!;
        using sml::operator&&;
        using sml::operator,;

        return sml::make_transition_table(
            *st::WaitForHelo + ev::HELO / a::response{250, hostname} = st::Idle
            ,st::WaitForHelo + ev::EHLO / a::response{250, hostname} = st::Idle
            ,st::WaitForHelo + ev::MAIL / a::response{503}
            ,st::WaitForHelo + ev::RCPT / a::response{503}
            ,st::WaitForHelo + ev::DATA / a::response{503}
            ,st::WaitForHelo + ev::RSET / a::response{250}

            ,st::Idle + ev::HELO / a::response{250, hostname}
            ,st::Idle + ev::EHLO / a::response{250, hostname}
            ,st::Idle + ev::MAIL [ !g::is_self ] / a::response{550}
            ,st::Idle + ev::MAIL [ g::MAIL_has_parameters ] / a::response{555}
            ,st::Idle + ev::MAIL [ g::is_self ] / (a::clear_buffers, a::response{250}) = st::ReceivingRecipients
            ,st::Idle + ev::RCPT / a::response{503}
            ,st::Idle + ev::DATA / a::response{503}
            ,st::Idle + ev::RSET / a::response{250}

            ,st::ReceivingRecipients + ev::HELO / a::response{250, hostname} = st::Idle
            ,st::ReceivingRecipients + ev::EHLO / a::response{250, hostname} = st::Idle
            ,st::ReceivingRecipients + ev::MAIL / a::response{503}
            ,st::ReceivingRecipients + ev::RCPT [ g::is_recipient_valid && !g::has_max_recipients && !g::RCPT_has_parameters ] / (a::store_recipient, a::response{250})
            ,st::ReceivingRecipients + ev::RCPT [ !g::is_recipient_valid ] / a::response{550}
            ,st::ReceivingRecipients + ev::RCPT [ g::RCPT_has_parameters ] / a::response{555}
            ,st::ReceivingRecipients + ev::RCPT [ g::has_max_recipients ] / a::response{452}
            ,st::ReceivingRecipients + ev::RSET / a::response{250} = st::Idle
            ,st::ReceivingRecipients + ev::DATA [ !g::has_recipients ] / a::response{554}
            ,st::ReceivingRecipients + ev::DATA [ g::has_recipients ] / a::response{354} = st::ReceivingData

            ,st::ReceivingData + sml::on_entry<_> / a::parser_parse_data
            ,st::ReceivingData + sml::on_exit<_> / a::parser_parse_commands
            ,st::ReceivingData + ev::data_contents / (a::store_data_contents, a::finish_message, a::response{250}) = st::Idle
            ,st::ReceivingData + ev::data_too_big / a::response{552} = st::Idle

            ,
            *st::AnyState + ev::NOOP / a::response{250}
            ,st::AnyState + ev::VRFY / a::response{252}
            ,st::AnyState + ev::command_unknown_or_too_long / a::response{500}
            ,st::AnyState + ev::syntax_error / a::response{501}
        );
    }
};


namespace st {

constexpr auto Session = sml::state<SessionTransitionTable>;

} // namespace st


struct TransitionTable
{
    auto operator()() const
    {
        namespace ev = ev::for_transition_table;
        using sml::X;
        using sml::operator,;

        return sml::make_transition_table(
            *st::Start + ev::connected / a::response{220, hostname} = st::Session

            ,st::Session + ev::QUIT / (a::response{221, hostname}, a::disconnect) = X
            ,st::Session + ev::server_closing / (a::response{421, hostname}, a::disconnect) = X
        );
    }
};


using StateMachine = boost::sml::sm<TransitionTable, boost::sml::logger<Logger>>;


} // namespace TigerMail::SMTP

# endif // TIGERMAIL_SMTP_STATE_MACHINE_HPP
