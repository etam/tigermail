/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "session_handler.hpp"

#include "format_response.hpp"


namespace TigerMail::SMTP {


Impl::Impl(
    std::function<bool(std::string_view)>&& is_self,
    MessageReceivedHandlerT message_received,
    const Limits& limits,
    std::ostream& output,
    bool& should_disconnect)
    : m_is_self{std::move(is_self)}
    , m_message_received{message_received}
    , m_limits{limits}
    , m_recipients_buffer{}
    , m_message_buffer{}
    , m_output{output}
    , m_should_disconnect{should_disconnect}
{}

bool Impl::is_self(std::string_view reverse_path) const
{
    return m_is_self(reverse_path);
}

void Impl::clear_buffers()
{
    m_recipients_buffer.clear();
    m_message_buffer.reset();
}

bool Impl::is_recipient_valid(std::string_view recipient) const
{
    // TODO: this could be better
    return recipient.find('@') != std::string_view::npos;
}

bool Impl::has_recipients() const
{
    return !m_recipients_buffer.empty();
}

bool Impl::has_max_recipients() const
{
    return m_recipients_buffer.size() == m_limits.max_recipients;
}

void Impl::send_response(int code, std::string_view msg)
{
    m_output << format_response{code, msg};
}

void Impl::store_recipient(std::string_view recipient)
{
    m_recipients_buffer.emplace_back(recipient);
}

void Impl::store_data_contents(gsl::not_null<std::shared_ptr<const std::string>> data)
{
    m_message_buffer = data;
}

void Impl::finish_message()
{
    m_message_received({
            .recipients = std::move(m_recipients_buffer),
            .message = gsl::not_null{m_message_buffer},
        });
}

void Impl::disconnect()
{
    m_should_disconnect = true;
}


SessionHandler::SessionHandler(
    std::function<bool(std::string_view)>&& is_self,
    MessageReceivedHandlerT message_received,
    const SMTP::Limits& limits)
    : m_limits{limits}
    , m_output_buffer{}
    , m_should_disconnect{false}
    , m_smtp_impl{std::move(is_self), message_received, m_limits, m_output_buffer, m_should_disconnect}
    , m_parser{m_smtp_impl, m_limits}
{}

std::string SessionHandler::handle_hello()
{
    auto result = m_output_buffer.str();
    m_output_buffer.str("");
    return result;
}

std::tuple<std::string, bool> SessionHandler::handle_input(std::string_view input)
{
    m_parser.process_input(input);
    auto result = std::make_tuple(m_output_buffer.str(), m_should_disconnect);
    m_output_buffer.str("");
    return result;
}

std::string SessionHandler::handle_stop()
{
    m_parser.stop();
    auto result = m_output_buffer.str();
    m_output_buffer.str("");
    return result;
}


} // namespace TigerMail::SMTP
