/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SMTP_EVENTS_HPP
#define TIGERMAIL_SMTP_EVENTS_HPP

#include <memory>
#include <string>
#include <vector>

#include <gsl/pointers>

#include "../common/state_machine.hpp"


namespace TigerMail::SMTP::ev {


DECLARE_EVENT(connected);
DECLARE_EVENT(HELO);
DECLARE_EVENT(EHLO);
DECLARE_EVENT_WITH_DATA(MAIL) {
    std::string reverse_path;
    std::vector<std::string> parameters;
};
DECLARE_EVENT_WITH_DATA(RCPT) {
    std::string forward_path;
    std::vector<std::string> parameters;
};
DECLARE_EVENT(DATA);
DECLARE_EVENT_WITH_DATA(data_contents) {
    gsl::not_null<std::shared_ptr<const std::string>> data;
};
DECLARE_EVENT(data_too_big);
DECLARE_EVENT(RSET);
DECLARE_EVENT(QUIT);
DECLARE_EVENT(server_closing);
DECLARE_EVENT(NOOP);
DECLARE_EVENT(VRFY);
DECLARE_EVENT(command_unknown_or_too_long);
DECLARE_EVENT(syntax_error);


} // namespace TigerMail::SMTP::ev

#endif // TIGERMAIL_SMTP_EVENTS_HPP
