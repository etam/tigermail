/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SMTP_IMPL_INTERFACE_HPP
#define TIGERMAIL_SMTP_IMPL_INTERFACE_HPP

#include <string>
#include <string_view>
#include <memory>

#include <gsl/pointers>


namespace TigerMail::SMTP {


class ImplInterface
{
public:
    virtual ~ImplInterface() noexcept = default;

    virtual bool is_self(std::string_view reverse_path) const = 0;
    virtual void clear_buffers() = 0;
    virtual bool is_recipient_valid(std::string_view recipient) const = 0;
    virtual bool has_recipients() const = 0;
    virtual bool has_max_recipients() const = 0;
    virtual void send_response(int code, std::string_view msg) = 0;
    virtual void store_recipient(std::string_view recipient) = 0;
    virtual void store_data_contents(gsl::not_null<std::shared_ptr<const std::string>> data) = 0;
    virtual void finish_message() = 0;
    virtual void disconnect() = 0;
};


} // namespace TigerMail::SMTP

#endif // TIGERMAIL_SMTP_IMPL_INTERFACE_HPP
