/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "format_response.hpp"

#include <ostream>
#include <string_view>

#include <gsl/gsl_assert>


namespace TigerMail::SMTP {


std::ostream& operator<<(std::ostream& o, const format_response& res)
{
    Expects(res.code >= 100
            && res.code < 1000
            && res.msg.find("\r\n") == std::string_view::npos);

    o << res.code;
    if (!res.msg.empty()) {
        o << ' ' << res.msg;
    }
    return o << "\r\n";
}


} // namespace TigerMail::SMTP
