/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "parser.hpp"

#include <boost/fusion/include/adapt_struct.hpp>

#include <boost/spirit/home/x3.hpp>

#include "../pop3_smtp_common/parser_x3.hpp"


BOOST_FUSION_ADAPT_STRUCT(
    TigerMail::SMTP::ev::MAIL,
    reverse_path, parameters
)

BOOST_FUSION_ADAPT_STRUCT(
    TigerMail::SMTP::ev::RCPT,
    forward_path, parameters
)


namespace TigerMail::SMTP {
namespace detail {

using POP3_SMTP_common::parse_type;


namespace {


namespace x3 = boost::spirit::x3;
namespace ascii = boost::spirit::x3::ascii;
using x3::eoi;
using x3::no_skip;
using x3::no_case;
using ascii::char_;

const x3::rule<class MAIL_ID, ev::MAIL> MAIL;
const x3::rule<class RCPT_ID, ev::RCPT> RCPT;

// the standard allows much more complex formats
// but it's good enough
const auto MAIL_def =
    no_skip[no_case[" FROM:<"] >> +(char_ - '>') >> '>' >> *(' ' >> +(char_ - ' ')) >> eoi];
const auto RCPT_def =
    no_skip[no_case[" TO:<"] >> +(char_ - '>') >> '>' >> *(' ' >> +(char_ - ' ')) >> eoi];

BOOST_SPIRIT_DEFINE(MAIL)
BOOST_SPIRIT_DEFINE(RCPT)


} // namespace


ev::MAIL parse_MAIL(std::string_view args)
{
    return parse_type<ev::MAIL>(MAIL, args);
}

ev::RCPT parse_RCPT(std::string_view args)
{
    return parse_type<ev::RCPT>(RCPT, args);
}

ev::VRFY parse_VRFY(std::string_view args)
{
    // for now we don't verify anything at all.
    if (args.size() < 2 || !starts_with(args, " ")) {
        throw SyntaxError{};
    }
    return {};
}


} // namespace detail


namespace {


class CommandProcessorClient
    : public POP3_SMTP_common::CommandProcessorClientInterface
{
private:
    StateMachine* m_sm;

public:
    explicit
    CommandProcessorClient(StateMachine* sm)
        : m_sm{sm}
    {}

    void command_too_long() override
    {
        m_sm->process_event(ev::command_unknown_or_too_long{});
    }

    void parse_command(std::string_view command) override
    {
        detail::parse_command(command, m_sm);
    }
};


} // namespace


Parser::Parser(ImplInterface& smtp, const Limits& limits)
    : m_logger{}
    , m_sm{smtp, static_cast<ParserControlInterface&>(*this), m_logger}
    , m_limits{limits}
    , m_input_buffer{}
    , m_data_buffer{}
    , m_parsing_commands{true}
    , m_ignoring_input_until_end{false}
{
    m_sm.process_event(ev::connected{});
}

void Parser::parse_commands()
{
    m_parsing_commands = true;
}

void Parser::parse_data()
{
    m_parsing_commands = false;
}

void Parser::process_input(std::string_view new_input)
{
    m_input_buffer.append(new_input);
    bool cont = true;
    while (cont) {
        if (m_parsing_commands) {
            auto command_processor_client = CommandProcessorClient{&m_sm};
            std::tie(m_input_buffer, cont) = POP3_SMTP_common::process_command(
                std::move(m_input_buffer),
                &command_processor_client,
                m_limits.max_command_len,
                &m_ignoring_input_until_end);
        }
        else {
            std::tie(m_input_buffer, cont) = detail::process_data(
                std::move(m_input_buffer), &m_sm, m_limits,
                &m_data_buffer, &m_ignoring_input_until_end);
        }
    }
}

void Parser::stop()
{
    m_sm.process_event(ev::server_closing{});
}


} // namespace TigerMail::SMTP
