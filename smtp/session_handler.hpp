/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SMTP_SESSION_HANDLER_HPP
#define TIGERMAIL_SMTP_SESSION_HANDLER_HPP

#include <functional>
#include <memory>
#include <optional>
#include <sstream>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

#include <gsl/pointers>

#include "../pop3_smtp_common/server/session_handler_interface.hpp"

#include "impl_interface.hpp"
#include "limits.hpp"
#include "parser.hpp"


namespace TigerMail::SMTP {

using POP3_SMTP_common::server::SessionHandlerInterface;


struct Message
{
    std::vector<std::string> recipients;
    gsl::not_null<std::shared_ptr<const std::string>> message;
};


using MessageReceivedHandlerT = std::function<void(Message&&)>;


class Impl
    : public ImplInterface
{
private:
    std::function<bool(std::string_view)> m_is_self;
    MessageReceivedHandlerT m_message_received;
    const Limits& m_limits;
    std::vector<std::string> m_recipients_buffer;
    std::shared_ptr<const std::string> m_message_buffer;
    std::ostream& m_output;
    bool& m_should_disconnect;

public:
    Impl(
        std::function<bool(std::string_view)>&& is_self,
        MessageReceivedHandlerT m_message_received,
        const Limits& limits,
        std::ostream& output,
        bool& should_disconnect);

    bool is_self(std::string_view reverse_path) const override;
    void clear_buffers() override;
    bool is_recipient_valid(std::string_view recipient) const override;
    bool has_recipients() const override;
    bool has_max_recipients() const override;
    void send_response(int code, std::string_view msg) override;
    void store_recipient(std::string_view recipient) override;
    void store_data_contents(gsl::not_null<std::shared_ptr<const std::string>> data) override;
    void finish_message() override;
    void disconnect() override;
};


class SessionHandler
    : public SessionHandlerInterface
{
private:
    const SMTP::Limits m_limits;
    std::ostringstream m_output_buffer;
    bool m_should_disconnect;
    Impl m_smtp_impl;
    Parser m_parser;

public:
    SessionHandler(
        std::function<bool(std::string_view)>&& is_self,
        MessageReceivedHandlerT message_received,
        const Limits& limits);

    std::string handle_hello() override;
    std::tuple<std::string, bool> handle_input(std::string_view input) override;
    std::string handle_stop() override;
};


} // namespace TigerMail::SMTP

#endif // TIGERMAIL_POP3_SMTP_SESSION_HANDLER_HPP
