/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SMTP_LIMITS_HPP
#define TIGERMAIL_SMTP_LIMITS_HPP

#include <cstddef>


namespace TigerMail::SMTP {


struct Limits
{
    // https://tools.ietf.org/html/rfc5321#section-4.5.3.1
    std::size_t max_command_len = 512;
    std::size_t max_data_len = 1024 * 1024; // 1MB
    std::size_t max_recipients = 100;
};


} // namespace TigerMail::SMTP

#endif // TIGERMAIL_SMTP_LIMITS_HPP
