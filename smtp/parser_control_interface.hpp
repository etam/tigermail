/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SMTP_PARSER_CONTROL_INTERFACE_HPP
#define TIGERMAIL_SMTP_PARSER_CONTROL_INTERFACE_HPP


namespace TigerMail::SMTP {


class ParserControlInterface
{
public:
    virtual ~ParserControlInterface() noexcept = default;

    virtual void parse_commands() = 0;
    virtual void parse_data() = 0;
};


} // namespace TigerMail::SMTP

#endif // TIGERMAIL_SMTP_PARSER_CONTROL_INTERFACE_HPP
