/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <string_view>
#include <tuple>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <range/v3/core.hpp>
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/indirect.hpp>
#include <range/v3/view/transform.hpp>

#include "../../common/test/out/std_tuple.hpp"

#include "../session_handler.hpp"

#include "out/session_handler.hpp"


namespace TigerMail::SMTP {


static
bool operator==(const Message& m1,
                const Message& m2)
{
    return m1.recipients == m2.recipients
        && *m1.message == *m2.message;
}

static
bool operator!=(const Message& m1,
                const Message& m2)
{
    return !(m1 == m2);
}


} // namespace TigerMail::SMTP


// Based on integration.cpp
BOOST_AUTO_TEST_CASE(smtp_session_handler)
{
    using TigerMail::SMTP::SessionHandler;
    using TigerMail::SMTP::Message;

    auto messages_received = std::vector<Message>{};

    auto session_handler = SessionHandler{
        [](std::string_view reverse_path) {
            return reverse_path == "me@example.com";
        },
        [&](Message&& message) {
            messages_received.push_back(std::move(message));
        },
        {}};

    const auto input = std::vector<std::string_view>{
        "HELO server\r\n",
        "NOOP\r\n",
        "MAIL FROM:<me@example.com>\r\n",
        "RCPT TO:<someone@example.net>\r\n",
        "DATA\r\n",
        "From: me@example.com\r\n",
        "To: someone@example.net\r\n",
        "Subject: test\r\n",
        "\r\n",
        "Hi\r\n",
        ".\r\n",
        "QUIT\r\n"
    };

    const auto expected_hello_msg = std::string_view{"220 localhost\r\n"};
    const auto expected_output = std::vector<std::tuple<std::string_view, bool>>{
        {"250 localhost\r\n", false}, // HELO
        {"250\r\n", false}, // NOOP
        {"250\r\n", false}, // MAIL
        {"250\r\n", false}, // RCPT
        {"354\r\n", false}, // DATA
        {"", false},
        {"", false},
        {"", false},
        {"", false},
        {"", false},
        {"250\r\n", false}, // data_contents
        {"221 localhost\r\n", true}, // QUIT
    };

    const auto expected_messages_received = std::vector<Message>{
        {
            .recipients = {"someone@example.net"},
            .message = gsl::not_null{std::make_shared<const std::string>(
                    "From: me@example.com\r\n"
                    "To: someone@example.net\r\n"
                    "Subject: test\r\n"
                    "\r\n"
                    "Hi"
            )},
        },
    };

    const auto hello_msg = session_handler.handle_hello();
    const auto output =
        input
        | ranges::views::transform(
            [&](const auto& input_line) {
                return session_handler.handle_input(input_line);
            })
        | ranges::to_vector;
    const auto stop_msg = session_handler.handle_stop();

    BOOST_CHECK_EQUAL(hello_msg, expected_hello_msg);
    BOOST_CHECK_EQUAL_COLLECTIONS(
        output.begin(), output.end(),
        expected_output.begin(), expected_output.end());
    BOOST_CHECK_EQUAL(stop_msg, "");

    BOOST_CHECK_EQUAL_COLLECTIONS(
        messages_received.begin(), messages_received.end(),
        expected_messages_received.begin(), expected_messages_received.end());
}
