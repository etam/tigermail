/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>

#include <boost/test/unit_test.hpp>

#include "../../parser.hpp"

using TigerMail::SMTP::detail::parse_RCPT;
using TigerMail::SMTP::detail::SyntaxError;


BOOST_AUTO_TEST_SUITE(test_parse_RCPT)

BOOST_AUTO_TEST_CASE(correct_no_parameters)
{
    const auto rcpt = parse_RCPT(" TO:<me@example.com>");
    BOOST_CHECK_EQUAL(rcpt.forward_path, "me@example.com");
    BOOST_CHECK_EQUAL(rcpt.parameters.size(), 0);
}

BOOST_AUTO_TEST_CASE(correct_with_parameters)
{
    const auto rcpt = parse_RCPT(" TO:<me@example.com> a bb");
    BOOST_CHECK_EQUAL(rcpt.forward_path, "me@example.com");
    const auto expected_params = std::vector<std::string>{"a", "bb"};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        rcpt.parameters.begin(), rcpt.parameters.end(),
        expected_params.begin(), expected_params.end());
}

BOOST_AUTO_TEST_CASE(correct_lowercase)
{
    const auto rcpt = parse_RCPT(" to:<me@example.com>");
    BOOST_CHECK_EQUAL(rcpt.forward_path, "me@example.com");
    BOOST_CHECK_EQUAL(rcpt.parameters.size(), 0);
}

BOOST_AUTO_TEST_CASE(rubbish)
{
    BOOST_CHECK_THROW(parse_RCPT("aoeuidhtns"), SyntaxError);
}

BOOST_AUTO_TEST_CASE(trailing_space_without_params)
{
    BOOST_CHECK_THROW(parse_RCPT(" FROM:<me@example.com> "), SyntaxError);
}

BOOST_AUTO_TEST_CASE(trailing_space_after_params)
{
    BOOST_CHECK_THROW(parse_RCPT(" FROM:<me@example.com> foo bar "), SyntaxError);
}

BOOST_AUTO_TEST_SUITE_END()
