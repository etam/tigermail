/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <any>
#include <string>
#include <typeinfo>

#include <boost/test/unit_test.hpp>

#include "../../parser.hpp"

#include "state_machine.hpp"

namespace ev = TigerMail::SMTP::ev;

using TigerMail::SMTP::Limits;
using TigerMail::SMTP::detail::process_data;
using TigerMail::SMTP::test::parser::StateMachine;


namespace {


struct Fixture
{
    StateMachine sm;
    Limits limits;
    std::string buffer;
    bool ignoring_input_until_end = false;

    auto run_process_data(std::string&& input)
    {
        return process_data(std::move(input), &sm, limits, &buffer,
                            &ignoring_input_until_end);
    }
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(test_data_command, Fixture)

BOOST_AUTO_TEST_CASE(immediate_eod)
{
    const auto [unprocessed_input, cont] =
        run_process_data(".\r\n");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_contents));
    BOOST_CHECK_EQUAL(*std::any_cast<ev::data_contents>(sm.events()[0]).data, "");
}

BOOST_AUTO_TEST_CASE(immediate_eod_extra_input)
{
    const auto [unprocessed_input, cont] =
        run_process_data(".\r\nfoo bar");

    BOOST_CHECK_EQUAL(unprocessed_input, "foo bar");
    BOOST_CHECK(cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_contents));
    BOOST_CHECK_EQUAL(*std::any_cast<ev::data_contents>(sm.events()[0]).data, "");
}

BOOST_AUTO_TEST_CASE(split_immediate_eod)
{
    buffer = ".\r";

    const auto [unprocessed_input, cont] =
        run_process_data("\n");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_contents));
    BOOST_CHECK_EQUAL(*std::any_cast<ev::data_contents>(sm.events()[0]).data, "");
}

BOOST_AUTO_TEST_CASE(split_immediate_eod_extra_input)
{
    const auto [unprocessed_input, cont] =
        run_process_data(".\r\nfoo bar");

    BOOST_CHECK_EQUAL(unprocessed_input, "foo bar");
    BOOST_CHECK(cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_contents));
    BOOST_CHECK_EQUAL(*std::any_cast<ev::data_contents>(sm.events()[0]).data, "");
}

BOOST_AUTO_TEST_CASE(eod)
{
    const auto [unprocessed_input, cont] =
        run_process_data("foo\r\nbar\r\n.\r\n");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_contents));
    BOOST_CHECK_EQUAL(*std::any_cast<ev::data_contents>(sm.events()[0]).data,
        "foo\r\nbar");
}

BOOST_AUTO_TEST_CASE(eod_extra_input)
{
    const auto [unprocessed_input, cont] =
        run_process_data("foo\r\nbar\r\n.\r\nnext command");

    BOOST_CHECK_EQUAL(unprocessed_input, "next command");
    BOOST_CHECK(cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_contents));
    BOOST_CHECK_EQUAL(*std::any_cast<ev::data_contents>(sm.events()[0]).data,
        "foo\r\nbar");
}

BOOST_AUTO_TEST_CASE(something_in_buffer_eod)
{
    buffer = "foo";

    const auto [unprocessed_input, cont] =
        run_process_data("\r\nbar\r\n.\r\n");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_contents));
    BOOST_CHECK_EQUAL(*std::any_cast<ev::data_contents>(sm.events()[0]).data,
        "foo\r\nbar");
}

BOOST_AUTO_TEST_CASE(something_in_buffer_eod_extra_input)
{
    buffer = "foo";

    const auto [unprocessed_input, cont] =
        run_process_data("\r\nbar\r\n.\r\nnext command");

    BOOST_CHECK_EQUAL(unprocessed_input, "next command");
    BOOST_CHECK(cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_contents));
    BOOST_CHECK_EQUAL(*std::any_cast<ev::data_contents>(sm.events()[0]).data,
        "foo\r\nbar");
}

BOOST_AUTO_TEST_CASE(too_long_eod)
{
    limits.max_data_len = 10;
    
    const auto [unprocessed_input, cont] =
        run_process_data("0123456789abcdef\r\n.\r\n");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_too_big));
}

BOOST_AUTO_TEST_CASE(too_long_eod_extra_input)
{
    limits.max_data_len = 10;
    
    const auto [unprocessed_input, cont] =
        run_process_data("0123456789abcdef\r\n.\r\nnext command");

    BOOST_CHECK_EQUAL(unprocessed_input, "next command");
    BOOST_CHECK(cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_too_big));
}

BOOST_AUTO_TEST_CASE(something_in_buffer_too_long_eod)
{
    limits.max_data_len = 10;
    buffer = "012345";

    const auto [unprocessed_input, cont] =
        run_process_data("6789abcdef\r\n.\r\n");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_too_big));
}

BOOST_AUTO_TEST_CASE(something_in_buffer_too_long_eod_extra_input)
{
    limits.max_data_len = 10;
    buffer = "012345";

    const auto [unprocessed_input, cont] =
        run_process_data("6789abcdef\r\n.\r\nnext command");

    BOOST_CHECK_EQUAL(unprocessed_input, "next command");
    BOOST_CHECK(cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_too_big));
}

BOOST_AUTO_TEST_CASE(split_eod)
{
    buffer = "foo\r\nbar\r\n";

    const auto [unprocessed_input, cont] =
        run_process_data(".\r\n");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_contents));
    BOOST_CHECK_EQUAL(*std::any_cast<ev::data_contents>(sm.events()[0]).data,
        "foo\r\nbar");
}

BOOST_AUTO_TEST_CASE(split_eod_extra_input)
{
    buffer = "foo\r\nbar\r\n";

    const auto [unprocessed_input, cont] =
        run_process_data(".\r\nnext command");

    BOOST_CHECK_EQUAL(unprocessed_input, "next command");
    BOOST_CHECK(cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_contents));
    BOOST_CHECK_EQUAL(*std::any_cast<ev::data_contents>(sm.events()[0]).data,
        "foo\r\nbar");
}

BOOST_AUTO_TEST_CASE(no_eod)
{
    const auto [unprocessed_input, cont] =
        run_process_data("foo\r\nbar\r\n");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "foo\r\nbar\r\n");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK_EQUAL(sm.events().size(), 0);
}

BOOST_AUTO_TEST_CASE(something_in_buffer_no_eod)
{
    buffer = "foo\r";

    const auto [unprocessed_input, cont] =
        run_process_data("\nbar\r\n");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "foo\r\nbar\r\n");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK_EQUAL(sm.events().size(), 0);
}

BOOST_AUTO_TEST_CASE(too_long_no_eod)
{
    limits.max_data_len = 10;

    const auto [unprocessed_input, cont] =
        run_process_data("0123456789abcdef");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "cdef");
    BOOST_CHECK(ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_too_big));
}

BOOST_AUTO_TEST_CASE(something_in_buffer_too_long_no_eod)
{
    limits.max_data_len = 10;
    buffer = "01234";

    const auto [unprocessed_input, cont] =
        run_process_data("56789abcdef");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "cdef");
    BOOST_CHECK(ignoring_input_until_end);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_REQUIRE(sm.events()[0].type() == typeid(ev::data_too_big));
}

BOOST_AUTO_TEST_CASE(ignored_eod)
{
    ignoring_input_until_end = true;
    buffer = "foo\r";

    const auto [unprocessed_input, cont] =
        run_process_data("\nbar\r\n.\r\n");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK_EQUAL(sm.events().size(), 0);
}

BOOST_AUTO_TEST_CASE(ignored_eod_extra_input)
{
    ignoring_input_until_end = true;
    buffer = "foo\r";

    const auto [unprocessed_input, cont] =
        run_process_data("\nbar\r\n.\r\nnext command");

    BOOST_CHECK_EQUAL(unprocessed_input, "next command");
    BOOST_CHECK(cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK_EQUAL(sm.events().size(), 0);
}

BOOST_AUTO_TEST_CASE(ignored_split_eod)
{
    ignoring_input_until_end = true;
    buffer = "fo\r\n";

    const auto [unprocessed_input, cont] =
        run_process_data(".\r\n");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK_EQUAL(sm.events().size(), 0);
}

BOOST_AUTO_TEST_CASE(ignored_split_eod_extra_input)
{
    ignoring_input_until_end = true;
    buffer = "fo\r\n";

    const auto [unprocessed_input, cont] =
        run_process_data(".\r\nnext command");

    BOOST_CHECK_EQUAL(unprocessed_input, "next command");
    BOOST_CHECK(cont);
    BOOST_CHECK_EQUAL(buffer, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK_EQUAL(sm.events().size(), 0);
}

BOOST_AUTO_TEST_CASE(ignored_no_eod)
{
    ignoring_input_until_end = true;
    buffer = "0123";

    const auto [unprocessed_input, cont] =
        run_process_data("456789abcdef");

    BOOST_CHECK_EQUAL(unprocessed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK_EQUAL(buffer, "cdef");
    BOOST_CHECK(ignoring_input_until_end);
    BOOST_CHECK_EQUAL(sm.events().size(), 0);
}

BOOST_AUTO_TEST_SUITE_END()
