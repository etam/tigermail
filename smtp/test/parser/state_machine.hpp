/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SMTP_TEST_PARSER_STATE_MACHINE_HPP
#define TIGERMAIL_SMTP_TEST_PARSER_STATE_MACHINE_HPP

#include <any>
#include <vector>


namespace TigerMail::SMTP::test::parser {


class StateMachine
{
private:
    std::vector<std::any> m_events;

public:
    StateMachine()
        : m_events{}
    {}

    template <typename Event>
    void process_event(Event event)
    {
        m_events.push_back(event);
    }

    auto& events() const
    {
        return m_events;
    }
};


} // namespace TigerMail::SMTP::test::parser

#endif // TIGERMAIL_SMTP_TEST_PARSER_STATE_MACHINE_HPP
