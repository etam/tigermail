/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <typeinfo>

#include <boost/test/unit_test.hpp>

#include "../../parser.hpp"

#include "state_machine.hpp"

namespace ev = TigerMail::SMTP::ev;

using TigerMail::SMTP::detail::parse_command;
using TigerMail::SMTP::test::parser::StateMachine;


namespace {


struct Fixture
{
    StateMachine sm;
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(test_parse_command, Fixture)

BOOST_AUTO_TEST_CASE(too_short)
{
    parse_command("hi", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::syntax_error));
}

BOOST_AUTO_TEST_CASE(HELO)
{
    parse_command("HELO foobar", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::HELO));
}

BOOST_AUTO_TEST_CASE(EHLO)
{
    parse_command("EHLO foobar", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::EHLO));
}

// more detailed testing is in parse_MAIL.cpp
BOOST_AUTO_TEST_CASE(MAIL)
{
    parse_command("MAIL FROM:<me@example.com>", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::MAIL));
}

// more detailed testing is in parse_RCPT.cpp
BOOST_AUTO_TEST_CASE(RCPT)
{
    parse_command("RCPT TO:<someone@example.net>", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::RCPT));
}

BOOST_AUTO_TEST_CASE(DATA)
{
    parse_command("DATA", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::DATA));
}

BOOST_AUTO_TEST_CASE(RSET)
{
    parse_command("RSET", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::RSET));
}

BOOST_AUTO_TEST_CASE(QUIT)
{
    parse_command("QUIT", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::QUIT));
}

BOOST_AUTO_TEST_CASE(NOOP)
{
    parse_command("NOOP", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::NOOP));
}

// more detailed testing is in parse_VRFY.cpp
BOOST_AUTO_TEST_CASE(VRFY)
{
    parse_command("VRFY someone", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::VRFY));
}

BOOST_AUTO_TEST_CASE(unknown)
{
    parse_command("aoeuidhtns", &sm);
    BOOST_REQUIRE_EQUAL(sm.events().size(), 1);
    BOOST_CHECK(sm.events()[0].type() == typeid(ev::command_unknown_or_too_long));
}

BOOST_AUTO_TEST_SUITE_END()
