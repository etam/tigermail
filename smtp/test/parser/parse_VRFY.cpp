/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../../parser.hpp"

using TigerMail::SMTP::detail::parse_VRFY;
using TigerMail::SMTP::detail::SyntaxError;


BOOST_AUTO_TEST_SUITE(test_parse_VRFY)

BOOST_AUTO_TEST_CASE(no_args)
{
    BOOST_CHECK_THROW(parse_VRFY(""), SyntaxError);
}

BOOST_AUTO_TEST_CASE(space)
{
    BOOST_CHECK_THROW(parse_VRFY(" "), SyntaxError);
}

BOOST_AUTO_TEST_CASE(something)
{
    BOOST_CHECK_NO_THROW(parse_VRFY(" a"));
}

BOOST_AUTO_TEST_SUITE_END()
