/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>
#include <boost/test/tools/output_test_stream.hpp>

#include <gsl/gsl_assert>

#include "../format_response.hpp"

using boost::test_tools::output_test_stream;
using TigerMail::SMTP::format_response;


BOOST_AUTO_TEST_SUITE(test_format_response)

BOOST_AUTO_TEST_CASE(just_code)
{
    auto output = output_test_stream{};
    output << format_response{123};
    BOOST_CHECK(output.is_equal("123\r\n"));
}

BOOST_AUTO_TEST_CASE(code_and_msg)
{
    auto output = output_test_stream{};
    output << format_response{123, "foo bar"};
    BOOST_CHECK(output.is_equal("123 foo bar\r\n"));
}

BOOST_AUTO_TEST_CASE(code_out_of_range)
{
    auto output = output_test_stream{};
    const auto format_obj = format_response{0};
    BOOST_CHECK_THROW(output << format_obj, gsl::fail_fast);
}

BOOST_AUTO_TEST_CASE(msg_has_newline)
{
    auto output = output_test_stream{};
    const auto format_obj = format_response{100, "foo\r\nbar"};
    BOOST_CHECK_THROW(output << format_obj, gsl::fail_fast);
}

BOOST_AUTO_TEST_SUITE_END()
