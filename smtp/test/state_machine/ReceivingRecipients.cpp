/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>

#include <boost/sml.hpp>

#include <boost/test/unit_test.hpp>

#include "../../state_machine.hpp"

#include "common.hpp"

namespace sml = boost::sml;
namespace ev = TigerMail::SMTP::ev;
namespace st = TigerMail::SMTP::st;

using TigerMail::SMTP::test::StateMachine::BaseFixture;


namespace {


struct ReceivingRecipientsFixture
    : BaseFixture
{
    ReceivingRecipientsFixture()
        : BaseFixture{}
    {
        sm.set_current_states(st::Session);
        sm.set_current_states<decltype(st::Session)>(
            st::ReceivingRecipients, st::AnyState);
    }
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(ReceivingRecipients, ReceivingRecipientsFixture)

//BOOST_AUTO_TEST_CASE(connected)

BOOST_AUTO_TEST_CASE(HELO)
{
    sm.process_event(ev::HELO{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 250);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(EHLO)
{
    sm.process_event(ev::EHLO{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 250);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(MAIL)
{
    sm.process_event(ev::MAIL{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingRecipients));
    BOOST_CHECK_EQUAL(smtp.last_response(), 503);
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(RCPT_valid_recipient)
{
    const auto recipient = std::string{"foo@bar"};

    sm.process_event(ev::RCPT{recipient, {}});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingRecipients));
    BOOST_CHECK_EQUAL(smtp.last_response(), 250);
    BOOST_CHECK(smtp.message().recipients == std::vector<std::string>{recipient});
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(RCPT_invalid_recipient)
{
    sm.process_event(ev::RCPT{"foo", {}});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingRecipients));
    BOOST_CHECK_EQUAL(smtp.last_response(), 550);
    BOOST_CHECK(smtp.message().recipients.empty());
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(RCPT_has_parameters)
{
    sm.process_event(ev::RCPT{"foo@bar", {"a", "bb"}});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingRecipients));
    BOOST_CHECK_EQUAL(smtp.last_response(), 555);
    BOOST_CHECK(smtp.message().recipients.empty());
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(RCPT_too_many_recipients)
{
    // given
    const auto recipient = std::string{"foo@bar"};
    for (std::size_t i = 0; i < max_recipients; ++i) {
        sm.process_event(ev::RCPT{recipient + std::to_string(i), {}});
        BOOST_REQUIRE_EQUAL(smtp.last_response(), 250);
    }
    BOOST_REQUIRE_EQUAL(smtp.message().recipients.size(), max_recipients);
    const auto last_recipient = std::string{"last@recipient"};

    // when
    sm.process_event(ev::RCPT{last_recipient, {}});

    // then
    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingRecipients));
    BOOST_CHECK_EQUAL(smtp.last_response(), 452);
    BOOST_CHECK_NE(smtp.message().recipients.back(), last_recipient);
    BOOST_CHECK_EQUAL(smtp.message().recipients.size(), max_recipients);
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(DATA_no_recipients)
{
    sm.process_event(ev::DATA{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingRecipients));
    BOOST_CHECK_EQUAL(smtp.last_response(), 554);
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(DATA_with_recipients)
{
    // given
    const auto recipient = std::string{"foo@bar"};
    sm.process_event(ev::RCPT{recipient, {}});
    BOOST_REQUIRE(smtp.has_recipients());

    // when
    sm.process_event(ev::DATA{});

    // then
    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingData));
    BOOST_CHECK_EQUAL(smtp.last_response(), 354);
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(!parser.parsing_commands());
}

// BOOST_AUTO_TEST_CASE(data_contents)
// BOOST_AUTO_TEST_CASE(data_too_big)

BOOST_AUTO_TEST_CASE(RSET)
{
    sm.process_event(ev::RSET{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 250);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(QUIT)
{
    sm.process_event(ev::QUIT{});

    BOOST_CHECK(sm.is(sml::X));
    BOOST_CHECK_EQUAL(smtp.last_response(), 221);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(smtp.disconnected());
}

BOOST_AUTO_TEST_CASE(server_closing)
{
    sm.process_event(ev::server_closing{});

    BOOST_CHECK(sm.is(sml::X));
    BOOST_CHECK_EQUAL(smtp.last_response(), 421);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(smtp.disconnected());
}

BOOST_AUTO_TEST_CASE(NOOP)
{
    sm.process_event(ev::NOOP{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingRecipients));
    BOOST_CHECK_EQUAL(smtp.last_response(), 250);
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(VRFY)
{
    sm.process_event(ev::VRFY{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingRecipients));
    BOOST_CHECK_EQUAL(smtp.last_response(), 252);
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(command_unknown_or_too_long)
{
    sm.process_event(ev::command_unknown_or_too_long{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingRecipients));
    BOOST_CHECK_EQUAL(smtp.last_response(), 500);
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(syntax_error)
{
    sm.process_event(ev::syntax_error{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingRecipients));
    BOOST_CHECK_EQUAL(smtp.last_response(), 501);
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_SUITE_END()
