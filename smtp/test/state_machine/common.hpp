/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SMTP_TEST_STATE_MACHINE_COMMON_HPP
#define TIGERMAIL_SMTP_TEST_STATE_MACHINE_COMMON_HPP

#include <memory>
#include <string>
#include <string_view>
#include <vector>

#include <boost/sml.hpp>

#include "../../impl_interface.hpp"
#include "../../parser_control_interface.hpp"
#include "../../state_machine.hpp"


namespace TigerMail::SMTP::test::StateMachine {

namespace sml = boost::sml;


struct Message
{
    std::vector<std::string> recipients;
    std::shared_ptr<const std::string> data;

    void clear();
};


class Impl
    : public ImplInterface
{
private:
    std::string m_self_address;
    int m_last_response;
    std::size_t m_max_recipients;
    Message m_message;
    bool m_clear_buffers_called;
    bool m_finish_message_called;
    bool m_disconnected;

public:
    Impl(std::string&& self_address, std::size_t max_recipients);
    ~Impl() override = default;

    bool is_self(std::string_view reverse_path) const override;
    void clear_buffers() override;
    bool is_recipient_valid(std::string_view recipient) const override;
    bool has_recipients() const override;
    bool has_max_recipients() const override;
    void send_response(int code, std::string_view msg) override;
    void store_recipient(std::string_view recipient) override;
    void store_data_contents(gsl::not_null<std::shared_ptr<const std::string>> data) override;
    void finish_message() override;
    void disconnect() override;

    int last_response() const;
    const Message& message() const;
    bool clear_buffers_called() const;
    bool finished_message_called() const;
    bool disconnected() const;
};

class Parser
    : public ParserControlInterface
{
private:
    bool m_parsing_commands;

public:
    Parser();
    ~Parser() override = default;

    void parse_commands() override;
    void parse_data() override;

    bool parsing_commands() const;
};


extern const std::string self_address;
extern const std::string not_self_address;


struct BaseFixture
{
    std::size_t max_recipients;
    Impl smtp;
    Parser parser;
    sml::sm<TransitionTable, sml::testing> sm;

    BaseFixture();
};


} // TigerMail::SMTP::test::StateMachine

#endif // TIGERMAIL_SMTP_TEST_STATE_MACHINE_COMMON_HPP
