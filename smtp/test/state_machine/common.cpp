/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "common.hpp"

#include "../../../common/utils.hpp"


namespace TigerMail::SMTP::test::StateMachine {


void Message::clear()
{
    recipients.clear();
    data.reset();
}


Impl::Impl(std::string&& self_address, std::size_t max_recipients)
    : m_self_address{std::move(self_address)}
    , m_last_response{0}
    , m_max_recipients{max_recipients}
    , m_message{}
    , m_clear_buffers_called{false}
    , m_finish_message_called{false}
    , m_disconnected{false}
{}

bool Impl::is_self(std::string_view reverse_path) const
{
    return reverse_path == m_self_address;
}

void Impl::clear_buffers()
{
    m_message.clear();
    m_clear_buffers_called = true;
}

bool Impl::is_recipient_valid(const std::string_view recipient) const
{
    return recipient.find('@') != std::string_view::npos;
}

bool Impl::has_recipients() const
{
    return !m_message.recipients.empty();
}

bool Impl::has_max_recipients() const
{
    return m_message.recipients.size() == m_max_recipients;
}

void Impl::store_recipient(std::string_view recipient)
{
    m_message.recipients.emplace_back(recipient);
}

void Impl::send_response(int code, std::string_view)
{
    m_last_response = code;
}

void Impl::store_data_contents(gsl::not_null<std::shared_ptr<const std::string>> data_contents)
{
    m_message.data = data_contents;
}

void Impl::finish_message()
{
    m_finish_message_called = true;
}

void Impl::disconnect()
{
    m_disconnected = true;
}


int Impl::last_response() const
{
    return m_last_response;
}

const Message& Impl::message() const
{
    return m_message;
}

bool Impl::clear_buffers_called() const
{
    return m_clear_buffers_called;
}

bool Impl::finished_message_called() const
{
    return m_finish_message_called;
}

bool Impl::disconnected() const
{
    return m_disconnected;
}


Parser::Parser()
    : m_parsing_commands{true}
{}

void Parser::parse_commands()
{
    m_parsing_commands = true;
}

void Parser::parse_data()
{
    m_parsing_commands = false;
}

bool Parser::parsing_commands() const
{
    return m_parsing_commands;
}


const std::string self_address{"this_is_a_self@address"};
const std::string not_self_address{"this_is_not_a_self@address"};


BaseFixture::BaseFixture()
    : max_recipients{5}
    , smtp{copy(self_address), max_recipients}
    , parser{}
    , sm{static_cast<ImplInterface&>(smtp),
         static_cast<ParserControlInterface&>(parser)}
{}


} // namespace TigerMail::SMTP::test::StateMachine
