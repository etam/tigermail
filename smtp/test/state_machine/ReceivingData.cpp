/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <memory>
#include <string>

#include <boost/sml.hpp>

#include <boost/test/unit_test.hpp>

#include <gsl/pointers>

#include "../../state_machine.hpp"

#include "common.hpp"

namespace sml = boost::sml;
namespace ev = TigerMail::SMTP::ev;
namespace st = TigerMail::SMTP::st;

using TigerMail::SMTP::test::StateMachine::BaseFixture;


namespace {


struct ReceivingDataFixture
    : BaseFixture
{
    ReceivingDataFixture()
        : BaseFixture{}
    {
        sm.set_current_states(st::Session);
        sm.set_current_states<decltype(st::Session)>(
            st::ReceivingData, st::AnyState);

        parser.parse_data();
    }
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(ReceivingData, ReceivingDataFixture)

// connected
// HELO
// EHLO
// MAIL
// RCPT
// DATA

BOOST_AUTO_TEST_CASE(data_contents)
{
    const auto data = std::make_shared<const std::string>("some line of text");

    sm.process_event(ev::data_contents{
            .data = gsl::not_null{data},
        });

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 250);
    BOOST_CHECK_EQUAL(*smtp.message().data, *data);
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(data_too_big)
{
    sm.process_event(ev::data_too_big{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 552);
    BOOST_CHECK(!smtp.message().data);
    BOOST_CHECK(!smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

// RSET
// QUIT

BOOST_AUTO_TEST_CASE(server_closing)
{
    sm.process_event(ev::server_closing{});

    BOOST_CHECK(sm.is(sml::X));
    BOOST_CHECK_EQUAL(smtp.last_response(), 421);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

// NOOP
// VRFY
// command_unknown_or_too_long
// syntax_error

BOOST_AUTO_TEST_SUITE_END()
