/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/sml.hpp>

#include <boost/test/unit_test.hpp>

#include "../../state_machine.hpp"

#include "common.hpp"

namespace sml = boost::sml;
namespace ev = TigerMail::SMTP::ev;
namespace st = TigerMail::SMTP::st;

using TigerMail::SMTP::test::StateMachine::BaseFixture;
using TigerMail::SMTP::test::StateMachine::self_address;
using TigerMail::SMTP::test::StateMachine::not_self_address;


namespace {


struct IdleFixture
    : BaseFixture
{
    IdleFixture()
        : BaseFixture{}
    {
        sm.set_current_states(st::Session);
        sm.set_current_states<decltype(st::Session)>(
            st::Idle, st::AnyState);
    }
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(Idle, IdleFixture)

//BOOST_AUTO_TEST_CASE(connected)

BOOST_AUTO_TEST_CASE(HELO)
{
    sm.process_event(ev::HELO{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 250);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(EHLO)
{
    sm.process_event(ev::EHLO{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 250);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(MAIL_not_self)
{
    sm.process_event(ev::MAIL{not_self_address, {}});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 550);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(MAIL_any_mail_parameters)
{
    sm.process_event(ev::MAIL{self_address, {"some parameter"}});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 555);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(MAIL_is_self)
{
    sm.process_event(ev::MAIL{self_address, {}});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::ReceivingRecipients));
    BOOST_CHECK_EQUAL(smtp.last_response(), 250);
    BOOST_CHECK(smtp.clear_buffers_called());
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(RCPT)
{
    sm.process_event(ev::RCPT{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 503);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(DATA)
{
    sm.process_event(ev::DATA{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 503);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

// BOOST_AUTO_TEST_CASE(data_contents)
// BOOST_AUTO_TEST_CASE(data_too_big)

BOOST_AUTO_TEST_CASE(RSET)
{
    sm.process_event(ev::RSET{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 250);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(QUIT)
{
    sm.process_event(ev::QUIT{});

    BOOST_CHECK(sm.is(sml::X));
    BOOST_CHECK_EQUAL(smtp.last_response(), 221);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(smtp.disconnected());
}

BOOST_AUTO_TEST_CASE(server_closing)
{
    sm.process_event(ev::server_closing{});

    BOOST_CHECK(sm.is(sml::X));
    BOOST_CHECK_EQUAL(smtp.last_response(), 421);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(smtp.disconnected());
}

BOOST_AUTO_TEST_CASE(NOOP)
{
    sm.process_event(ev::NOOP{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 250);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(VRFY)
{
    sm.process_event(ev::VRFY{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 252);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(command_unknown_or_too_long)
{
    sm.process_event(ev::command_unknown_or_too_long{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 500);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_CASE(syntax_error)
{
    sm.process_event(ev::syntax_error{});

    BOOST_CHECK(sm.is(st::Session));
    BOOST_CHECK(sm.is<decltype(st::Session)>(st::Idle));
    BOOST_CHECK_EQUAL(smtp.last_response(), 501);
    BOOST_CHECK(!smtp.finished_message_called());
    BOOST_CHECK(!smtp.disconnected());
    BOOST_CHECK(parser.parsing_commands());
}

BOOST_AUTO_TEST_SUITE_END()
