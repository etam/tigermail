/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#include <boost/test/unit_test.hpp>

#include "../limits.hpp"
#include "../parser.hpp"
#include "../session_handler.hpp"
#include "../state_machine.hpp"

#include "out/session_handler.hpp"

using TigerMail::SMTP::Impl;
using TigerMail::SMTP::Limits;
using TigerMail::SMTP::Message;
using TigerMail::SMTP::Parser;


namespace TigerMail::SMTP {


static
bool operator==(const Message& m1, const Message& m2)
{
    return m1.recipients == m2.recipients
        && *m1.message == *m2.message;
}

static
bool operator!=(const Message& m1, const Message& m2)
{
    return !(m1 == m2);
}


} // namespace TigerMail::SMTP


namespace {


class TestImpl
    : public Impl
{
private:
    std::ostringstream m_output;
    std::vector<Message> m_sent_messages;
    bool m_disconnected;

public:
    explicit
    TestImpl(const Limits& limits)
        : Impl{
            [](std::string_view reverse_path) {
                return reverse_path == "me@example.com";
            },
            [this](Message&& msg) {
                m_sent_messages.push_back(std::move(msg));
            },
            limits,
            m_output,
            m_disconnected
        }
        , m_output{}
        , m_sent_messages{}
        , m_disconnected{false}
    {}

    void send_response(int code, std::string_view msg) override
    {
        if (m_disconnected) {
            return;
        }
        Impl::send_response(code, msg);
    }

    std::string output() const
    {
        return m_output.str();
    }

    auto& sent_messages() const
    {
        return m_sent_messages;
    }

    bool disconnected() const
    {
        return m_disconnected;
    }
};

} // namespace


BOOST_AUTO_TEST_CASE(integration)
{
    const auto limits = Limits{};
    auto smtp = TestImpl{limits};
    auto parser = Parser{smtp, limits};

    const auto input = std::vector<std::string_view>{
        "HELO server\r\n",
        "NOOP\r\n",
        "MAIL FROM:<me@example.com>\r\n",
        "RCPT TO:<someone@example.net>\r\n",
        "DATA\r\n",
        "Hi\r\n",
        ".\r\n",
        "QUIT\r\n"
    };

    constexpr const auto expected_output = std::string_view{
        "220 localhost\r\n" // connected
        "250 localhost\r\n" // HELO
        "250\r\n" // NOOP
        "250\r\n" // MAIL
        "250\r\n" // RCPT
        "354\r\n" // DATA
        "250\r\n" // data_contents
        "221 localhost\r\n" // QUIT
    };

    const auto expected_messages = std::vector<Message>{
        {
            {"someone@example.net"},
            gsl::not_null{std::make_shared<std::string>("Hi")},
        },
    };

    for (auto input_chunk : input) {
        parser.process_input(input_chunk);
    }

    BOOST_CHECK_EQUAL(smtp.output(), expected_output);

    BOOST_CHECK_EQUAL_COLLECTIONS(
        smtp.sent_messages().begin(),
        smtp.sent_messages().end(),
        expected_messages.begin(),
        expected_messages.end()
    );

    BOOST_CHECK(smtp.disconnected());
}
