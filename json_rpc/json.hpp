/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_JSON_RPC_JSON_HPP
#define TIGERMAIL_JSON_RPC_JSON_HPP

#include <functional>
#include <initializer_list>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>

#include <json/value.h>

#include "../common/buffer.hpp"
#include "../common/fixed_size_buffer.hpp"
#include "../common/hex.hpp"


namespace TigerMail::JsonRpc {


class JsonError
    : public std::runtime_error
{
    using BaseClass = std::runtime_error;
    using BaseClass::BaseClass;
};


Json::Value parse_json(std::string_view input);


Json::Value make_array(std::initializer_list<Json::Value> l);
Json::Value make_object(std::initializer_list<std::pair<std::string, Json::Value>> l);


template <typename Iterable>
Json::Value make_array(const Iterable& v)
{
    auto array = Json::Value{Json::arrayValue};
    for (const auto& i : v) {
        array.append(i);
    }
    return array;
}

template <typename Iterable, typename F>
Json::Value make_array(const Iterable& v, F convert)
{
    auto array = Json::Value{Json::arrayValue};
    for (const auto& i : v) {
        array.append(convert(i));
    }
    return array;
}


// name is passed only for error reporting
// and to optimize non-error execution, it's passed as function, that is called when needed
bool asBool(std::function<std::string()> name, const Json::Value& v);
int asInt(std::function<std::string()> name, const Json::Value& v);

template <typename Int = int>
Int asIntFromZxhex(std::function<std::string()> name, const Json::Value& v)
{
    if (!v.isString()) {
        throw JsonError{name() + " is not a string"};
    }
    try {
        return zxhex2int<Int>(v.asString());
    }
    catch (ConversionError&) {
        throw JsonError{name() + " conversion error"};
    }
}

std::string asString(std::function<std::string()> name, const Json::Value& v);

template <typename T = Buffer>
T asDataFromZxhex(std::function<std::string()> name, const Json::Value& v)
{
    if (!v.isString()) {
        throw JsonError{name() + " is not a string"};
    }
    try {
        return zxhex2bin<T>(v.asString());
    }
    catch (ConversionError&) {
        throw JsonError{name() + " conversion error"};
    }
}

template <typename T>
std::optional<T> asOptional(
    T(*inner_parser)(std::function<std::string()>, const Json::Value&),
    std::function<std::string()> name,
    const Json::Value& v)
{
    if (v.isNull()) {
        return std::nullopt;
    }
    return inner_parser(name, v);
}

template <typename T>
std::vector<T> asVector(
    T(*inner_parser)(std::function<std::string()>, const Json::Value&),
    std::function<std::string()> name,
    const Json::Value& v)
{
    if (!v.isArray()) {
        throw JsonError{name() + "s are not an array"};
    }
    // This could use ranges, but this is a header file and it might have
    // significant impact on compilation time.
    auto result = std::vector<T>{};
    result.reserve(v.size());
    for (const auto& i : v) {
        result.push_back(inner_parser(name, i));
    }
    return result;
}


} // namespace TigerMail::JsonRpc {

#endif // TIGERMAIL_JSON_RPC_JSON_HPP
