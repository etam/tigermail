/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <optional>
#include <string>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <json/value.h>
#include <json/writer.h>

#include "../json.hpp"

#include "../../common/test/out/std_optional.hpp"


BOOST_AUTO_TEST_SUITE(json)


BOOST_AUTO_TEST_SUITE(parse_json)

using TigerMail::JsonRpc::JsonError;
using TigerMail::JsonRpc::make_array;
using TigerMail::JsonRpc::make_object;
using TigerMail::JsonRpc::parse_json;


BOOST_AUTO_TEST_CASE(correct)
{
    const auto obj = parse_json(R"json({"a": "foo", "b": [1, 2, 3], "c": null})json");
    const auto expected_obj = make_object({
            {"a", "foo"},
            {"b", make_array({1, 2, 3})},
            {"c", Json::nullValue},
        });
    BOOST_CHECK_EQUAL(obj, expected_obj);
}

BOOST_AUTO_TEST_CASE(invalid)
{
    BOOST_CHECK_THROW(parse_json(R"json({"a": yo! what's up?)json"), JsonError);
}


BOOST_AUTO_TEST_SUITE_END() // parse_json



BOOST_AUTO_TEST_SUITE(make_array)

using TigerMail::JsonRpc::make_array;


BOOST_AUTO_TEST_CASE(initializer_list)
{
    const auto array = make_array({1, "foo"});
    const auto expected_array =
        [] {
            auto array = Json::Value{Json::arrayValue};
            array.append(1);
            array.append("foo");
            return array;
        }();
    BOOST_CHECK_EQUAL(array, expected_array);
}

BOOST_AUTO_TEST_CASE(iterable)
{
    const auto array = make_array(std::vector<int>{1, 3, 5});
    const auto expected_array =
        [] {
            auto array = Json::Value{Json::arrayValue};
            array.append(1);
            array.append(3);
            array.append(5);
            return array;
        }();
    BOOST_CHECK_EQUAL(array, expected_array);
}

BOOST_AUTO_TEST_CASE(convert)
{
    const auto array = make_array(
        std::vector<int>{1, 3, 5},
        [](int i) {
            return std::to_string(i+1);
        });
    const auto expected_array =
        [] {
            auto array = Json::Value{Json::arrayValue};
            array.append("2");
            array.append("4");
            array.append("6");
            return array;
        }();
    BOOST_CHECK_EQUAL(array, expected_array);
}


BOOST_AUTO_TEST_SUITE_END() // make_array


BOOST_AUTO_TEST_CASE(make_object)
{
    using TigerMail::JsonRpc::make_object;
    const auto object = make_object({
            {"a", 1},
            {"b", "foo"},
        });
    const auto expected_object =
        [] {
            auto object = Json::Value{Json::objectValue};
            object["a"] = 1;
            object["b"] = "foo";
            return object;
        }();
    BOOST_CHECK_EQUAL(object, expected_object);
}


static std::string name() { return ""; }


BOOST_AUTO_TEST_CASE(asBool)
{
    using TigerMail::JsonRpc::JsonError;
    using TigerMail::JsonRpc::asBool;
    BOOST_CHECK_THROW(asBool(name, 5), JsonError);
    BOOST_CHECK_EQUAL(asBool(name, true), true);
}

BOOST_AUTO_TEST_CASE(asInt)
{
    using TigerMail::JsonRpc::JsonError;
    using TigerMail::JsonRpc::asInt;
    BOOST_CHECK_THROW(asInt(name, "abc"), JsonError);
    BOOST_CHECK_EQUAL(asInt(name, 5), 5);
}

BOOST_AUTO_TEST_CASE(asIntFromZxhex)
{
    using TigerMail::JsonRpc::JsonError;
    using TigerMail::JsonRpc::asIntFromZxhex;
    BOOST_CHECK_THROW(asIntFromZxhex(name, ""), JsonError);
    BOOST_CHECK_THROW(asIntFromZxhex(name, 5), JsonError);
    BOOST_CHECK_EQUAL(asIntFromZxhex(name, "0xff"), 255);
}

BOOST_AUTO_TEST_CASE(asIntFromZxhex_uint8)
{
    using TigerMail::JsonRpc::asIntFromZxhex;
    BOOST_CHECK_EQUAL(asIntFromZxhex<std::uint8_t>(name, "0x25"), 37);
}

BOOST_AUTO_TEST_CASE(asString)
{
    using TigerMail::JsonRpc::JsonError;
    using TigerMail::JsonRpc::asString;
    BOOST_CHECK_THROW(asString(name, 5), JsonError);
    BOOST_CHECK_EQUAL(asString(name, "abc"), "abc");
}

BOOST_AUTO_TEST_CASE(asDataFromZxhex)
{
    using TigerMail::Buffer;
    using TigerMail::JsonRpc::JsonError;
    using TigerMail::JsonRpc::asDataFromZxhex;
    BOOST_CHECK_THROW(asDataFromZxhex(name, 5), JsonError);
    BOOST_CHECK_THROW(asDataFromZxhex(name, "abc"), JsonError);

    const auto data = asDataFromZxhex(name, "0xdeadbeef");
    const auto expected_data = Buffer{0xde,0xad,0xbe,0xef};
    BOOST_CHECK_EQUAL_COLLECTIONS(data.begin(), data.end(),
                                  expected_data.begin(), expected_data.end());
}

BOOST_AUTO_TEST_CASE(asOptional)
{
    using TigerMail::JsonRpc::JsonError;
    using TigerMail::JsonRpc::asInt;
    using TigerMail::JsonRpc::asOptional;
    BOOST_CHECK_THROW(asOptional(asInt, name, true), JsonError);
    BOOST_CHECK_EQUAL(asOptional(asInt, name, {}), std::nullopt);
    BOOST_CHECK_EQUAL(asOptional(asInt, name, 5), std::make_optional(5));
}

BOOST_AUTO_TEST_CASE(asVector)
{
    using TigerMail::JsonRpc::JsonError;
    using TigerMail::JsonRpc::asInt;
    using TigerMail::JsonRpc::asVector;
    using TigerMail::JsonRpc::make_array;
    BOOST_CHECK_THROW(asVector(asInt, name, 5), JsonError);
    BOOST_CHECK_THROW(asVector(asInt, name, make_array({5, "foo"})), JsonError);

    const auto vector = asVector(asInt, name, make_array({5, 7, 3}));
    const auto expected_vector = std::vector<int>{5, 7, 3};
    BOOST_CHECK_EQUAL_COLLECTIONS(vector.begin(), vector.end(),
                                  expected_vector.begin(), expected_vector.end());
}


BOOST_AUTO_TEST_SUITE_END() // json
