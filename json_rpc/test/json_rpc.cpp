/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>

#include <boost/test/unit_test.hpp>

#include <json/value.h>

#include "../json.hpp"
#include "../json_rpc.hpp"


BOOST_AUTO_TEST_SUITE(json_rpc)


BOOST_AUTO_TEST_CASE(format_request)
{
    using TigerMail::JsonRpc::format_request;
    BOOST_CHECK_EQUAL(format_request("foo", Json::nullValue, Json::nullValue),
                      R"json({"jsonrpc":"2.0","method":"foo"})json");
    BOOST_CHECK_EQUAL(format_request("bar", 5, "bla"),
                      R"json({"id":"bla","jsonrpc":"2.0","method":"bar","params":5})json");
}


BOOST_AUTO_TEST_SUITE(check_response)

using TigerMail::JsonRpc::check_response;
using TigerMail::JsonRpc::parse_json;
using TigerMail::JsonRpc::JsonError;
using TigerMail::JsonRpc::JsonRpcError;


BOOST_AUTO_TEST_CASE(correct)
{
    const auto obj = parse_json(R"json({"jsonrpc":"2.0", "result": "foo", "id":1})json");
    BOOST_CHECK_NO_THROW(check_response(obj));
}

BOOST_AUTO_TEST_CASE(wrong_jsonrpc_version)
{
    const auto obj = parse_json(R"json({"jsonrpc":"1.0", "result": "foo", "id":1})json");
    BOOST_CHECK_THROW(check_response(obj), JsonError);
}

BOOST_AUTO_TEST_CASE(error)
{
    const auto obj = parse_json(R"json({"jsonrpc":"2.0", "error": {"code": -32000, "message": "foo"}, "id":1})json");
    const auto is_expected_exception =
        [](const JsonRpcError& e) {
            return e.code() == -32000 && std::string{e.what()} == "foo";
        };
    BOOST_CHECK_EXCEPTION(check_response(obj), JsonRpcError, is_expected_exception);
}


BOOST_AUTO_TEST_SUITE_END() // check_response


BOOST_AUTO_TEST_SUITE_END() // json_rpc
