/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_JSON_RPC_JSON_RPC_HPP
#define TIGERMAIL_JSON_RPC_JSON_RPC_HPP

#include <string_view>
#include <stdexcept>

#include <json/value.h>


namespace TigerMail::JsonRpc {


class JsonRpcError
    : public std::runtime_error
{
private:
    int m_code;

public:
    JsonRpcError(int code, const std::string& what);
    ~JsonRpcError() noexcept override = default;

    int code() const
    {
        return m_code;
    }
};


std::string format_request(std::string_view method,
                           const Json::Value& params,
                           const Json::Value& id);

void check_response(const Json::Value& response);


} // namespace TigerMail::JsonRpc

#endif // TIGERMAIL_JSON_RPC_JSON_RPC_HPP
