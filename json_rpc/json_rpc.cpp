/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "json_rpc.hpp"

#include <stdexcept>
#include <string>

#include <gsl/gsl_assert>

#include <json/writer.h>

#include "json.hpp"



namespace TigerMail::JsonRpc {


JsonRpcError::JsonRpcError(int code, const std::string& what)
    : std::runtime_error{what}
    , m_code{code}
{}


std::string format_request(std::string_view method,
                           const Json::Value& params,
                           const Json::Value& id)
{
    Expects(id.isInt() || id.isString() || id.isNull());

    auto request_obj = make_object({
            {"jsonrpc", "2.0"},
            {"method", std::string{method}},
        });
    if (!params.isNull()) {
        request_obj["params"] = params;
    }
    if (!id.isNull()) {
        request_obj["id"] = id;
    }

    auto writer_builder = Json::StreamWriterBuilder{};
    writer_builder["indentation"] = "";
    return Json::writeString(writer_builder, request_obj);
}


void check_response(const Json::Value& response)
{
    if (!response.isObject()) {
        throw JsonError{"response is not an object"};
    }

    {
        const auto& json_rpc_version = response["jsonrpc"];
        if (!json_rpc_version.isString()) {
            throw JsonError{"jsonrpc version is not a string"};
        }
        if (json_rpc_version.asString() != "2.0") {
            throw JsonError{"invalid jsonrpc version"};
        }
    }

    {
        const auto& id = response["id"];
        if (!(id.isInt() || id.isString() || id.isNull())) {
            throw JsonError{"invalid type of id"};
        }
    }

    if (response.isMember("error")) {
        const auto& error = response["error"];
        if (!error.isObject()) {
            throw JsonError{"error is not an object"};
        }
        const auto& code = error["code"];
        if (!code.isInt()) {
            throw JsonError{"error code is not an int"};
        }
        const auto& message = error["message"];
        if (!message.isString()) {
            throw JsonError{"error message is not a string"};
        }
        throw JsonRpcError{code.asInt(), message.asString()};
    }
}


} // namespace TigerMail::JsonRpc
