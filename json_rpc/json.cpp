/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "json.hpp"

#include <sstream>

#include <json/reader.h>

#include "../common/hex.hpp"


namespace TigerMail::JsonRpc {


::Json::Value parse_json(std::string_view input)
{
    auto stream = std::stringstream{};
    stream << input;
    auto json = ::Json::Value{};
    try {
        stream >> json;
    }
    catch (::Json::Exception& e) {
        throw JsonError(e.what());
    }
    return json;
}


::Json::Value make_array(const std::initializer_list<::Json::Value> l)
{
    auto array = ::Json::Value{::Json::arrayValue};
    for (const auto& i : l) {
        array.append(i);
    }
    return array;
}

::Json::Value make_object(const std::initializer_list<std::pair<std::string, ::Json::Value>> l)
{
    auto obj = ::Json::Value{::Json::objectValue};
    for (const auto& p : l) {
        obj[p.first] = p.second;
    }
    return obj;
}

bool asBool(std::function<std::string()> name, const ::Json::Value& v)
{
    if (!v.isBool()) {
        throw JsonError{name() + " is not a bool"};
    }
    return v.asBool();
}

int asInt(std::function<std::string()> name, const ::Json::Value& v)
{
    if (!v.isInt()) {
        throw JsonError{name() + " is not an int"};
    }
    return v.asInt();
}

std::string asString(std::function<std::string()> name, const ::Json::Value& v)
{
    if (!v.isString()) {
        throw JsonError{name() + " is not a string"};
    }
    return v.asString();
}


} // namespace TigerMail::JsonRpc
