#!/bin/bash

export CPPFLAGS="-D_FORTIFY_SOURCE=2"
export CFLAGS="-fmessage-length=0 -fstack-protector -funwind-tables -fasynchronous-unwind-tables"
export CXXFLAGS="-fmessage-length=0 -fstack-protector -funwind-tables -fasynchronous-unwind-tables"
meson -Dwarning_level=2 -Doptimization=2 --buildtype=debugoptimized --wrap_mode=nodownload --auto_features=enabled . build_release
