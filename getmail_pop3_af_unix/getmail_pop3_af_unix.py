#!/usr/bin/python

import imp
import poplib
import socket


# This is ugly monkey patching
# Use host as socket path. Ignore port.
def my_POP3_init(self, host, port=poplib.POP3_PORT,
                 timeout=socket._GLOBAL_DEFAULT_TIMEOUT):
    self.host = host
    self.port = port
    self.sock = socket.socket(socket.AF_UNIX)
    if timeout is not socket._GLOBAL_DEFAULT_TIMEOUT:
        self.sock.settimeout(timeout)
    self.sock.connect(host)
    self.file = self.sock.makefile('rb')
    self._debugging = 0
    self.welcome = self._getresp()


poplib.POP3.__init__ = my_POP3_init


if __name__ == "__main__":
    getmail = imp.load_source('getmail', '/usr/bin/getmail')
    getmail.main()
