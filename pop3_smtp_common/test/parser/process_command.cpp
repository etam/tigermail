/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>

#include <boost/test/unit_test.hpp>

#include "../../parser.hpp"

using TigerMail::POP3_SMTP_common::CommandProcessorClientInterface;
using TigerMail::POP3_SMTP_common::process_command;


namespace {


class CommandProcessorClient
    : public CommandProcessorClientInterface
{
private:
    bool m_command_too_long_called;
    std::vector<std::string> m_parsed_commands;

public:
    CommandProcessorClient()
        : m_command_too_long_called{false}
        , m_parsed_commands{}
    {}

    void command_too_long() override
    {
        m_command_too_long_called = true;
    }

    void parse_command(std::string_view command) override
    {
        m_parsed_commands.emplace_back(command);
    }

    bool command_too_long_called() const
    {
        return m_command_too_long_called;
    }

    const auto& parsed_commands() const
    {
        return m_parsed_commands;
    }
};


struct Fixture
{
    bool ignoring_input_until_end = false;
    std::size_t max_command_len = 10;
    CommandProcessorClient client;

    auto run_process_command(std::string&& input)
    {
        return process_command(std::move(input), &client,
                               max_command_len,
                               &ignoring_input_until_end);
    }
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(test_process_command, Fixture)


BOOST_AUTO_TEST_CASE(eol)
{
    const auto [unparsed_input, cont] =
        run_process_command("abcd ef gh\r\n");

    BOOST_CHECK_EQUAL(unparsed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK(!client.command_too_long_called());
    const auto expected_commands = std::vector<std::string>{"abcd ef gh"};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        client.parsed_commands().begin(), client.parsed_commands().end(),
        expected_commands.begin(), expected_commands.end());
}

BOOST_AUTO_TEST_CASE(eol_extra_input)
{
    const auto [unparsed_input, cont] =
        run_process_command("abcd ef gh\r\nfoo bar");

    BOOST_CHECK_EQUAL(unparsed_input, "foo bar");
    BOOST_CHECK(cont);
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK(!client.command_too_long_called());
    const auto expected_commands = std::vector<std::string>{"abcd ef gh"};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        client.parsed_commands().begin(), client.parsed_commands().end(),
        expected_commands.begin(), expected_commands.end());
}

BOOST_AUTO_TEST_CASE(too_long_eol)
{
    const auto [unparsed_input, cont] =
        run_process_command("abcd ef ghi jklmnop\r\nfoo bar");

    BOOST_CHECK_EQUAL(unparsed_input, "foo bar");
    BOOST_CHECK(cont);
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK(client.command_too_long_called());
    const auto expected_commands = std::vector<std::string>{};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        client.parsed_commands().begin(), client.parsed_commands().end(),
        expected_commands.begin(), expected_commands.end());
}

BOOST_AUTO_TEST_CASE(no_eol)
{
    const auto [unparsed_input, cont] =
        run_process_command("abcd efg");

    BOOST_CHECK_EQUAL(unparsed_input, "abcd efg");
    BOOST_CHECK(!cont);
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK(!client.command_too_long_called());
    const auto expected_commands = std::vector<std::string>{};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        client.parsed_commands().begin(), client.parsed_commands().end(),
        expected_commands.begin(), expected_commands.end());
}

BOOST_AUTO_TEST_CASE(too_long_no_eol)
{
    const auto [unparsed_input, cont] =
        run_process_command("abcd efghi jklmnop");

    BOOST_CHECK_EQUAL(unparsed_input, "p");
    BOOST_CHECK(!cont);
    BOOST_CHECK(ignoring_input_until_end);
    BOOST_CHECK(client.command_too_long_called());
    const auto expected_commands = std::vector<std::string>{};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        client.parsed_commands().begin(), client.parsed_commands().end(),
        expected_commands.begin(), expected_commands.end());
}

BOOST_AUTO_TEST_CASE(ignored_no_eol)
{
    ignoring_input_until_end = true;

    const auto [unparsed_input, cont] =
        run_process_command("aoeuidhtns");

    BOOST_CHECK_EQUAL(unparsed_input, "s");
    BOOST_CHECK(!cont);
    BOOST_CHECK(ignoring_input_until_end);
    BOOST_CHECK(!client.command_too_long_called());
    const auto expected_commands = std::vector<std::string>{};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        client.parsed_commands().begin(), client.parsed_commands().end(),
        expected_commands.begin(), expected_commands.end());
}

BOOST_AUTO_TEST_CASE(ignored_eol)
{
    ignoring_input_until_end = true;

    const auto [unparsed_input, cont] =
        run_process_command("qjkxbmwvz\r\n");

    BOOST_CHECK_EQUAL(unparsed_input, "");
    BOOST_CHECK(!cont);
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK(!client.command_too_long_called());
    const auto expected_commands = std::vector<std::string>{};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        client.parsed_commands().begin(), client.parsed_commands().end(),
        expected_commands.begin(), expected_commands.end());
}

BOOST_AUTO_TEST_CASE(ignored_eol_extra_input)
{
    ignoring_input_until_end = true;

    const auto [unparsed_input, cont] =
        run_process_command("qjkxbmwvz\r\nfoo bar");

    BOOST_CHECK_EQUAL(unparsed_input, "foo bar");
    BOOST_CHECK(cont);
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK(!client.command_too_long_called());
    const auto expected_commands = std::vector<std::string>{};
    BOOST_CHECK_EQUAL_COLLECTIONS(
        client.parsed_commands().begin(), client.parsed_commands().end(),
        expected_commands.begin(), expected_commands.end());
}

BOOST_AUTO_TEST_CASE(sequence)
{
    const auto inputs = std::vector<std::string>{
        "HELO\r",
        "\nEHLO\r\nHELO abc\r",
        "\nNOOP\r\n"
    };

    auto input = std::string{};
    for (const auto& new_input : inputs) {
        input.append(new_input);
        bool cont = true;
        while (cont) {
            std::tie(input, cont) = run_process_command(std::move(input));
        }
    }

    BOOST_CHECK_EQUAL(input, "");
    BOOST_CHECK(!ignoring_input_until_end);
    BOOST_CHECK(!client.command_too_long_called());
    const auto expected_commands = std::vector<std::string>{
        "HELO",
        "EHLO",
        "HELO abc",
        "NOOP"
    };
    BOOST_CHECK_EQUAL_COLLECTIONS(
        client.parsed_commands().begin(), client.parsed_commands().end(),
        expected_commands.begin(), expected_commands.end());
}


BOOST_AUTO_TEST_SUITE_END()
