/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "parser.hpp"


namespace TigerMail::POP3_SMTP_common {


std::tuple<std::string, bool> process_command(
    std::string&& input,
    CommandProcessorClientInterface* client,
    const std::size_t max_command_len,
    bool* ignoring_input_until_end
)
{
    static constexpr const auto eol = std::string_view{"\r\n"};
    const auto eol_pos = input.find(eol);

    if (eol_pos == std::string_view::npos) {
        if (input.size() > max_command_len) {
            client->command_too_long();
            *ignoring_input_until_end = true;
        }
        if (*ignoring_input_until_end) {
            // in more general case it would be
            // input.substr(input.size() - eol.size() + 1);
            // which in this case is just a single character
            return {std::string{input.back()}, false};
        }
        else {
            return {std::move(input), false};
        }
    }

    if (*ignoring_input_until_end) {
        *ignoring_input_until_end = false;
    }
    else {
        const auto command_str = input.substr(0, eol_pos);
        if (command_str.size() > max_command_len) {
            client->command_too_long();
        }
        else {
            client->parse_command(command_str);
        }
    }

    if (eol_pos + eol.size() == input.size()) {
        return {"", false};
    }
    return {input.substr(eol_pos + eol.size()), true};
}


} // namespace TigerMail::POP3_SMTP_common
