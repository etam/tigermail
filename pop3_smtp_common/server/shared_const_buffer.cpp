/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "shared_const_buffer.hpp"


namespace TigerMail::POP3_SMTP_common::server {


SharedConstBuffer::SharedConstBuffer(std::string&& data)
    : m_data{std::make_shared<std::string>(std::move(data))}
    , m_buffer_view{asio::buffer(*m_data)}
{}

SharedConstBuffer::const_iterator SharedConstBuffer::begin() const
{
    return &m_buffer_view;
}

SharedConstBuffer::const_iterator SharedConstBuffer::end() const
{
    return &m_buffer_view + 1;
}


} // namespace TigerMail::POP3_SMTP_common::server
