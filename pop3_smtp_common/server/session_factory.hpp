/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_SMTP_COMMON_SERVER_SESSION_FACTORY_HPP
#define TIGERMAIL_POP3_SMTP_COMMON_SERVER_SESSION_FACTORY_HPP

#include <memory>
#include <tuple>
#include <type_traits>
#include <utility>

#include <boost/asio/generic/stream_protocol.hpp>

#include "session.hpp"


namespace TigerMail::POP3_SMTP_common::server {


class ConnectionManager;


class SessionFactoryInterface
{
public:
    virtual ~SessionFactoryInterface() noexcept = default;

    virtual SessionPtr make_session(
        asio::generic::stream_protocol::socket&& socket,
        ConnectionManager* connection_manager) const = 0;
};


template <typename SessionHandler,
          typename... Args>
class SessionFactory
    : public SessionFactoryInterface
{
private:
    std::tuple<Args...> m_args;

    // https://stackoverflow.com/questions/42826184/why-can-stdapply-call-a-lambda-but-not-the-equivalent-template-function
    struct my_make_unique
    {
        std::unique_ptr<SessionHandler> operator()(const Args&... args) const
        {
            return std::make_unique<SessionHandler>(args...);
        }
    };

public:
    static_assert(std::is_base_of_v<SessionHandlerInterface, SessionHandler>);

    explicit
    SessionFactory(Args&&... args)
        : m_args{std::forward<Args>(args)...}
    {}

    SessionPtr make_session(
        asio::generic::stream_protocol::socket&& socket,
        ConnectionManager* connection_manager) const override
    {
        auto handler_ptr = std::apply(my_make_unique{}, m_args);
        return std::make_shared<Session>(std::move(socket), connection_manager,
                                         std::move(handler_ptr));
    }
};


template <typename SessionHandler, typename... Args>
SessionFactory<SessionHandler, Args...> make_session_factory(Args&&... args)
{
    return SessionFactory<SessionHandler, Args...>{std::forward<Args>(args)...};
}


} // namespace TigerMail::POP3_SMTP_common::server

#endif // TIGERMAIL_POP3_SMTP_COMMON_SERVER_SESSION_FACTORY_HPP
