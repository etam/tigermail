/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "session.hpp"

#include <boost/asio/buffer.hpp>
#include <boost/asio/write.hpp>

#include "connection_manager.hpp"
#include "shared_const_buffer.hpp"


namespace TigerMail::POP3_SMTP_common::server {


Session::Session(
    asio::generic::stream_protocol::socket&& socket,
    ConnectionManager* connection_manager,
    std::unique_ptr<SessionHandlerInterface>&& session_handler)
    : m_socket{std::move(socket)}
    , m_connection_manager{connection_manager}
    , m_session_handler{std::move(session_handler)}
    , m_input_buffer(4*1024, '\0')
    , m_shutting_down{false}
{}

void Session::start()
{
    auto msg = m_session_handler->handle_hello();

    if (!msg.empty()) {
        auto self = shared_from_this();
        do_write(
            std::move(msg),
            [this, self] {
                do_read();
            });
        return;
    }

    do_read();
}

void Session::graceful_stop()
{
    m_shutting_down = true;

    // async_read are called with ec operatation_aborted
    m_socket.shutdown(asio::socket_base::shutdown_receive);

    auto msg = m_session_handler->handle_stop();
    if (msg.empty()) {
        stop();
        return;
    }

    auto self = shared_from_this();
    do_write(
        std::move(msg),
        [this, self] {
            stop();
        });
}

void Session::stop()
{
    if (!m_socket.is_open()) {
        return;
    }

    m_shutting_down = true;
    m_socket.shutdown(asio::socket_base::shutdown_both);
    m_socket.close();
}

void Session::do_read()
{
    auto self = shared_from_this();

    m_socket.async_read_some(
        asio::buffer(m_input_buffer),
        [this, self](boost::system::error_code ec, std::size_t bytes_transferred) {
            if (ec) {
                if (ec != boost::asio::error::eof
                    && !(m_shutting_down
                         && ec == boost::asio::error::operation_aborted)) {
                    //log_error(ec.message());
                }
                if (!m_shutting_down) {
                    m_connection_manager->stop(self);
                }
                return;
            }

            try {
                const auto buffer_view =
                    std::string_view{m_input_buffer.data(), bytes_transferred};

                auto [response, write_response_and_disconnect] =
                    m_session_handler->handle_input(buffer_view);

                if (write_response_and_disconnect) {
                    do_write(
                        std::move(response),
                        [this, self] {
                            m_connection_manager->stop(self);
                        });
                    return;
                }

                do_write(
                    std::move(response),
                    [this, self] {
                        do_read();
                    });
            }
            catch (std::exception &e) {
                //log_error(std::string{"unexpected exception thrown by handler: "} + e.what());
                m_connection_manager->graceful_stop(self);
            }
        });
}

template <typename F>
void Session::do_write(std::string&& response, F handler)
{
    auto self = shared_from_this();
    asio::async_write(
        m_socket, SharedConstBuffer{std::move(response)},
        [this, self, handler](boost::system::error_code ec, std::size_t /*bytes_transferred*/) {
            if (ec) {
                if (ec != asio::error::eof
                    && !(m_shutting_down && ec == asio::error::operation_aborted)) {
                    // log_error(ec.message());
                }
                if (!m_shutting_down) {
                    m_connection_manager->stop(self);
                }
                return;
            }

            try {
                handler();
            }
            catch (std::exception& e) {
                // log_error(std::string{"unexpected exception thrown by handler: "} + e.what());
                m_connection_manager->graceful_stop(self);
            }
        });
}


} // namespace TigerMail::POP3_SMTP_common::server
