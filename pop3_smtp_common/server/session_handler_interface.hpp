/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_SMTP_COMMON_SERVER_SESSION_HANDLER_INTERFACE_HPP
#define TIGERMAIL_POP3_SMTP_COMMON_SERVER_SESSION_HANDLER_INTERFACE_HPP

#include <string>
#include <string_view>
#include <tuple>


namespace TigerMail::POP3_SMTP_common::server {


class SessionHandlerInterface
{
public:
    virtual ~SessionHandlerInterface() noexcept = default;

    virtual std::string handle_hello() = 0;
    /// return: response, should disconnect after responding
    virtual std::tuple<std::string, bool> handle_input(std::string_view input) = 0;
    virtual std::string handle_stop() = 0;
};


} // namespace TigerMail::POP3_SMTP_common::server

#endif // TIGERMAIL_POP3_SMTP_COMMON_SERVER_SESSION_HANDLER_INTERFACE_HPP
