/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <csignal>
#include <exception>
#include <iostream>
#include <string>
#include <string_view>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/local/stream_protocol.hpp>
#include <boost/asio/signal_set.hpp>

#include <gsl/pointers>

#include "server.hpp"
#include "session_factory.hpp"

namespace asio = boost::asio;
using TigerMail::POP3_SMTP_common::server::LocalEndpoint;
using TigerMail::POP3_SMTP_common::server::Server;
using TigerMail::POP3_SMTP_common::server::SessionFactory;
using TigerMail::POP3_SMTP_common::server::SessionHandlerInterface;


namespace {


class EchoSessionHandler
    : public SessionHandlerInterface
{
public:
    std::string handle_hello() override
    {
        return "hello\r\n";
    }

    std::tuple<std::string, bool> handle_input(std::string_view input) override
    {
        auto should_disconnect = (input.find("quit") != std::string_view::npos);
        return {std::string{input}, should_disconnect};
    }

    std::string handle_stop() override
    {
        return "bye\r\n";
    }
};


} // namespace


int main()
try {
    auto io_context = asio::io_context{};
    auto signals = asio::signal_set{io_context, SIGINT, SIGTERM};

    const auto echo_session_factory = SessionFactory<EchoSessionHandler>{};

    auto unix_endpoint = LocalEndpoint{"echo.socket"};
    auto echo_unix_server = Server{
        io_context,
        asio::local::stream_protocol::acceptor{io_context, unix_endpoint},
        gsl::not_null{&echo_session_factory}};

    auto tcp_endpoint = asio::ip::tcp::endpoint(asio::ip::address_v4::loopback(), 3369);
    auto echo_tcp_server = Server{
        io_context,
        asio::ip::tcp::acceptor{io_context, tcp_endpoint},
        gsl::not_null{&echo_session_factory}};

    signals.async_wait(
        [&](boost::system::error_code /*ec*/, int /*signal*/) {
            echo_unix_server.stop();
            echo_tcp_server.stop();
        });

    // #pragma omp parallel
    io_context.run();
}
catch (std::exception& e) {
    std::cerr << "Unhandled exception: " << e.what() << "\n";
}
