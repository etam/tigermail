/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_SMTP_COMMON_SERVER_SERVER_HPP
#define TIGERMAIL_POP3_SMTP_COMMON_SERVER_SERVER_HPP

#include <memory>
#include <string>
#include <variant>

#include <unistd.h>

#include <boost/asio/generic/stream_protocol.hpp>
#include <boost/asio/local/stream_protocol.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <gsl/pointers>

#include "connection_manager.hpp"
#include "session.hpp"
#include "session_factory.hpp"


namespace TigerMail::POP3_SMTP_common::server {

namespace asio = boost::asio;


class LocalEndpoint
    : public asio::local::stream_protocol::endpoint
{
private:
    typedef asio::local::stream_protocol::endpoint parent_type;

public:
    LocalEndpoint(const std::string& path);
    ~LocalEndpoint() noexcept;
};


class Server
{
public:
    using Acceptor = std::variant<
        asio::local::stream_protocol::acceptor,
        asio::ip::tcp::acceptor
    >;

private:
    Acceptor m_acceptor;
    asio::generic::stream_protocol::socket m_socket;
    ConnectionManager m_connection_manager;
    gsl::not_null<const SessionFactoryInterface*> m_session_factory;

public:
    Server(asio::io_context& io_context,
           Acceptor&& acceptor,
           gsl::not_null<const SessionFactoryInterface*> session_factory);

    void stop();

private:
    void do_accept();
};


} // namespace TigerMail::POP3_SMTP_common::server

#endif // TIGERMAIL_POP3_SMTP_COMMON_SERVER_SERVER_HPP
