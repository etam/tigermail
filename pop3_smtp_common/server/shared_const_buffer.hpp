/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_SMTP_COMMON_SHARED_CONST_BUFFER_HPP
#define TIGERMAIL_POP3_SMTP_COMMON_SHARED_CONST_BUFFER_HPP

#include <memory>
#include <string>

#include <boost/asio/buffer.hpp>


namespace TigerMail::POP3_SMTP_common::server {

namespace asio = boost::asio;


class SharedConstBuffer
{
private:
    std::shared_ptr<std::string> m_data;
    asio::const_buffer m_buffer_view;

public:
    explicit
    SharedConstBuffer(std::string&& data);

    using value_type = boost::asio::const_buffer;
    using const_iterator = const boost::asio::const_buffer*;

    const_iterator begin() const;
    const_iterator end() const;
};


} // namespace TigerMail::POP3_SMTP_common::server

#endif // TIGERMAIL_POP3_SMTP_COMMON_SHARED_CONST_BUFFER_HPP
