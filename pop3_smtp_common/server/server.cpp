/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "server.hpp"


namespace TigerMail::POP3_SMTP_common::server {


LocalEndpoint::LocalEndpoint(const std::string& path)
    : parent_type(path)
{
    ::unlink(path.c_str());
}

LocalEndpoint::~LocalEndpoint() noexcept
{
    ::unlink(path().c_str());
}


Server::Server(asio::io_context& io_context,
               Acceptor&& acceptor,
               gsl::not_null<const SessionFactoryInterface*> session_factory)
    : m_acceptor{std::move(acceptor)}
    , m_socket{io_context}
    , m_connection_manager{}
    , m_session_factory{session_factory}
{
    do_accept();
}

void Server::stop()
{
    std::visit([](auto& acceptor) { acceptor.close(); }, m_acceptor);
    m_connection_manager.graceful_stop_all();
}

void Server::do_accept()
{
    std::visit(
        [this](auto& acceptor) {
            acceptor.async_accept(
                m_socket,
                [this, &acceptor](boost::system::error_code ec) {
                    if (!acceptor.is_open()) {
                        return;
                    }
                    if (!ec) {
                        m_connection_manager.start(
                            m_session_factory->make_session(
                                std::move(m_socket), &m_connection_manager));
                    }
                    do_accept();
                });
        }, m_acceptor);
}


} // namespace TigerMail::POP3_SMTP_common::server
