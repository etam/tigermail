/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_SMTP_COMMON_SERVER_SESSION_HPP
#define TIGERMAIL_POP3_SMTP_COMMON_SERVER_SESSION_HPP

#include <string>
#include <memory>

#include <boost/asio/generic/stream_protocol.hpp>

#include <boost/core/noncopyable.hpp>

#include "session_handler_interface.hpp"


namespace TigerMail::POP3_SMTP_common::server {

namespace asio = boost::asio;

class ConnectionManager;


class Session
    : public std::enable_shared_from_this<Session>
    , public boost::noncopyable
{
private:
    asio::generic::stream_protocol::socket m_socket;
    ConnectionManager* m_connection_manager;
    std::unique_ptr<SessionHandlerInterface> m_session_handler;
    std::string m_input_buffer;
    bool m_shutting_down;

public:
    Session(
        asio::generic::stream_protocol::socket&& socket,
        ConnectionManager* connection_manager,
        std::unique_ptr<SessionHandlerInterface>&& session_handler);

    void start();
    void graceful_stop();
    void stop();

private:
    void do_read();

    template <typename F>
    void do_write(std::string&& response, F handler);
};

using SessionPtr = std::shared_ptr<Session>;


} // namespace TigerMail::POP3_SMTP_common::server

#endif // TIGERMAIL_POP3_SMTP_COMMON_SERVER_SESSION_HPP
