/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "connection_manager.hpp"


namespace TigerMail::POP3_SMTP_common::server {


void ConnectionManager::start(SessionPtr session)
{
    sessions.insert(session);
    session->start();
}

void ConnectionManager::graceful_stop(SessionPtr session)
{
    sessions.erase(session);
    session->graceful_stop();
}

void ConnectionManager::stop(SessionPtr session)
{
    sessions.erase(session);
    session->stop();
}

void ConnectionManager::graceful_stop_all()
{
    for (auto& session : sessions)
        session->graceful_stop();
    sessions.clear();
}


} // namespace TigerMail::POP3_SMTP_common::server
