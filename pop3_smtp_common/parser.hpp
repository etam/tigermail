/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_SMTP_COMMON_PARSER_HPP
#define TIGERMAIL_POP3_SMTP_COMMON_PARSER_HPP

#include <cstddef>
#include <string>
#include <string_view>
#include <tuple>


namespace TigerMail::POP3_SMTP_common {


class SyntaxError
    : public std::exception
{};


template <typename Event>
Event disallow_args(const std::string_view args)
{
    if (!args.empty()) {
        throw SyntaxError{};
    }
    return {};
}


class CommandProcessorClientInterface
{
public:
    virtual ~CommandProcessorClientInterface() noexcept = default;

    virtual void command_too_long() = 0;
    virtual void parse_command(std::string_view command) = 0;
};


/**
 * return: {unparsed input, should continue parsing}
 *
 * If input contains eol, just give to client.
 * If no eol in sight, return input as unparsed and don't continue.
 * If input gets too long, tell client and drop further input until
 * eol, but return last character, to find eol if it get split in inputs.
 */
std::tuple<std::string, bool> process_command(
    std::string&& input,
    CommandProcessorClientInterface* client,
    std::size_t max_commmand_len,
    bool* ignoring_input_until_end
);


} // namespace TigerMail::POP3_SMTP_common

#endif // TIGERMAIL_POP3_SMTP_COMMON_PARSER_HPP
