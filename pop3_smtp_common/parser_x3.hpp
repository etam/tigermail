/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_POP3_SMTP_COMMON_PARSER_X3_HPP
#define TIGERMAIL_POP3_SMTP_COMMON_PARSER_X3_HPP

#include <string_view>

#include <boost/spirit/home/x3.hpp>

#include "parser.hpp"


namespace TigerMail::POP3_SMTP_common {


template <typename T, typename Parser>
T parse_type(const Parser& parser, std::string_view args)
{
    namespace x3 = boost::spirit::x3;
    using x3::phrase_parse;

    auto obj = T{};
    auto begin = args.begin();
    const auto end = args.end();
    const bool r = phrase_parse(begin, end, parser, x3::space, obj);
    if (!r || begin != end) {
        throw SyntaxError{};
    }
    return obj;
}


} // namespace TigerMail::POP3_SMTP_common

#endif // TIGERMAIL_POP3_SMTP_COMMON_PARSER_X3_HPP
