/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ipc_client.hpp"

#include <chrono>
#include <sstream>
#include <stdexcept>
#include <string>

#include <boost/asio/local/stream_protocol.hpp>

#include <gsl/gsl_assert>

#include "../json_rpc/json.hpp"
#include "../json_rpc/json_rpc.hpp"


namespace TigerMail::Swarm {

namespace asio = boost::asio;
using asio::local::stream_protocol;


namespace {


std::string getline(std::istream& stream)
{
    auto result = std::string{};
    std::getline(stream, result);
    return result;
}


} // namespace


Json::Value get_bzz_info(const stream_protocol::endpoint& endpoint)
{
    auto s = stream_protocol::iostream{endpoint};
    s.expires_after(std::chrono::seconds{1});

    if (!s) {
        auto err_msg = std::ostringstream{};
        err_msg << "Unable to connect to " << endpoint << ": " << s.error().message();
        throw std::runtime_error{err_msg.str()};
    }

    s << JsonRpc::format_request("bzz_info", Json::nullValue, 1) << '\n';
    // for some reason s >> Json::Value is hanging forever
    return JsonRpc::parse_json(getline(s));
}


asio::ip::tcp::endpoint get_swarm_tcp_endpoint(const Json::Value& bzz_info)
{
    JsonRpc::check_response(bzz_info);
    const auto& result = bzz_info["result"];
    if (!result.isObject()) {
        throw JsonRpc::JsonError{"result is not an object"};
    }
    const auto& addr = result["ListenAddr"];
    if (!addr.isString()) {
        throw JsonRpc::JsonError{"ListenAddr is not a string"};
    }
    const auto& port_str = result["Port"];
    if (!port_str.isString()) {
        throw JsonRpc::JsonError{"Port is not a string"};
    }
    const auto port = std::stoi(port_str.asString());
    if (port < 0 || port > 65535) {
        throw JsonRpc::JsonError{"Port is out of range"};
    }
    return {asio::ip::make_address(addr.asString()), static_cast<unsigned short>(port)};
}


} // namespace TigerMail::Swarm
