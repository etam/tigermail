/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tcp_client.hpp"

#include <algorithm>
#include <chrono>
#include <future>
#include <sstream>
#include <stdexcept>
#include <tuple>

#include <boost/asio/spawn.hpp>
#include <boost/asio/strand.hpp>

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>

#include <boost/log/trivial.hpp>

#include <boost/process.hpp>

#include <gsl/gsl_assert>

#include "../common/hex.hpp"
#include "../common/utils.hpp"


namespace TigerMail::Swarm {

namespace http = boost::beast::http;


TCPClient::TCPClient(boost::asio::io_context& io)
    : m_io{io}
    , m_stream{boost::asio::make_strand(io)}
    , m_connected{false}
    , m_host{}
{}

TCPClient::~TCPClient() noexcept
{
    if (m_connected) {
        m_stream.socket().shutdown(boost::asio::socket_base::shutdown_both);
        m_stream.close();
    }
}

void TCPClient::connect(
    const std::variant<
        boost::asio::ip::tcp::endpoint,
        boost::asio::local::stream_protocol::endpoint
    > endpoint,
    std::any yield_)
{
    auto yield = std::any_cast<boost::asio::yield_context>(yield_);

    std::visit(
        [&](const auto& endpoint) {
            m_stream.async_connect(endpoint, yield);
        },
        endpoint);
    m_connected = true;

    auto host = std::ostringstream{};
    std::visit(overloaded{
            [&](const boost::asio::ip::tcp::endpoint& endpoint) {
                host << endpoint.address().to_string() << ':' << endpoint.port();
            },
            [&](const boost::asio::local::stream_protocol::endpoint& endpoint) {
                host << endpoint.path();
            }
        },
        endpoint);
    m_host = host.str();
}

std::optional<Buffer> TCPClient::bzz_get(const SwarmHash& id, std::any yield_)
{
    Expects(m_connected);
    static_assert(SwarmHash::Size == 32);
    auto yield = std::any_cast<boost::asio::yield_context>(yield_);

    {
        const auto target = std::string{"/bzz:/"} + bin2hex(id) + "/";
        auto req = http::request<http::string_body>{http::verb::get, target, 11};
        req.set(http::field::host, m_host);

        http::async_write(m_stream, req, yield);
    }

    m_stream.expires_after(std::chrono::seconds{10});

    auto res = http::response<http::vector_body<std::uint8_t>>{};
    http::async_read(m_stream, m_buffer, res, yield);
    m_stream.expires_never();

    if (res.result() == http::status::not_found) {
        return std::nullopt;
    }
    return std::move(res.body());
}

SwarmHash TCPClient::bzz_post(BufferView data, std::any yield_)
{
    Expects(m_connected);
    auto yield = std::any_cast<boost::asio::yield_context>(yield_);

    {
        auto req = http::request<http::vector_body<std::uint8_t>>{http::verb::post, "/bzz:/", 11};
        req.set(http::field::host, m_host);
        req.set(http::field::content_type, "application/octet-stream");
        req.body() = std::vector(data.begin(), data.end());
        req.prepare_payload();

        http::async_write(m_stream, req, yield);
    }

    m_stream.expires_after(std::chrono::seconds{10});

    auto res = http::response<http::string_body>{};
    http::async_read(m_stream, m_buffer, res, yield);
    m_stream.expires_never();

    return hex2bin<SwarmHash>(res.body());
}


namespace {


/*
 * More complex names are allowed, but it would require careful handling, with
 * url encoding and stuff. Just dumb it down.
 */
bool is_name_valid(std::string_view name)
{
    return std::all_of(
        name.begin(), name.end(),
        [](const char c) {
            return
                ('a' <= c && c <= 'z')
                || ('A' <= c && c <= 'Z')
                || ('0' <= c && c <= '9')
                || c == '_';
        });
}


} // namespace


std::optional<Buffer> TCPClient::feed_get(
    const Ethereum::Address& address, std::string_view name,
    std::any yield_)
{
    Expects(m_connected);
    Expects(is_name_valid(name));
    auto yield = std::any_cast<boost::asio::yield_context>(yield_);

    {
        auto target = std::ostringstream{};
        target << "/bzz-feed:/?user=" << bin2hex(address) << "&name=" << name;
        auto req = http::request<http::string_body>{http::verb::get, target.str(), 11};
        req.set(http::field::host, m_host);

        http::async_write(m_stream, req, yield);
    }

    m_stream.expires_after(std::chrono::seconds{10});

    auto res = http::response<http::vector_body<std::uint8_t>>{};
    http::async_read(m_stream, m_buffer, res, yield);
    m_stream.expires_never();

    if (res.result() == http::status::not_found) {
        return std::nullopt;
    }
    return std::move(res.body());
}

void TCPClient::feed_update(
    const Ethereum::Address& address, std::string_view password, std::string_view name,
    BufferView data, std::any yield_)
{
    Expects(m_connected);
    Expects(is_name_valid(name));
    auto yield = std::any_cast<boost::asio::yield_context>(yield_);

    // TODO: implement feed update signing and upload via HTTP
    namespace bp = boost::process;

    const auto [exe, args] =
        [&] {
            #ifdef FUZZING_BUILD_MODE_UNSAFE_FOR_PRODUCTION
            (void)address;
            (void)data;
            return std::make_tuple(
                bp::exe=bp::search_path("bash"),
                bp::args={
                    "-c",
                    "--",
                    "[[ \"$1\" == \"x\" ]] && { cat -; exit 1; }; exit 0;",
                    "arg0",
                    std::string{name}
                }
            );
            #else
            return std::make_tuple(
                bp::exe=bp::search_path("swarm"),
                bp::args={
                    "--bzzaccount", bin2hex(address),
                    "--password", "/dev/fd/0",
                    "feed", "update",
                    "--name", std::string{name},
                    bin2zxhex(data)
                }
            );
            #endif
        }();

    // surprisingly, stdout has only error message, and stderr has some additional output
    auto ec = boost::system::error_code{};
    auto future_stdout = std::future<std::string>{};
    const int rc = bp::async_system(
        m_io,
        yield[ec],
        exe, args,
        bp::std_in < boost::asio::buffer(password),
        bp::std_out > future_stdout,
        bp::std_err > bp::null);

    if (rc != 0) {
        auto msg = std::ostringstream{};
        const auto stdout =
            [&]() -> std::string {
                try {
                    return future_stdout.get();
                }
                catch (std::future_error& e) {
                    return e.what();
                }
            }();
        msg << "feed update failed. "
            << "external command return code = " << rc << ", "
            << "error code = " << ec << ", "
            << "stdout: " << stdout;
        throw std::runtime_error{msg.str()};
    }
}


} // namespace TigerMail::Swarm
