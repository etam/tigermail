/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIGERMAIL_SWARM_TCP_CLIENT_HPP
#define TIGERMAIL_SWARM_TCP_CLIENT_HPP

#include <any>
#include <string>
#include <string_view>
#include <optional>
#include <variant>

#include <boost/asio/generic/stream_protocol.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/local/stream_protocol.hpp>

#include <boost/beast/core/basic_stream.hpp>
#include <boost/beast/core/flat_buffer.hpp>

#include "../common/buffer.hpp"
#include "../common/buffer_view.hpp"

#include "swarm_client_interface.hpp"


namespace TigerMail::Swarm {


class TCPClient
    : public SwarmClientInterface
{
private:
    boost::asio::io_context& m_io; // TODO: remove when https://github.com/boostorg/process/issues/88 is resolved
    boost::beast::basic_stream<boost::asio::generic::stream_protocol> m_stream;
    bool m_connected;
    boost::beast::flat_buffer m_buffer;
    std::string m_host;

public:
    explicit
    TCPClient(boost::asio::io_context& io);
    ~TCPClient() noexcept override;

    void connect(
        std::variant<
            boost::asio::ip::tcp::endpoint,
            boost::asio::local::stream_protocol::endpoint
        > endpoint,
        std::any yield);

    std::optional<Buffer> bzz_get(const SwarmHash& id, std::any yield) override;
    SwarmHash bzz_post(BufferView data, std::any yield) override;

    std::optional<Buffer> feed_get(const Ethereum::Address& address, std::string_view name,
                                   std::any yield) override;
    void feed_update(const Ethereum::Address& address, std::string_view password, std::string_view name,
                     BufferView data, std::any yield) override;
};


} // namespace TigerMail::Swarm

#endif // TIGERMAIL_SWARM_TCP_CLIENT_HPP
