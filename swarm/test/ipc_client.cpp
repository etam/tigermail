/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include "../../json_rpc/json.hpp"

#include "../ipc_client.hpp"


BOOST_AUTO_TEST_SUITE(ipc_client)

BOOST_AUTO_TEST_CASE(get_swarm_tcp_endpoint)
{
    using TigerMail::JsonRpc::parse_json;
    using TigerMail::Swarm::get_swarm_tcp_endpoint;

    const auto input = R"json({
    "jsonrpc": "2.0",
    "id": 1,
    "result": {
        "Hash": "BMT",
        "DbCapacity": 5000000,
        "CacheCapacity": 10000,
        "BaseKey": "K2lx5cMVcFVx3ij3VysRAPZVBRJE9w71Qh0Kxxy+Cyw=",
        "ChunkDbPath": "/home/etam/.ethereum/swarm/bzz-59ce6471985491fba25db40b0315caa75817f5f4/chunks",
        "Validators": null,
        "Discovery": true,
        "PeersBroadcastSetSize": 3,
        "MaxPeersPerRequest": 5,
        "KeepAliveInterval": 500000000,
        "Swap": {
            "BuyAt": 20000000000,
            "SellAt": 20000000000,
            "PayAt": 100,
            "DropAt": 10000,
            "AutoCashInterval": 300000000000,
            "AutoCashThreshold": 50000000000000,
            "AutoDepositInterval": 300000000000,
            "AutoDepositThreshold": 50000000000000,
            "AutoDepositBuffer": 100000000000000,
            "PublicKey": "",
            "Contract": "0x0000000000000000000000000000000000000000",
            "Beneficiary": "0x0000000000000000000000000000000000000000"
        },
        "Pss": {
            "MsgTTL": 120000000000,
            "CacheTTL": 10000000000,
            "SymKeyCacheCapacity": 512,
            "AllowRaw": false
        },
        "Contract": "0x0000000000000000000000000000000000000000",
        "EnsRoot": "0x112234455c3a32fd11230c42e7bccd4a84e02010",
        "EnsAPIs": null,
        "Path": "/home/etam/.ethereum/swarm/bzz-59ce6471985491fba25db40b0315caa75817f5f4",
        "ListenAddr": "127.0.0.1",
        "Port":"8500",
        "PublicKey": "0x04475c0319f96bb19f6cef7b12094b1b28abd0b10384a1e16939b675f250016dd9be8b149cb110ef94a8e809fcce8174f95da4b284e467fd1196a4314f93b53aa9",
        "BzzKey": "0x2b6971e5c315705571de28f7572b1100f655051244f70ef5421d0ac71cbe0b2c",
        "NodeID": "5bbefcbd8ab618eefa1ad9af59ce6471985491fba25db40b0315caa75817f5f4",
        "NetworkID": 3,
        "SwapEnabled": false,
        "SyncEnabled": true,
        "SyncingSkipCheck": false,
        "DeliverySkipCheck": true,
        "MaxStreamPeerServers": 10000,
        "LightNodeEnabled": true,
        "BootnodeMode": false,
        "SyncUpdateDelay": 15000000000,
        "SwapAPI": "",
        "Cors": "",
        "BzzAccount": "59ce6471985491fba25db40b0315caa75817f5f4",
        "GlobalStoreAPI": "",
        "ContractCode": "0x606060405260008054600160a060020a033316600160a060020a03199091161790556102ec806100306000396000f3006060604052600436106100565763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166341c0e1b581146100585780637bf786f81461006b578063fbf788d61461009c575b005b341561006357600080fd5b6100566100ca565b341561007657600080fd5b61008a600160a060020a03600435166100f1565b60405190815260200160405180910390f35b34156100a757600080fd5b610056600160a060020a036004351660243560ff60443516606435608435610103565b60005433600160a060020a03908116911614156100ef57600054600160a060020a0316ff5b565b60016020526000908152604090205481565b600160a060020a0385166000908152600160205260408120548190861161012957600080fd5b3087876040516c01000000000000000000000000600160a060020a03948516810282529290931690910260148301526028820152604801604051809103902091506001828686866040516000815260200160405260006040516020015260405193845260ff90921660208085019190915260408085019290925260608401929092526080909201915160208103908084039060008661646e5a03f115156101cf57600080fd5b505060206040510351600054600160a060020a039081169116146101f257600080fd5b50600160a060020a03808716600090815260016020526040902054860390301631811161026257600160a060020a0387166000818152600160205260409081902088905582156108fc0290839051600060405180830381858888f19350505050151561025d57600080fd5b6102b7565b6000547f2250e2993c15843b32621c89447cc589ee7a9f049c026986e545d3c2c0c6f97890600160a060020a0316604051600160a060020a03909116815260200160405180910390a186600160a060020a0316ff5b505050505050505600a165627a7a72305820533e856fc37e3d64d1706bcc7dfb6b1d490c8d566ea498d9d01ec08965a896ca0029",
        "ContractAbi": "[{\"constant\":false,\"inputs\":[],\"name\":\"kill\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"\",\"type\":\"address\"}],\"name\":\"sent\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"beneficiary\",\"type\":\"address\"},{\"name\":\"amount\",\"type\":\"uint256\"},{\"name\":\"sig_v\",\"type\":\"uint8\"},{\"name\":\"sig_r\",\"type\":\"bytes32\"},{\"name\":\"sig_s\",\"type\":\"bytes32\"}],\"name\":\"cash\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"fallback\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"name\":\"deadbeat\",\"type\":\"address\"}],\"name\":\"Overdraft\",\"type\":\"event\"}]"
    }
})json";
    const auto endpoint = get_swarm_tcp_endpoint(parse_json(input));

    BOOST_CHECK_EQUAL(endpoint.address().to_string(), "127.0.0.1");
    BOOST_CHECK_EQUAL(endpoint.port(), 8500);
}

BOOST_AUTO_TEST_SUITE_END() // ipc_client
