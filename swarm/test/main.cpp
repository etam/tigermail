/*
Copyright 2019 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#define BOOST_TEST_MODULE "swarm"
#include <boost/test/unit_test.hpp>

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/console.hpp>


namespace {


struct LoggingFixture
{
    void setup()
    {
        namespace log = boost::log;
        // https://stackoverflow.com/a/17473875
        // using cerr instead of cout, because cout is buffered
        // and output gets out of sync with output from gdb, when using signal_helper.gdb
        log::add_console_log(std::cerr, log::keywords::format = ">> %Message%");
        log::core::get()->set_filter
            (
                log::trivial::severity >= log::trivial::debug
            );
    }
};


} // namespace


BOOST_TEST_GLOBAL_FIXTURE(LoggingFixture);
