/*
Copyright 2019-2020 Adam Mizerski <adam@mizerski.pl>

This file is part of TigerMail.

TigerMail is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TigerMail is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TigerMail.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <string>
#include <optional>

#include <boost/asio/spawn.hpp>
#include <boost/test/unit_test.hpp>

#include <gsl/gsl_assert>

#include "../../common/buffer.hpp"
#include "../../common/hex.hpp"
#include "../../common/test/out/buffer.hpp"
#include "../../common/test/out/fixed_size_buffer.hpp"
#include "../../common/test/out/std_optional.hpp"

#include "../../ethereum/common.hpp"

#include "../ipc_client.hpp"
#include "../tcp_client.hpp"

using TigerMail::Buffer;
using TigerMail::bin2hex;
using TigerMail::hex2bin;
using TigerMail::Ethereum::Address;
using TigerMail::Swarm::SwarmHash;
using TigerMail::Swarm::TCPClient;
using TigerMail::Swarm::get_bzz_info;
using TigerMail::Swarm::get_swarm_tcp_endpoint;


namespace {


// don't commit those!
constexpr const auto have_swarm_running = false;
const auto address = Address{};
const auto pass = std::string{""};
const auto socket_path = std::string{std::getenv("HOME")} + "/.ethereum/bzzd.ipc";


struct Fixture
{
    boost::asio::io_context io;
    TCPClient tcp_client{io};
    boost::asio::ip::tcp::endpoint endpoint{get_swarm_tcp_endpoint(get_bzz_info(socket_path))};
};


} // namespace


BOOST_FIXTURE_TEST_SUITE(tcp_client, Fixture, *boost::unit_test::enable_if<have_swarm_running>())


BOOST_AUTO_TEST_CASE(bzz)
{
    const auto input_data = Buffer{'s', 'o', 'm', 'e', '-', 'd', 'a', 't', 'a'};
    auto id = SwarmHash{};
    auto output_data = std::optional<Buffer>{};

    boost::asio::spawn(
        io,
        [&](boost::asio::yield_context yield) {
            tcp_client.connect(endpoint, yield);
            id = tcp_client.bzz_post(input_data, yield);
            output_data = tcp_client.bzz_get(id, yield);
        });

    io.run();

    const auto expected_id = SwarmHash{0xa9,0x46,0x4f,0xe6,0xff,0x8a,0xb5,0xd1,0x86,0x61,0xb7,0x98,0xe9,0x4b,0xeb,0xb8,0x21,0xe6,0xf2,0x29,0xca,0x47,0xb0,0x72,0x0a,0xe0,0x0b,0x07,0x06,0xbb,0x6e,0xed};
    BOOST_CHECK_EQUAL(id, expected_id);
    BOOST_REQUIRE_NE(output_data, std::nullopt);
    BOOST_CHECK_EQUAL_COLLECTIONS(input_data.begin(), input_data.end(), output_data->begin(), output_data->end());
}

BOOST_AUTO_TEST_CASE(feed)
{
    const auto name = std::string{"test"};
    const auto input_data = Buffer{0xde,0xad,0xbe,0xef};
    auto output_data = std::optional<Buffer>{};

    boost::asio::spawn(
        io,
        [&](boost::asio::yield_context yield) {
            tcp_client.connect(endpoint, yield);
            tcp_client.feed_update(address, pass, name, input_data, yield);
            output_data = tcp_client.feed_get(address, name, yield);
        });

    io.run();

    BOOST_REQUIRE_NE(output_data, std::nullopt);
    BOOST_CHECK_EQUAL_COLLECTIONS(input_data.begin(), input_data.end(), output_data->begin(), output_data->end());
}

BOOST_AUTO_TEST_CASE(feed_illegal_name)
{
    const auto name = std::string{"name with spaces"};
    const auto input_data = Buffer{0xde,0xad,0xbe,0xef};
    auto output_data = std::optional<Buffer>{};
    auto update_failed = false;
    auto get_failed = false;

    boost::asio::spawn(
        io,
        [&](boost::asio::yield_context yield) {
            tcp_client.connect(endpoint, yield);

            try {
                tcp_client.feed_update(address, pass, name, input_data, yield);
            }
            catch (gsl::fail_fast&) {
                update_failed = true;
            }

            try {
                output_data = tcp_client.feed_get(address, name, yield);
            }
            catch (gsl::fail_fast&) {
                get_failed = true;
            }
        });

    io.run();

    BOOST_REQUIRE(update_failed);
    BOOST_REQUIRE(get_failed);
    BOOST_REQUIRE_EQUAL(output_data, std::nullopt);
}


BOOST_AUTO_TEST_SUITE_END() // tcp_client
