#!/bin/bash

export CC="ccache afl-gcc"
export CXX="ccache afl-g++"
meson setup -Dwarning_level=2 -Dafl-fuzz=true . build_fuzz_afl
